package com.kloxus.app.model

data class SendMessageModel(
    val `data`: ChatMessageData,
    val message: String,
    val status: Int
)