package com.kloxus.app.model

data class GetFavModel(
    val `data`: ArrayList<HomeData>,
    val lastPage: String,
    val message: String,
    val status: Int
)