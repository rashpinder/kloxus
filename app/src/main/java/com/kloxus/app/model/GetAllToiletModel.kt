package com.kloxus.app.model

data class GetAllToiletModel(
    val `data`: ArrayList<Data>,
    val message: String,
    val lastPage: String,
    val status: Int
)