package com.kloxus.app.model

data class GetToiletbyCityData(
    val address: String,
    val available: String,
    val created: String,
    val description: String,
    val distance: String,
    val gender: String,
    val lat: String,
    val log: String,
    val price: String,
    val rating: String,
    val title: String,
    val toiletID: String,
    val toiletImage: String,
    val toiletLocation: String,
    val userID: String,
    val bookID: String,
    val currency: String,
    val currencySymbol: String
)