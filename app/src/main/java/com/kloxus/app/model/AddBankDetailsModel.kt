package com.kloxus.app.model

class AddBankDetailsModel {
    var mErrorModel: ErrorModel = ErrorModel()
    var mSucessModel: SucessModel = SucessModel()

    fun getmErrorModel(): ErrorModel {
        return mErrorModel
    }

    fun setmErrorModel(mErrorModel: ErrorModel) {
        this.mErrorModel = mErrorModel
    }

    fun getmSucessModel(): SucessModel {
        return mSucessModel
    }

    fun setmSucessModel(mSucessModel: SucessModel) {
        this.mSucessModel = mSucessModel
    }
}


