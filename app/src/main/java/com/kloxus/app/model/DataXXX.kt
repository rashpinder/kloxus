package com.kloxus.app.model

data class DataXXX(
    val address: String,
    val available: String,
    val created: String,
    val currency: String,
    val currencySymbol: String,
    val description: String,
    val distance: String,
    val gender: String,
    val lat: String,
    val log: String,
    val newGender: List<String>,
    val price: String,
    val rating: String,
    val timeZone: String,
    val title: String,
    val toiletID: String,
    val toiletImage: String,
    val toiletImage0: String,
    val toiletImage1: String,
    val toiletImage2: String,
    val toiletImage3: String,
    val toiletImage4: String,
    val toiletImages: List<String>,
    val toiletLocation: String,
    val userID: String
)