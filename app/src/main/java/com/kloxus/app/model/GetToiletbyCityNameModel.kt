package com.kloxus.app.model

data class GetToiletbyCityNameModel(
    val `data`: ArrayList<GetToiletbyCityData>,
    val lastPage: String,
    val message: String,
    val status: Int
)