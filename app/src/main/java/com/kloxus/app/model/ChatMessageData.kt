package com.kloxus.app.model

data class ChatMessageData(
    val created: String,
    val id: String,
    val message: String,
    val name: String,
    val profileImage: String,
    val readStatus: String,
    val roomID: String,
    val userID: String,
    var viewType : Int = -1
)