package com.kloxus.app.model

data class GetAllDetailsAfterBookingModel(
    val `data`: DataAfterbooking,
    val message: String,
    val status: Int
)