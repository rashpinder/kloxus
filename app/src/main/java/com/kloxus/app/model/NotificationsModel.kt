package com.kloxus.app.model

data class NotificationsModel(
    val count: String,
    val `data`: ArrayList<DataX>,
    val message: String,
    val lastPage: String,
    val status: Int
)