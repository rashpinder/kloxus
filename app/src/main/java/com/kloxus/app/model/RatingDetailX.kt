package com.kloxus.app.model

data class RatingDetailX(
    val created: String,
    val name: String,
    val profileImage: String,
    val rating: String,
    val ratingID: String,
    val review: String,
    val toiletID: String,
    val userID: String
)