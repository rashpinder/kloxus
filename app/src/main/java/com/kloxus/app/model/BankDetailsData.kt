package com.kloxus.app.model

data class BankDetailsData(
    val accountHolderName: String,
    val accountNumber: String,
    val backDocument: String,
    val backImage: String,
    val city: String,
    val country: String,
    val created: String,
    val dob: String,
    val firstName: String,
    val frontDocument: String,
    val frontImage: String,
    val id: String,
    val idNumber: String,
    val lastName: String,
    val line1: String,
    val line2: String,
    val phone: String,
    val postalCode: String,
    val routingNumber: String,
    val ssnLast4: String,
    val state: String,
    val stripeAccountID: String,
    val stripeBankID: String,
    val userID: String,
    val verified: String
)