package com.kloxus.app.model

data class ShowBankDetailsModel(
    val `data`: BankDetailsData,
    val message: String,
    val status: Int
)