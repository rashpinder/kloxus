package com.kloxus.app.model

data class LoginModel(
    val `data`: UserData,
    val message: String,
    val status: Int
)