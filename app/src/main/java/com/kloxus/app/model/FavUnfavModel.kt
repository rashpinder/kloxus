package com.kloxus.app.model

data class FavUnfavModel(
    val isFav: String,
    val message: String,
    val status: Int
)