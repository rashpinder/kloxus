package com.kloxus.app.model

import com.google.gson.annotations.SerializedName


class ErrorModel {
    @SerializedName("deleted")
    var isDeleted = false

    @SerializedName("id")
    var id: String? = null

    @SerializedName("object")
    var obb: String? = null
        }

