package com.kloxus.app.model

data class GetAllChatUsersModel(
    val `data`: List<GetAllChatUsersData>,
    val lastPage: String,
    val message: String,
    val status: Int
)