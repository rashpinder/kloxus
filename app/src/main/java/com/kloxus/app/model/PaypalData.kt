package com.kloxus.app.model

data class PaypalData(
    val paypalCity: String,
    val paypalCountry: String,
    val paypalEmail: String,
    val paypalID: String,
    val paypalMiddleName: String,
    val paypalName: String,
    val paypalPostalCode: String,
    val paypalState: String,
    val paypalStreet1: String,
    val paypalToken: String,
    val userID: String
)