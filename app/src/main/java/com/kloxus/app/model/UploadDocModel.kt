package com.kloxus.app.model

data class UploadDocModel(
    val message: String,
    val status: Int,
    val success: String,
    val fileID: String,
)