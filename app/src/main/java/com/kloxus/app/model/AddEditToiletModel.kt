package com.kloxus.app.model

data class AddEditToiletModel(
    val `data`: Data,
    val message: String,
    val status: Int
)