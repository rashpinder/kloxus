package com.kloxus.app.model

import com.google.gson.annotations.SerializedName

class SucessModel {
    @SerializedName("success")
    var success: String? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("status")
    var status = 0
}