package com.kloxus.app.model

data class GetMessagesModel(
    val `data`: List<ChatMessageData>,
    val message: String,
    val status: Int,
    val userDetails: UserDetails
)