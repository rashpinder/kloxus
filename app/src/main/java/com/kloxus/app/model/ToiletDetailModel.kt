package com.kloxus.app.model

data class ToiletDetailModel(
    val `data`: Data,
    val message: String,
    val status: Int
)