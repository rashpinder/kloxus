package com.kloxus.app.model

data class AllCitiesModel(
    val `data`: List<DataXX>,
    val message: String,
    val status: Int
)