package com.kloxus.app.model

data class CardsData(
    val brand: String,
    val card_holder_name: String,
    val card_id: String,
    val country: String,
    val exp_month: String,
    val exp_year: String,
    val last4: String,
    val isDefault: Int
)