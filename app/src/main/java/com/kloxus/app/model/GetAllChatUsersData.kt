package com.kloxus.app.model

data class GetAllChatUsersData(
    val created: String,
    val id: String,
    var lastMessage: String,
    val lastMessageTime: String,
    val messageCount: String,
    val name: String,
    val otherUserID: String,
    val profileImage: String,
    val roomID: String,
    val userID: String
)