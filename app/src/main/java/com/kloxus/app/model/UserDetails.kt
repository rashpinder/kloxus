package com.kloxus.app.model

data class UserDetails(
    val created: String,
    val customerID: String,
    val email: String,
    val facebookToken: String,
    val firstName: String,
    val googleID: String,
    val isAdmin: String,
    val lastName: String,
    val password: String,
    val phoneNo: String,
    val profileImage: String,
    val stripeAccountID: String,
    val stripeAccountStatus: String,
    val userID: String,
    val verificationCode: String,
    val verified: String
)