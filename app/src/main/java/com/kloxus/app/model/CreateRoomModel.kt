package com.kloxus.app.model

data class CreateRoomModel(
    val `data`: CreateRoomData,
    val message: String,
    val status: Int
)