package com.kloxus.app.model

data class GetCardsModel(
    val `data`: ArrayList<CardsData>,
    val message: String,
    val status: Int
)