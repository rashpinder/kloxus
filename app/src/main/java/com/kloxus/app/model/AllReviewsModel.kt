package com.kloxus.app.model

data class AllReviewsModel(
    val lastPage: String,
    val message: String,
    val ratingDetails: ArrayList<RatingDetailX>,
    val status: Int
)