package com.kloxus.app.model

data class EditProfileModel(
    val `data`: EditProfileData,
    val message: String,
    val status: Int,

)