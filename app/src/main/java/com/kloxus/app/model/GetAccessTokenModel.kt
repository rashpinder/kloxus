package com.kloxus.app.model

data class GetAccessTokenModel(
    val accessToken: String,
    val message: String,
    val status: Int
)