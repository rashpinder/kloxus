package com.kloxus.app.model

data class ShowPaypalDetailModel(
    val `data`: PaypalData,
    val message: String,
    val status: Int
)