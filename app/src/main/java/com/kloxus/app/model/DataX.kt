package com.kloxus.app.model

data class DataX(
    val created: String,
    val description: String,
    val detailsID: String,
    val notification_id: String,
    val notification_read_status: String,
    val notification_type: String,
    val otherUserID: String,
    val profileImage: String,
    val roomID: String,
    val title: String,
    val userID: String,
    val userName: String,
    val toiletName: String,
    val bookID: String
)