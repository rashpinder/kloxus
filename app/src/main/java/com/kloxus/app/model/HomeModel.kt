package com.kloxus.app.model

data class HomeModel(
    val `data`: ArrayList<HomeData>,
    val message: String,
    val lastPage: String,
    val status: Int
)