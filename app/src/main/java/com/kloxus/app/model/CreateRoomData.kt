package com.kloxus.app.model

data class CreateRoomData(
    val created: String,
    val id: String,
    val lastMessage: String,
    val messageCount: String,
    val name: String,
    val otherUserID: String,
    val profileImage: String,
    val roomID: String,
    val userID: String
)