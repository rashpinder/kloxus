package com.kloxus.app.model

data class EditProfileData(
    val created: String,
    val customerID: String,
    val email: String,
    val facebookToken: String,
    val firstName: String,
    val googleID: String,
    val isAdmin: String,
    val lastName: String,
    val password: String,
    val phoneNo: String,
    val profileImage: String,
    val countryCode: String,
    val stripeAccountID: String,
    val stripeAccountStatus: String,
    val userID: String,
    val verificationCode: String,
    val verified: String,
    val totalMessage: Int,
    val notificationCount: Int,
    val defaultPaymentMethod: Int,
    val isBank: Int,
    val isPaypal: Int
)