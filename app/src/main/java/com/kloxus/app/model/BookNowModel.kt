package com.kloxus.app.model

data class BookNowModel(
    val isRequestSend: String,
    val message: String,
    val status: Int
)