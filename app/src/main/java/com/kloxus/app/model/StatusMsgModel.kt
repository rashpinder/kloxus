package com.kloxus.app.model

data class StatusMsgModel(
    val isRequestSend: String,
    val message: String,
    val isPaid: String,
    val status: Int,
    val bookID: String,
    val serviceCharge: String,
    val totalPrice: String
)