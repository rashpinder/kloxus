package com.kloxus.app.model

data class RatingDetail(
    val name: String,
    val profileImage: String,
    val rating: String,
    val review: String
)