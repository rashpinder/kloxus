package com.kloxus.app.interfaces

import com.kloxus.app.model.GetToiletbyCityData

interface LoadMoreDataByCityListener {
    public fun onLoadMoreListner(mModel : GetToiletbyCityData)
}