package com.kloxus.app.interfaces

import com.kloxus.app.model.HomeData

interface LoadMoreScrollListner {
    public fun onLoadMoreListner(mModel : HomeData)
}