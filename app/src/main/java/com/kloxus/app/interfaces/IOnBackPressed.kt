package com.kloxus.app.interfaces

interface IOnBackPressed {
    fun onBackFragmentListner(backID: Int)
}