package com.kloxus.app.fonts;

import android.content.Context;
import android.graphics.Typeface;

public class OpenSansBold {
    /*
     * Initialize Context
     * */
    Context mContext;
    /*
     * Initialize the Typeface
     * */
    private Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    private String path = "OPENSANS-BOLD.TTF";

    /*
     * Default Constructor
     * */
    public OpenSansBold() {
    }

    /*
     * Constructor with Context
     * */
    public OpenSansBold(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null) {
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        }
        return fontTypeface;
    }

}
