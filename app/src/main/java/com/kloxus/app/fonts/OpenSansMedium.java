package com.kloxus.app.fonts;

import android.content.Context;
import android.graphics.Typeface;


public class OpenSansMedium {
    /*
     * Initialize the Typeface
     * */
    public static Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    String path = "OPENSANS-SEMIBOLD.TTF";
    /*
     * Initialize Context
     * */
    public Context mContext;
    /*
     * Default Constructor
     * */
    public OpenSansMedium() {
    }
    /*
     * Constructor with Context
     * */
    public OpenSansMedium(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null)
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
            return fontTypeface;
    }
}