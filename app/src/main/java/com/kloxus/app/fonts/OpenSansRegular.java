package com.kloxus.app.fonts;

import android.content.Context;
import android.graphics.Typeface;


public class OpenSansRegular {
    /*
     * Initialize the Typeface
     * */
    public static Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    public String path = "OPENSANS-REGULAR.TTF";
    /*
     * Initialize Context
     * */
    public Context mContext;
    /*
     * Default Constructor
     * */
    public OpenSansRegular() {
    }

    /*
     * Constructor with Context
     * */
    public OpenSansRegular(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null)
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        return fontTypeface;
    }

}