package com.pregnancyrisk.app.fragments

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.kloxus.app.R
import com.kloxus.app.utils.*

import java.util.*
import java.util.regex.Pattern

open class BaseFragment : Fragment() {
    /*
       * Get Class Name
       * */
    var TAG = this@BaseFragment.javaClass.simpleName


    /*
    * Initialize Other Classes Objects...
    * */
    var progressDialog: Dialog? = null


    /*
    * Getting User ID
    * */
    fun getUserId(): String? {
        return activity?.let { KloxusPrefrences().readString(it, USER_ID, "") }
    }

    /*
    * Getting User Email
    * */
    fun getUserEmail(): String? {
        return activity?.let { KloxusPrefrences().readString(it, USER_EMAIL, "") }
    }

    /*
    * Getting User Name
    * */
    fun getUserName(): String? {
        return activity?.let { KloxusPrefrences().readString(it, FIRST_NAME, "") }
    }

    /*
    * Getting User Bio
    * */
    fun getLast(): String? {
        return activity?.let { KloxusPrefrences().readString(it, LAST_NAME, "") }
    }


    /*
    * Getting Profile Pic
    * */
    fun getProfilePic(): String? {
        return activity?.let { KloxusPrefrences().readString(it, PROFILE_IMAGE, "") }
    }

    /*
    * Is User Login
    * */
    fun isUserLogin(): Boolean? {
        return activity?.let { KloxusPrefrences().readBoolean(it, IS_LOGIN, false) }
    }


    /*
    * Show Progress Dialog
    * */
    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)
            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }


    open fun hideKeyboard(activity: Activity) {
        val view =
            activity.findViewById<View>(android.R.id.content)
        if (view != null) {
            val imm =
                activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }


    /*
     * Hide Progress Dialog
     * */
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }


    /*
     * Validate Email Address
     * */
    fun isValidEmaillId(email: String?): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }


    /*
     * Check Internet Connections
     * */
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }


    /*
     * Toast Message
     * */
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }


    /*
     *
     * Error Alert Dialog
     * */
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }


    /*to switch between fragments*/
    fun switchFragment(
        fragment: Fragment?,
        Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager = activity?.supportFragmentManager
        if (fragment != null) {
            val fragmentTransaction = fragmentManager?.beginTransaction()
            fragmentTransaction?.replace(R.id.containerBookingsFL, fragment, Tag)
            if (addToStack) fragmentTransaction?.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction?.commit()
            fragmentManager?.executePendingTransactions()
        }
    }
}
