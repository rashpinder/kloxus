package com.kloxus.app.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.kloxus.app.R
import com.kloxus.app.adapters.UserModeAdapter
import com.kloxus.app.model.AllCitiesModel
import com.kloxus.app.model.DataXX
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.AUTH_TOKEN
import com.kloxus.app.utils.KloxusPrefrences
import com.kloxus.app.utils.USER_ID
import com.pregnancyrisk.app.fragments.BaseFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserModeFragment : BaseFragment() {

    lateinit var mUserModeAdapter: UserModeAdapter
    @BindView(R.id.userRV)
    lateinit var userRV: RecyclerView
    lateinit var mRoot : View
    @BindView(R.id.txtNoDataFountTV)
    lateinit var txtNoDataFountTV: TextView
    var mAllUserList: List<DataXX>? = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {}
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mRoot =inflater.inflate(R.layout.fragment_user_mode, container, false)
        ButterKnife.bind(this, mRoot)
        getUserList()
        return mRoot
    }

    private fun getUserList() {
        if (activity?.let { isNetworkAvailable(it) }!!)
            executeGetUserListRequest()
        else
            showToast(activity, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = activity?.let { KloxusPrefrences().readString(it, USER_ID, null) }
        mMap["authToken"] = activity?.let { KloxusPrefrences().readString(it, AUTH_TOKEN, null) }
        mMap["type"] = "1"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetUserListRequest() {
        showProgressDialog(activity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getAllBookedListRequest(mParam())
            ?.enqueue(object : Callback<AllCitiesModel> {
                override fun onFailure(call: Call<AllCitiesModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<AllCitiesModel>,
                    response: Response<AllCitiesModel>
                ) {
                    dismissProgressDialog()
                    val mModel: AllCitiesModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            mAllUserList = mModel.data
                            getOwnerListAdapter()
                            txtNoDataFountTV.setVisibility(View.GONE)
                        } else {
                            txtNoDataFountTV.setVisibility(View.VISIBLE)
                            userRV.setVisibility(View.GONE)
                            txtNoDataFountTV.setText(mModel.message)
                        }
                    }
                }
            })
    }

    private fun getOwnerListAdapter() {
        mUserModeAdapter = UserModeAdapter(activity,mAllUserList)
        userRV.setLayoutManager(LinearLayoutManager(activity))
        userRV.setAdapter(mUserModeAdapter)
    }

}