package com.kloxus.app.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import android.provider.Settings
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.kloxus.app.R
import com.kloxus.app.activities.*
import com.kloxus.app.utils.ROOM_ID
import com.kloxus.app.utils.USER_NAME
import org.json.JSONObject
import java.util.*


class NotificationReciever : FirebaseMessagingService() {
    val TAG: String = "NotificationReciever"
    var mTitle = ""
    var mBody = ""
    var mName = ""
    var mRoomID = ""
    var userID = ""
    var otherUserID = ""
    var detailsID = ""
    var notification_read_status = ""
    var created = ""
    var mType = ""
    var bookID = ""
    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        if (p0.data.size > 0) {
            try {
                var mTempData = p0.data.toString()
                var mNewData = mTempData.split("=")
                val mJsonObject: JSONObject = JSONObject(mNewData[1])
                Log.e(TAG, "**Notifications Data**" + mJsonObject)
                if (!mJsonObject.isNull("notification_type")) {
                    mType = mJsonObject.getString("notification_type")
                }
                if (!mJsonObject.isNull("title")) {
                    mTitle = mJsonObject.getString("title")
                }
                if (!mJsonObject.isNull("description")) {
                    mBody = mJsonObject.getString("description")
                }
                if (!mJsonObject.isNull("userID")) {
                    userID = mJsonObject.getString("userID")
                }
                if (!mJsonObject.isNull("otherUserID")) {
                    otherUserID = mJsonObject.getString("otherUserID")
                }
                if (!mJsonObject.isNull("roomID")) {
                    mRoomID = mJsonObject.getString("roomID")
                }
                if (!mJsonObject.isNull("userName")) {
                    mName = mJsonObject.getString("userName")
                }
                if (!mJsonObject.isNull("detailsID")) {
                    detailsID = mJsonObject.getString("detailsID")
                }
                if (!mJsonObject.isNull("notification_read_status")) {
                    notification_read_status = mJsonObject.getString("notification_read_status")
                }
                if (!mJsonObject.isNull("created")) {
                    created = mJsonObject.getString("created")
                }
                if (!mJsonObject.isNull("bookID")) {
                    bookID = mJsonObject.getString("bookID")
                }
            } catch (e: Exception) {
                Log.e(TAG, "onMessageReceived: $e")
            }
            sendNotification()
        }
    }


    private fun sendNotification() {
        var intent: Intent? = null
        if (mType.equals("1")) {
            intent = Intent(this, HomeActivity::class.java)
        } else if (mType.equals("3")) {
            intent = Intent(this, ChatActivity::class.java)
            intent.putExtra(USER_NAME, mTitle)
            intent.putExtra(ROOM_ID, mRoomID)
        } else if (mType.equals("2")) {
            intent = Intent(this, NotificationsActivity::class.java)
        } else if (mType.equals("5")) {
            intent = Intent(this, ToiletPathActivity::class.java)
            intent.putExtra("toilet_id", detailsID)
            intent.putExtra("book_id", bookID)
        } else if (mType.equals("6")) {
            intent = Intent(this, NotificationsActivity::class.java)
        } else if (mType.equals("4")) {
            intent = Intent(this, AddBankDetailsTypeActivity::class.java)
        }else if (mType.equals("7")) {
            intent = Intent(this, AddBankDetailsTypeActivity::class.java)
        }
//        else if (mType.equals("8")) {
//            intent = Intent(this, ToiletDetailActivity::class.java)
//            intent.putExtra("toilet_id", detailsID)
//        }
        else if (mType.equals("8")) {
            intent = Intent(this, NotificationsActivity::class.java)
        }
        else{
            intent = Intent(this, HomeActivity::class.java)
        }

        intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        val pendingIntent = PendingIntent.getActivity(
            this,
            generateRandomInt(5),
            intent,
            PendingIntent.FLAG_ONE_SHOT
        )
        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri =
            RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val notificationBuilder =
            NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(mTitle + " Send a message")
                .setContentText(mBody)
                .setAutoCancel(true)
                .setVibrate(longArrayOf(500, 500))
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        val nb =
            NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(mTitle)
                .setColor(getColor(R.color.colorAccent))
                .setContentText(mBody)
                .setAutoCancel(true)
                .setVibrate(longArrayOf(500, 500))
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.color=ContextCompat.getColor(this, R.color.colorAccent)
            nb.color= ContextCompat.getColor(this, R.color.colorAccent)
        } else {
            notificationBuilder.color=ContextCompat.getColor(this, R.color.colorAccent)
            nb.color= ContextCompat.getColor(this, R.color.colorAccent)
        }

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(channel)
        }
        if (mType.equals("3")) {
            notificationManager.notify(generateRandomInt(5), notificationBuilder.build())}
        else {
            notificationManager.notify(generateRandomInt(5), nb.build())}
    }


    fun generateRandomInt(upperRange: Int): Int {
        val random = Random()
        return random.nextInt(upperRange)
    }

}