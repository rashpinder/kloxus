package  com.kloxus.app.retrofit

import com.google.gson.JsonObject
import com.kloxus.app.model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @POST("signUp.php")
    fun signUpRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<LoginModel>

    @POST("logIn.php")
    fun signInRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<LoginModel>

    @POST("facebookLogin.php")
    fun signInWithFbRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<LoginModel>

    @POST("googleLogin.php")
    fun loginWithGoogleRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<LoginModel>

    @POST("forgetPassword.php")
    fun forgotPwdRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @POST("logOut.php")
    fun logoutRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @POST("home.php")
    fun homeRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<HomeModel>


    @POST("getAllToilet.php")
    fun getAllToiletRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<GetAllToiletModel>

    @POST("submitRating.php")
    fun submitRatingRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @POST("getToiletDetail.php")
    fun getToiletDetailRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<ToiletDetailModel>


    @POST("getToiletDetailsForBooking.php")
    fun getToiletDetailsFterBookingRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<GetAllDetailsAfterBookingModel>

    @POST("getProfileDetails.php")
    fun getProfileDetailRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<EditProfileModel>

    @POST("favouriteUnfavouriteToilet.php")
    fun favUnfavRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<FavUnfavModel>

    @POST("getAllFavoriteToilet.php")
    fun getAllfavUnfavRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<GetFavModel>

    @Multipart
    @POST("addEditToiletV4.php")
    fun addToiletRequest(
        @Part("userID") userID: RequestBody?,
        @Part("authToken") authToken: RequestBody?,
        @Part("description") description: RequestBody?,
        @Part("title") title: RequestBody?,
        @Part("price") price: RequestBody?,
        @Part("address") address: RequestBody?,
        @Part("latitude") latitude: RequestBody?,
        @Part("longitude") longitude: RequestBody?,
        @Part("location") location: RequestBody?,
        @Part("available") available: RequestBody?,
        @Part("gender") gender: RequestBody?,
        @Part("toiletOptions") toiletOptions: RequestBody?,
        @Part("currency") currency: RequestBody?,
        @Part("currencySymbol") currencySymbol: RequestBody?,
        @Part("isRequestSend") isRequestSend: RequestBody?,
        @Part("isPaid") isPaid: RequestBody?,
        @Part toiletImage : MultipartBody.Part?,
        @Part toiletImage1 : MultipartBody.Part?,
        @Part toiletImage2 : MultipartBody.Part?,
        @Part toiletImage3 : MultipartBody.Part?
    ): Call<AddEditToiletModel?>?

    @Multipart
    @POST("addEditToiletV4.php")
    fun editToiletRequest(
        @Part("userID") userID: RequestBody?,
        @Part("toiletID") toiletID: RequestBody?,
        @Part("authToken") authToken: RequestBody?,
        @Part("description") description: RequestBody?,
        @Part("title") title: RequestBody?,
        @Part("price") price: RequestBody?,
        @Part("address") address: RequestBody?,
        @Part("latitude") latitude: RequestBody?,
        @Part("longitude") longitude: RequestBody?,
        @Part("location") location: RequestBody?,
        @Part("available") available: RequestBody?,
        @Part("gender") gender: RequestBody?,
        @Part("toiletOptions") toiletOptions: RequestBody?,
        @Part("currency") currency: RequestBody?,
        @Part("currencySymbol") currencySymbol: RequestBody?,
        @Part("isRequestSend") isRequestSend: RequestBody?,
        @Part("isPaid") isPaid: RequestBody?,
        @Part toiletImage : MultipartBody.Part?,
        @Part toiletImage1 : MultipartBody.Part?,
        @Part toiletImage2 : MultipartBody.Part?,
        @Part toiletImage3 : MultipartBody.Part?
    ): Call<AddEditToiletModel?>?

    @Multipart
    @POST("editProfile.php")
    fun editProfileRequest(
        @Part("userID") userID: RequestBody?,
        @Part("authToken") authToken: RequestBody?,
        @Part("firstName") firstName: RequestBody?,
        @Part("lastName") lastName: RequestBody?,
        @Part("phoneNo") phoneNo: RequestBody?,
        @Part("countryCode") countryCode: RequestBody?,
        @Part profileImage : MultipartBody.Part?
    ): Call<EditProfileModel?>?

    @POST("createRoom.php")
    fun createRoomRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<CreateRoomModel>


    @POST("sendMessage.php")
    fun sendMessageRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<SendMessageModel>

    @POST("getAllChatMessages.php")
    fun getAllChatMessageRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<GetMessagesModel>

    @POST("getAllChatUser.php")
    fun getAllChatUsersRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<GetAllChatUsersModel>

    @POST("updateMessageSeen.php")
    fun seenMsgRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @POST("getCardDetailsByUserID.php")
    fun getCardDetailsRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<GetCardsModel>

    @POST("saveUserCardDetail.php")
    fun saveCardDetailsRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @POST("notificationActivity.php")
    fun getNotificationsRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<NotificationsModel>

    @POST("changePassword.php")
    fun changePasswordRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @POST("sendPayment.php")
    fun sendPaymentRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

//    @POST("bookingToilet.php")
//    fun bookToiletRequest(
//        @Body mParams: Map<String?, String?>?
//    ): Call<StatusMsgModel>

    @POST("bookingToiletV2.php")
    fun bookToiletRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @POST("getAllCityNameUserBooked.php")
    fun getAllBookedListRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<AllCitiesModel>

    @POST("getAllToiletByCityName.php")
    fun getAllToiletByCitiesRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<GetToiletbyCityNameModel>


 @POST("createUserStripeAccount.php")
    fun addBankDetailsRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<JsonObject>

 @POST("appoveRejectToiletRequest.php")
    fun approveRejectRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

 @POST("requestForRefund.php")
    fun refundRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

 @POST("deleteCard.php")
    fun deleteCardRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

 @POST("getBankAccountDetail.php")
    fun getBankDetailsRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<ShowBankDetailsModel>

 @POST("deleteBankAccount.php")
    fun deleteBankDetailsRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @Multipart
    @POST("uploadDocuments.php")
    fun uploadDocRequest(
        @Part("userID") userID: RequestBody?,
        @Part("authToken") authToken: RequestBody?,
        @Part("imageType") imageType: RequestBody?,
        @Part documentImage : MultipartBody.Part?
    ): Call<UploadDocModel?>?


    @POST("updatePaymentDefaultMethod.php")
    fun updatePaymentMethodRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @POST("getPaypalDetail.php")
    fun getPaypalDetailsRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<ShowPaypalDetailModel>

    @Multipart
    @POST("PaypalLogin.php")
    fun paypalLoginRequest(
        @Part("userID") userID: RequestBody?,
        @Part("authToken") authToken: RequestBody?
    ): Call<PaypalLoginModel?>?


    @POST("getAccessTokenForPaypal.php")
    fun getAccessTokenRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<GetAccessTokenModel>


    @POST("getPaypalTransactionDetails.php")
    fun getTransactionDetailsRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @POST("checkBookinhAlready.php")
    fun getbookingStatusRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @POST("updatePaymentStatus.php")
    fun getpaymentStatusRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>


    @POST("deleteToilet.php")
    fun deleteToiletRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @POST("getAllRatingReview.php")
    fun getReviewsRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<AllReviewsModel>

}