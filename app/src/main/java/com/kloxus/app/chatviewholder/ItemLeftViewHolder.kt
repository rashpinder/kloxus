package com.merger.app.chatviewholder

import android.app.Activity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kloxus.app.R
import com.kloxus.app.model.ChatMessageData
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class ItemLeftViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var txtTimeTV = itemView.findViewById(R.id.txtTimeTV) as TextView
    var txtMessageTV = itemView.findViewById(R.id.txtMessageTV) as TextView
    var imgOtherUserProfileIV = itemView.findViewById<TextView>(R.id.imgOtherUserProfileIV) as de.hdodenhof.circleimageview.CircleImageView
    var imgPostIV = itemView.findViewById<TextView>(R.id.imgPostIV) as ImageView

    fun bindData(mActivity: Activity?, mModel: ChatMessageData?) {
        txtTimeTV.setText(mModel!!.name+","+gettingLongToFormatedTime(mModel!!.created!!.toLong()))
        txtMessageTV.setText(mModel!!.message)
        mActivity?.let {
            Glide.with(it)
                .load(mModel.profileImage)
                .placeholder(R.drawable.ic_ph)
                .error(R.drawable.ic_ph)
                .into(imgOtherUserProfileIV)
        }
    }

    fun gettingLongToFormatedTime(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("HH:mm a")
        return jdf.format(date)
    }
}