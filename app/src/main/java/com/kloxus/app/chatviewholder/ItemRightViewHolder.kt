package com.merger.app.chatviewholder

import android.app.Activity
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kloxus.app.R
import com.kloxus.app.model.ChatMessageData
import java.text.SimpleDateFormat
import java.util.*

class ItemRightViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var txtTimeTV = itemView.findViewById<TextView>(R.id.txtTimeTV) as TextView
    var txtMessageTV = itemView.findViewById<TextView>(R.id.txtMessageTV) as TextView
//    var itemImageIV = itemView.findViewById<TextView>(R.id.itemImageIV) as de.hdodenhof.circleimageview.CircleImageView

    fun bindData(mActivity: Activity?, mModel: ChatMessageData?) {
        txtTimeTV.setText(gettingLongToFormatedTime(mModel!!.created!!.toLong()))
        txtMessageTV.setText(mModel!!.message)
//        mActivity?.let {
//            Glide.with(it)
//                .load(mModel!!.profileImage)
//                .placeholder(R.drawable.ic_listing_ph)
//                .error(R.drawable.ic_listing_ph)
//                .into(itemImageIV)
//        }
    }


    fun gettingLongToFormatedTime(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("HH:mm a")
        return jdf.format(date)
    }

}