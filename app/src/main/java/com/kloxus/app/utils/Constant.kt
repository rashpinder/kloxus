package com.kloxus.app.utils

    /*
* Base Server Url
* */
//    const val BASE_URL = "https://www.dharmani.com/kloxus/webservices/"

//live url
//    const val BASE_URL = "http://3.126.50.37/webservices/"
    const val BASE_URL = "https://kloxus.com/webservices/"
    const val SOCKET_URL = "https://jaohar-uk.herokuapp.com"


    /*
     * Constants @params
     * */
    var REQUEST_CODE = 123
    const val REQUEST_PERMISSION_CODE = 919

    /*
* Fragment Tags
* */
    const val USER_TAG = "user_tag"
    const val OWNER_TAG = "owner_tag"
    const val LATITUDE = "latitude"
    const val LONGITUTE = "longitude"
    const val CURR_LAT = "curr_latitude"
    const val CURR_LNG = "curr_longitude"
    const val SUB_CATEGORES_TAG = "subcategories_tag"
    const val DETAILS_TAG = "details_tag"


    /*
* Links Constants
* */
    const val LINK_TYPE = "link_type"
    const val LINK_ABOUT = "link_about"
    const val LINK_PP = "link_pp"
    const val LINK_TERMS = "link_terms"

    /*
* Link Urls
* */
//    const val ABOUT_WEB_LINK = "https://www.dharmani.com/kloxus/AboutUs.html"

    //const val PP_WEB_LINK = BASE_URL + "PrivacyPolicy.html"
//    const val TERMS_WEB_LINK = "https://www.dharmani.com/kloxus/TermsAndConditions.html"

//    const val ABOUT_WEB_LINK = "http://3.126.50.37/AboutUs.html"
//    const val TERMS_WEB_LINK = "http://3.126.50.37/privacyPolicy.php"

    const val ABOUT_WEB_LINK = "https://kloxus.com/AboutUs.html"
    const val TERMS_WEB_LINK = "https://kloxus.com/privacyPolicy.php"

    /*
* Shared Preferences Keys
* */
    const val USER_ID = "id"
    const val FIRST_NAME = "first_name"
    const val LAST_NAME = "last_name"
    const val IS_LOGIN = "is_login"
    const val USER_EMAIL = "email"
    const val IS_ADMIN = "is_admin"
    const val PHONE = "phone"
    const val COUNTRY_CODE = "country_code"
    const val PASSWORD = "password"
    const val PROFILE_IMAGE = "profile_image"
    const val CUSTOMER_ID = "customer_id"
    const val AUTH_TOKEN = "auth_token"
    const val VERIFIED = "verified"
    const val FB_TOKEN = "fb_token"
    const val VERIFICATION_CODE = "veri_code"
    const val GOOGLE_ID = "google_id"
    const val CREATED = "created"
    val DEVICE_TOKEN = "device_token"
    val SELECTED_LOCATION = "selected_location"
    val SELECTED_TOILET_LOCATION = "selected_toilet_location"

    val LAT_ADD_TOILET = "lat_add_toilet"
    val LNG_ADD_TOILET = "lng_add_toilet"
    val LOC_ADD_TOILET = "loc_add_toilet"
val DEFAULT_PAYMENT_METHOD = "default_payment_method"
val DEFAULT_CARD = "default_card"
val IS_BANK = "is_bank"
val IS_STRIPE = "is_stripe"
val IS_PAYPAL = "is_paypal"

    /*
* Data Transfer
* */
    const val USER_NAME = "user_name"
    const val ROOM_ID = "room_id"
    const val HOME_PATH = "home_path"


    /*
* View Holder
* */
    const val LEFT_VIEW_HOLDER = 0
    const val RIGHT_VIEW_HOLDER = 1
    const val LAST_MSG = "last_msg"

//live stripe key
//const val STRIPE_API_KEY = "pk_test_51IQ6i1Ar3GGTZB1M4CWFY1kFdxCy9ckhWHhswu8O0MiOfFLgVSjnFhxpCCNC8OWh2wkl7RlNtUJrPbYrKcfCvb2x002iRy3nOR"
const val STRIPE_API_KEY = "pk_test_51IucZdG2gKqR1k4gJCJaf7MnpsIAL1NNgRZosx8sGCvu4zD95Fel1NNlBB2vWptxRSAcetcziIOQuxwBYgWMU7OU00JlmkOpw5"
//const val STRIPE_API_KEY = "sk_test_51IucZdG2gKqR1k4g1AapyThNdFOZauNUQlhiztIrx1rGHzWaFL6cIhesaZhU6KczE0kfR1EYEajOuAecgl4GRL8P00QS0jYGcy"

class Constant {
    var MONTHS_OF_YEAR = arrayOf(
        "0",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10",
        "11"
    )
    var YEARS = arrayOf(
        "2020",
        "2021",
        "2022",
        "2023",
        "2024",
        "2025",
        "2026",
        "2027",
        "2028",
        "2029",
        "2030",
        "2031",
        "2032",
        "2033",
        "2034",
        "2035",
        "2036",
        "2037",
        "2038",
        "2039",
        "2040",
        "2041",
        "2042",
        "2043",
    )
}



