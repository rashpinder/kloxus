package com.kloxus.app.utils

import android.content.Context
import android.content.SharedPreferences

public class KloxusPrefrences {


    /***************
     * Define SharedPrefrances Keys & MODE
     */
    val PREF_NAME = "App_PREF"
    val MODE = Context.MODE_PRIVATE


    val HOME_SEL = "home_sel"
    val SAVED_TOILET_SEL = "saved_toilet_sel"
    val MY_TOILET_SEL = "my_toilet_sel"
    val YOUR_BOOKINGS_SEL = "your_bookings_sel"
    val PAYMENT_SEL = "payment_sel"
    val SETTINGS_SEL = "settings_sel"
    val MSG_SEL = "msg_sel"
    val PROFILE_SEL = "profile_sel"
    val REFER_SEL = "refer_sel"

    /*
     * Write the Boolean Value
     * */
   public fun writeBoolean(
        context: Context,
        key: String?,
        value: Boolean
    ) {
        getEditor(context).putBoolean(key, value).apply()
    }

    /*
     * Read the Boolean Valueget
     * */
    public fun readBoolean(
        context: Context,
        key: String?,
        defValue: Boolean
    ): Boolean {
        return getPreferences(context).getBoolean(key, defValue)
    }

    /*
     * Write the Integer Value
     * */
    public fun writeInteger(
        context: Context,
        key: String?,
        value: Int
    ) {
        getEditor(context).putInt(key, value).apply()
    }

    /*
     * Read the Interger Value
     * */
    public fun readInteger(
        context: Context,
        key: String?,
        defValue: Int
    ): Int {
        return getPreferences(context).getInt(key, defValue)
    }

    /*
     * Write the String Value
     * */
    public fun writeString(
        context: Context,
        key: String?,
        value: String?
    ) {
        getEditor(context).putString(key, value).apply()
    }

    /*
     * Read the String Value
     * */
    public  fun readString(
        context: Context,
        key: String?,
        defValue: String?
    ): String? {
        return getPreferences(context).getString(key, defValue)
    }

    /*
     * Write the Float Value
     * */
    public fun writeFloat(
        context: Context,
        key: String?,
        value: Float
    ) {
        getEditor(context).putFloat(key, value).apply()
    }

    /*
     * Read the Float Value
     * */
    public fun readFloat(
        context: Context,
        key: String?,
        defValue: Float
    ): Float {
        return getPreferences(context).getFloat(key, defValue)
    }

    /*
     * Write the Long Value
     * */
    public fun writeLong(
        context: Context,
        key: String?,
        value: Long
    ) {
        getEditor(context).putLong(key, value).apply()
    }

    /*
     * Read the Long Value
     * */
    public fun readLong(
        context: Context,
        key: String?,
        defValue: Long
    ): Long {
        return getPreferences(context).getLong(key, defValue)
    }

    /*
     * Return the SharedPreferences
     * with
     * @Prefreces Name & Prefreces = MODE
     * */
    public fun getPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(PREF_NAME, MODE)
    }

    /*
     * Return the SharedPreferences Editor
     * */
    public fun getEditor(context: Context): SharedPreferences.Editor {
        return getPreferences(context).edit()
    }

}