package com.kloxus.app.utils
import android.app.Application
import io.socket.client.IO
import io.socket.client.Socket
import java.net.URISyntaxException

class KloxusApp : Application() {
    /**
     * Getting the Current Class Name
     */
    val TAG: String = "KloxusApp"

    /**
     * Initialize the Applications Instance
     */
    private var mInstance: KloxusApp? = null

    /**
     * Synchronized the Application Class Instance
     */
    @Synchronized
    fun getInstance(): KloxusApp? {
        return mInstance
    }
    /*
     * Socket
     * */
    var mSocket: Socket? = null

    override fun onCreate() {
        super.onCreate()
        // Required initialization logic here!
        mInstance = this

        try {
            mSocket = IO.socket(SOCKET_URL)
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }
    }

    fun getSocket(): Socket? {
        return mSocket
    }


}