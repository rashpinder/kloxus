package com.kloxus.app.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import com.bumptech.glide.Glide
import com.kloxus.app.R
import com.kloxus.app.interfaces.GetChatUserInterface
import com.kloxus.app.interfaces.ItemChatClickListner
import com.kloxus.app.interfaces.LoadMoreScrollListner
import com.kloxus.app.model.GetAllChatUsersData

class ChatUserDataAdapter(
    val mActivity: Activity?,
    var mArrayList: ArrayList<GetAllChatUsersData?>?,
    var mItemChatClickListner: ItemChatClickListner,
    var mLoadMoreScrollListner : GetChatUserInterface
) :
    RecyclerView.Adapter<ChatUserDataAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_messages_list, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel: GetAllChatUsersData = mArrayList!!.get(position)!!
        holder.txtNameTV.setText(mModel.name)
        holder.txtMsgTV.setText(mModel.lastMessage)
        mActivity?.let {
            Glide.with(it)
                .load(mModel.profileImage)
                .placeholder(R.drawable.ic_ph)
                .error(R.drawable.ic_ph)
                .into(holder.imgUserIV)
        }
        if (mModel.messageCount.toInt()>99){
            holder.badge.setText("99+")
            holder.badge.isVisible=true
        } else if (mModel.messageCount.toInt()>0 ){
            holder.badge.setText(mModel.messageCount)
            holder.badge.isVisible=true
        }
        else{
            holder.badge.isVisible=false
        }

        holder.itemView.setOnClickListener({
            mItemChatClickListner.onItemClickListner(mModel, position)
        })

        if (mArrayList!!.size % 10 == 0 && mArrayList!!.size - 1 == position){
            mLoadMoreScrollListner.onLoadMoreListner(mModel)
        }
    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        lateinit var imgUserIV: ImageView
        lateinit var txtNameTV: TextView
        lateinit var txtMsgTV: TextView
        lateinit var badge: TextView

        init {
            imgUserIV = itemView.findViewById<View>(R.id.imgUserIV) as ImageView
            txtNameTV = itemView.findViewById<View>(R.id.txtNameTV) as TextView
            txtMsgTV = itemView.findViewById<View>(R.id.txtMsgTV) as TextView
            badge = itemView.findViewById<View>(R.id.badge) as TextView
        }
    }


}
