package com.kloxus.app.adapters

import LoadMoreReviews
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kloxus.app.R
import com.kloxus.app.model.RatingDetailX

class SeeAllReviewAdapter(
    val mActivity: Activity?,
    var mArrayList: ArrayList<RatingDetailX>?,
    var mLoadMoreScrollListner1 : LoadMoreReviews
) : RecyclerView.Adapter<SeeAllReviewAdapter.MyViewHolder>() {

    public interface LoadMoreReviews {
        fun onLoadMoreListner(mModel : RatingDetailX)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_review_list, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel : RatingDetailX = mArrayList!!.get(position)

        holder.txtTitleTV.setText(mModel.review+" - "+mModel.name)


        if (mArrayList!!.size % 10 == 0 && mArrayList!!.size - 1 == position){
            mLoadMoreScrollListner1.onLoadMoreListner(mModel)
        }
    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        lateinit var txtTitleTV: TextView

        init {
            txtTitleTV = itemView.findViewById<View>(R.id.txtTitleTV) as TextView
        }
    }

}
