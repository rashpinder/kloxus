package com.kloxus.app.adapters

import android.content.Context
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.kloxus.app.R

class SlidingImageAdapter(
    private val context: Context,
    images: ArrayList<String>,
    addClick: AddClick
) :
    PagerAdapter() {
    private val inflater: LayoutInflater
    private val modelArrayList: ArrayList<String>
    var addClick: AddClick

    interface AddClick {
        fun Click(position: Int)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return modelArrayList.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout: View = inflater.inflate(R.layout.item_sliding_pages, view, false)!!
        val imageView = imageLayout.findViewById<ImageView>(R.id.image)
        val linearLayout = imageLayout.findViewById<LinearLayout>(R.id.linearLayout)
        imageView.scaleType=ImageView.ScaleType.FIT_XY
//        val mImagesModel: ImagesModel = modelArrayList[position]
//        if (modelArrayList!= null) {
//            if (modelArrayList[position].contains("https")) {
//                Glide.with(context).load(modelArrayList[position])
//                    .placeholder(R.drawable.ic_ph)
//                    .error(R.drawable.ic_ph)
//                    .into(imageView)
//            } else {
//                Glide.with(context)
//                    .load(modelArrayList[position].replace("http://", "https://"))
//                    .into(imageView)
//            }
//        } else {
//            if (modelArrayList[position].contains("https")) {
//                imageView.setImageBitmap(
//                    (context as BaseActivity).getBitmapFromFilePath(
//                        modelArrayList[position]
//                    )
//                )
//            } else {
//                imageView.setImageBitmap(
//                    (context as BaseActivity).getBitmapFromFilePath(
//                        modelArrayList[position].replace("http://", "https://")
//                    )
//                )
//            }
//        }

        Glide.with(context).load(modelArrayList[position])
            .placeholder(R.drawable.ic_booking_ph)
            .error(R.drawable.ic_booking_ph)
            .into(imageView)

//        if (modelArrayList.size == 0) {
//            imageView.setBackgroundResource(R.drawable.ic_ph)
//        }
        linearLayout.setOnClickListener { addClick.Click(position) }
        view.addView(imageLayout, 0)
        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}
    override fun saveState(): Parcelable? {
        return null
    }

    init {
        modelArrayList = images
        this.addClick = addClick
        inflater = LayoutInflater.from(context)
    }
}