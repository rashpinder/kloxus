package com.kloxus.app.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kloxus.app.R
import com.kloxus.app.activities.ToiletProfileActivity
import com.kloxus.app.interfaces.LoadMoreListner
import com.kloxus.app.model.Data

class ManageToiletAdapter(
    val mActivity: Activity?,
    var mArrayList: ArrayList<Data>?,
    var mLoadMoreScrollListner : LoadMoreListner
) : RecyclerView.Adapter<ManageToiletAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_manage_toilet_list, null)
        return MyViewHolder(view)
    }

    interface PaginationInterface {
        fun mPaginationInterface(isLastScrolled: Boolean)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel : Data = mArrayList!!.get(position)

        holder.txtTitleTV.setText(mModel.title)
        holder.txtAddressTV.setText(mModel.address)
//        holder.txtDescTV.setText(mModel.description)
        if (mModel.toiletOptions!!.contains("3") && mModel.toiletOptions!!.contains("2")  && mModel.toiletOptions!!.contains("4") && mModel.toiletOptions!!.contains("1")&& mModel.toiletOptions!!.contains("5")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.all)
        }
        else if (mModel.toiletOptions!!.contains("1") && mModel.toiletOptions!!.contains("2") && mModel.toiletOptions!!.contains("3")&& mModel.toiletOptions!!.contains("4")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.a)
        }
        else if (mModel.toiletOptions!!.contains("1") && mModel.toiletOptions!!.contains("2") && mModel.toiletOptions!!.contains("3")&& mModel.toiletOptions!!.contains("5")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.b)
        }else if (mModel.toiletOptions!!.contains("1") && mModel.toiletOptions!!.contains("4") && mModel.toiletOptions!!.contains("3")&& mModel.toiletOptions!!.contains("5")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.c)
        }
        else if (mModel.toiletOptions!!.contains("2") && mModel.toiletOptions!!.contains("4") && mModel.toiletOptions!!.contains("3")&& mModel.toiletOptions!!.contains("5")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.d)
        }
        else if (mModel.toiletOptions!!.contains("2") && mModel.toiletOptions!!.contains("4") && mModel.toiletOptions!!.contains("1")&& mModel.toiletOptions!!.contains("5")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.e)
        }

        else if (mModel.toiletOptions!!.contains("1") && mModel.toiletOptions!!.contains("2") && mModel.toiletOptions!!.contains("3")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.f)
        } else if (mModel.toiletOptions!!.contains("1") && mModel.toiletOptions!!.contains("2") && mModel.toiletOptions!!.contains("4")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.ff)
        } else if (mModel.toiletOptions!!.contains("1") && mModel.toiletOptions!!.contains("3") && mModel.toiletOptions!!.contains("4")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.g)
        } else if (mModel.toiletOptions!!.contains("1") && mModel.toiletOptions!!.contains("5") && mModel.toiletOptions!!.contains("4")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.h)
        }else if (mModel.toiletOptions!!.contains("2") && mModel.toiletOptions!!.contains("3") && mModel.toiletOptions!!.contains("4")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.i)
        }else if (mModel.toiletOptions!!.contains("2") && mModel.toiletOptions!!.contains("3") && mModel.toiletOptions!!.contains("5")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.j)
        }else if (mModel.toiletOptions!!.contains("2") && mModel.toiletOptions!!.contains("4") && mModel.toiletOptions!!.contains("5")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.k)
        }else if (mModel.toiletOptions!!.contains("1") && mModel.toiletOptions!!.contains("3") && mModel.toiletOptions!!.contains("5")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.fff)
        }
        else if (mModel.toiletOptions!!.contains("3") && mModel.toiletOptions!!.contains("4") && mModel.toiletOptions!!.contains("5")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.ffff)
        }
        else if (mModel.toiletOptions!!.contains("1") && mModel.toiletOptions!!.contains("2")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.l)
        } else if (mModel.toiletOptions!!.contains("1") && mModel.toiletOptions!!.contains("3")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.m)
        } else if (mModel.toiletOptions!!.contains("1") && mModel.toiletOptions!!.contains("4")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.n)
        } else if (mModel.toiletOptions!!.contains("1") && mModel.toiletOptions!!.contains("5")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.o)
        } else if (mModel.toiletOptions!!.contains("2") && mModel.toiletOptions!!.contains("3")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.p)
        } else if (mModel.toiletOptions!!.contains("2") && mModel.toiletOptions!!.contains("4")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.q)
        } else if (mModel.toiletOptions!!.contains("2") && mModel.toiletOptions!!.contains("5")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.r)
        } else if (mModel.toiletOptions!!.contains("3") && mModel.toiletOptions!!.contains("4")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.s)
        } else if (mModel.toiletOptions!!.contains("3") && mModel.toiletOptions!!.contains("5")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.t)
        } else if (mModel.toiletOptions!!.contains("4") && mModel.toiletOptions!!.contains("5")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.u)
        }
        else if (mModel.toiletOptions!!.contains("1")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.toilet_paper)
        } else if (mModel.toiletOptions!!.contains("2")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.soap)
        } else if (mModel.toiletOptions!!.contains("3")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.hand_dryer)
        } else if (mModel.toiletOptions!!.contains("4")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.ser)
        } else if (mModel.toiletOptions!!.contains("5")) {
            holder.txtDescTV.text= mActivity!!.getString(R.string.dis)
        }

        if (mActivity != null) {
            Glide.with(mActivity)
                .load(mModel.toiletImage)
                .placeholder(R.drawable.ic_bg_sp)
                .error(R.drawable.ic_bg_sp)
                .into(holder.imgToiletIV)
        }

        holder.mainLL.setOnClickListener({
            val i = Intent(mActivity, ToiletProfileActivity::class.java)
            i.putExtra("toilet_id",mModel.toiletID)
            mActivity!!.startActivity(i)
        })

        if (mArrayList!!.size % 10 == 0 && mArrayList!!.size - 1 == position){
            mLoadMoreScrollListner.onLoadMoreListner(mModel)
        }
    }

    override fun getItemCount(): Int {
        if (mArrayList!!.isEmpty()){
            return 0
        }
        else
            return mArrayList!!.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        lateinit var txtTitleTV: TextView
        lateinit var txtDescTV: TextView
        lateinit var txtAddressTV: TextView
        lateinit var imgToiletIV: ImageView
        lateinit var mainLL: LinearLayout

        init {
            txtTitleTV = itemView.findViewById<View>(R.id.txtTitleTV) as TextView
            txtDescTV = itemView.findViewById<View>(R.id.txtDescTV) as TextView
            txtAddressTV = itemView.findViewById<View>(R.id.txtAddressTV) as TextView
            imgToiletIV = itemView.findViewById<View>(R.id.imgToiletIV) as ImageView
            mainLL = itemView.findViewById<View>(R.id.mainLL) as LinearLayout
        }
    }

}
