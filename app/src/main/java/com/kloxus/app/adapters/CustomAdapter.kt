package com.kloxus.app.adapters

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.iarcuschin.simpleratingbar.SimpleRatingBar
import com.kloxus.app.R
import com.kloxus.app.activities.ToiletDetailActivity
import com.kloxus.app.model.HomeData

class CustomAdapter(val mActivity: Activity, var mArrayList: ArrayList<HomeData>) :
    RecyclerView.Adapter<CustomAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_bottomsheet_ist, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val mModel: HomeData = mArrayList.get(position)
        holder.txtTitleTV.text = mModel.title
        holder.txtkmTV.text = mModel.distance
        holder.txtRatingTV.text = mModel.rating

        if (mModel.isAvailable.equals("1")) {
            holder.parentLL.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.white))
        } else {
            holder.parentLL.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.colorGreyL))
        }
        if (mModel.isPaid.equals("1")) {
            holder.txtPriceTV.text = mModel.currencySymbol + mModel.price
        } else {
            holder.txtPriceTV.text = mModel.price
        }

        holder.ratingBar.starBorderWidth = 1.8F
        holder.ratingBar.rating = mModel.rating.toFloat()


        if (mActivity != null) {
            Glide.with(mActivity)
                .load(mModel.toiletImage)
                .placeholder(R.drawable.ic_bg_sp)
                .error(R.drawable.ic_bg_sp)
                .into(holder.imgToiletIV)
        }
        holder.ratingBar.setOnClickListener {
            holder.ratingBar.rating = mModel.rating.toFloat()
        }
        holder.mainLL.setOnClickListener({
            val i = Intent(mActivity, ToiletDetailActivity::class.java)
            i.putExtra("toilet_id", mModel.toiletID)
            i.putExtra("value", "home")
            mActivity?.startActivity(i)
        })


    }

    override fun getItemCount(): Int {
        if (mArrayList.isEmpty()) {
            return 0
        } else if (mArrayList.size == 1) {
            return 1
        } else
            return 2
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        lateinit var txtTitleTV: TextView
        lateinit var txtkmTV: TextView
        lateinit var txtRatingTV: TextView
        lateinit var txtPriceTV: TextView
        lateinit var imgToiletIV: ImageView
        lateinit var ratingBar: SimpleRatingBar
        lateinit var mainLL: LinearLayout
        lateinit var parentLL: LinearLayout

        init {
            mainLL = itemView.findViewById<View>(R.id.mainLL) as LinearLayout
            parentLL = itemView.findViewById<View>(R.id.parentLL) as LinearLayout
            txtTitleTV = itemView.findViewById<View>(R.id.txtTitleTV) as TextView
            txtkmTV = itemView.findViewById<View>(R.id.txtkmTV) as TextView
            imgToiletIV = itemView.findViewById<View>(R.id.imgToiletIV) as ImageView
            ratingBar = itemView.findViewById<View>(R.id.ratingBar) as SimpleRatingBar
            txtRatingTV = itemView.findViewById<View>(R.id.txtRatingTV) as TextView
            txtPriceTV = itemView.findViewById<View>(R.id.txtPriceTV) as TextView
        }
    }

    fun updateWithSearchData(list: ArrayList<HomeData>?) {
        if (list != null) {
            mArrayList = list
        }
        notifyDataSetChanged()
    }

}
