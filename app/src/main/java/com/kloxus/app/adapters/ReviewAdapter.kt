package com.kloxus.app.adapters

import android.app.Activity
import android.opengl.Visibility
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.kloxus.app.R
import com.kloxus.app.model.RatingDetail

class ReviewAdapter(
    val mActivity: Activity?,
    var mArrayList: ArrayList<RatingDetail>?
) : RecyclerView.Adapter<ReviewAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_review_list, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel: RatingDetail = mArrayList!!.get(position)
        if (mModel.review.equals("")) {
            holder.mainLL.visibility = View.GONE
        } else {
            holder.txtTitleTV.text = mModel.review + " - " + mModel.name
            holder.mainLL.visibility = View.VISIBLE
//            holder.mainLL.isVisible = true
        }
    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        lateinit var txtTitleTV: TextView
        lateinit var mainLL: LinearLayout

        init {
            txtTitleTV = itemView.findViewById<View>(R.id.txtTitleTV) as TextView
            mainLL = itemView.findViewById<View>(R.id.mainLL) as LinearLayout
        }
    }

}
