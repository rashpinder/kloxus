package com.kloxus.app.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.iarcuschin.simpleratingbar.SimpleRatingBar
import com.kloxus.app.R
import com.kloxus.app.activities.BookingItemDescriptionActivity
import com.kloxus.app.interfaces.LoadMoreDataByCityListener
import com.kloxus.app.model.GetToiletbyCityData

class BookingDetailAdapter(
    val mActivity: Activity?,
    var mArrayList: ArrayList<GetToiletbyCityData>?,
    var mLoadMoreScrollListner : LoadMoreDataByCityListener,
    var mtype: String ,
    var mAddress: String
) : RecyclerView.Adapter<BookingDetailAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_booking_detail_list, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel : GetToiletbyCityData = mArrayList!!.get(position)

        holder.txtTitleTV.setText(mModel.title)
        holder.txtDescTV.setText(mModel.address)
        holder.ratingBar.setStarBorderWidth(1.8F)
        holder.ratingBar.setRating(mModel.rating.toFloat())


        if (mActivity != null) {
            Glide.with(mActivity)
                .load(mModel.toiletImage)
                .placeholder(R.drawable.ic_bg_sp)
                .error(R.drawable.ic_bg_sp)
                .into(holder.imgToiletIV)
        }

        holder.mainLL.setOnClickListener({
            val i = Intent(mActivity, BookingItemDescriptionActivity::class.java)
            i.putExtra("toilet_id",mModel.toiletID)
            i.putExtra("mtype",mtype)
            i.putExtra("book_id",mModel.bookID)
            i.putExtra("mAddress",mAddress)
            mActivity?.startActivity(i)
        })

        if (mArrayList!!.size % 10 == 0 && mArrayList!!.size - 1 == position){
            mLoadMoreScrollListner.onLoadMoreListner(mModel)
        }
    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        lateinit var txtTitleTV: TextView
        lateinit var txtDescTV: TextView
        lateinit var imgToiletIV: ImageView
        lateinit var ratingBar: SimpleRatingBar
        lateinit var mainLL: LinearLayout

        init {
            txtTitleTV = itemView.findViewById<View>(R.id.txtTitleTV) as TextView
            txtDescTV = itemView.findViewById<View>(R.id.txtDescTV) as TextView
            imgToiletIV = itemView.findViewById<View>(R.id.imgToiletIV) as ImageView
            ratingBar = itemView.findViewById<View>(R.id.ratingBar) as SimpleRatingBar
            mainLL = itemView.findViewById<View>(R.id.mainLL) as LinearLayout
        }
    }

}
