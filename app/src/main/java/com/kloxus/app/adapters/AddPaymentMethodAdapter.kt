package com.kloxus.app.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kloxus.app.R
import com.kloxus.app.activities.BookNowActivity
import com.kloxus.app.activities.BookingDetailActivity
import com.kloxus.app.activities.BookingItemDescriptionActivity
import com.kloxus.app.interfaces.OnCardClickInterface
import com.kloxus.app.model.CardsData

class AddPaymentMethodAdapter(val mActivity: Activity?, var mAllCardsList: ArrayList<CardsData>?, var onItemClickListener: OnCardClickInterface? = null) : RecyclerView.Adapter<AddPaymentMethodAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_payment_methods, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel : CardsData = mAllCardsList!!.get(position)
        holder.txtTitleTV.setText(mModel.card_id)
        
        holder.txtTitleTV.setText(mModel.brand)
        holder.txtCardNumberTV.setText(mActivity!!.resources.getString(R.string.ending_with) + " " + mModel.last4)

        if (mModel.brand.toLowerCase().equals("visa")) {
            holder.imgTypeIV.setImageResource(R.drawable.ic_visa)
        } else if (mModel.brand.toLowerCase().equals("discover")) {
            holder.imgTypeIV.setImageResource(R.drawable.ic_discover)
        } else if (mModel.brand.toLowerCase().equals("american express")) {
            holder.imgTypeIV.setImageResource(R.drawable.ic_ae)
        } else if (mModel.brand.toLowerCase().equals("mastercard")) {
            holder.imgTypeIV.setImageResource(R.drawable.ic_mc)
        } else {
            holder.imgTypeIV.setImageResource(R.drawable.ic_other)
        }
        holder.imgDeleteIV.setOnClickListener({
            onItemClickListener!!.onItemClickListner(mModel.card_id,position )
//            val i = Intent(mActivity, BookNowActivity::class.java)
//            mActivity?.startActivity(i)
        })

    }

    override fun getItemCount(): Int {
        return mAllCardsList!!.size
    }


    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        lateinit var txtTitleTV: TextView
        lateinit var txtCardNumberTV: TextView
        lateinit var imgTypeIV: ImageView
        lateinit var imgDeleteIV: ImageView
        lateinit var mainLL: LinearLayout

        init {
            txtTitleTV = itemView.findViewById<View>(R.id.txtTitleTV) as TextView
            txtCardNumberTV = itemView.findViewById<View>(R.id.txtCardNumberTV) as TextView
            imgTypeIV = itemView.findViewById<View>(R.id.imgTypeIV) as ImageView
            imgDeleteIV = itemView.findViewById<View>(R.id.imgDeleteIV) as ImageView
            mainLL = itemView.findViewById<View>(R.id.mainLL) as LinearLayout
        }
    }
}
