package com.kloxus.app.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kloxus.app.R
import com.kloxus.app.interfaces.OnCardClickInterface
import com.kloxus.app.model.CardsData
import com.kloxus.app.utils.KloxusPrefrences

class SelectPaymentAdapter(
    val mActivity: Activity?,
    var mAllCardsList: ArrayList<CardsData>?,
    var mOnCardClickListner: OnCardClickInterface
) : RecyclerView.Adapter<SelectPaymentAdapter.MyViewHolder>() {

    private var row_index = -1
//    private var row_index = 0
    private var count = 0
    var selected = false
    var mDefault = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(
            R.layout.item_select_payment_method,
            null
        )
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel: CardsData = mAllCardsList!!.get(position)
        mDefault=mModel.isDefault
        holder.txtTitleTV.setText(mModel.card_id)

        holder.txtTitleTV.setText(mModel.brand)
        holder.txtCardNumberTV.setText(mActivity!!.resources.getString(R.string.ending_with) + " " + mModel.last4)


        if (mModel.brand.toLowerCase().equals("visa")) {
            holder.imgTypeIV.setImageResource(R.drawable.ic_visa)
        } else if (mModel.brand.toLowerCase().equals("discover")) {
            holder.imgTypeIV.setImageResource(R.drawable.ic_discover)
        } else if (mModel.brand.toLowerCase().equals("american express")) {
            holder.imgTypeIV.setImageResource(R.drawable.ic_ae)
        } else if (mModel.brand.toLowerCase().equals("mastercard")) {
            holder.imgTypeIV.setImageResource(R.drawable.ic_mc)
        } else {
            holder.imgTypeIV.setImageResource(R.drawable.ic_other)
        }


        holder.mainLL.setOnClickListener({
            row_index = position
            count = 1
//            selected = true
            selected = false
            mOnCardClickListner.onItemClickListner(mModel.card_id, position)
            holder.imgRadioButtonIV.setImageResource(R.drawable.ic_active_rb)
            KloxusPrefrences().writeString(mActivity, "paymentMethod", "1")

//            notifyDataSetChanged()
        })

        if (mDefault==1 ){
            holder.imgRadioButtonIV.setImageResource(R.drawable.ic_active_rb)
        }
        else{
            holder.imgRadioButtonIV.setImageResource(R.drawable.ic_uncheck_radio)
        }

//        if (row_index == 0 && count == 1) {
//            holder.imgRadioButtonIV.setImageResource(R.drawable.ic_active_rb)
//        } else {
//            holder.imgRadioButtonIV.setImageResource(R.drawable.ic_uncheck)
//        }
//
//
//        if (row_index == position && count == 1) {
//            holder.imgRadioButtonIV.setImageResource(R.drawable.ic_active_rb)
//        } else {
//            holder.imgRadioButtonIV.setImageResource(R.drawable.ic_uncheck)
//        }

    }

    override fun getItemCount(): Int {
        return mAllCardsList!!.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        lateinit var txtTitleTV: TextView
        lateinit var txtCardNumberTV: TextView
        lateinit var imgTypeIV: ImageView
        lateinit var imgRadioButtonIV: ImageView
        lateinit var mainLL: LinearLayout

        init {
            txtTitleTV = itemView.findViewById<View>(R.id.txtTitleTV) as TextView
            txtCardNumberTV = itemView.findViewById<View>(R.id.txtCardNumberTV) as TextView
            imgTypeIV = itemView.findViewById<View>(R.id.imgTypeIV) as ImageView
            imgRadioButtonIV = itemView.findViewById<View>(R.id.imgRadioButtonIV) as ImageView
            mainLL = itemView.findViewById<View>(R.id.mainLL) as LinearLayout
        }
    }
}
