package com.kloxus.app.adapters

import android.app.Activity
import android.content.Intent
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kloxus.app.R
import com.kloxus.app.activities.ChatActivity
import com.kloxus.app.activities.ToiletPathActivity
import com.kloxus.app.activities.ToiletProfileActivity
import com.kloxus.app.interfaces.AcceptRejectbookingInterface
import com.kloxus.app.interfaces.LoadMoreNotificationListener
import com.kloxus.app.model.DataX
import com.kloxus.app.utils.ROOM_ID
import com.kloxus.app.utils.USER_NAME
import java.text.SimpleDateFormat
import java.util.*
import java.util.Locale


class NotificationAdapter(val mActivity: Activity?, var mArrayList: ArrayList<DataX>?,  var mItemClickListner: AcceptRejectbookingInterface,var mLoadMoreScrollListner : LoadMoreNotificationListener) : RecyclerView.Adapter<NotificationAdapter.MyViewHolder>() {
    private var mLastClickTab1: Long = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_notifications_list, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel : DataX = mArrayList!!.get(position)
        holder.txtTitleTV.setText(mModel.description)
        val engTime_ = GetHumanReadableDate(mModel.created.toLong(), "H:mm aa")
        holder.txtDateTV.setText(gettingLongToFormatedDate(mModel.created.toLong())+" at "+gettingLongToFormatedTime(mModel.created.toLong()))

        if (mActivity != null) {
            Glide.with(mActivity)
                .load(mModel.profileImage)
                .placeholder(R.drawable.ic_bg_sp)
                .error(R.drawable.ic_bg_sp)
                .into(holder.imgToiletIV)
        }
        if (mModel.notification_type.equals("2")){
            holder.reqLL.isVisible = true
            holder.txtDescTV.setText(mModel.title)
//            holder.txtTitleTV.setText(mModel.description+" "+mModel.toiletName)
//            holder.txtTitleTV.setText(mModel.description)
        } else if (mModel.notification_type.equals("5") && mModel.notification_type.equals("6")){
            holder.reqLL.isVisible = false
            holder.txtDescTV.setText(mModel.title)
        }
        else if (mModel.notification_type.equals("3")){
            holder.txtDescTV.setText(mModel.title+" "+"Send a message")
            holder.reqLL.isVisible = false
        }
        else{
            holder.reqLL.isVisible = false
            holder.txtDescTV.setText(mModel.title)
        }
        holder.txtAcceptTV.setOnClickListener {
            preventMultipleClick()
            mItemClickListner.onItemClickListner(mModel.notification_id, position,1,mModel.detailsID)
        }
        holder.txtRejectTV.setOnClickListener {
            preventMultipleClick()
            mItemClickListner.onItemClickListner(mModel.notification_id, position,2,mModel.detailsID)
        }
        holder.itemView.setOnClickListener({
        var mIntent: Intent? = null
        if (mModel.notification_type.equals("2")){
//                mIntent = Intent(mActivity, HomeActivity::class.java)
//                mActivity!!.startActivity(mIntent)
        }
        else if (mModel.notification_type.equals("3")){
            mIntent = Intent(mActivity, ChatActivity::class.java)
            mIntent!!.putExtra(USER_NAME, mModel.title)
            mIntent!!.putExtra(ROOM_ID, mModel.roomID)
            mActivity!!.startActivity(mIntent)
        }
        else if (mModel.notification_type.equals("5")){
            mIntent = Intent(mActivity, ToiletPathActivity::class.java)
            mIntent!!.putExtra("toilet_id", mModel.detailsID)
            mIntent!!.putExtra("book_id", mModel.bookID)
            mActivity!!.startActivity(mIntent)
        }
        else if (mModel.notification_type.equals("8")){
            mIntent = Intent(mActivity, ToiletProfileActivity::class.java)
            mIntent!!.putExtra("toilet_id", mModel.detailsID)
            mIntent!!.putExtra("case", "noti")
            mActivity!!.startActivity(mIntent)
        }
//        else if (mModel.notification_type.equals("6")){
//            mIntent = Intent(mActivity, HomeActivity::class.java)
////            mIntent!!.putExtra(BUSINESS_ID, mModel.businessID)
//            mActivity!!.startActivity(mIntent)
//        }
        } )

        if (mArrayList!!.size % 10 == 0 && mArrayList!!.size - 1 == position){
            mLoadMoreScrollListner.onLoadMoreListner(mModel)
        }
    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        lateinit var txtTitleTV: TextView
        lateinit var txtDateTV: TextView
        lateinit var imgToiletIV: ImageView
        lateinit var txtDescTV: TextView
        lateinit var txtAcceptTV: TextView
        lateinit var txtRejectTV: TextView
        lateinit var reqLL: LinearLayout

        init {
            txtTitleTV = itemView.findViewById<View>(R.id.txtTitleTV) as TextView
            txtDateTV = itemView.findViewById<View>(R.id.txtDateTV) as TextView
            txtRejectTV = itemView.findViewById<View>(R.id.txtRejectTV) as TextView
            txtAcceptTV = itemView.findViewById<View>(R.id.txtAcceptTV) as TextView
            txtDescTV = itemView.findViewById<View>(R.id.txtDescTV) as TextView
            imgToiletIV = itemView.findViewById<View>(R.id.imgToiletIV) as ImageView
            reqLL = itemView.findViewById<View>(R.id.reqLL) as LinearLayout
        }
    }

    fun gettingLongToFormatedDate(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("MMM dd")
        return jdf.format(date)
    }

    fun gettingLongToFormatedTime(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("HH:mm aa")
        return jdf.format(date)
    }

    fun GetHumanReadableDate(epochSec: Long, dateFormatStr: String?): String? {
        val date = Date(epochSec * 1000)
        val format = SimpleDateFormat(
            dateFormatStr,
            Locale.getDefault()
        )
        return format.format(date)
    }




    open fun preventMultipleClick() {
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 2000) {
            return
        }
        mLastClickTab1 = SystemClock.elapsedRealtime()
    }

}
