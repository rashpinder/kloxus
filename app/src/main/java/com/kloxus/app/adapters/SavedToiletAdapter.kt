package com.kloxus.app.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kloxus.app.R
import com.kloxus.app.activities.ToiletDetailActivity
import com.kloxus.app.interfaces.LoadMoreScrollListner
import com.kloxus.app.model.HomeData

class SavedToiletAdapter(
    val mActivity: Activity?,
    var mArrayList: ArrayList<HomeData>?,
    var onItemClickListener: OnItemClickListener? = null,
    var mLoadMoreScrollListner : LoadMoreScrollListner
) : RecyclerView.Adapter<SavedToiletAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_saved_toilets, null)
        return MyViewHolder(view)
    }

    //    MuteInterface muteInterface;
    interface OnItemClickListener {
        fun onItemClick(positon: Int, toilet_id: String)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel: HomeData = mArrayList!!.get(position)

        holder.txtTitleTV.setText(mModel.title)
        holder.txtDescTV.setText(mModel.description)


        if (mActivity != null) {
            Glide.with(mActivity)
                .load(mModel.toiletImage)
                .placeholder(R.drawable.ic_bg_sp)
                .error(R.drawable.ic_bg_sp)
                .into(holder.imgToiletIV)
        }
        holder.imgFavIV.setOnClickListener({
            onItemClickListener!!.onItemClick(position, mModel.toiletID)
        })

        holder.mainLL.setOnClickListener({
            val i = Intent(mActivity, ToiletDetailActivity::class.java)
            i.putExtra("toilet_id", mModel.toiletID)
            i.putExtra("value","saved_list")
            mActivity?.startActivity(i)
//            mActivity!!.finish()
        })

        if (mArrayList!!.size % 10 == 0 && mArrayList!!.size - 1 == position){
            mLoadMoreScrollListner.onLoadMoreListner(mModel)
        }
    }

    override fun getItemCount(): Int {
        if (mArrayList!!.isEmpty()) {
            return 0
        } else
            return mArrayList!!.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        lateinit var txtTitleTV: TextView
        lateinit var txtDescTV: TextView
        lateinit var imgToiletIV: ImageView
        lateinit var imgFavIV: ImageView
        lateinit var mainLL: LinearLayout

        init {
            txtTitleTV = itemView.findViewById<View>(R.id.txtTitleTV) as TextView
            txtDescTV = itemView.findViewById<View>(R.id.txtDescTV) as TextView
            imgToiletIV = itemView.findViewById<View>(R.id.imgToiletIV) as ImageView
            imgFavIV = itemView.findViewById<View>(R.id.imgFavIV) as ImageView
            mainLL = itemView.findViewById<View>(R.id.mainLL) as LinearLayout
        }
    }
}
