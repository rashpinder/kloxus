package com.kloxus.app.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kloxus.app.R
import com.kloxus.app.model.ChatMessageData
import com.kloxus.app.utils.LEFT_VIEW_HOLDER
import com.kloxus.app.utils.RIGHT_VIEW_HOLDER
import com.merger.app.chatviewholder.ItemLeftViewHolder
import com.merger.app.chatviewholder.ItemRightViewHolder
import java.util.*

class ChatMessagesAdapter(val mActivity: Activity?, var mArrayList: ArrayList<ChatMessageData?>?) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == RIGHT_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_chat_right, parent, false)
            return ItemRightViewHolder(mView)
        } else if (viewType == LEFT_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_chat_left, parent, false)
            return ItemLeftViewHolder(mView)
        }
        return null!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == RIGHT_VIEW_HOLDER) {
            (holder as ItemRightViewHolder).bindData(
                mActivity,
                mArrayList!![position] as ChatMessageData?
            )
        } else if (holder.itemViewType == LEFT_VIEW_HOLDER) {
            (holder as ItemLeftViewHolder).bindData(
                mActivity,
                mArrayList!![position] as ChatMessageData?
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (mArrayList!![position]!!.viewType == 1) return RIGHT_VIEW_HOLDER
        else if (mArrayList!![position]!!.viewType == 0) return LEFT_VIEW_HOLDER
        return -1
    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }


}
