package com.kloxus.app.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kloxus.app.R
import com.kloxus.app.activities.BookingDetailActivity
import com.kloxus.app.model.DataXX

class UserModeAdapter(val mActivity: Activity?,var mArrayList: List<DataXX>?) : RecyclerView.Adapter<UserModeAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_bookings_list, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel : DataXX = mArrayList!!.get(position)
        holder.txtDescTV.setText(mModel.toiletLocation)

        holder.mainLL.setOnClickListener({
            val i = Intent(mActivity, BookingDetailActivity::class.java)
            i.putExtra("location",mModel.toiletLocation)
            i.putExtra("type","1")
            mActivity?.startActivity(i)
        })
    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        lateinit var txtTitleTV: TextView
        lateinit var txtDescTV: TextView
        lateinit var imgBookingIV: ImageView
        lateinit var mainLL: LinearLayout

        init {
            txtTitleTV = itemView.findViewById<View>(R.id.txtTitleTV) as TextView
            txtDescTV = itemView.findViewById<View>(R.id.txtDescTV) as TextView
            imgBookingIV = itemView.findViewById<View>(R.id.imgBookingIV) as ImageView
            mainLL = itemView.findViewById<View>(R.id.mainLL) as LinearLayout
        }
    }

}
