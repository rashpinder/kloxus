package com.kloxus.app.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.kloxus.app.R
import com.kloxus.app.model.ShowBankDetailsModel
import com.kloxus.app.model.StatusMsgModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.AUTH_TOKEN
import com.kloxus.app.utils.KloxusPrefrences
import com.kloxus.app.utils.USER_ID
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ShowBankDetailsActivity : BaseActivity() {


    @BindView(R.id.editFirstNameET)
    lateinit var editFirstNameET: TextView

    @BindView(R.id.editLastNameET)
    lateinit var editLastNameET: TextView

    @BindView(R.id.editCityNameET)
    lateinit var editCityNameET: TextView

    @BindView(R.id.txtCountryNameET)
    lateinit var txtCountryNameET: TextView

    @BindView(R.id.editAddressLineOneET)
    lateinit var editAddressLineOneET: TextView

    @BindView(R.id.editAddressLineTwoET)
    lateinit var editAddressLineTwoET: TextView

    @BindView(R.id.editPostalCodeET)
    lateinit var editPostalCodeET: TextView

    @BindView(R.id.editStateNameET)
    lateinit var editStateNameET: TextView

    @BindView(R.id.txtDobET)
    lateinit var txtDobET: TextView

    @BindView(R.id.txtUploadImg)
    lateinit var txtUploadImg: TextView

    @BindView(R.id.txtUploadImgtwo)
    lateinit var txtUploadImgtwo: TextView

    @BindView(R.id.editIdNumET)
    lateinit var editIdNumET: TextView

    @BindView(R.id.editPhoneET)
    lateinit var editPhoneET: TextView

    @BindView(R.id.editssnET)
    lateinit var editssnET: TextView

    @BindView(R.id.editAHNameET)
    lateinit var editAHNameET: TextView

    @BindView(R.id.editRoutingNoET)
    lateinit var editRoutingNoET: TextView

    @BindView(R.id.editAccNoET)
    lateinit var editAccNoET: TextView

    @BindView(R.id.txtDeleteTV)
    lateinit var txtDeleteTV: TextView

    @BindView(R.id.imgBackIV)
    lateinit var imgBackIV: ImageView

    @BindView(R.id.imgAddImgOneIV)
    lateinit var imgAddImgOneIV: ImageView

    @BindView(R.id.imgAddImgTwoIV)
    lateinit var imgAddImgTwoIV: ImageView
    var acc_id: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_bank_details)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        getBankDetails()
    }

    @OnClick(
        R.id.imgBackIV,
        R.id.txtDeleteTV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackIV -> performBackClick()
            R.id.txtDeleteTV -> performDeleteClick()
        }
    }

    private fun performDeleteClick() {
        if (isNetworkAvailable(mActivity))
            executeDeleteBankDetailsRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))

    }

    private fun mDeleteParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["accountID"] = acc_id
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    private fun executeDeleteBankDetailsRequest() {
        showProgressDialog(this)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.deleteBankDetailsRequest(mDeleteParams())
            ?.enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {
                    dismissProgressDialog()
                    val mModel: StatusMsgModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            showToast(mActivity, mModel.message)
                            finish()
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }


    private fun getBankDetails() {
        if (isNetworkAvailable(mActivity))
            executeGetBankDetailsRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }

    private fun mBankParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetBankDetailsRequest() {
        showProgressDialog(this)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getBankDetailsRequest(mBankParams())
            ?.enqueue(object : Callback<ShowBankDetailsModel> {
                override fun onFailure(call: Call<ShowBankDetailsModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<ShowBankDetailsModel>,
                    response: Response<ShowBankDetailsModel>
                ) {
                    dismissProgressDialog()
                    val mModel: ShowBankDetailsModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            Glide.with(mActivity).load(mModel.data.frontImage)
                                .placeholder(R.drawable.ic_up_doc)
                                .error(R.drawable.ic_up_doc)
                                .into(imgAddImgOneIV)
                            Glide.with(mActivity).load(mModel.data.backImage )
                                .placeholder(R.drawable.ic_up_doc)
                                .error(R.drawable.ic_up_doc)
                                .into(imgAddImgTwoIV)

                            editFirstNameET.text = mModel.data.firstName
                            editLastNameET.text = mModel.data.lastName
                            editPhoneET.text = mModel.data.phone
                            editCityNameET.text = mModel.data.city
                            txtCountryNameET.text = mModel.data.country
                            editAddressLineOneET.text = mModel.data.line1
                            editAddressLineTwoET.text = mModel.data.line2
                            editPostalCodeET.text = mModel.data.postalCode
                            txtDobET.text = mModel.data.dob
                            editIdNumET.text = mModel.data.idNumber
                            editssnET.text = mModel.data.ssnLast4
                            editAccNoET.text = mModel.data.accountNumber
                            editAHNameET.text = mModel.data.accountHolderName
                            editRoutingNoET.text = mModel.data.routingNumber
                            acc_id = mModel.data.stripeAccountID
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }

    private fun performBackClick() {
        onBackPressed()
    }

}