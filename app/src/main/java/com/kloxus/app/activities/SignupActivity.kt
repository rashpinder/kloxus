package com.kloxus.app.activities

import android.Manifest
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging
import com.kloxus.app.R
import com.kloxus.app.model.LoginModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.text.DateFormat
import java.util.*


class SignupActivity : BaseActivity(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener,
    LocationListener,
    ResultCallback<LocationSettingsResult> {
    /************************
     * Fused Google Location
     */
    val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
    val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2
    protected val REQUEST_CHECK_SETTINGS = 0x1
    protected val KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates"
    protected val KEY_LOCATION = "location"
    protected val KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string"
    var mLatitude = 0.0
    var mLongitude = 0.0
    protected var mGoogleApiClient: GoogleApiClient? = null
    protected var mLocationRequest: LocationRequest? = null
    protected var mLocationSettingsRequest: LocationSettingsRequest? = null
    protected var mCurrentLocation: Location? = null
    protected var mRequestingLocationUpdates: Boolean? = null
    protected var mLastUpdateTime: String? = null
    private val mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION
    private val mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION

    @BindView(R.id.editEmailET)
    lateinit var editEmailET: EditText

    @BindView(R.id.editFirstNameET)
    lateinit var editFirstNameET: EditText

    @BindView(R.id.editLastNameET)
    lateinit var editLastNameET: EditText

    @BindView(R.id.editPasswordET)
    lateinit var editPasswordET: EditText

    @BindView(R.id.txtSignUpTV)
    lateinit var txtSignUpTV: TextView


    @BindView(R.id.txtAlreadyHaveAccountTV)
    lateinit var txtAlreadyHaveAccountTV: TextView
    var strDeviceToken: String? = null


    @BindView(R.id.emailRL)
    lateinit var emailRL: RelativeLayout

    @BindView(R.id.passRL)
    lateinit var passRL: RelativeLayout

    @BindView(R.id.FirstNameRL)
    lateinit var FirstNameRL: RelativeLayout

    @BindView(R.id.LastNameRL)
    lateinit var LastNameRL: RelativeLayout

    @BindView(R.id.txtTermsTV)
    lateinit var txtTermsTV: TextView

    @BindView(R.id.imgTermsIV)
    lateinit var imgTermsIV: ImageView

    @BindView(R.id.termsRL)
    lateinit var termsRL: RelativeLayout

    //Firebase
    private var mFirebaseAuth: FirebaseAuth? = null
    var terms = true
    var checked = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /*************
         * Fused Location
         */
        mRequestingLocationUpdates = false
        mLastUpdateTime = ""
        /*Update values using data stored in the Bundle.*/
        /*Update values using data stored in the Bundle.*/
        updateValuesFromBundle(savedInstanceState)
        buildGoogleApiClient()
        FirebaseApp.initializeApp(this)
        createLocationRequest()
        buildLocationSettingsRequest()
        if (checkPermission()) {
            checkLocationSettings()
        } else {
            requestPermission()
        }
        updateValuesFromBundle(savedInstanceState)
        setContentView(R.layout.activity_signup)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        FirebaseApp.initializeApp(this)
        mFirebaseAuth = FirebaseAuth.getInstance()

        getDeviceToken()
        editTextSignupSelector(editEmailET, emailRL, "")
        editTextSignupSelector(editPasswordET, passRL, "")
        editTextSignupSelector(editFirstNameET, FirstNameRL, "")
        editTextSignupSelector(editLastNameET, LastNameRL, "")
        customTextView(txtTermsTV)
    }


    private fun customTextView(view: TextView) {
        val spanTxt = SpannableStringBuilder(
            "I agree with "
        )
        spanTxt.append("Terms & Conditions")
        spanTxt.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {
                val intent = Intent()
                intent.action = Intent.ACTION_VIEW
                intent.addCategory(Intent.CATEGORY_BROWSABLE)
                intent.data = Uri.parse(TERMS_WEB_LINK)
                startActivity(intent)
            }
        }, spanTxt.length - "Terms & Conditions".length, spanTxt.length, 0)
        view.movementMethod = LinkMovementMethod.getInstance()
        view.setText(spanTxt, TextView.BufferType.SPANNABLE)
    }


    private fun getDeviceToken() {

        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                strDeviceToken = task.result!!

                Log.e(TAG, "**Push Token**$strDeviceToken")
                // Log and toast
                Log.d(TAG, "Token********   $strDeviceToken")
            })

    }

    @OnClick(
        R.id.txtAlreadyHaveAccountTV,
        R.id.txtSignUpTV,
        R.id.termsRL,
        R.id.txtTermsTV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.txtAlreadyHaveAccountTV -> performLoginClick()
            R.id.txtSignUpTV -> performSignUpClick()
            R.id.termsRL -> performTermsClick()
            R.id.txtTermsTV -> performTermsClick()

        }
    }


    private fun performTermsClick() {
        if (terms) {
            imgTermsIV.setImageResource(R.drawable.ic_check)
            terms = false
            checked = true
        } else {
            imgTermsIV.setImageResource(R.drawable.ic_uncheck)
            terms = true
            checked = false
        }
        return
    }

    private fun performLoginClick() {
        val i = Intent(mActivity, LoginActivity::class.java)
        startActivity(i)
        finish()
    }


    private fun performSignUpClick() {
        if (isValidate())
            if (isNetworkAvailable(mActivity))
                executeSignUpRequest()
            else
                showToast(mActivity, getString(R.string.internet_connection_error))
    }

    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["firstName"] = editFirstNameET.text.toString().trim { it <= ' ' }
        mMap["lastName"] = editLastNameET.text.toString().trim { it <= ' ' }
        mMap["email"] = editEmailET.text.toString().trim { it <= ' ' }
        mMap["password"] = editPasswordET.text.toString().trim { it <= ' ' }
        mMap["latitude"] = mLatitude.toString()
        mMap["longitude"] = mLongitude.toString()
        mMap["deviceToken"] = strDeviceToken
        mMap["deviceType"] = "2"
        mMap["timeZone"] =  TimeZone.getDefault().id
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeSignUpRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.signUpRequest(mParams())?.enqueue(object :
            Callback<LoginModel> {
            override fun onFailure(call: Call<LoginModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<LoginModel>, response: Response<LoginModel>) {
                dismissProgressDialog()
                val mModel: LoginModel? = response.body()
                if (mModel != null) {
                    if (mModel.status.equals(1)) {
                        showToast(mActivity, mModel.message)
//                        KloxusPrefrences().writeBoolean(mActivity, IS_LOGIN, true)
                        KloxusPrefrences().writeString(mActivity, USER_ID, mModel.data.userID)
                        KloxusPrefrences().writeString(
                            mActivity,
                            FIRST_NAME,
                            mModel.data.firstName
                        )
                        KloxusPrefrences().writeString(mActivity, LAST_NAME, mModel.data.lastName)
                        KloxusPrefrences().writeString(mActivity, USER_EMAIL, mModel.data.email)
                        KloxusPrefrences().writeString(mActivity, IS_ADMIN, mModel.data.isAdmin)
                        KloxusPrefrences().writeString(
                            mActivity,
                            PROFILE_IMAGE,
                            mModel.data.profileImage
                        )
                        KloxusPrefrences().writeString(mActivity, PHONE, mModel.data.phoneNo)
                        KloxusPrefrences().writeString(
                            mActivity,
                            CUSTOMER_ID,
                            mModel.data.customerID
                        )
                        KloxusPrefrences().writeString(mActivity, VERIFIED, mModel.data.verified)
                        KloxusPrefrences().writeString(
                            mActivity,
                            VERIFICATION_CODE,
                            mModel.data.verificationCode
                        )
                        KloxusPrefrences().writeString(
                            mActivity,
                            FB_TOKEN,
                            mModel.data.facebookToken
                        )
                        KloxusPrefrences().writeString(mActivity, GOOGLE_ID, mModel.data.googleID)
                        KloxusPrefrences().writeString(mActivity, CREATED, mModel.data.created)
                        KloxusPrefrences().writeString(
                            mActivity,
                            AUTH_TOKEN,
                            mModel.data.authToken
                        )
                        val intent = Intent(mActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        finish()
                    } else {
                        showAlertDialog(mActivity, mModel.message)
                    }
                }
            }
        })
    }

    /*
     * Set up validations for Sign In fields
     * */
    private fun isValidate(): Boolean {
        var flag = true
        if (editFirstNameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_first_name))
            flag = false
        } else if (editLastNameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_last_name))
            flag = false
        } else if (editEmailET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
            flag = false
        } else if (!isValidEmaillId(editEmailET.text.toString().trim { it <= ' ' })) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (editPasswordET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_password))
            flag = false
        } else if (editPasswordET.text.toString().length < 6) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password_length))
            flag = false
        } else if (!checked) {
            showAlertDialog(mActivity, getString(R.string.please_select_terms))
            flag = false
        }
        return flag
    }


    /******************
     * Fursed Google Location
     */
    private fun updateValuesFromBundle(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet()
                    .contains(KEY_REQUESTING_LOCATION_UPDATES)
            ) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                    KEY_REQUESTING_LOCATION_UPDATES
                )
            }
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION)
            }
            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime =
                    savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING)
            }
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the `#addApi` method to request the
     * LocationServices API.
     */
    @Synchronized
    protected fun buildGoogleApiClient() {
        Log.i("", "Building GoogleApiClient")
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
    }

    protected fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    /**
     * Uses a [LocationSettingsRequest.Builder] to build
     * a [LocationSettingsRequest] that is used for checking
     * if a device has the needed location settings.
     */
    protected fun buildLocationSettingsRequest() {
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest).setAlwaysShow(true)
        mLocationSettingsRequest = builder.build()
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * [SettingsApi.checkLocationSettings] method, with the results provided through a `PendingResult`.
     */
    fun checkLocationSettings() {
        val result = LocationServices.SettingsApi.checkLocationSettings(
            mGoogleApiClient,
            mLocationSettingsRequest
        )
        result.setResultCallback(this)
    }

    /**
     * The callback invoked when
     * [SettingsApi.checkLocationSettings] is called. Examines the
     * [LocationSettingsResult] object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    override fun onResult(locationSettingsResult: LocationSettingsResult) {
        val status = locationSettingsResult.status
        when (status.statusCode) {
            LocationSettingsStatusCodes.SUCCESS -> {
                Log.i("", "All location settings are satisfied.")
                startLocationUpdates()
            }
            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                Log.i(
                    "", "Location settings are not satisfied. Show the user a dialog to" +
                            "upgrade location settings "
                )
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS)
                } catch (e: IntentSender.SendIntentException) {
                    Log.i("", "PendingIntent unable to execute request.")
                }
            }
            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(
                "", "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created."
            )
            else -> {
            }
        }
    }


    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected fun startLocationUpdates() {
        try {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
            ).setResultCallback { mRequestingLocationUpdates = true }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected fun stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
            mGoogleApiClient,
            this
        ).setResultCallback { mRequestingLocationUpdates = false }
    }


    override fun onStart() {
        super.onStart()
        Log.e(TAG, "onStart")
        if (mGoogleApiClient != null) mGoogleApiClient!!.connect()
    }

    override fun onResume() {
        super.onResume()
        Log.e(TAG, "onResume")
        if (mGoogleApiClient!!.isConnected && mRequestingLocationUpdates!!) {
            startLocationUpdates()
        }
    }


    override fun onPause() {
        super.onPause()
        Log.e(TAG, "onPause")
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient!!.isConnected) {
            stopLocationUpdates()
        }
        //  dismissProgressDialog();
    }

    override fun onStop() {
        super.onStop()
        Log.e(TAG, "onStop")
        mGoogleApiClient!!.disconnect()
        dismissProgressDialog()
    }

    override fun onRestart() {
        super.onRestart()
        Log.e(TAG, "onRestart")
    }


    override fun onConnected(connectionHint: Bundle?) {
        Log.i("", "Connected to GoogleApiClient")
        if (mCurrentLocation == null) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
            mLastUpdateTime = DateFormat.getTimeInstance().format(Date())
        }
    }

    /**
     * Callback that fires when the location changes.
     */
    override fun onLocationChanged(location: Location) {
        mCurrentLocation = location
        mLastUpdateTime = DateFormat.getTimeInstance().format(Date())
        mLatitude = mCurrentLocation!!.latitude
        mLongitude = mCurrentLocation!!.longitude
        Log.e(TAG, "*********sign_LATITUDE********$mLatitude")
        Log.e(TAG, "*********LONGITUDE********$mLongitude")
        try {
            mLatitude = mCurrentLocation!!.latitude
            mLongitude = mCurrentLocation!!.longitude

            var geocoder: Geocoder? = null
            var addresses: List<Address>? = null
            geocoder = Geocoder(this, Locale.getDefault())
            try {
                addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            if (addresses != null && addresses.size > 0) {
//            val locality = addresses[0].locality
                val state = addresses[0].adminArea

                val city = addresses[0].locality
                KloxusPrefrences().writeString(mActivity, SELECTED_LOCATION, city)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onConnectionSuspended(cause: Int) {
        Log.i("", "Connection suspended")
    }

    override fun onConnectionFailed(result: ConnectionResult) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i("", "Connection failed: ConnectionResult.getErrorCode() = " + result.errorCode)
    }

    /**
     * Stores activity data in the Bundle.
     */
    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        savedInstanceState.putBoolean(
            KEY_REQUESTING_LOCATION_UPDATES,
            mRequestingLocationUpdates!!
        )
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation)
        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime)
        super.onSaveInstanceState(savedInstanceState)
    }
    /*****************************************/

    /** */
    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) ACCESS_FINE_LOCATION
     * 2) ACCESS_COARSE_LOCATION
     */
    fun checkPermission(): Boolean {
        val mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation)
        val mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation)
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED
    }

    fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(mAccessFineLocation, mAccessCourseLocation),
            REQUEST_PERMISSION_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_PERMISSION_CODE -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    checkLocationSettings()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    requestPermission()
                }
                return
            }
        }
    }
}