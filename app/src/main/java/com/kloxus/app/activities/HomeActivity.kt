package com.kloxus.app.activities

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.DrawableRes
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.GravityCompat
import androidx.core.view.isVisible
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.navigation.NavigationView
import com.google.firebase.FirebaseApp
import com.iarcuschin.simpleratingbar.SimpleRatingBar
import com.kloxus.app.R
import com.kloxus.app.adapters.CustomAdapter
import com.kloxus.app.model.*
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.*
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


class HomeActivity : FragmentActivity(), OnMapReadyCallback, LocationListener,
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
    ResultCallback<LocationSettingsResult> {

    var AUTOCOMPLETE_REQUEST_CODE = 5
    var progressDialog: Dialog? = null
    var not_first_time_showing_info_window = false
    var mLat = 0.0
    var mLong = 0.0

    @BindView(R.id.imgMenuRL)
    lateinit var imgMenuRL: RelativeLayout

    @BindView(R.id.nav_bar)
    lateinit var nav_bar: NavigationView
    lateinit var homeLL: LinearLayout
    lateinit var referLL: LinearLayout
    lateinit var settingLL: LinearLayout
    lateinit var paymentLL: LinearLayout
    lateinit var bookingLL: LinearLayout
    lateinit var myToiletLL: LinearLayout
    lateinit var savedToiletLL: LinearLayout
    lateinit var msgLL: LinearLayout
    lateinit var txtLoginTV: TextView
    lateinit var referTV: TextView
    lateinit var bottomSheetDialog: BottomSheetDialog
    lateinit var profileRL: RelativeLayout
    lateinit var userNameTV: TextView
    lateinit var txtMailTV: TextView
    lateinit var badge: TextView
    lateinit var messagesNoRL: RelativeLayout
    lateinit var imgUserProfileIV: ImageView
    lateinit var homeTV: TextView
    lateinit var savedToiletTV: TextView
    lateinit var messagesTV: TextView
    lateinit var myToiletTV: TextView
    lateinit var bookingTV: TextView
    lateinit var paymentTV: TextView
    lateinit var settingTV: TextView

    @BindView(R.id.drawer_layout)
    lateinit var drawer_layout: DrawerLayout

    @BindView(R.id.notificationRL)
    lateinit var notificationRL: RelativeLayout

    @BindView(R.id.aroundRL)
    lateinit var aroundRL: RelativeLayout

    @BindView(R.id.seeAllRL)
    lateinit var seeAllRL: RelativeLayout

    @BindView(R.id.mainRL)
    lateinit var mainRL: RelativeLayout

    @BindView(R.id.gapRL)
    lateinit var gapRL: RelativeLayout

    @BindView(R.id.searchRL)
    lateinit var searchRL: RelativeLayout

    @BindView(R.id.toiletLocLL)
    lateinit var toiletLocLL: LinearLayout

    @BindView(R.id.imgCancelIV)
    lateinit var imgCancelIV: ImageView

    @BindView(R.id.editSearchET)
    lateinit var editSearchET: TextView

    @BindView(R.id.bottomsheet)
    lateinit var bottomsheet: View

    @BindView(R.id.bottomsheetRV)
    lateinit var bottomsheetRV: RecyclerView

    @BindView(R.id.imgBadgeIV)
    lateinit var imgBadgeIV: ImageView

    var latLng: LatLng? = null
    private var mMap: GoogleMap? = null
    var mLastLocation: Location? = null
    var mCurrLocationMarker: Marker? = null
    var mMarker: Marker? = null
    var mGoogleApiClient: GoogleApiClient? = null
    var mLocationRequest: LocationRequest? = null
    private var mLatlngg: LatLng? = null
    var behavior: BottomSheetBehavior<*>? = null
    private var mAdapter: CustomAdapter? = null
    var mapView: View? = null
    var mHomeArrayList: ArrayList<HomeData>? = ArrayList()
    var mModel: HomeModel? = null
    var mToiletDetailModel: ToiletDetailModel? = null
    var mLatitude: String = ""
    var mLatt: String = ""
    var mLongg: String = ""
    var mHomePath: String = ""
    var mLongitude: String = ""
    var price: String = ""
    var rating: String = ""
    var distance: String = ""
    var toilet_img: String = ""
    var toilet_title: String = ""
    var mToiletID: String = ""
    var bitmapdraw: BitmapDrawable? = null
    var height = 80
    var width = 80
    var nearbyToiletsLatitude = 0.0
    var nearbyToiletsLongitude = 0.0
    var nearToiletsLatitude = 0.0
    var nearToiletsLongitude = 0.0
    var mDestinationLat = 0.0
    var mDestinationLong = 0.0
    var mDoubleLatitude = 0.0
    var mDoubleLongitude = 0.0
    var mRecentItemsList: List<*>? = null
    var mapFragment: SupportMapFragment? = null
    private val mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION
    private val mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION
    val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
    protected val REQUEST_CHECK_SETTINGS = 0x1
    protected val KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates"
    protected val KEY_LOCATION = "location"
    protected val KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string"
    protected var mLocationSettingsRequest: LocationSettingsRequest? = null
    protected var mCurrentLocation: Location? = null
    protected var mRequestingLocationUpdates: Boolean? = null
    protected var mLastUpdateTime: String? = null
    var city: String? = null
    var address: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mRequestingLocationUpdates = false
        mLastUpdateTime = ""
        buildGoogleApiClient()
        FirebaseApp.initializeApp(this)
        createLocationRequest()
        buildLocationSettingsRequest()
        setContentView(R.layout.activity_home)
        ButterKnife.bind(this)
        setStatusBar(this)
        getIntentData()
        toiletLocLL.isVisible = false
//        KloxusPrefrences().writeBoolean(this, KloxusPrefrences().HOME_SEL, true)
//        homeTV.setTextColor(resources.getColor(R.color.black))
//        savedToiletTV.setTextColor(resources.getColor(R.color.colorGreyTextMenu))
//        myToiletTV.setTextColor(resources.getColor(R.color.colorGreyTextMenu))
//        bookingTV.setTextColor(resources.getColor(R.color.colorGreyTextMenu))
//        paymentTV.setTextColor(resources.getColor(R.color.colorGreyTextMenu))
//        settingTV.setTextColor(resources.getColor(R.color.colorGreyTextMenu))
//        getProfileDetail()
        mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapView = mapFragment?.view
        mapFragment!!.getMapAsync(this)
        //Set Navigation Drawer
        setNavigationViewListener()
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        behavior = BottomSheetBehavior.from(bottomsheet)
        (behavior as BottomSheetBehavior<View>).state = BottomSheetBehavior.STATE_EXPANDED
//        (behavior as BottomSheetBehavior<View>).isDraggable = false
        (behavior as BottomSheetBehavior<View>).setBottomSheetCallback(object :
            BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                // React to state change
                if (newState == BottomSheetBehavior.STATE_EXPANDED) {

                    (behavior as BottomSheetBehavior<View>).setState(BottomSheetBehavior.STATE_EXPANDED)
                } else if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    (behavior as BottomSheetBehavior<View>).peekHeight = mainRL.height
                    (behavior as BottomSheetBehavior<View>).state =
                        BottomSheetBehavior.STATE_COLLAPSED
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
//                (behavior as BottomSheetBehavior<View>).setPeekHeight(mainRL.getHeight())
//                (behavior as BottomSheetBehavior<View>).setState(BottomSheetBehavior.STATE_COLLAPSED)
                // React to dragging events
            }
        })


//        editSearchET.setOnTouchListener(
//            OnTouchListener { v, event ->
//                Log.i("click", "onMtouch")
////                editSearchET.isFocusable = true
////                editSearchET.requestFocus()
//                if (event.action == MotionEvent.ACTION_DOWN) {
//                    (behavior as BottomSheetBehavior<View>).state =
//                        BottomSheetBehavior.STATE_EXPANDED
//                    SearchItem()
//                }
//                false
//            },
//        )

        /*
       * For places Location google api
       * */
        initializePlaceSdKLocation()
    }

    /*
    * Initailze places location
    *
    * */
    fun initializePlaceSdKLocation() {
        // Initialize the SDK
        Places.initialize(this, getString(R.string.places_api_key))

        // Create a new Places client instance
        val placesClient: PlacesClient = Places.createClient(this)
    }


    private class InfoWindowRefresher(private val markerToRefresh: Marker) :
        Callback<Any?>, com.squareup.picasso.Callback {
        override fun onSuccess() {
            markerToRefresh.showInfoWindow()
            if (markerToRefresh != null && markerToRefresh.isInfoWindowShown) {
                Log.i("", "conditions met, redrawing window")
//                markerToRefresh.setTag(new Boolean("True"));
                markerToRefresh.showInfoWindow()
            }
        }

        override fun onError(e: java.lang.Exception?) {

        }
//        override fun onError() {}
        override fun onResponse(call: Call<Any?>, response: Response<Any?>) {

        }

        override fun onFailure(call: Call<Any?>, t: Throwable) {

        }

    }

    override fun onResume() {
        super.onResume()
        KloxusPrefrences().writeBoolean(this, KloxusPrefrences().HOME_SEL, true)
        homeTV.setTextColor(resources.getColor(R.color.black))
        savedToiletTV.setTextColor(resources.getColor(R.color.colorGreyTextMenu))
        myToiletTV.setTextColor(resources.getColor(R.color.colorGreyTextMenu))
        bookingTV.setTextColor(resources.getColor(R.color.colorGreyTextMenu))
        paymentTV.setTextColor(resources.getColor(R.color.colorGreyTextMenu))
        settingTV.setTextColor(resources.getColor(R.color.colorGreyTextMenu))
        if (KloxusPrefrences().readBoolean(this, IS_LOGIN, false)) {
            getProfileDetail()
            txtLoginTV.isVisible=false
        } else {
            txtLoginTV.isVisible=true
        }

//        mapFragment = supportFragmentManager
//            .findFragmentById(R.id.map) as SupportMapFragment?
//        mapView = mapFragment?.view
//        mapFragment!!.getMapAsync(this)
    }


    private fun getProfileDetail() {
        if (isNetworkAvailable(this))
            executeGetProfileDetailRequest()
        else
            showToast(this, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        return mMap
    }

    private fun executeGetProfileDetailRequest() {
//        showProgressDialog(this)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getProfileDetailRequest(mParam())
            ?.enqueue(object : Callback<EditProfileModel> {
                override fun onFailure(call: Call<EditProfileModel>, t: Throwable) {
//                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<EditProfileModel>,
                    response: Response<EditProfileModel>
                ) {
//                    dismissProgressDialog()
                    val mModel: EditProfileModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            KloxusPrefrences().writeInteger(
                                this@HomeActivity,
                                IS_BANK,
                                mModel.data.isBank
                            )
                            Glide.with(this@HomeActivity).load(mModel.data.profileImage)
                                .placeholder(R.drawable.ic_ph_menu)
                                .error(R.drawable.ic_ph_menu)
                                .into(imgUserProfileIV)

                            txtMailTV.text = mModel.data.email
                            userNameTV.text = mModel.data.firstName + " " + mModel.data.lastName
                            if (mModel.data.totalMessage > 99) {
                                messagesNoRL.isVisible = true
                                badge.text = "99+"
                            } else if (mModel.data.totalMessage > 0) {
                                badge.text = mModel.data.totalMessage.toString()
                                messagesNoRL.isVisible = true
                            } else {
                                messagesNoRL.isVisible = false
                            }
                            imgBadgeIV.isVisible = mModel.data.notificationCount > 0

                            KloxusPrefrences().writeString(
                                this@HomeActivity,
                                FIRST_NAME,
                                mModel.data.firstName
                            )
                            KloxusPrefrences().writeString(
                                this@HomeActivity,
                                LAST_NAME,
                                mModel.data.lastName
                            )
                            KloxusPrefrences().writeString(
                                this@HomeActivity,
                                FIRST_NAME,
                                mModel.data.firstName
                            )
                            KloxusPrefrences().writeString(
                                this@HomeActivity,
                                PROFILE_IMAGE,
                                mModel.data.profileImage
                            )
                            KloxusPrefrences().writeString(
                                this@HomeActivity,
                                USER_EMAIL,
                                mModel.data.email
                            )
                            KloxusPrefrences().writeInteger(
                                this@HomeActivity,
                                IS_STRIPE,
                                mModel.data.isBank
                            )
                            KloxusPrefrences().writeInteger(
                                this@HomeActivity,
                                IS_PAYPAL,
                                mModel.data.isPaypal
                            )
                            KloxusPrefrences().writeString(
                                this@HomeActivity,
                                PASSWORD,
                                mModel.data.password
                            )
                            KloxusPrefrences().writeString(
                                this@HomeActivity,
                                PHONE,
                                mModel.data.phoneNo
                            )
                            KloxusPrefrences().writeString(
                                this@HomeActivity,
                                COUNTRY_CODE,
                                mModel.data.countryCode
                            )

                        } else if (mModel.status.equals(3)) {
                            showAlertDialogFinishHome(this@HomeActivity, mModel.message)
                        } else {
                            showAlertDialog(this@HomeActivity, mModel.message)
                        }
                    }
                }
            })
    }

    private fun SearchItem() {
        // Set the fields to specify which types of place data to
        // return after the user has made a selection.
        val fields: List<Place.Field> = Arrays.asList<Place.Field>(
            Place.Field.ID,
            Place.Field.NAME,
            Place.Field.LAT_LNG,
            Place.Field.ADDRESS
        )
        // Start the autocomplete intent.
        val intent: Intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.OVERLAY, fields
        )
            .build(this)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val place: Place = Autocomplete.getPlaceFromIntent(data!!)
                latLng = place.latLng
                mLat = latLng!!.latitude
                mLong = latLng!!.longitude
                mLatitude = latLng!!.latitude.toString()
                mLongitude = latLng!!.longitude.toString()


                Log.e("tag", "*********SLATITUDE********$mLatitude")
                Log.e("tag", "*********SLONGITUDE********$mLongitude")
                address = place.address
                editSearchET.setText(place.address)
                mMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
                mMap!!.animateCamera(CameraUpdateFactory.zoomTo(14f))
                getHomeData()


            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                val status: Status =
                    Autocomplete.getStatusFromIntent(data!!)
//                Log.e(TAG, "msg"+status.statusMessage)
            } else if (resultCode == RESULT_CANCELED) {
//                showToast(mActivity,"Error")
//                Log.e(TAG, resultCode.toString())
                // The user canceled the operation.
            }
        }
    }


//        editSearchET.addTextChangedListener(object : TextWatcher {
//            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//
//                // TODO Auto-generated method stub
//            }
//
//            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
//
//                // TODO Auto-generated method stub
//            }
//
//            override fun afterTextChanged(s: Editable) {
//
//                // filter your list from your input
//                filter(s.toString())
//                //you can use runnable postDelayed like 500 ms to delay search text
//            }
//        })
//        editSearchET.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
//            if (actionId == EditorInfo.IME_ACTION_DONE) {
//                //do here your stuff f
//                if (mRecentItemsList!!.size == 0) {
//                    showToast(this, "No Result found")
//                } else {
//                    //move map camera
//                    mMap!!.moveCamera(CameraUpdateFactory.newLatLng(mLatlngg))
//                    mMap!!.animateCamera(CameraUpdateFactory.zoomTo(11f))
//                }
//                hideCloseKeyboard(this)
//                return@OnEditorActionListener true
//            }
//            false
//        })

// private fun SearchItem() {
//        editSearchET.addTextChangedListener(object : TextWatcher {
//            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//
//                // TODO Auto-generated method stub
//            }
//
//            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
//
//                // TODO Auto-generated method stub
//            }
//
//            override fun afterTextChanged(s: Editable) {
//
//                // filter your list from your input
//                filter(s.toString())
//                //you can use runnable postDelayed like 500 ms to delay search text
//            }
//        })
//        editSearchET.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
//            if (actionId == EditorInfo.IME_ACTION_DONE) {
//                //do here your stuff f
//                if (mRecentItemsList!!.size == 0) {
//                    showToast(this, "No Result found")
//                } else {
//                    //move map camera
//                    mMap!!.moveCamera(CameraUpdateFactory.newLatLng(mLatlngg))
//                    mMap!!.animateCamera(CameraUpdateFactory.zoomTo(11f))
//                }
//                hideCloseKeyboard(this)
//                return@OnEditorActionListener true
//            }
//            false
//        })
//    }

    /*
     *Hide/Close Keyboard forcefully
     *
     * */
    open fun hideCloseKeyboard(activity: Activity) {
        val imm =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }


//    fun filter(text: String) {
//        mRecentItemsList = ArrayList<HomeData>()
//        for (d in mHomeArrayList!!) {
//            //or use .equal(text) with you want equal match
//            //use .toLowerCase() for better matches
//            if (d.title.toLowerCase().contains(text.toLowerCase()) || d.toiletLocation.toLowerCase()
//                    .contains(text.toLowerCase())
//            ) {
//
//                (mRecentItemsList as ArrayList<HomeData>).add(d)
//                val hs = HashSet<HomeData>()
//
//                hs.addAll(mRecentItemsList as ArrayList<HomeData>) // demoArrayList= name of arrayList from which u want to remove duplicates
//
//
//                (mRecentItemsList as ArrayList<HomeData>).clear()
//                (mRecentItemsList as ArrayList<HomeData>).addAll(hs)
//                mLatt = d.lat
//                mLongg = d.log
//                mLatlngg = LatLng(mLatt.toDouble(), mLongg.toDouble())
//            }
//        }
//        //update recyclerview
//        if (mHomeArrayList!!.size > 0) {
//            mAdapter!!.updateWithSearchData(mRecentItemsList as ArrayList<HomeData>?)
//        }
//    }

    private fun getIntentData() {
        if (intent != null) {
            mHomePath = intent.getStringExtra(HOME_PATH).toString()
            mToiletID = intent.getStringExtra("toilet_id").toString()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap!!.uiSettings.isCompassEnabled = false
        // Set a listener for info window events.

        if (mapView != null &&
            mapView!!.findViewById<View?>("1".toInt()) != null
        ) {

            // Get the button view
            val locationButton =
                (mapView!!.findViewById<View>("1".toInt()).parent as View).findViewById<View>("2".toInt())
            // and next place it, on bottom right (as Google Maps app)
            val layoutParams = locationButton.layoutParams as RelativeLayout.LayoutParams
            // position on center
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
            layoutParams.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE)
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE)
            layoutParams.setMargins(0, 0, 30, 30)
        }
        //Custom Theme
        val style = MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style_dark)
        mMap!!.setMapStyle(style)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
                == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
                == PackageManager.PERMISSION_GRANTED
            ) {
                checkLocationSettings()
//                buildGoogleApiClient()
                mMap!!.isMyLocationEnabled = true

            } else {
                requestPermission()
            }
        } else {
//            buildGoogleApiClient()
            mMap!!.isMyLocationEnabled = true
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(mAccessFineLocation, mAccessCourseLocation),
            REQUEST_PERMISSION_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_PERMISSION_CODE -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    checkLocationSettings()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    requestPermission()
                }
                return
            }
        }
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * [SettingsApi.checkLocationSettings] method, with the results provided through a `PendingResult`.
     */
    fun checkLocationSettings() {
        val result = LocationServices.SettingsApi.checkLocationSettings(
            mGoogleApiClient,
            mLocationSettingsRequest
        )
        result.setResultCallback(this)
    }

    /**
     * The callback invoked when
     * [SettingsApi.checkLocationSettings] is called. Examines the
     * [LocationSettingsResult] object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    override fun onResult(locationSettingsResult: LocationSettingsResult) {
        val status = locationSettingsResult.status
        when (status.statusCode) {
            LocationSettingsStatusCodes.SUCCESS -> {
                Log.i("", "All location settings are satisfied.")
                startLocationUpdates()
            }
            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                Log.i(
                    "", "Location settings are not satisfied. Show the user a dialog to" +
                            "upgrade location settings "
                )
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(this, REQUEST_CHECK_SETTINGS)
                } catch (e: IntentSender.SendIntentException) {
                    Log.i("", "PendingIntent unable to execute request.")
                }
            }
            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(
                "", "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created."
            )
            else -> {
            }
        }
    }


    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected fun startLocationUpdates() {
        try {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
            ).setResultCallback { mRequestingLocationUpdates = true }
        } catch (e: Exception) {
            e.printStackTrace()
//            Log.e(TAG, "msg" + e.printStackTrace())
        }
    }

    /******************
     * Fursed Google Location
     */
    private fun updateValuesFromBundle(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet()
                    .contains(KEY_REQUESTING_LOCATION_UPDATES)
            ) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                    KEY_REQUESTING_LOCATION_UPDATES
                )
            }
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION)
            }
            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime =
                    savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING)
            }
        }
    }

    protected fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    /**
     * Uses a [LocationSettingsRequest.Builder] to build
     * a [LocationSettingsRequest] that is used for checking
     * if a device has the needed location settings.
     */
    protected fun buildLocationSettingsRequest() {
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest).setAlwaysShow(true)
        mLocationSettingsRequest = builder.build()
    }

    private fun getHomeData() {
        if (mHomeArrayList != null) {
            mHomeArrayList!!.clear()
        }
        if (isNetworkAvailable(this))
            executeHomeDataRequest()
        else
            showToast(this, getString(R.string.internet_connection_error))
    }

    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
//        mMap["latitude"] = KloxusPrefrences().readString(this, CURR_LAT, null)
//        mMap["longitude"] = KloxusPrefrences().readString(this, CURR_LNG, null)
        mMap["latitude"] = mLatitude
        mMap["longitude"] = mLongitude
        mMap["search"] = ""
        mMap["perPage"] = ""
        mMap["pageNo"] = ""
        mMap["timeZone"] = TimeZone.getDefault().id
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    private fun executeHomeDataRequest() {
        showProgressDialog(this)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.homeRequest(mParams())?.enqueue(object :
            Callback<HomeModel> {
            override fun onFailure(call: Call<HomeModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<HomeModel>, response: Response<HomeModel>) {
                dismissProgressDialog()
                mModel = response.body()

                if (mModel != null) {
                    if (mModel!!.status.equals(1)) {
                        val mModel: HomeModel? = response.body()
//                        mHomeArrayList!!.addAll(mModel!!.data)
                        mHomeArrayList= mModel!!.data
                        mAdapter = mModel.data.let {
                            CustomAdapter(
                                this@HomeActivity,
                                it
                            )
                        }
                        for (i in mModel.data.indices) {
                            if (!mModel.data.get(i).lat
                                    .equals("") && !mModel.data.get(i).log
                                    .equals("")
                            ) {
                                nearbyToiletsLatitude =
                                    mModel.data.get(i).lat.toDouble()
                                nearbyToiletsLongitude =
                                    mModel.data.get(i).log.toDouble()
                            }


                            mMarker = mMap?.let {
                                CreateToiletMarker(
                                    it,
                                    mModel.data.get(i).lat.toDouble(),
                                    mModel.data.get(i).log.toDouble(),
                                    mModel.data.get(i).title,
                                    mModel.data.get(i).rating,
                                    mModel.data.get(i).price,
                                    mModel.data.get(i).distance,
                                    mModel.data.get(i).toiletImage,
                                    )
                            }

                            mMap!!.setOnMarkerClickListener(OnMarkerClickListener { mark ->
                                mark.showInfoWindow()
                                val handler = Handler()
                                handler.postDelayed(Runnable {
                                    mark.showInfoWindow()
                                }, 100)
                                true
                            }
                            )


                            mMap!!.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
                                // Use default InfoWindow frame
                                override fun getInfoWindow(arg0: Marker): View? {
                                    return null
                                }

                                // Defines the contents of the InfoWindow
                                override fun getInfoContents(arg0: Marker): View {
//                                    aroundRL.isVisible = false
//                                    gapRL.isVisible = false
                                    (behavior as BottomSheetBehavior<View>).peekHeight =
                                        mainRL.height
                                    (behavior as BottomSheetBehavior<View>).state =
                                        BottomSheetBehavior.STATE_COLLAPSED
                                    // Getting view from the layout file info_window_layout
                                    val v: View = layoutInflater.inflate(
                                        R.layout.item_toilet_loc,
                                        null
                                    )

                                    // Getting the position from the marker
                                    val latLng = arg0.position
                                    nearToiletsLatitude = latLng.latitude
                                    nearToiletsLongitude = latLng.longitude

                                    Log.e("position", "jzbxjzhbx" + latLng)

                                    // Getting reference to the TextView to set latitude
                                    val imgToiletWindowIV =
                                        v.findViewById<View>(R.id.imgToiletWindowIV) as CircleImageView

                                    // Getting reference to the TextView to set longitude
                                    val txtTitleTV =
                                        v.findViewById<View>(R.id.txtTitleTV) as TextView
                                    val txtkmTV = v.findViewById<View>(R.id.txtkmTV) as TextView
                                    val txtRatingTV =
                                        v.findViewById<View>(R.id.txtRatingTV) as TextView
                                    val txtPriceTV =
                                        v.findViewById<View>(R.id.txtPriceTV) as TextView
                                    val ratingWindowBar =
                                        v.findViewById<View>(R.id.ratingWindowBar) as SimpleRatingBar
                                    for (i in mModel.data.indices) {
                                        if (nearToiletsLatitude == (mModel.data.get(i).lat.toDouble()) && nearToiletsLongitude == (mModel.data.get(
                                                i
                                            ).log.toDouble())
                                        ) {

                                            if (not_first_time_showing_info_window) {
                                                if (mModel.data.get(i).toiletImage.isEmpty()) {
                                                    imgToiletWindowIV.setImageResource(R.drawable.ic_bg_sp)
                                                } else {
                                                    Picasso.get()
                                                        .load(mModel.data.get(i).toiletImage)
                                                        .error(R.drawable.ic_bg_sp)
                                                        .placeholder(R.drawable.ic_bg_sp)
                                                        .into(imgToiletWindowIV)
                                                }
                                            } else {
                                                not_first_time_showing_info_window = true
                                                if (mModel.data.get(i).toiletImage.isEmpty()) {
                                                    imgToiletWindowIV.setImageResource(R.drawable.ic_bg_sp)
                                                } else {
                                                    Picasso.get()
                                                        .load(mModel.data.get(i).toiletImage)
                                                        .error(R.drawable.ic_bg_sp)
                                                        .placeholder(R.drawable.ic_bg_sp).into(
                                                            imgToiletWindowIV, InfoWindowRefresher(
                                                                arg0
                                                            )
                                                        )
                                                }
                                            }
                                            txtTitleTV.text = mModel.data.get(i).title

                                            // Setting the longitude
                                            txtkmTV.text = mModel.data.get(i).distance
                                            txtPriceTV.text = "$" + mModel.data.get(i).price
                                            txtRatingTV.text = mModel.data.get(i).rating
                                            ratingWindowBar.rating =
                                                mModel.data.get(i).rating.toFloat()
                                            ratingWindowBar.starBorderWidth = 1.8F
//                                            ratingWindowBar.setRating(mModel.data.get(i).rating.toFloat())

                                        }
                                    }
//                                    mMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
//                                    mMap!!.animateCamera(CameraUpdateFactory.zoomTo(11f))

                                    // Returning the view containing InfoWindow contents
                                    return v
                                }
                            })

                            mMap!!.setOnInfoWindowClickListener {

                                for (i in mModel.data.indices) {
                                    if (nearToiletsLatitude == (mModel.data.get(i).lat.toDouble()) && nearToiletsLongitude == (mModel.data.get(
                                            i
                                        ).log.toDouble())
                                    ) {
                                        val intent = Intent(
                                            this@HomeActivity,
                                            ToiletDetailActivity::class.java
                                        )
                                        intent.putExtra("toilet_id", mModel.data.get(i).toiletID)
                                        intent.putExtra("value", "home")
                                        this@HomeActivity.startActivity(intent)
                                    }
                                }
                            }
                        }
                        bottomsheetRV.isVisible = true
                        aroundRL.isVisible = true
                        bottomsheetRV.setHasFixedSize(true)
                        bottomsheetRV.layoutManager = LinearLayoutManager(this@HomeActivity)
                        bottomsheetRV.adapter = mAdapter
//                        mAdapter!!.notifyDataSetChanged()

                    }
//                    else if (mModel!!.status.equals(3)) {
//
//                        val preferences: SharedPreferences = KloxusPrefrences().getPreferences(
//                            Objects.requireNonNull(this@HomeActivity)
//                        )
//                        val editor = preferences.edit()
//                        editor.clear()
//                        editor.apply()
//                        this@HomeActivity.onBackPressed()
//                        showAlertDialogFinishHome(this@HomeActivity, mModel!!.message)
//
//                    }
                    else {
//                        mHomeArrayList!!.clear()
                        bottomsheetRV.isVisible = false
//                        bottomsheetRV.setHasFixedSize(true)
//                        bottomsheetRV.layoutManager = LinearLayoutManager(this@HomeActivity)
//                        bottomsheetRV.adapter = mAdapter
//                        mAdapter!!.notifyDataSetChanged()


                        aroundRL.isVisible = false
                        gapRL.isVisible = false
                        drawer_layout.closeDrawer(GravityCompat.START)
//                        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {

//                        }
//                    else {
                        showAlertDialog(this@HomeActivity, mModel!!.message)


//                        }
                    }

                }
            }
        })
    }

    protected fun CreateToiletMarker(
        googleMap: GoogleMap,
        latitude: Double,
        longitude: Double,
        title: String,
        rating: String,
        price: String,
        distance: String,
        toilet_img: String
    ): Marker? {
        this.price = price
        this.distance = distance
        this.rating = rating
        this.toilet_title = title
        this.toilet_img = toilet_img
        return googleMap.addMarker(
            MarkerOptions()
                .position(LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .icon(getBitmapFromVector(this, R.drawable.ic_marker))

        )


    }


    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API).build()
        with(mGoogleApiClient) { this?.connect() }
    }

    override fun onConnected(bundle: Bundle?) {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = 1000
        mLocationRequest!!.fastestInterval = 1000
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
            )
        }
    }

    override fun onConnectionSuspended(i: Int) {}

    override fun onLocationChanged(location: Location) {
        mLastLocation = location
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker!!.remove()
        }
        //Place current location marker
        val latLng = LatLng(location.latitude, location.longitude)
        Log.e("latlng", "**latlng**$latLng")
        mLatitude = location.latitude.toString()
        mLongitude = location.longitude.toString()
        mDoubleLatitude = location.latitude
        mDoubleLongitude = location.longitude
        Log.e("latlng", "**latlng**$mLatitude")
        Log.e("latlng", "**latlng**$mLongitude")
        KloxusPrefrences().writeString(this, CURR_LAT, mLatitude)
        KloxusPrefrences().writeString(this, CURR_LNG, mLongitude)

//        //move map camera
//        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
//        mMap!!.animateCamera(CameraUpdateFactory.zoomTo(11f))

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
        }

        getHomeData()
        //move map camera
        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        mMap!!.animateCamera(CameraUpdateFactory.zoomTo(14f))


    }

    override fun onPause() {
        super.onPause()
        if (mGoogleApiClient!!.isConnected) {
            //stop location updates
            if (mGoogleApiClient != null) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
            }
        }
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult?) {}

    fun getBitmapFromVector(
        context: Context,
        @DrawableRes vectorResourceId: Int
    ): BitmapDescriptor? {
        val vectorDrawable = ResourcesCompat.getDrawable(
            context.resources, vectorResourceId, null
        )
        if (vectorDrawable == null) {
//            Log.e(TAG, "Requested vector resource was not found")
            return BitmapDescriptorFactory.defaultMarker()
        }
        val bitmap = Bitmap.createBitmap(
            80,
            100, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
//        DrawableCompat.setTint(vectorDrawable)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    private fun setNavigationViewListener() {
        val navigationView: NavigationView = findViewById(R.id.nav_bar)
        val header = navigationView.getHeaderView(0)
        homeLL = header.findViewById(R.id.homeLL)
        txtLoginTV = header.findViewById(R.id.txtLoginTV)
        savedToiletLL = header.findViewById(R.id.savedToiletLL)
        myToiletLL = header.findViewById(R.id.myToiletLL)
        bookingLL = header.findViewById(R.id.bookingLL)
        paymentLL = header.findViewById(R.id.paymentLL)
        settingLL = header.findViewById(R.id.settingLL)
        referLL = header.findViewById(R.id.referLL)
        msgLL = header.findViewById(R.id.msgLL)
        profileRL = header.findViewById(R.id.profileRL)
        userNameTV = header.findViewById(R.id.userNameTV)
        txtMailTV = header.findViewById(R.id.txtMailTV)
        badge = header.findViewById(R.id.badge)
        messagesNoRL = header.findViewById(R.id.messagesNoRL)
        imgUserProfileIV = header.findViewById(R.id.imgUserProfileIV)
        homeTV = header.findViewById(R.id.homeTV)
        messagesTV = header.findViewById(R.id.messagesTV)
        savedToiletTV = header.findViewById(R.id.savedToiletTV)
        myToiletTV = header.findViewById(R.id.myToiletTV)
        bookingTV = header.findViewById(R.id.bookingTV)
        paymentTV = header.findViewById(R.id.paymentTV)
        settingTV = header.findViewById(R.id.settingTV)
        referTV = header.findViewById(R.id.referTV)

        homeLL.setOnClickListener(View.OnClickListener {
                getHomeData()
                setLeftMenuSelection(1)
//            mapView = mapFragment?.getView()
//            mapFragment!!.getMapAsync(this)

        })

        referLL.setOnClickListener(View.OnClickListener {
                setLeftMenuSelection(9)

        })
        settingLL.setOnClickListener(View.OnClickListener {
            if (KloxusPrefrences().readBoolean(this, IS_LOGIN, false)) {
            setLeftMenuSelection(2)
            val i = Intent(this, SettingActivity::class.java)
            startActivity(i)}
            else{
                showLoginAlertDialog(this,getString(R.string.yiu_need_to_login))
            }
        })
        myToiletLL.setOnClickListener(View.OnClickListener {
            if (KloxusPrefrences().readBoolean(this, IS_LOGIN, false)) {
            setLeftMenuSelection(3)
            val i = Intent(this, AllToiletsActivity::class.java)
            startActivity(i)}
            else{
                showLoginAlertDialog(this,getString(R.string.yiu_need_to_login))
            }
        })
        savedToiletLL.setOnClickListener(View.OnClickListener {
            if (KloxusPrefrences().readBoolean(this, IS_LOGIN, false)) {
            setLeftMenuSelection(4)
            val i = Intent(this, SavedToiletsActivity::class.java)
            startActivity(i)}
            else{
                showLoginAlertDialog(this,getString(R.string.yiu_need_to_login))
            }
        })
        paymentLL.setOnClickListener(View.OnClickListener {
            if (KloxusPrefrences().readBoolean(this, IS_LOGIN, false)) {
            setLeftMenuSelection(5)
            val i = Intent(this, PaymentActivity::class.java)
            i.putExtra("value", "home")
            startActivity(i)}
            else{
                showLoginAlertDialog(this,getString(R.string.yiu_need_to_login))
            }

        })
        msgLL.setOnClickListener(View.OnClickListener {
            if (KloxusPrefrences().readBoolean(this, IS_LOGIN, false)) {
            setLeftMenuSelection(6)
            val i = Intent(this, MessageActivity::class.java)
            startActivity(i)}
            else{
                showLoginAlertDialog(this,getString(R.string.yiu_need_to_login))
            }
        })
        bookingLL.setOnClickListener(View.OnClickListener {
            if (KloxusPrefrences().readBoolean(this, IS_LOGIN, false)) {
            setLeftMenuSelection(7)
            val i = Intent(this, YourBookingsActivity::class.java)
            startActivity(i)}
            else{
                showLoginAlertDialog(this,getString(R.string.yiu_need_to_login))
            }
        })
        profileRL.setOnClickListener(View.OnClickListener {
            if (KloxusPrefrences().readBoolean(this, IS_LOGIN, false)) {
            setLeftMenuSelection(8)
            val i = Intent(this, EditProfileActivity::class.java)
            i.putExtra("value", "home")
            startActivity(i)}
            else{
//                showLoginAlertDialog(this,getString(R.string.yiu_need_to_login))
            }
        })

        txtLoginTV.setOnClickListener(View.OnClickListener {
//            if (KloxusPrefrences().readBoolean(this, IS_LOGIN, false)) {
           performLoginClick()
//        }
//            else{
//                showLoginAlertDialog(this,getString(R.string.yiu_need_to_login))
//            }
        })

    }

    fun setLeftMenuSelection(pos: Int) {
        homeTV.setTextColor(resources.getColor(R.color.colorGreyTextMenu))
        savedToiletTV.setTextColor(resources.getColor(R.color.colorGreyTextMenu))
        myToiletTV.setTextColor(resources.getColor(R.color.colorGreyTextMenu))
        bookingTV.setTextColor(resources.getColor(R.color.colorGreyTextMenu))
        paymentTV.setTextColor(resources.getColor(R.color.colorGreyTextMenu))
        settingTV.setTextColor(resources.getColor(R.color.colorGreyTextMenu))
        referTV.setTextColor(resources.getColor(R.color.colorGreyTextMenu))
        KloxusPrefrences().writeBoolean(this, KloxusPrefrences().HOME_SEL, false)
        KloxusPrefrences().writeBoolean(this, KloxusPrefrences().PROFILE_SEL, false)
        KloxusPrefrences().writeBoolean(this, KloxusPrefrences().MSG_SEL, false)
        KloxusPrefrences().writeBoolean(this, KloxusPrefrences().SAVED_TOILET_SEL, false)
        KloxusPrefrences().writeBoolean(this, KloxusPrefrences().YOUR_BOOKINGS_SEL, false)
        KloxusPrefrences().writeBoolean(this, KloxusPrefrences().PAYMENT_SEL, false)
        KloxusPrefrences().writeBoolean(this, KloxusPrefrences().SETTINGS_SEL, false)
        KloxusPrefrences().writeBoolean(this, KloxusPrefrences().PROFILE_SEL, false)
        KloxusPrefrences().writeBoolean(this, KloxusPrefrences().REFER_SEL, false)

        if (pos == 1) {
            drawer_layout.closeDrawer(GravityCompat.START)
            homeTV.setTextColor(resources.getColor(R.color.black))
            KloxusPrefrences().writeBoolean(this, KloxusPrefrences().HOME_SEL, true)
        } else if (pos == 2) {
            drawer_layout.closeDrawer(GravityCompat.START)
            settingTV.setTextColor(resources.getColor(R.color.black))
        } else if (pos == 3) {
            myToiletTV.setTextColor(resources.getColor(R.color.black))
            drawer_layout.closeDrawer(GravityCompat.START)
        } else if (pos == 4) {
            drawer_layout.closeDrawer(GravityCompat.START)
            savedToiletTV.setTextColor(resources.getColor(R.color.black))
        } else if (pos == 5) {
            drawer_layout.closeDrawer(GravityCompat.START)
            paymentTV.setTextColor(resources.getColor(R.color.black))
        } else if (pos == 6) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else if (pos == 7) {
            drawer_layout.closeDrawer(GravityCompat.START)
            bookingTV.setTextColor(resources.getColor(R.color.black))
        } else if (pos == 8) {
            drawer_layout.closeDrawer(GravityCompat.START)
        }else if (pos == 9) {
            drawer_layout.closeDrawer(GravityCompat.START)
            homeTV.setTextColor(resources.getColor(R.color.black))
            KloxusPrefrences().writeBoolean(this, KloxusPrefrences().HOME_SEL, true)
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(
                Intent.EXTRA_TEXT,
                "Hey checkout our Kloxus app :  " + "https://play.google.com/store/apps/details?id=com.kloxus.app"
            )
            sendIntent.type = "text/plain"
            startActivity(sendIntent)
        }
    }


    @OnClick(
        R.id.imgMenuRL,
        R.id.notificationRL,
        R.id.seeAllRL,
        R.id.aroundRL,
        R.id.mainRL,
        R.id.gapRL,
        R.id.toiletLocLL,
        R.id.imgCancelIV,
        R.id.editSearchET
    )

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgMenuRL -> performMenuClick()
            R.id.notificationRL -> performNotificationClick()
            R.id.seeAllRL -> performSeeAllClick()
            R.id.aroundRL -> performAroundClick()
            R.id.mainRL -> performMAinRLClick()
            R.id.gapRL -> performgapRLClick()
            R.id.toiletLocLL -> performToiletLocClick()
            R.id.imgCancelIV -> performCancelClick()
            R.id.editSearchET -> performSearchClick()
        }
    }

    private fun performSearchClick() {
//                editSearchET.isFocusable = true
//                editSearchET.requestFocus()
//                if (event.action == MotionEvent.ACTION_DOWN) {
                    (behavior as BottomSheetBehavior<View>).state =
                        BottomSheetBehavior.STATE_EXPANDED
                    SearchItem()
                }
//    }

    private fun performLoginClick() {
        val i = Intent(this, LoginActivity::class.java)
        startActivity(i)
        finish()
//        showLoginAlertDialog(this, getString(R.string.yiu_need_to_login))
    }

    private fun performCancelClick() {
        hideCloseKeyboard(this)
        editSearchET.setText("")
    }

    private fun performToiletLocClick() {

    }

    private fun performgapRLClick() {

    }

    private fun performMAinRLClick() {

    }

    private fun performAroundClick() {

    }

    private fun performSeeAllClick() {
        val i = Intent(this, SeeAllActivity::class.java)
        i.putExtra("latitude", mLatitude)
        i.putExtra("longitude", mLongitude)
        startActivity(i)
    }

    private fun performNotificationClick() {
        if (KloxusPrefrences().readBoolean(this, IS_LOGIN, false)) {
            val i = Intent(this, NotificationsActivity::class.java)
        startActivity(i)}
        else{
            showLoginAlertDialog(this,getString(R.string.yiu_need_to_login))
        }
    }

    private fun performMenuClick() {
        drawerOpenClick()
    }

    private fun drawerOpenClick() {
//        hideCloseKeyboard(this)
//        (behavior as BottomSheetBehavior<View>).setState(BottomSheetBehavior.STATE_COLLAPSED);
        drawer_layout.openDrawer(Gravity.LEFT)
    }


    /*
     Transparent status bar
      */
    fun setStatusBar(mActivity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            // edited here
            this.window.statusBarColor = resources.getColor(R.color.white)
        }
    }

    /*
     * Show Progress Dialog
     * */

    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(this)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)
            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        progressDialog!!.show()
//        if (progressDialog != null) progressDialog!!.show()
    }


    /*
     * Hide Progress Dialog
     * */
    fun dismissProgressDialog() {
        try {
            if (this.progressDialog != null && this.progressDialog!!.isShowing) {
                this.progressDialog!!.dismiss()
            }
        } catch (e: IllegalArgumentException) {
            // Handle or log or ignore
        } catch (e: Exception) {
            // Handle or log or ignore
        } finally {
            this.progressDialog = null
        }
    }


    /*
     * Check Internet Connections
     * */
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }


    /*
     * Toast Message
     * */
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(this, strMessage, Toast.LENGTH_SHORT).show()
    }


    /*
     *
     * Error Alert Dialog
     * */
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(this)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

    /*
     *
     * Error Alert Dialog
     * */
    fun showLoginAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(this)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(true)
        alertDialog.setCancelable(true)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            val i = Intent(this, LoginActivity::class.java)
            startActivity(i)
            finish()
        }
        alertDialog.show()
    }

    /*
     *
     * Error Alert Dialog FinishActivity
     * */
    fun showAlertDialogFinish(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(this)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            this.finish()
        }
        alertDialog.show()
    }

    /*
     *
     * Error Alert Dialog FinishActivity
     * */
    fun showAlertDialogFinishHome(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(this)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            logout()
//            val preferences: SharedPreferences = KloxusPrefrences().getPreferences(
//                Objects.requireNonNull(this@HomeActivity)
//            )
//            val editor = preferences.edit()
//            editor.clear()
//            editor.apply()
//            this@HomeActivity.onBackPressed()
//            val mIntent = Intent(this@HomeActivity, LoginActivity::class.java)
//            mIntent.flags =
//                Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
//            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP // To clean up all activities
//            startActivity(mIntent)
//            finish()
//            finishAffinity()

        }
        alertDialog.show()
    }

    private fun logout() {
        if (!isNetworkAvailable(this)) {
            showAlertDialog(this, getString(R.string.internet_connection_error))
        } else {
            executeLogoutApi()
        }
    }


    private fun executeLogoutApi() {
        if (isNetworkAvailable(this))
            executeLogoutRequest()
        else
            showToast(this, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mLogoutParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
//        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeLogoutRequest() {
        showProgressDialog(this)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.logoutRequest(mLogoutParam())?.enqueue(object : Callback<StatusMsgModel> {
            override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<StatusMsgModel>,
                response: Response<StatusMsgModel>
            ) {
                dismissProgressDialog()
                val mModel: StatusMsgModel? = response.body()
                if (mModel != null) {
                    if (mModel.status.equals(1)) {
                        showToast(this@HomeActivity, mModel.message)
                        val preferences: SharedPreferences = KloxusPrefrences().getPreferences(
                            Objects.requireNonNull(this@HomeActivity)
                        )
                        val editor = preferences.edit()
                        editor.clear()
                        editor.apply()
                        this@HomeActivity.onBackPressed()
//                        val mIntent = Intent(mActivity, LoginActivity::class.java)
                        val mIntent = Intent(this@HomeActivity, LoginActivity::class.java)
                        mIntent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP // To clean up all activities
                        startActivity(mIntent)
                        finish()
                        finishAffinity()
                    } else {
                        showAlertDialog(this@HomeActivity, mModel.message)
                    }
                }
            }
        })
    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}