package com.kloxus.app.activities

import android.Manifest
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging
import com.kloxus.app.R
import com.kloxus.app.model.LoginModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.URL
import java.text.DateFormat
import java.util.*


class LoginActivity : BaseActivity(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener,
    LocationListener,
    ResultCallback<LocationSettingsResult> {
    /************************
     * Fused Google Location
     */
    val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
    val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2
    protected val REQUEST_CHECK_SETTINGS = 0x1
    protected val KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates"
    protected val KEY_LOCATION = "location"
    protected val KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string"
    var mLatitude = 0.0
    var mLongitude = 0.0
    protected var mGoogleApiClient: GoogleApiClient? = null
    protected var mLocationRequest: LocationRequest? = null
    protected var mLocationSettingsRequest: LocationSettingsRequest? = null
    protected var mCurrentLocation: Location? = null
    protected var mRequestingLocationUpdates: Boolean? = null
    protected var mLastUpdateTime: String? = null
    private val mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION
    private val mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION
    @RequiresApi(Build.VERSION_CODES.Q)
    private val mAccessBgLocation = Manifest.permission.ACCESS_BACKGROUND_LOCATION

    @BindView(R.id.editEmailET)
    lateinit var editEmailET: EditText
    @BindView(R.id.editPasswordET)
    lateinit var editPasswordET: EditText
    @BindView(R.id.txtForgotPassTV)
    lateinit var txtForgotPassTV: TextView
    @BindView(R.id.txtLoginTV)
    lateinit var txtLoginTV: TextView
    @BindView(R.id.txtSignUpTV)
    lateinit var txtSignUpTV: TextView
    @BindView(R.id.imgGoogleIV)
    lateinit var imgGoogleIV: ImageView
    @BindView(R.id.imgFbIV)
    lateinit var imgFbIV: ImageView
    private var fbProfilePicture: URL? = null
    var FacebookProfilePicture = ""
    var strDeviceToken: String? = null
    var google_email: String? = null
    var google_id: String? = null
    var google_name: String? = null

    var mGoogleSignInClient: GoogleSignInClient? = null
    var GoogleProfilePicture = ""
    protected val GOOGLE_SIGN_IN = 12324
    var fbId: String? = null
    var fbFirstName: String? = null
    var fbLastName: String? = null
    var gFirstName: String? = null
    var gLastName: String? = null
    var fbEmail: String? = null
    var fbSocialUserserName: String? = null
    var mCallbackManager: CallbackManager? = null

    @BindView(R.id.emailRL)
    lateinit var emailRL: RelativeLayout
    @BindView(R.id.passRL)
    lateinit var passRL: RelativeLayout

    //Firebase
    private var mFirebaseAuth: FirebaseAuth? = null

    @BindView(R.id.imgEyeIV)
    lateinit var imgEyeIV: ImageView
    var value: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /*************
         * Fused Location
         */
        mRequestingLocationUpdates = false
        mLastUpdateTime = ""
        /*Update values using data stored in the Bundle.*/
        updateValuesFromBundle(savedInstanceState)
        buildGoogleApiClient()
        FirebaseApp.initializeApp(this)
        createLocationRequest()
        buildLocationSettingsRequest()
        if (checkPermission()) {
            checkLocationSettings()
        } else {
            requestPermission()
        }
        updateValuesFromBundle(savedInstanceState)
        setContentView(R.layout.activity_login)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        //        get device token
        FirebaseApp.initializeApp(this)
        // Configure Google Sign In
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        mFirebaseAuth = FirebaseAuth.getInstance()

        //Logout First
        FirebaseAuth.getInstance().signOut()
        LoginManager.getInstance().logOut()
        signOut()
        getDeviceToken()
        editTextSignupSelector(editEmailET, emailRL, "")
        editTextSignupSelector(editPasswordET, passRL, "")
    }

    private fun performEyeClick() {
        if (value == true) {
            imgEyeIV.setImageDrawable(getDrawable(R.drawable.ic_eye_hide))
            editPasswordET.inputType = InputType.TYPE_CLASS_TEXT or
                    InputType.TYPE_TEXT_VARIATION_PASSWORD
            editPasswordET.setSelection(editPasswordET.text.length)
            value=false
        } else if (value==false) {
            imgEyeIV.setImageDrawable(getDrawable(R.drawable.ic_eye))
            editPasswordET.inputType = InputType.TYPE_CLASS_TEXT or
                    InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
            editPasswordET.setSelection(editPasswordET.text.length)


            value=true
        }
    }

    // [START signOut]
    private fun signOut() {
        if (mGoogleSignInClient != null) {
            mGoogleSignInClient!!.signOut()
                .addOnCompleteListener(this) {
                    // [START_EXCLUDE]
                    Log.e(TAG, "==Logout Successfully==")
                    // [END_EXCLUDE]
                }
        }
    }

    private fun getDeviceToken() {

        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                strDeviceToken = task.result!!
                KloxusPrefrences().writeString(
                    mActivity,
                    DEVICE_TOKEN,
                    strDeviceToken
                )
                Log.e(TAG, "**Push Token**$strDeviceToken")
                // Log and toast
                Log.d(TAG, "Token********   $strDeviceToken")
            })

    }

    @OnClick(
        R.id.txtForgotPassTV,
        R.id.txtLoginTV,
        R.id.txtSignUpTV,
        R.id.imgGoogleIV,
        R.id.imgFbIV,
        R.id.imgEyeIV
    )

    fun onClick(view: View) {
        when (view.id) {
            R.id.txtForgotPassTV -> performForgotPwdClick()
            R.id.txtLoginTV -> performLoginClick()
            R.id.txtSignUpTV -> performSignUpClick()
            R.id.imgGoogleIV -> performGoogleClick()
            R.id.imgFbIV -> performFbClick()
            R.id.imgEyeIV -> performEyeClick()
        }
    }

    private fun performFbClick() {
        preventMultipleClick()
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            FacebookSdk.sdkInitialize(applicationContext)
            LoginManager.getInstance().logOut()
            AppEventsLogger.activateApp(application)
            mCallbackManager = CallbackManager.Factory.create()
            loginWithFacebook()
        }
    }


    private fun loginWithFacebook() {
        if (isNetworkAvailable(mActivity)) {
            LoginManager.getInstance()
                .logInWithReadPermissions(mActivity, Arrays.asList("public_profile", "email"))
            LoginManager.getInstance()
                .registerCallback(mCallbackManager, object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) {
                        Log.d("onSuccess: ", loginResult.accessToken.token)
                        getFacebookData(loginResult)
                    }

                    override fun onCancel() {
//                    Toast.makeText(mActivity, "Cancel", Toast.LENGTH_SHORT).show();
                    }

                    override fun onError(error: FacebookException) {
                        Toast.makeText(mActivity, error.toString(), Toast.LENGTH_SHORT).show()
                    }
                })
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    private fun getFacebookData(loginResult: LoginResult) {
        showProgressDialog(mActivity)
        val graphRequest = GraphRequest.newMeRequest(
            loginResult.accessToken
        ) { `object`, response ->
            dismissProgressDialog()
            try {
                if (`object`.has("id")) {
                    fbId = `object`.getString("id")
                    Log.e("LoginActivity", "id$fbId")
                }
                //check permission first userName
                if (`object`.has("first_name")) {
                    fbFirstName = `object`.getString("first_name")
                    Log.e("LoginActivity", "first_name$fbFirstName")
                }
                //check permisson last userName
                if (`object`.has("last_name")) {
                    fbLastName = `object`.getString("last_name")
                    Log.e("LoginActivity", "last_name$fbLastName")
                }
                //check permisson email
                if (`object`.has("email")) {
                    fbEmail = `object`.getString("email")
                    Log.e("LoginActivity", "email$fbEmail")
                }
                val jsonObject = JSONObject(`object`.getString("picture"))
                if (jsonObject != null) {
                    val dataObject = jsonObject.getJSONObject("data")
                    Log.e("Loginactivity", "json oject get picture$dataObject")
                    fbProfilePicture =
                        URL("https://graph.facebook.com/$fbId/picture?width=500&height=500")
                    Log.e("LoginActivity", "json object=>$`object`")
                }
                FacebookProfilePicture = if (fbProfilePicture != null) {
                    fbProfilePicture.toString()
                } else {
                    ""
                }
                fbSocialUserserName = "$fbFirstName $fbLastName"
                executeLoginWithFbApi()
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
        val bundle = Bundle()
        Log.e("LoginActivity", "bundle set")
        bundle.putString("fields", "id, first_name, last_name,email,picture")
        graphRequest.parameters = bundle
        graphRequest.executeAsync()
    }


    /*
     * Execute fb api
     * */
    private fun mFbParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["firstName"] = fbFirstName!!
        mMap["lastName"] = fbLastName!!
        mMap["email"] = fbEmail!!
        mMap["facebookToken"] = fbId!!
        mMap["facebookImage"] = FacebookProfilePicture
        mMap["latitude"] = mLatitude.toString()
        mMap["longitude"] = mLongitude.toString()
        mMap["deviceToken"] = strDeviceToken
        mMap["deviceType"] = "2"
        mMap["timeZone"] = TimeZone.getDefault().id
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }


    private fun executeLoginWithFbApi() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.signInWithFbRequest((mFbParams()))?.enqueue(object :
            Callback<LoginModel> {
            override fun onFailure(call: Call<LoginModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<LoginModel>, response: Response<LoginModel>) {
                dismissProgressDialog()
                val mModel: LoginModel? = response.body()
                if (mModel != null) {
                    if (mModel.status.equals(1)) {
                        showToast(mActivity, mModel?.message)
                        KloxusPrefrences().writeBoolean(mActivity, IS_LOGIN, true)
                        KloxusPrefrences().writeString(mActivity, USER_ID, mModel.data?.userID)
                        KloxusPrefrences().writeString(
                            mActivity,
                            FIRST_NAME,
                            mModel.data?.firstName
                        )
                        KloxusPrefrences().writeString(mActivity, LAST_NAME, mModel.data?.lastName)
                        KloxusPrefrences().writeString(mActivity, USER_EMAIL, mModel.data?.email)
                        KloxusPrefrences().writeString(mActivity, IS_ADMIN, mModel.data?.isAdmin)
                        KloxusPrefrences().writeString(
                            mActivity,
                            PROFILE_IMAGE,
                            mModel.data?.profileImage
                        )
                        KloxusPrefrences().writeString(mActivity, PHONE, mModel.data?.phoneNo)
                        KloxusPrefrences().writeString(
                            mActivity,
                            CUSTOMER_ID,
                            mModel.data?.customerID
                        )
                        KloxusPrefrences().writeString(mActivity, VERIFIED, mModel.data?.verified)
                        KloxusPrefrences().writeString(
                            mActivity,
                            VERIFICATION_CODE,
                            mModel.data?.verificationCode
                        )
                        KloxusPrefrences().writeString(
                            mActivity,
                            FB_TOKEN,
                            mModel.data?.facebookToken
                        )
                        KloxusPrefrences().writeString(mActivity, GOOGLE_ID, mModel.data?.googleID)
                        KloxusPrefrences().writeString(mActivity, CREATED, mModel.data?.created)
                        KloxusPrefrences().writeString(
                            mActivity,
                            AUTH_TOKEN,
                            mModel.data?.authToken
                        )
                        KloxusPrefrences().writeString(mActivity, COUNTRY_CODE, mModel.data?.countryCode)
                        KloxusPrefrences().writeString(mActivity, PASSWORD, mModel.data?.password)

                        val intent = Intent(mActivity, HomeActivity::class.java)
                        intent.putExtra(LATITUDE, mLatitude.toString())
                        intent.putExtra(LONGITUTE, mLongitude.toString())
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        finish()
                    } else {
                        showAlertDialog(mActivity, mModel?.message)
                    }
                }
            }
        })
    }

    private fun performGoogleClick() {
        preventMultipleClick()
        val signInIntent: Intent = mGoogleSignInClient!!.getSignInIntent()
        startActivityForResult(signInIntent, GOOGLE_SIGN_IN)
    }


    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            google_email = account.email
            google_id = account.id
            google_name = account.displayName

            val name = account.displayName

            val idx = name.lastIndexOf(' ')
            if (idx == -1) {
//                Toast.makeText(mActivity, "Invalid full name", Toast.LENGTH_LONG).show()
                return
            }

            gFirstName = name.substring(0, idx)
            gLastName = name.substring(idx + 1)
            Log.e("SPLITED NAME", gFirstName + " - " + gLastName)

            GoogleProfilePicture = if (account.photoUrl != null) {
                account.photoUrl.toString()
                //                    mBase64GoogleImage=convertUrlToBase64(GoogleProfilePicture);
            } else {
                ""
                //                    mBase64GoogleImage="";
            }
            executeGoogleApi()
        } catch (e: ApiException) {
            Log.e("MyTAG", "signInResult:failed code=" + e.statusCode)
            Toast.makeText(mActivity, "Failed", Toast.LENGTH_LONG)
        }
    }


    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (mCallbackManager != null) {
            // Pass the activity result back to the Facebook SDK
            mCallbackManager!!.onActivityResult(requestCode, resultCode, data)
        }
        when (requestCode) {
            GOOGLE_SIGN_IN -> {
                val completedTask: Task<GoogleSignInAccount> =
                    GoogleSignIn.getSignedInAccountFromIntent(data)
                handleSignInResult(completedTask)
            }
            REQUEST_CHECK_SETTINGS -> when (resultCode) {
                RESULT_OK -> {
                    Log.i("", "User agreed to make required location settings changes.")
                    startLocationUpdates()
                }
                RESULT_CANCELED -> {
                    Log.i("", "User chose not to make required location settings changes.")
                    requestPermission()
                }
                else -> {
                }
            }
        }
    }

    private fun executeGoogleApi() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            loginWithGoogle()
        }
    }

    /*
     * Execute google api
     * */

    private fun mGoogleParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["firstName"] = gFirstName!!
        mMap["lastName"] = gLastName!!
        mMap["email"] = google_email!!
        mMap["googleID"] = google_id!!
        mMap["latitude"] = mLatitude.toString()
        mMap["longitude"] = mLongitude.toString()
        mMap["deviceToken"] = strDeviceToken
        mMap["deviceType"] = "2"
        mMap["googleImage"] = GoogleProfilePicture
        mMap["timeZone"] = TimeZone.getDefault().id
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun loginWithGoogle() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.loginWithGoogleRequest(mGoogleParams())?.enqueue(object :
            Callback<LoginModel> {
            override fun onFailure(call: Call<LoginModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<LoginModel>, response: Response<LoginModel>) {
                dismissProgressDialog()
                val mModel: LoginModel? = response.body()
                if (mModel != null) {
                    if (mModel.status.equals(1)) {
                        showToast(mActivity, mModel?.message)
                        KloxusPrefrences().writeBoolean(mActivity, IS_LOGIN, true)
                        KloxusPrefrences().writeString(mActivity, USER_ID, mModel.data?.userID)
                        KloxusPrefrences().writeString(
                            mActivity,
                            FIRST_NAME,
                            mModel.data?.firstName
                        )
                        KloxusPrefrences().writeString(mActivity, LAST_NAME, mModel.data?.lastName)
                        KloxusPrefrences().writeString(mActivity, USER_EMAIL, mModel.data?.email)
                        KloxusPrefrences().writeString(mActivity, IS_ADMIN, mModel.data?.isAdmin)
                        KloxusPrefrences().writeString(
                            mActivity,
                            PROFILE_IMAGE,
                            mModel.data?.profileImage
                        )
                        KloxusPrefrences().writeString(mActivity, PHONE, mModel.data?.phoneNo)
                        KloxusPrefrences().writeString(
                            mActivity,
                            CUSTOMER_ID,
                            mModel.data?.customerID
                        )
                        KloxusPrefrences().writeString(mActivity, VERIFIED, mModel.data?.verified)
                        KloxusPrefrences().writeString(
                            mActivity,
                            VERIFICATION_CODE,
                            mModel.data?.verificationCode
                        )
                        KloxusPrefrences().writeString(
                            mActivity,
                            FB_TOKEN,
                            mModel.data?.facebookToken
                        )
                        KloxusPrefrences().writeString(mActivity, GOOGLE_ID, mModel.data?.googleID)
                        KloxusPrefrences().writeString(mActivity, CREATED, mModel.data?.created)
                        KloxusPrefrences().writeString(
                            mActivity,
                            AUTH_TOKEN,
                            mModel.data?.authToken
                        )
                        KloxusPrefrences().writeString(mActivity, COUNTRY_CODE, mModel.data?.countryCode)
                        KloxusPrefrences().writeString(mActivity, PASSWORD, mModel.data?.password)

                        val intent = Intent(mActivity, HomeActivity::class.java)
                        intent.putExtra("latitude", mLatitude.toString())
                        intent.putExtra("longitude", mLongitude.toString())
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        finish()
                    } else {
                        showAlertDialog(mActivity, mModel?.message)
                    }
                }
            }
        })
    }


    private fun performForgotPwdClick() {
        val i = Intent(mActivity, ForgotPassword::class.java)
        startActivity(i)
    }


    private fun performLoginClick() {
        if (isValidate())
            if (isNetworkAvailable(mActivity))
                executeSignInRequest()
            else
                showToast(mActivity, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> =HashMap()
        mMap["email"] = editEmailET.text.toString().trim { it <= ' ' }
        mMap["password"] = editPasswordET.text.toString().trim { it <= ' ' }
        mMap["latitude"] = mLatitude.toString()
        mMap["longitude"] = mLongitude.toString()
        mMap["deviceToken"] = strDeviceToken
        mMap["deviceType"] = "2"
        mMap["timeZone"] = TimeZone.getDefault().id
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeSignInRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.signInRequest(mParam())?.enqueue(object : Callback<LoginModel> {
            override fun onFailure(call: Call<LoginModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<LoginModel>, response: Response<LoginModel>) {
                dismissProgressDialog()
                val mModel: LoginModel? = response.body()
                if (mModel != null) {
                    if (mModel.status.equals(1)) {
                        showToast(mActivity, mModel?.message)
                        KloxusPrefrences().writeBoolean(mActivity, IS_LOGIN, true)
                        KloxusPrefrences().writeString(mActivity, USER_ID, mModel.data?.userID)
                        KloxusPrefrences().writeString(
                            mActivity,
                            FIRST_NAME,
                            mModel.data?.firstName
                        )
                        KloxusPrefrences().writeString(mActivity, LAST_NAME, mModel.data?.lastName)
                        KloxusPrefrences().writeString(mActivity, USER_EMAIL, mModel.data?.email)
                        KloxusPrefrences().writeString(mActivity, IS_ADMIN, mModel.data?.isAdmin)
                        KloxusPrefrences().writeString(
                            mActivity,
                            PROFILE_IMAGE,
                            mModel.data?.profileImage
                        )
                        KloxusPrefrences().writeString(mActivity, PHONE, mModel.data?.phoneNo)
                        KloxusPrefrences().writeString(
                            mActivity,
                            CUSTOMER_ID,
                            mModel.data?.customerID
                        )
                        KloxusPrefrences().writeString(mActivity, VERIFIED, mModel.data?.verified)
                        KloxusPrefrences().writeString(
                            mActivity,
                            VERIFICATION_CODE,
                            mModel.data?.verificationCode
                        )
                        KloxusPrefrences().writeString(
                            mActivity,
                            FB_TOKEN,
                            mModel.data?.facebookToken
                        )
                        KloxusPrefrences().writeString(mActivity, GOOGLE_ID, mModel.data?.googleID)
                        KloxusPrefrences().writeString(mActivity, CREATED, mModel.data?.created)
                        KloxusPrefrences().writeString(
                            mActivity,
                            AUTH_TOKEN,
                            mModel.data?.authToken
                        )
                        KloxusPrefrences().writeString(mActivity, COUNTRY_CODE, mModel.data?.countryCode)
                        KloxusPrefrences().writeString(mActivity, PASSWORD, mModel.data?.password)


                        val intent = Intent(mActivity, HomeActivity::class.java)
                        intent.putExtra("latitude", mLatitude.toString())
                        intent.putExtra("longitude", mLongitude.toString())
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        finish()
                    } else {
                        showAlertDialog(mActivity, mModel?.message)
                    }
                }
            }
        })
    }


    private fun performSignUpClick() {
        val i = Intent(mActivity, SignupActivity::class.java)
        startActivity(i)
        finish()
    }

    /*
     * Set up validations for Sign In fields
     * */
    private fun isValidate(): Boolean {
        var flag = true
        if (editEmailET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
            flag = false
        } else if (!isValidEmaillId(editEmailET.text.toString().trim { it <= ' ' })) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (editPasswordET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_password))
            flag = false
        }
        return flag
    }

    /******************
     * Fursed Google Location
     */
    private fun updateValuesFromBundle(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet()
                    .contains(KEY_REQUESTING_LOCATION_UPDATES)
            ) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                    KEY_REQUESTING_LOCATION_UPDATES
                )
            }
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION)
            }
            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime =
                    savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING)
            }
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the `#addApi` method to request the
     * LocationServices API.
     */
    @Synchronized
    protected fun buildGoogleApiClient() {
        Log.i("", "Building GoogleApiClient")
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
    }

    protected fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    /**
     * Uses a [LocationSettingsRequest.Builder] to build
     * a [LocationSettingsRequest] that is used for checking
     * if a device has the needed location settings.
     */
    protected fun buildLocationSettingsRequest() {
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest).setAlwaysShow(true)
        mLocationSettingsRequest = builder.build()
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * [SettingsApi.checkLocationSettings] method, with the results provided through a `PendingResult`.
     */
    fun checkLocationSettings() {
        val result = LocationServices.SettingsApi.checkLocationSettings(
            mGoogleApiClient,
            mLocationSettingsRequest
        )
        result.setResultCallback(this)
    }

    /**
     * The callback invoked when
     * [SettingsApi.checkLocationSettings] is called. Examines the
     * [LocationSettingsResult] object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    override fun onResult(locationSettingsResult: LocationSettingsResult) {
        val status = locationSettingsResult.status
        when (status.statusCode) {
            LocationSettingsStatusCodes.SUCCESS -> {
                Log.i("", "All location settings are satisfied.")
                startLocationUpdates()
            }
            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                Log.i(
                    "", "Location settings are not satisfied. Show the user a dialog to" +
                            "upgrade location settings "
                )
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS)
                } catch (e: SendIntentException) {
                    Log.i("", "PendingIntent unable to execute request.")
                }
            }
            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(
                "", "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created."
            )
            else -> {
            }
        }
    }


    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected fun startLocationUpdates() {
        try {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
//                && ActivityCompat.checkSelfPermission(
//                    this,
//                    Manifest.permission.ACCESS_BACKGROUND_LOCATION
//                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
            ).setResultCallback { mRequestingLocationUpdates = true }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(TAG, "msg" + e.printStackTrace())
        }
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected fun stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
            mGoogleApiClient,
            this
        ).setResultCallback { mRequestingLocationUpdates = false }
    }


    override fun onStart() {
        super.onStart()
        Log.e(TAG, "onStart")
        if (mGoogleApiClient != null) mGoogleApiClient!!.connect()
    }

    override fun onResume() {
        super.onResume()
        Log.e(TAG, "onResume")
        if (mGoogleApiClient!!.isConnected && mRequestingLocationUpdates!!) {
            startLocationUpdates()
        }
    }


    override fun onPause() {
        super.onPause()
        Log.e(TAG, "onPause")
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient!!.isConnected) {
            stopLocationUpdates()
        }
        //  dismissProgressDialog();
    }

    override fun onStop() {
        super.onStop()
        Log.e(TAG, "onStop")
        mGoogleApiClient!!.disconnect()
        dismissProgressDialog()
    }

    override fun onRestart() {
        super.onRestart()
        Log.e(TAG, "onRestart")
    }


    override fun onConnected(connectionHint: Bundle?) {
        Log.i("", "Connected to GoogleApiClient")
        if (mCurrentLocation == null) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
            mLastUpdateTime = DateFormat.getTimeInstance().format(Date())
        }
    }

    /**
     * Callback that fires when the location changes.
     */
    override fun onLocationChanged(location: Location) {
        mCurrentLocation = location
        mLastUpdateTime = DateFormat.getTimeInstance().format(Date())
        mLatitude = mCurrentLocation!!.latitude
        mLongitude = mCurrentLocation!!.longitude
        Log.e(TAG, "*********LATITUDE********$mLatitude")
        Log.e(TAG, "*********LONGITUDE********$mLongitude")


            //        KloxusPrefrences.writeString(mActivity, KloxusPrefrences.CURRENT_LOCATION_LATITUDE, "" + mLatitude);
//        KloxusPrefrences.writeString(mActivity, KloxusPrefrences.CURRENT_LOCATION_LONGITUDE, "" + mLongitude);
        try {
//            val df = DecimalFormat()
//            df.maximumFractionDigits = 3
//            mLatitude = df.format(mCurrentLocation!!.latitude).toDouble()
//            mLongitude = df.format(mCurrentLocation!!.longitude).toDouble()
   mLatitude =mCurrentLocation!!.latitude
            mLongitude = mCurrentLocation!!.longitude

            var geocoder: Geocoder? = null
            var addresses: List<Address>? = null
            geocoder = Geocoder(this, Locale.getDefault())
            try {
                addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            if (addresses != null && addresses.size > 0) {
//            val locality = addresses[0].locality
                val state = addresses[0].adminArea

                val city = addresses[0].locality
                KloxusPrefrences().writeString(mActivity, SELECTED_LOCATION, city)
//            val geocoder: Geocoder
//            val addresses: List<Address>
//            geocoder = Geocoder(this, Locale.getDefault())
//            addresses = geocoder.getFromLocation(
//                mLatitude,
//                mLongitude,
//                1
//            ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//            val address =
//                addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//
//
//            val city = addresses[0].locality
//            KloxusPrefrences().writeString(mActivity, SELECTED_LOCATION, city)
//            val state = addresses[0].adminArea
//            val country = addresses[0].countryName
//            val postalCode = addresses[0].postalCode
//            val knownName = addresses[0].featureName // Only if available else return NULL
            //            KloxusPrefrences.writeString(mActivity, KloxusPrefrences.SELECTED_LOCATION, address);
        }} catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onConnectionSuspended(cause: Int) {
        Log.i("", "Connection suspended")
    }

    override fun onConnectionFailed(result: ConnectionResult) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i("", "Connection failed: ConnectionResult.getErrorCode() = " + result.errorCode)
    }

    /**
     * Stores activity data in the Bundle.
     */
    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        savedInstanceState.putBoolean(
            KEY_REQUESTING_LOCATION_UPDATES,
            mRequestingLocationUpdates!!
        )
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation)
        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime)
        super.onSaveInstanceState(savedInstanceState)
    }
    /*****************************************/

    /** */
    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) ACCESS_FINE_LOCATION
     * 2) ACCESS_COARSE_LOCATION
     */
    fun checkPermission(): Boolean {
        val mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation)
        val mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation)
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED
    }

    fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(mAccessFineLocation, mAccessCourseLocation),
            REQUEST_PERMISSION_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions!!, grantResults)
        when (requestCode) {
            REQUEST_PERMISSION_CODE -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    checkLocationSettings()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    requestPermission()
                }
                return
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

}