package com.kloxus.app.activities

import android.Manifest
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import com.google.firebase.FirebaseApp
import com.kloxus.app.R
import com.kloxus.app.model.*
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.*
//import com.paypal.android.sdk.payments.*
//import com.paypal.android.sdk.payments.PaymentActivity
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.math.BigDecimal
import java.text.DateFormat
import java.util.*


class BookNowActivity : BaseActivity(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener,
    LocationListener,
    ResultCallback<LocationSettingsResult> {
    /************************
     * Fused Google Location
     */
    val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
    val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2
    protected val REQUEST_CHECK_SETTINGS = 0x1
    protected val KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates"
    protected val KEY_LOCATION = "location"
    protected val KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string"
    var mLatitude = 0.0
    var mLongitude = 0.0
    protected var mGoogleApiClient: GoogleApiClient? = null
    protected var mLocationRequest: LocationRequest? = null
    protected var mLocationSettingsRequest: LocationSettingsRequest? = null
    protected var mCurrentLocation: Location? = null
    protected var mRequestingLocationUpdates: Boolean? = null
    protected var mLastUpdateTime: String? = null
    private val mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION
    private val mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION

    var mAccesstoken: String = ""
    var payID: String = ""
    var paymentMethod: String = "0"

//    val clientKey = "ATIYGZKeGagXbOqizJxhzF6BTDZjrpUdCy8JU_MGzo-JqopcItZEdQCyggGSqKZKsExi54iEbX628ENs"
//    val PAYPAL_REQUEST_CODE = 123

    // Paypal Configuration Object
//    private val config = PayPalConfiguration() // Start with mock environment.  When ready,
//        // switch to sandbox (ENVIRONMENT_SANDBOX)
//        // or live (ENVIRONMENT_PRODUCTION)
//        .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX) // on below line we are passing a client id.
//        .clientId(clientKey)
//    private val amountEdt: EditText? = null
//    private val paymentTV: TextView? = null
//    private var config: PayPalConfiguration? =
//        PayPalConfiguration() // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
//            // or live (ENVIRONMENT_PRODUCTION)
//            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
//            .clientId(clientKey)

    @BindView(R.id.txtPriceTV)
    lateinit var txtPriceTV: TextView

    @BindView(R.id.txtAddrTV)
    lateinit var txtAddrTV: TextView

    @BindView(R.id.txtToiletAddressTV)
    lateinit var txtToiletAddressTV: TextView

    @BindView(R.id.txtToiletNameTV)
    lateinit var txtToiletNameTV: TextView

    @BindView(R.id.txtBookNowTV)
    lateinit var txtBookNowTV: TextView

    @BindView(R.id.addPaymentLL)
    lateinit var addPaymentLL: RelativeLayout
    var isPaymentSelect = false
    var mDefault = 0

    @BindView(R.id.imgCrossIV)
    lateinit var imgCrossIV: ImageView
    var mPrice: String = ""
    var mCurr: String = ""
    var mAddress: String = ""
    var isPaid: String = ""
    var mTitle: String = ""
    var mAvailable: String = ""
    var mDesc: String = ""
    var mName: String = ""
    var mUser_id: String = ""
    var toilet_id: String = ""

    var mAllCardsList: ArrayList<CardsData>? = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /*************
         * Fused Location
         */
        mRequestingLocationUpdates = false
        mLastUpdateTime = ""
        /*Update values using data stored in the Bundle.*/
        updateValuesFromBundle(savedInstanceState)
        buildGoogleApiClient()
        FirebaseApp.initializeApp(this)
        createLocationRequest()
        buildLocationSettingsRequest()
        if (checkPermission()) {
            checkLocationSettings()
        } else {
            requestPermission()
        }
        updateValuesFromBundle(savedInstanceState)
        setContentView(R.layout.activity_book_now)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        getIntentData()
//        val intent = Intent(this, PayPalService::class.java)
//        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
//        startService(intent)
        setData()
//        KloxusPrefrences().writeString(mActivity, "card_id", "")
        getProfileDetail()

    }


    private fun getAllCards() {
        if (isNetworkAvailable(mActivity))
            executeGetAllCardsRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetAllCardsRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getCardDetailsRequest(mParam())
            ?.enqueue(object : Callback<GetCardsModel> {
                override fun onFailure(call: Call<GetCardsModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<GetCardsModel>,
                    response: Response<GetCardsModel>
                ) {

                    dismissProgressDialog()
                    val mModel: GetCardsModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {

                            mAllCardsList = mModel.data
                            for (i in mModel.data.indices) {
                                if (mModel.data.get(i).isDefault == 1) {
                                    KloxusPrefrences().writeString(
                                        mActivity,
                                        "card_id",
                                        mModel.data.get(i).card_id
                                    )
                                }
                            }
                        } else {
                        }
                    }
                }
            })
    }

    private fun getProfileDetail() {
        if (isNetworkAvailable(this))
            executeGetProfileDetailRequest()
        else
            showToast(this, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mProfileParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetProfileDetailRequest() {
        showProgressDialog(this)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getProfileDetailRequest(mProfileParam())
            ?.enqueue(object : Callback<EditProfileModel> {
                override fun onFailure(call: Call<EditProfileModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<EditProfileModel>,
                    response: Response<EditProfileModel>
                ) {
                    dismissProgressDialog()
                    val mModel: EditProfileModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            mModel.data.firstName
                            txtAddrTV.text =
                                mModel.data.firstName + " " + mModel.data.lastName + " " + " " + mModel.data.phoneNo
                            mDefault = mModel.data.defaultPaymentMethod
//                            KloxusPrefrences().writeString(
//                                mActivity, "paymentMethod",
//                                mDefault.toString()
//                            )
//                            if (mDefault == 1) {
//                                getAllCards()
//                            } else {
//
//                            }
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }

    private fun setData() {
        txtToiletAddressTV.text = mAddress
        txtToiletNameTV.text = mTitle
        if (isPaid.equals("1")){
            txtPriceTV.text = mCurr + mPrice
        }
        else{
            txtPriceTV.text = mPrice
        }
//        txtPriceTV.text = mCurr + mPrice
    }

    private fun getIntentData() {
        if (intent != null) {
            mAddress = intent.getStringExtra("address").toString()
            mPrice = intent.getStringExtra("price").toString()
            mCurr = intent.getStringExtra("curr").toString()
            mTitle = intent.getStringExtra("title").toString()
            mAvailable = intent.getStringExtra("available").toString()
            mDesc = intent.getStringExtra("desc").toString()
            mName = intent.getStringExtra("name").toString()
            mUser_id = intent.getStringExtra("user_id").toString()
            toilet_id = intent.getStringExtra("toilet_id").toString()
            isPaid = intent.getStringExtra("isPaid").toString()

        }
    }

    @OnClick(
        R.id.imgCrossIV,
        R.id.addPaymentLL,
        R.id.txtBookNowTV,
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgCrossIV -> performCancelClick()
            R.id.addPaymentLL -> performAddPaymentClick()
            R.id.txtBookNowTV -> performBookNowClick()
        }
    }

    private fun performAddPaymentClick() {
        val i = Intent(this, SelectPaymentMethodActivity::class.java)
        i.putExtra("toilet_id", toilet_id)
        i.putExtra("mPrice", mPrice)
        i.putExtra("mCurr", mCurr)
        i.putExtra("value", "book_id")
        i.putExtra("default", mDefault)
        startActivity(i)
    }

    private fun performBookNowClick() {
        preventMultipleClick()
        if (isNetworkAvailable(mActivity)) {
            executeCheckStatusApi()
        } else showToast(
            mActivity,
            getString(R.string.internet_connection_error)
        )
    }

    /*
    * Execute api param
    * */
    private fun mcheckStatusParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["toiletID"] = toilet_id
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeCheckStatusApi() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getbookingStatusRequest(mcheckStatusParam())
            ?.enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {


                    val mModel: StatusMsgModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            if (mModel.isPaid.equals("0")){
                                paymentMethod="0"
                            }
                            else{
                                paymentMethod="3"
                            }

//                            if (KloxusPrefrences().readString(mActivity, "paymentMethod", null)
//                                    .equals(
//                                        "1"
//                                    )
//                            ) {
//                                executeCardTokenApi()
//                            } else if (KloxusPrefrences().readString(
//                                    mActivity,
//                                    "paymentMethod",
//                                    null
//                                ).equals("2")
//                            ) {
//                                executeBookNowPaypalApi()
//                            } else {
                                executeBookNowApi()
//                            }
                        } else {
                            dismissProgressDialog()
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }




    private fun executeBookNowPaypalApi() {
//        getPayment()
    }

//    private fun getPayment() {
//
//        // Getting the amount from editText
//        val amount: String = mPrice
//
//        // Creating a paypal payment on below line.
//        val payment = PayPalPayment(
//            BigDecimal(amount), "USD", "Price",
//            PayPalPayment.PAYMENT_INTENT_SALE
//        )
//
//        // Creating Paypal Payment activity intent
//        val intent = Intent(this, PaymentActivity::class.java)
//
//        //putting the paypal configuration to the intent
//        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
//
//        // Puting paypal payment to the intent
//        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment)
//
//        // Starting the intent activity for result
//        // the request code will be used on the method onActivityResult
//        startActivityForResult(intent, PAYPAL_REQUEST_CODE)
//    }
//
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//
//        // If the result is from paypal
//        if (requestCode == PAYPAL_REQUEST_CODE) {
//
//            // If the result is OK i.e. user has not canceled the payment
//            if (resultCode == RESULT_OK) {
//
//                // Getting the payment confirmation
//                val confirm: PaymentConfirmation =
//                    data!!.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION)!!
//
//                // if confirmation is not null
//                if (confirm != null) {
//                    try {
//                        // Getting the payment details
//                        val paymentDetails = confirm.toJSONObject().toString(4)
//                        // on below line we are extracting json response and displaying it in a text view.
//                        val payObj = JSONObject(paymentDetails)
//                        payID = payObj.getJSONObject("response").getString("id")
//                        val state = payObj.getJSONObject("response").getString("state")
//                        // Transaction information
//                        getAccessToken()
//                        // Transaction information
////                        val transaction = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
////                            Transaction()
////                        } else {
////                            TODO("VERSION.SDK_INT < Q")
////                        }
////// Add transaction to a list
////
////// Add transaction to a list
////                        val transactions: MutableList<Transaction> = ArrayList()
////                        transactions.add(transaction)
//
////                        paymentTV.setText("Payment $state\n with payment id is $payID")
//                    } catch (e: JSONException) {
//                        // handling json exception on below line
//                        Log.e("Error", "an extremely unlikely failure occurred: ", e)
//                    }
//                }
//            } else if (resultCode == RESULT_CANCELED) {
//                // on below line we are checking the payment status.
//                Log.i("paymentExample", "The user canceled.")
//            } else if (resultCode == com.paypal.android.sdk.payments.PaymentActivity.RESULT_EXTRAS_INVALID) {
//                // on below line when the invalid paypal config is submitted.
//                Log.i(
//                    "paymentExample",
//                    "An invalid Payment or PayPalConfiguration was submitted. Please see the docs."
//                )
//            }
//        }
//    }

    private fun getAccessToken() {
        if (isNetworkAvailable(this))
            executeGetAccessTokenRequest()
        else
            showToast(this, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mGetAccessTokenParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetAccessTokenRequest() {
        showProgressDialog(this)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getAccessTokenRequest(mGetAccessTokenParam())
            ?.enqueue(object : Callback<GetAccessTokenModel> {
                override fun onFailure(call: Call<GetAccessTokenModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<GetAccessTokenModel>,
                    response: Response<GetAccessTokenModel>
                ) {
//                    dismissProgressDialog()
                    val mModel: GetAccessTokenModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            mAccesstoken = mModel.accessToken
                            getTransDetails()
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }

    private fun getTransDetails() {
        if (isNetworkAvailable(this))
            executeGetTransactionDetailsRequest()
        else
            showToast(this, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mGetTransParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["payID"] = payID
//        mMap["payID"] = "PAY-1B56960729604235TKQQIYVY"
        mMap["accessToken"] = mAccesstoken
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetTransactionDetailsRequest() {
//        showProgressDialog(this)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getTransactionDetailsRequest(mGetTransParam())
            ?.enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {
                    dismissProgressDialog()
                    val mModel: StatusMsgModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
//                            showToast(mActivity, mModel.message)
                            executeBookNowApi()
//                            finish()
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }



    /*
     * Execute api param
     * */
    private fun mCardParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["toiletID"] = toilet_id
        mMap["cardID"] = KloxusPrefrences().readString(mActivity, "card_id", null)
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeCardTokenApi() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.sendPaymentRequest(mCardParam())
            ?.enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {

                    dismissProgressDialog()
                    val mModel: StatusMsgModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            executeBookNowApi()
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }

    /*
     * Execute api param
     * */
    private fun mBookParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["toiletID"] = toilet_id
        mMap["address"] = mAddress
//        mMap["paymentMethod"] = KloxusPrefrences().readString(this, "paymentMethod", null)
        mMap["paymentMethod"] = paymentMethod
        mMap["bookingFor"] = txtAddrTV.text.toString().trim { it <= ' ' }
        mMap["location"] = KloxusPrefrences().readString(this, SELECTED_LOCATION, null)
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeBookNowApi() {
//        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.bookToiletRequest(mBookParam())
            ?.enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {

                    dismissProgressDialog()
                    val mModel: StatusMsgModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
//                            KloxusPrefrences().writeString(mActivity, "card_id", "")

//                            showToast(mActivity, mModel.message)
//                            finish()
                            if(mModel.isRequestSend.equals("0")){
                                showAlertDialogFinish(mActivity,mModel.message)
                            }
                            else{
                                val intent = Intent(mActivity, SelectPaymentMethodActivity::class.java)
                                intent.putExtra("toilet_id",toilet_id)
                                intent.putExtra("mPrice", mPrice)
                                intent.putExtra("mCurr", mCurr)
                                intent.putExtra("book_id", mModel.bookID)
//                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                startActivity(intent)
                                finish()
                            }
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }

    private fun performCancelClick() {
        onBackPressed()
    }

    /******************
     * Fursed Google Location
     */
    private fun updateValuesFromBundle(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet()
                    .contains(KEY_REQUESTING_LOCATION_UPDATES)
            ) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                    KEY_REQUESTING_LOCATION_UPDATES
                )
            }
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION)
            }
            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime =
                    savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING)
            }
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the `#addApi` method to request the
     * LocationServices API.
     */
    @Synchronized
    protected fun buildGoogleApiClient() {
        Log.i("", "Building GoogleApiClient")
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
    }

    protected fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    /**
     * Uses a [LocationSettingsRequest.Builder] to build
     * a [LocationSettingsRequest] that is used for checking
     * if a device has the needed location settings.
     */
    protected fun buildLocationSettingsRequest() {
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest).setAlwaysShow(true)
        mLocationSettingsRequest = builder.build()
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * [SettingsApi.checkLocationSettings] method, with the results provided through a `PendingResult`.
     */
    fun checkLocationSettings() {
        val result = LocationServices.SettingsApi.checkLocationSettings(
            mGoogleApiClient,
            mLocationSettingsRequest
        )
        result.setResultCallback(this)
    }

    /**
     * The callback invoked when
     * [SettingsApi.checkLocationSettings] is called. Examines the
     * [LocationSettingsResult] object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    override fun onResult(locationSettingsResult: LocationSettingsResult) {
        val status = locationSettingsResult.status
        when (status.statusCode) {
            LocationSettingsStatusCodes.SUCCESS -> {
                Log.i("", "All location settings are satisfied.")
                startLocationUpdates()
            }
            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                Log.i(
                    "", "Location settings are not satisfied. Show the user a dialog to" +
                            "upgrade location settings "
                )
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS)
                } catch (e: IntentSender.SendIntentException) {
                    Log.i("", "PendingIntent unable to execute request.")
                }
            }
            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(
                "", "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created."
            )
            else -> {
            }
        }
    }


    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected fun startLocationUpdates() {
        try {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
//                && ActivityCompat.checkSelfPermission(
//                    this,
//                    Manifest.permission.ACCESS_BACKGROUND_LOCATION
//                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
            ).setResultCallback { mRequestingLocationUpdates = true }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(TAG, "msg" + e.printStackTrace())
        }
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected fun stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
            mGoogleApiClient,
            this
        ).setResultCallback { mRequestingLocationUpdates = false }
    }


    override fun onStart() {
        super.onStart()
        Log.e(TAG, "onStart")
        if (mGoogleApiClient != null) mGoogleApiClient!!.connect()
    }

    override fun onResume() {
        super.onResume()
        Log.e(TAG, "onResume")
        if (mGoogleApiClient!!.isConnected && mRequestingLocationUpdates!!) {
            startLocationUpdates()
        }
    }


    override fun onPause() {
        super.onPause()
        Log.e(TAG, "onPause")
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient!!.isConnected) {
            stopLocationUpdates()
        }
        //  dismissProgressDialog();
    }

    override fun onStop() {
        super.onStop()
        Log.e(TAG, "onStop")
        mGoogleApiClient!!.disconnect()
        dismissProgressDialog()
    }

    override fun onRestart() {
        super.onRestart()
        Log.e(TAG, "onRestart")
    }


    override fun onConnected(connectionHint: Bundle?) {
        Log.i("", "Connected to GoogleApiClient")
        if (mCurrentLocation == null) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
            mLastUpdateTime = DateFormat.getTimeInstance().format(Date())
        }
    }

    /**
     * Callback that fires when the location changes.
     */
    override fun onLocationChanged(location: Location) {
        mCurrentLocation = location
        mLastUpdateTime = DateFormat.getTimeInstance().format(Date())
        mLatitude = mCurrentLocation!!.latitude
        mLongitude = mCurrentLocation!!.longitude
        Log.e(TAG, "*********LATITUDE********$mLatitude")
        Log.e(TAG, "*********LONGITUDE********$mLongitude")


        //        KloxusPrefrences.writeString(mActivity, KloxusPrefrences.CURRENT_LOCATION_LATITUDE, "" + mLatitude);
//        KloxusPrefrences.writeString(mActivity, KloxusPrefrences.CURRENT_LOCATION_LONGITUDE, "" + mLongitude);
        try {
//            val df = DecimalFormat()
//            df.maximumFractionDigits = 3
            mLatitude = mCurrentLocation!!.latitude
            mLongitude = mCurrentLocation!!.longitude

            var geocoder: Geocoder? = null
            var addresses: List<Address>? = null
            geocoder = Geocoder(this, Locale.getDefault())
            try {
                addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            if (addresses != null && addresses.size > 0) {
//            val locality = addresses[0].locality
                val state = addresses[0].adminArea

                val city = addresses[0].subAdminArea
                KloxusPrefrences().writeString(mActivity, SELECTED_LOCATION, city)
//            val geocoder: Geocoder
//            val addresses: List<Address>
//            geocoder = Geocoder(this, Locale.getDefault())
//            addresses = geocoder.getFromLocation(
//                mLatitude,
//                mLongitude,
//                1
//            ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//            val address =
//                addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//
//
//            val city = addresses[0].locality
//            KloxusPrefrences().writeString(mActivity, SELECTED_LOCATION, city)
//            val state = addresses[0].adminArea
//            val country = addresses[0].countryName
//            val postalCode = addresses[0].postalCode
//            val knownName = addresses[0].featureName // Only if available else return NULL
                //            KloxusPrefrences.writeString(mActivity, KloxusPrefrences.SELECTED_LOCATION, address);
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onConnectionSuspended(cause: Int) {
        Log.i("", "Connection suspended")
    }

    override fun onConnectionFailed(result: ConnectionResult) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i("", "Connection failed: ConnectionResult.getErrorCode() = " + result.errorCode)
    }

    /**
     * Stores activity data in the Bundle.
     */
    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        savedInstanceState.putBoolean(
            KEY_REQUESTING_LOCATION_UPDATES,
            mRequestingLocationUpdates!!
        )
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation)
        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime)
        super.onSaveInstanceState(savedInstanceState)
    }
    /*****************************************/

    /** */
    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) ACCESS_FINE_LOCATION
     * 2) ACCESS_COARSE_LOCATION
     */
    fun checkPermission(): Boolean {
        val mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation)
        val mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation)
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED
    }

    fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(mAccessFineLocation, mAccessCourseLocation),
            REQUEST_PERMISSION_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_PERMISSION_CODE -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    checkLocationSettings()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    requestPermission()
                }
                return
            }
        }
    }

}