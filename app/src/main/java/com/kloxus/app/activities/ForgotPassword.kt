package com.kloxus.app.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.kloxus.app.R
import com.kloxus.app.model.StatusMsgModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class ForgotPassword : BaseActivity() {

    @BindView(R.id.editEmailET)
    lateinit var editEmailET: EditText
    @BindView(R.id.txtSubmitTV)
    lateinit var txtSubmitTV: TextView
    @BindView(R.id.emailRL)
    lateinit var emailRL: RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        editTextSignupSelector(editEmailET, emailRL, "")
    }

    @OnClick(R.id.txtSubmitTV)
    fun onClick(view: View) {
        when (view.id) {
            R.id.txtSubmitTV -> performSubmitClick()
        }
    }

    private fun performSubmitClick() {
        if (isValidate())
            if (isNetworkAvailable(mActivity))
                executeForgotPwdRequest()
            else
                showToast(mActivity,getString(R.string.internet_connection_error))
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["email"] = editEmailET.text.toString().trim { it <= ' ' }
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeForgotPwdRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.forgotPwdRequest(mParam())?.enqueue(object : Callback<StatusMsgModel> {

            override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<StatusMsgModel>, response: Response<StatusMsgModel>) {
                dismissProgressDialog()
                val mModel: StatusMsgModel? = response.body()
                if (mModel != null) {
                    if (mModel.status.equals(1)) {
                        showAlertDialogFinish(mActivity, mModel.message)
                    } else {
                        showAlertDialog(mActivity, mModel.message)
                    }
                }
            }
        })
    }

    /*
   * Set up validations
   * */
    private fun isValidate(): Boolean {
        var flag = true
        if (editEmailET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
            flag = false
        } else if (!isValidEmaillId(editEmailET.text.toString().trim { it <= ' ' })) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        }
        return flag
    }

}