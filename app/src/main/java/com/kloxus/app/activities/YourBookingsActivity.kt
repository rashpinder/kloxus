package com.kloxus.app.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.kloxus.app.R
import com.kloxus.app.fragments.OwnerModeFragment
import com.kloxus.app.fragments.UserModeFragment
import com.kloxus.app.utils.OWNER_TAG
import com.kloxus.app.utils.USER_TAG

class YourBookingsActivity : BaseActivity() {

    @BindView(R.id.imgCrossRL)
    lateinit var imgCrossRL: RelativeLayout
    @BindView(R.id.userLL)
    lateinit var userLL: LinearLayout
    @BindView(R.id.ownerLL)
    lateinit var ownerLL: LinearLayout
    @BindView(R.id.containerBookingsFL)
    lateinit var containerBookingsFL: FrameLayout
    @BindView(R.id.txtUserModeTV)
    lateinit var txtUserModeTV: TextView
    @BindView(R.id.txtOwnerModeTV)
    lateinit var txtOwnerModeTV: TextView
    @BindView(R.id.ownerView)
    lateinit var ownerView: View
    @BindView(R.id.userView)
    lateinit var userView: View


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_your_bookings)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        performUserClick()
    }

    @OnClick(
        R.id.imgCrossRL,
        R.id.ownerLL,
        R.id.userLL
    )

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgCrossRL -> performCrossPwdClick()
            R.id.userLL -> performUserClick()
            R.id.ownerLL -> performOwnerClick()
        }
    }

    private fun performOwnerClick() {
        preventMultipleClick()
        switchFragment(OwnerModeFragment(), OWNER_TAG, false, null)
        userView.setBackgroundColor(getResources().getColor(R.color.colorGrey))
        ownerView.setBackgroundColor(getResources().getColor(R.color.black))
        txtOwnerModeTV.setTextColor(getResources().getColor(R.color.black))
        txtUserModeTV.setTextColor(getResources().getColor(R.color.colorGrey))
    }

    private fun performUserClick() {
        preventMultipleClick()
        switchFragment(UserModeFragment(), USER_TAG, false, null)
        userView.setBackgroundColor(getResources().getColor(R.color.black))
        ownerView.setBackgroundColor(getResources().getColor(R.color.colorGrey))
        txtOwnerModeTV.setTextColor(getResources().getColor(R.color.colorGrey))
        txtUserModeTV.setTextColor(getResources().getColor(R.color.black))
    }

    private fun performCrossPwdClick() {
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


}