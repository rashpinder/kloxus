package com.kloxus.app.activities

import LoadMoreReviews
import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.chahinem.pageindicator.PageIndicator
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import com.google.firebase.FirebaseApp
import com.iarcuschin.simpleratingbar.SimpleRatingBar
import com.joooonho.SelectableRoundedImageView
import com.kloxus.app.R
import com.kloxus.app.adapters.ReviewAdapter
import com.kloxus.app.adapters.SlidingImageAdapter
import com.kloxus.app.interfaces.LoadMoreListner
import com.kloxus.app.model.*
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.*
import kotlinx.android.synthetic.main.activity_add_toilet.*
import kotlinx.android.synthetic.main.item_toilet_loc.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.text.DateFormat
import java.util.*
import kotlin.collections.HashMap


class ToiletDetailActivity : BaseActivity(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener,
    LocationListener,
    ResultCallback<LocationSettingsResult> {


    /************************
     * Fused Google Location
     */
    val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
    val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2
    protected val REQUEST_CHECK_SETTINGS = 0x1
    protected val KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates"
    protected val KEY_LOCATION = "location"
    protected val KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string"
    var mLatitude = 0.0
    var mLongitude = 0.0
    protected var mGoogleApiClient: GoogleApiClient? = null
    protected var mLocationRequest: LocationRequest? = null
    protected var mLocationSettingsRequest: LocationSettingsRequest? = null
    protected var mCurrentLocation: Location? = null
    protected var mRequestingLocationUpdates: Boolean? = null
    protected var mLastUpdateTime: String? = null
    private val mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION
    private val mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION

    @BindView(R.id.txtAddressTV)
    lateinit var txtAddressTV: TextView

    @BindView(R.id.imgToiletIV)
    lateinit var imgToiletIV: SelectableRoundedImageView

    @BindView(R.id.txtViewAllTV)
    lateinit var txtViewAllTV: TextView

    @BindView(R.id.ratingLL)
    lateinit var ratingLL: LinearLayout

    @BindView(R.id.txtNameTV)
    lateinit var txtNameTV: TextView

    @BindView(R.id.txtRatingTV)
    lateinit var txtRatingTV: TextView

    @BindView(R.id.txtDescTV)
    lateinit var txtDescTV: TextView

    @BindView(R.id.txtTimeTV)
    lateinit var txtTimeTV: TextView

    @BindView(R.id.txtPriceTV)
    lateinit var txtPriceTV: TextView

    @BindView(R.id.txtBookNowTV)
    lateinit var txtBookNowTV: TextView

    @BindView(R.id.imgBackIV)
    lateinit var imgBackIV: ImageView

    @BindView(R.id.imgLikeIV)
    lateinit var imgLikeIV: ImageView

    @BindView(R.id.imgProfileIV)
    lateinit var imgProfileIV: ImageView

    @BindView(R.id.imgSendIV)
    lateinit var imgSendIV: ImageView
    var paymentMethod: String = "0"
    var city: String=""

    @BindView(R.id.ratingBar)
    lateinit var ratingBar: SimpleRatingBar
    var mModel: ToiletDetailModel? = null
    lateinit var alertDialog: Dialog
    var mToiletId: String = ""
    var rating: String = ""
    var mFavStatus: Boolean = false
    var value: String = ""
    var mOtherUserId: String = ""

    var mItemsList: ArrayList<String> = ArrayList<String>()

    @BindView(R.id.imageRL)
    lateinit var imageRL: RelativeLayout

    @BindView(R.id.mViewPagerVP)
    lateinit var mViewPagerVP: ViewPager

    @BindView(R.id.indicator)
    lateinit var indicator: PageIndicator
    var mImagesArrayList1: ArrayList<String> = ArrayList<String>()
    var mGenderArrayList: ArrayList<String> = ArrayList<String>()
    var mReviewsArrayList: ArrayList<RatingDetail> = ArrayList<RatingDetail>()
    lateinit var mReviewAdapter: ReviewAdapter

    var mPrice: String = ""
    var mCurr: String = ""
    var mAddress: String = ""
    var isPaid: String = ""
    var mTitle: String = ""
    var mAvailable: String = ""
    var mDesc: String = ""
    var mName: String = ""
    var mUser_id: String = ""
    var toilet_id: String = ""

    @BindView(R.id.reviewRV)
    lateinit var reviewRV: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /*************
         * Fused Location
         */
        mRequestingLocationUpdates = false
        mLastUpdateTime = ""
        /*Update values using data stored in the Bundle.*/
        updateValuesFromBundle(savedInstanceState)
        buildGoogleApiClient()
        FirebaseApp.initializeApp(this)
        createLocationRequest()
        buildLocationSettingsRequest()
        if (checkPermission()) {
            checkLocationSettings()
        } else {
            requestPermission()
        }
        updateValuesFromBundle(savedInstanceState)
        setContentView(R.layout.activity_toilet_detail)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        getIntentData()
        getToiletDetail()
        Log.e(TAG, "**oncreate*")
    }


    //Set ViewPager Adapter
    private fun setViewPagerAdapter() {
        val mSlidingImageAdapter =
            SlidingImageAdapter(
                mActivity,
                mImagesArrayList1,
                object : SlidingImageAdapter.AddClick {
                    override fun Click(position: Int) {

                    }
                })

        mViewPagerVP.adapter = mSlidingImageAdapter
        indicator.attachTo(mViewPagerVP)
    }


    private fun getIntentData() {
        if (intent != null) {
            mToiletId = intent.getStringExtra("toilet_id").toString()
            value = intent.getStringExtra("value").toString()

        }
    }


    private fun getToiletDetail() {
        if (isNetworkAvailable(mActivity))
            executeGetToiletDetailRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["toiletID"] = mToiletId
        mMap["timeZone"] = TimeZone.getDefault().id
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetToiletDetailRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getToiletDetailRequest(mParam())
            ?.enqueue(object : Callback<ToiletDetailModel> {
                override fun onFailure(call: Call<ToiletDetailModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<ToiletDetailModel>,
                    response: Response<ToiletDetailModel>
                ) {
                    dismissProgressDialog()
                    mModel = response.body()
                    if (mModel != null) {
                        if (mModel!!.status.equals(1)) {
                            if(mModel!!.data.isAvailable.equals("1")){
                                txtBookNowTV.setBackground(resources.getDrawable(R.drawable.bg_btn))
                            } else {
                                txtBookNowTV.setBackground(resources.getDrawable(R.drawable.bg_grey_btn))
                            }
                            if(mModel!!.data.totalRating.toInt()<=2){
                                txtViewAllTV.isVisible=false
                            }
                            else{
                                txtViewAllTV.isVisible=true
                            }
                            Glide.with(mActivity).load(mModel!!.data.profileImage)
                                .placeholder(R.drawable.ic_ph)
                                .error(R.drawable.ic_ph)
                                .into(imgProfileIV)
                            if (mModel!!.data.isPaid.equals("0")) {
                                txtPriceTV.text = mModel!!.data.price
                            } else {
                                txtPriceTV.text = mModel!!.data.currencySymbol + mModel!!.data.price
                            }
                            mCurr = mModel!!.data.currencySymbol
                            mPrice = mModel!!.data.price
                            mAddress = mModel!!.data.address
                            mName = mModel!!.data.name


                            txtTitleTV.text = mModel!!.data.title
                            txtAddressTV.text = mModel!!.data.address
                            txtTimeTV.text = " " + mModel!!.data.available
//                            txtDescTV.setText(mModel!!.data.description)
                            for (i in mModel!!.data.newGender.indices) {

                                mGenderArrayList.add(mModel!!.data.newGender.get(i))
                            }
                            if (mGenderArrayList.size == 1) {
                                if (mGenderArrayList.contains("1")) {
                                    imgCheckMaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                } else if (mGenderArrayList.contains("2")) {
                                    imgCheckFemaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                } else if (mGenderArrayList.contains("3")) {
                                    imgCheckOtherIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                }
                            } else if (mGenderArrayList.size == 2) {
                                if (mGenderArrayList.contains("3") && mGenderArrayList.contains("1")) {
                                    imgCheckOtherIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                    imgCheckMaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                } else if (mGenderArrayList.contains("3") && mGenderArrayList.contains(
                                        "2"
                                    )
                                ) {
                                    imgCheckOtherIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                    imgCheckFemaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                } else if (mGenderArrayList.contains("1") && mGenderArrayList.contains(
                                        "2"
                                    )
                                ) {
                                    imgCheckMaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                    imgCheckFemaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                }
                            } else {
                                if (mGenderArrayList.contains("1") && mGenderArrayList.contains("2") && mGenderArrayList.contains(
                                        "3"
                                    )
                                ) {
                                    imgCheckMaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                    imgCheckFemaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                    imgCheckOtherIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                }
                            }

//        options = mModel.data.toiletOptions
                            for (i in mModel!!.data.toiletOptions.indices) {

                                mItemsList.add(mModel!!.data.toiletOptions.get(i))
                            }
                            if (mItemsList.contains("3") && mItemsList.contains("2") && mItemsList.contains(
                                    "4"
                                ) && mItemsList.contains("1") && mItemsList.contains("5")
                            ) {
                                imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                            } else if (mItemsList.contains("1") && mItemsList.contains("2") && mItemsList.contains(
                                    "3"
                                ) && mItemsList.contains("4")
                            ) {
                                imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                            } else if (mItemsList.contains("1") && mItemsList.contains("2") && mItemsList.contains(
                                    "3"
                                ) && mItemsList.contains("5")
                            ) {
                                imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//
                            } else if (mItemsList.contains("1") && mItemsList.contains("4") && mItemsList.contains(
                                    "3"
                                ) && mItemsList.contains("5")
                            ) {
                                imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//
                            } else if (mItemsList.contains("2") && mItemsList.contains("4") && mItemsList.contains(
                                    "3"
                                ) && mItemsList.contains("5")
                            ) {
                                imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//
                            } else if (mItemsList.contains("2") && mItemsList.contains("4") && mItemsList.contains(
                                    "1"
                                ) && mItemsList.contains("5")
                            ) {
                                imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//
                            } else if (mItemsList.contains("1") && mItemsList.contains("2") && mItemsList.contains(
                                    "3"
                                )
                            ) {
                                imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//
                            } else if (mItemsList.contains("3") && mItemsList.contains("4") && mItemsList.contains(
                                    "5"
                                )
                            ) {
                                imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//
                            } else if (mItemsList.contains("1") && mItemsList.contains("3") && mItemsList.contains(
                                    "4"
                                )
                            ) {
                                imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//
                            } else if (mItemsList.contains("1") && mItemsList.contains("2") && mItemsList.contains(
                                    "4"
                                )
                            ) {
                                imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//
                            } else if (mItemsList.contains("1") && mItemsList.contains("5") && mItemsList.contains(
                                    "4"
                                )
                            ) {
                                imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//
                            } else if (mItemsList.contains("2") && mItemsList.contains("3") && mItemsList.contains(
                                    "4"
                                )
                            ) {
                                imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//
                            } else if (mItemsList.contains("2") && mItemsList.contains("3") && mItemsList.contains(
                                    "5"
                                )
                            ) {
                                imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//
                            } else if (mItemsList.contains("2") && mItemsList.contains("4") && mItemsList.contains(
                                    "5"
                                )
                            ) {
                                imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//
                            } else if (mItemsList.contains("1") && mItemsList.contains("3") && mItemsList.contains(
                                    "5"
                                )
                            ) {
                                imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                            } else if (mItemsList.contains("1") && mItemsList.contains("2")) {
                                imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                            } else if (mItemsList.contains("1") && mItemsList.contains("3")) {
                                imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                            } else if (mItemsList.contains("1") && mItemsList.contains("4")) {
                                imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))

                            } else if (mItemsList.contains("1") && mItemsList.contains("5")) {
                                imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))

                            } else if (mItemsList.contains("2") && mItemsList.contains("3")) {
                                imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))

                            } else if (mItemsList.contains("2") && mItemsList.contains("4")) {
                                imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))

                            } else if (mItemsList.contains("2") && mItemsList.contains("5")) {
                                imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))

                            } else if (mItemsList.contains("3") && mItemsList.contains("4")) {
                                imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))

                            } else if (mItemsList.contains("3") && mItemsList.contains("5")) {
                                imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))

                            } else if (mItemsList.contains("4") && mItemsList.contains("5")) {
                                imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))

                            } else if (mItemsList.contains("1")) {
                                imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))

                            } else if (mItemsList.contains("2")) {

                                imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                            } else if (mItemsList.contains("3")) {
                                imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))

                            } else if (mItemsList.contains("4")) {
                                imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                            } else if (mItemsList.contains("5")) {
                                imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                            }
//                            if(mModel!!.data.ratingDetails!=null){
                            for (i in mModel!!.data.ratingDetails.indices) {
                                mReviewsArrayList.add(mModel!!.data.ratingDetails.get(i))
                            }

                            ratingBar.starBorderWidth = 1.8F
                            txtRatingTV.text = mModel!!.data.rating
                            ratingBar.rating = mModel!!.data.rating.toFloat()
                            txtNameTV.text = mModel!!.data.name
                            mOtherUserId = mModel!!.data.userID
                            if (mModel!!.data.isFav.equals("1")) {
                                imgLikeIV.setImageResource(R.drawable.ic_fav)
                                mFavStatus = true
                            } else {
                                mFavStatus = false
                                imgLikeIV.setImageResource(R.drawable.ic_unfav)
                            }
                            if (mModel!!.data.toiletImage != "" || mModel!!.data.toiletImage.contains(
                                    "https"
                                )
                            ) {
                                mImagesArrayList1.add(mModel!!.data.toiletImage)
                            }
                            if (mModel!!.data.toiletImage1 != "" || mModel!!.data.toiletImage1.contains(
                                    "https"
                                )
                            ) {
                                mImagesArrayList1.add(mModel!!.data.toiletImage1)
                            }
                            if (mModel!!.data.toiletImage2 != "" || mModel!!.data.toiletImage2.contains(
                                    "https"
                                )
                            ) {
                                mImagesArrayList1.add(mModel!!.data.toiletImage2)
                            }
                            if (mModel!!.data.toiletImage3 != "" || mModel!!.data.toiletImage3.contains(
                                    "https"
                                )
                            ) {
                                mImagesArrayList1.add(mModel!!.data.toiletImage3)
                            }
                            setViewPagerAdapter()
                            setRatingAdapter()
//                            ratingBar.cancelLongPress()
//                            ratingBar.isLongClickable()
                        } else {
                            showAlertDialog(mActivity, mModel?.message)
                        }
                    }
                }
            })
    }

    private fun setRatingAdapter() {
        mReviewAdapter = ReviewAdapter(mActivity, mReviewsArrayList)
        reviewRV.layoutManager = LinearLayoutManager(mActivity)
        reviewRV.adapter = mReviewAdapter
    }

    private fun performBookNowClick() {
        preventMultipleClick()
        if (isUserLogin()) {
            if (isNetworkAvailable(mActivity)) {
                executeCheckStatusApi()
            } else showToast(
                mActivity,
                getString(R.string.internet_connection_error)
            )
        } else {
            showLoginAlertDialog(mActivity, getString(R.string.yiu_need_to_login))
        }
    }

    /*
    * Execute api param
    * */
    private fun mcheckStatusParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["toiletID"] = mModel!!.data.toiletID
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeCheckStatusApi() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getbookingStatusRequest(mcheckStatusParam())
            ?.enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {


                    val mModel: StatusMsgModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            if (mModel.isPaid.equals("0")) {
                                paymentMethod = "0"
                            } else {
                                paymentMethod = "3"
                            }

//                            if (KloxusPrefrences().readString(mActivity, "paymentMethod", null)
//                                    .equals(
//                                        "1"
//                                    )
//                            ) {
//                                executeCardTokenApi()
//                            } else if (KloxusPrefrences().readString(
//                                    mActivity,
//                                    "paymentMethod",
//                                    null
//                                ).equals("2")
//                            ) {
//                                executeBookNowPaypalApi()
//                            } else {
                            executeBookNowApi()
//                            }
                        } else {
                            dismissProgressDialog()
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }


    /*
     * Execute api param
     * */
    private fun mBookParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["toiletID"] = mToiletId
        mMap["address"] = mAddress
//        mMap["paymentMethod"] = KloxusPrefrences().readString(this, "paymentMethod", null)
        mMap["paymentMethod"] = paymentMethod
        mMap["bookingFor"] = mName
        mMap["location"] = KloxusPrefrences().readString(this, SELECTED_LOCATION, null)
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeBookNowApi() {
//        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.bookToiletRequest(mBookParam())
            ?.enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {

                    dismissProgressDialog()
                    val mModel: StatusMsgModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
//                            KloxusPrefrences().writeString(mActivity, "card_id", "")

//                            showToast(mActivity, mModel.message)
//                            finish()
                            if (mModel.isRequestSend.equals("0")) {
                                showAlertDialogFinish(mActivity, mModel.message)
                            } else {
                                val intent =
                                    Intent(mActivity, SelectPaymentMethodActivity::class.java)
                                intent.putExtra("toilet_id", mToiletId)
                                intent.putExtra("mPrice", mPrice)
                                intent.putExtra("mCurr", mCurr)
                                intent.putExtra("book_id", mModel.bookID)
                                intent.putExtra("serviceCharge", mModel.serviceCharge)
                                intent.putExtra("totalPrice", mModel.totalPrice)
//                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                startActivity(intent)
//                                finish()
                            }
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }


//    private fun performBookNowClick() {
//        if (isUserLogin()) {
//            val i = Intent(this, BookNowActivity::class.java)
//            i.putExtra("address", mModel!!.data.address)
//            i.putExtra("price", mModel!!.data.price)
//            i.putExtra("curr", mModel!!.data.currencySymbol)
//            i.putExtra("title", mModel!!.data.title)
//            i.putExtra("available", mModel!!.data.available)
//            i.putExtra("desc", mModel!!.data.description)
//            i.putExtra("name", mModel!!.data.name)
//            i.putExtra("user_id", mModel!!.data.userID)
//            i.putExtra("toilet_id", mModel!!.data.toiletID)
//            i.putExtra("isPaid", mModel!!.data.isPaid)
//            startActivity(i)
//        } else{
//            showLoginAlertDialog(mActivity,getString(R.string.yiu_need_to_login))
//        }


    @OnClick(
        R.id.imgBackIV,
        R.id.txtBookNowTV,
        R.id.imgLikeIV,
        R.id.ratingBar,
        R.id.ratingLL,
        R.id.imgSendIV,
        R.id.txtViewAllTV
    )

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackIV -> performCancelClick()
            R.id.txtBookNowTV -> performBookNowClick()
            R.id.imgLikeIV -> performLikeClick()
            R.id.ratingLL -> performRatingClick()
            R.id.ratingBar -> performRatingClick()
            R.id.imgSendIV -> performMessageClick()
            R.id.txtViewAllTV -> performViewAllClick()
        }
    }

    private fun performViewAllClick() {
        val i = Intent(this, ViewAllReviewsActivity::class.java)
        i.putExtra("toilet_id",mToiletId)
        startActivity(i)
    }

    private fun performMessageClick() {
        if (isUserLogin()) {
            if (isNetworkAvailable(mActivity))
                executeCreateRoomIdRequest()
            else
                showToast(mActivity, getString(R.string.internet_connection_error))
        } else {
            showLoginAlertDialog(mActivity, getString(R.string.yiu_need_to_login))
        }
    }


    private fun mCreateRoomParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["otherUserID"] = mOtherUserId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeCreateRoomIdRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.createRoomRequest(mCreateRoomParam())
            ?.enqueue(object : Callback<CreateRoomModel> {
                override fun onFailure(call: Call<CreateRoomModel>, t: Throwable) {
                    dismissProgressDialog()
                }


                override fun onResponse(
                    call: Call<CreateRoomModel>,
                    response: Response<CreateRoomModel>
                ) {
                    Log.e(TAG, response.body().toString())
                    dismissProgressDialog()
                    val mModel: CreateRoomModel = response.body()!!
                    if (mModel.status == 1) {
                        val mIntent = Intent(mActivity, ChatActivity::class.java)
                        mIntent.putExtra(USER_NAME, mModel.data.name)
                        mIntent.putExtra(ROOM_ID, mModel.data.roomID)
                        startActivity(mIntent)
                    } else if (mModel.status == 0) {
                        showToast(mActivity, mModel.message)
                    }
                }
            })
    }


    private fun performRatingClick() {
//        ratingBar.setRating(mModel!!.data.rating.toFloat())
//        showRatingDialog(mActivity)
    }

    private fun performLikeClick() {
        if (isUserLogin()) {
            if (isNetworkAvailable(mActivity))
                executeFavUnfavRequest()
            else
                showToast(mActivity, getString(R.string.internet_connection_error))
        } else {
            showLoginAlertDialog(mActivity, getString(R.string.yiu_need_to_login))
        }
    }


    /*
     * Execute api param
     * */
    private fun mFavUnfavParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(mActivity, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(mActivity, AUTH_TOKEN, null)
        mMap["toiletID"] = mToiletId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeFavUnfavRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.favUnfavRequest(mFavUnfavParam())
            ?.enqueue(object : Callback<FavUnfavModel> {
                override fun onFailure(call: Call<FavUnfavModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<FavUnfavModel>,
                    response: Response<FavUnfavModel>
                ) {
                    dismissProgressDialog()
                    val mModel: FavUnfavModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            showToast(mActivity, mModel.message)
                            if (mModel.isFav.equals("1")) {
                                imgLikeIV.setImageResource(R.drawable.ic_fav)
                                mFavStatus = true
                            } else {
                                mFavStatus = false
                                imgLikeIV.setImageResource(R.drawable.ic_unfav)
                            }
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }

    private fun performCancelClick() {
        onBackPressed()
    }


    /******************
     * Fursed Google Location
     */
    private fun updateValuesFromBundle(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet()
                    .contains(KEY_REQUESTING_LOCATION_UPDATES)
            ) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                    KEY_REQUESTING_LOCATION_UPDATES
                )
            }
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION)
            }
            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime =
                    savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING)
            }
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the `#addApi` method to request the
     * LocationServices API.
     */
    @Synchronized
    protected fun buildGoogleApiClient() {
        Log.i("", "Building GoogleApiClient")
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
    }

    protected fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    /**
     * Uses a [LocationSettingsRequest.Builder] to build
     * a [LocationSettingsRequest] that is used for checking
     * if a device has the needed location settings.
     */
    protected fun buildLocationSettingsRequest() {
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest).setAlwaysShow(true)
        mLocationSettingsRequest = builder.build()
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * [SettingsApi.checkLocationSettings] method, with the results provided through a `PendingResult`.
     */
    fun checkLocationSettings() {
        val result = LocationServices.SettingsApi.checkLocationSettings(
            mGoogleApiClient,
            mLocationSettingsRequest
        )
        result.setResultCallback(this)
    }

    /**
     * The callback invoked when
     * [SettingsApi.checkLocationSettings] is called. Examines the
     * [LocationSettingsResult] object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    override fun onResult(locationSettingsResult: LocationSettingsResult) {
        val status = locationSettingsResult.status
        when (status.statusCode) {
            LocationSettingsStatusCodes.SUCCESS -> {
                Log.i("", "All location settings are satisfied.")
                startLocationUpdates()
            }
            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                Log.i(
                    "", "Location settings are not satisfied. Show the user a dialog to" +
                            "upgrade location settings "
                )
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS)
                } catch (e: IntentSender.SendIntentException) {
                    Log.i("", "PendingIntent unable to execute request.")
                }
            }
            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(
                "", "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created."
            )
            else -> {
            }
        }
    }


    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected fun startLocationUpdates() {
        try {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
//                && ActivityCompat.checkSelfPermission(
//                    this,
//                    Manifest.permission.ACCESS_BACKGROUND_LOCATION
//                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
            ).setResultCallback { mRequestingLocationUpdates = true }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(TAG, "msg" + e.printStackTrace())
        }
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected fun stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
            mGoogleApiClient,
            this
        ).setResultCallback { mRequestingLocationUpdates = false }
    }


    override fun onStart() {
        super.onStart()
        Log.e(TAG, "onStart")
        if (mGoogleApiClient != null) mGoogleApiClient!!.connect()
    }

    override fun onResume() {
        super.onResume()
        Log.e(TAG, "onResume")
        if (mGoogleApiClient!!.isConnected && mRequestingLocationUpdates!!) {
            startLocationUpdates()
        }
    }


    override fun onPause() {
        super.onPause()
        Log.e(TAG, "onPause")
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient!!.isConnected) {
            stopLocationUpdates()
        }
        //  dismissProgressDialog();
    }

    override fun onStop() {
        super.onStop()
        Log.e(TAG, "onStop")
        mGoogleApiClient!!.disconnect()
        dismissProgressDialog()
    }

    override fun onRestart() {
        super.onRestart()
        Log.e(TAG, "onRestart")
    }


    override fun onConnected(connectionHint: Bundle?) {
        Log.i("", "Connected to GoogleApiClient")
        if (mCurrentLocation == null) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
            mLastUpdateTime = DateFormat.getTimeInstance().format(Date())
        }
    }

    /**
     * Callback that fires when the location changes.
     */
    override fun onLocationChanged(location: Location) {
        mCurrentLocation = location
        mLastUpdateTime = DateFormat.getTimeInstance().format(Date())
        mLatitude = mCurrentLocation!!.latitude
        mLongitude = mCurrentLocation!!.longitude
        Log.e(TAG, "*********LATITUDE********$mLatitude")
        Log.e(TAG, "*********LONGITUDE********$mLongitude")


        //        KloxusPrefrences.writeString(mActivity, KloxusPrefrences.CURRENT_LOCATION_LATITUDE, "" + mLatitude);
//        KloxusPrefrences.writeString(mActivity, KloxusPrefrences.CURRENT_LOCATION_LONGITUDE, "" + mLongitude);
        try {
//            val df = DecimalFormat()
//            df.maximumFractionDigits = 3
            mLatitude = mCurrentLocation!!.latitude
            mLongitude = mCurrentLocation!!.longitude

//            mLatitude = 50.941181
//            mLongitude = 6.972361

            var geocoder: Geocoder? = null
            var addresses: List<Address>? = null
            geocoder = Geocoder(this, Locale.getDefault())
            try {
                addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            if (addresses != null && addresses.size > 0) {
//            val locality = addresses[0].locality
                val state = addresses[0].adminArea
                if (!addresses[0].locality.equals(null) || !addresses[0].locality.equals("")) {
                     city = addresses[0].locality
                } else {
                     city = addresses[0].subAdminArea
                }
                KloxusPrefrences().writeString(mActivity, SELECTED_LOCATION, city)
//            val geocoder: Geocoder
//            val addresses: List<Address>
//            geocoder = Geocoder(this, Locale.getDefault())
//            addresses = geocoder.getFromLocation(
//                mLatitude,
//                mLongitude,
//                1
//            ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//            val address =
//                addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//
//
//            val city = addresses[0].locality
//            KloxusPrefrences().writeString(mActivity, SELECTED_LOCATION, city)
//            val state = addresses[0].adminArea
//            val country = addresses[0].countryName
//            val postalCode = addresses[0].postalCode
//            val knownName = addresses[0].featureName // Only if available else return NULL
                //            KloxusPrefrences.writeString(mActivity, KloxusPrefrences.SELECTED_LOCATION, address);
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onConnectionSuspended(cause: Int) {
        Log.i("", "Connection suspended")
    }

    override fun onConnectionFailed(result: ConnectionResult) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i("", "Connection failed: ConnectionResult.getErrorCode() = " + result.errorCode)
    }

    /**
     * Stores activity data in the Bundle.
     */
    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        savedInstanceState.putBoolean(
            KEY_REQUESTING_LOCATION_UPDATES,
            mRequestingLocationUpdates!!
        )
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation)
        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime)
        super.onSaveInstanceState(savedInstanceState)
    }
    /*****************************************/

    /** */
    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) ACCESS_FINE_LOCATION
     * 2) ACCESS_COARSE_LOCATION
     */
    fun checkPermission(): Boolean {
        val mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation)
        val mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation)
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED
    }

    fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(mAccessFineLocation, mAccessCourseLocation),
            REQUEST_PERMISSION_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_PERMISSION_CODE -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    checkLocationSettings()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    requestPermission()
                }
                return
            }
        }
    }
}