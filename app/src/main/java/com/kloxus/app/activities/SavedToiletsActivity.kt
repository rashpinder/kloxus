package com.kloxus.app.activities

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.kloxus.app.R
import com.kloxus.app.adapters.SavedToiletAdapter
import com.kloxus.app.interfaces.LoadMoreScrollListner
import com.kloxus.app.model.HomeData
import com.kloxus.app.model.FavUnfavModel
import com.kloxus.app.model.GetFavModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.AUTH_TOKEN
import com.kloxus.app.utils.KloxusPrefrences
import com.kloxus.app.utils.USER_ID
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SavedToiletsActivity : BaseActivity(),
    SavedToiletAdapter.OnItemClickListener  {

    lateinit var mSavedToiletsAdapter: SavedToiletAdapter

    @BindView(R.id.savedToiletsRV)
    lateinit var savedToiletsRV: RecyclerView

    @BindView(R.id.imgCrossIV)
    lateinit var imgCrossIV: ImageView

    @BindView(R.id.txtNoDataFountTV)
    lateinit var txtNoDataFountTV: TextView
    private var mPos = 0
    private var mToiletId = ""
    var onItemClickListener: SavedToiletAdapter.OnItemClickListener? = null


    var mSavedToiletsList: ArrayList<HomeData>? = ArrayList()

    @BindView(R.id.mProgressRL)
    lateinit var mProgressRL: RelativeLayout

    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var isLoading: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_saved_toilets)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        onItemClickListener = this

    }

    override fun onResume() {
        super.onResume()
        mPageNo=1
        if (mSavedToiletsList!=null){
            mSavedToiletsList!!.clear()
        }
        getSavedToilets()
    }

    @OnClick(R.id.imgCrossIV)

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgCrossIV -> performCrossPwdClick()
        }
    }

    private fun performCrossPwdClick() {
        onBackPressed()
    }


    private fun getSavedToilets() {
        if (isNetworkAvailable(mActivity))
            executeGetSavedToiletRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetSavedToiletRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getAllfavUnfavRequest(mParam())
            ?.enqueue(object : Callback<GetFavModel> {
                override fun onFailure(call: Call<GetFavModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<GetFavModel>,
                    response: Response<GetFavModel>
                ) {
                    dismissProgressDialog()
                    val mModel: GetFavModel? = response.body()
                    if (mModel!!.status.equals(1)) {
                        isLoading = !mModel.lastPage.equals(true)
                        mSavedToiletsList = mModel.data
                        setAllToiletAdapter()
                        txtNoDataFountTV.visibility = View.GONE
                    } else {
//                        showAlertDialog(mActivity, mModel.message)
                        txtNoDataFountTV.visibility = View.VISIBLE
                        txtNoDataFountTV.text = mModel.message
                    }
                }
            })
    }


    var mLoadMoreScrollListner: LoadMoreScrollListner = object : LoadMoreScrollListner {
        override fun onLoadMoreListner(mModel: HomeData) {
            if (isLoading) {
                ++mPageNo
                executeMoreAccordingCatIdRequest()
            }
        }
    }


    /*
     * Execute api param
     * */
    private fun mLoadMoreParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeMoreAccordingCatIdRequest() {
        mProgressRL.visibility = View.VISIBLE
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getAllfavUnfavRequest(mLoadMoreParam())
            ?.enqueue(object : Callback<GetFavModel> {
                override fun onFailure(call: Call<GetFavModel>, t: Throwable) {
                    dismissProgressDialog()
                    mProgressRL.visibility = View.GONE
                }

                override fun onResponse(
                    call: Call<GetFavModel>,
                    response: Response<GetFavModel>
                ) {
                    mProgressRL.visibility = View.GONE
                    var mGetFavModel = response.body()!!
                    if (mGetFavModel.status == 1) {
                        mGetFavModel.data.let {
                            mSavedToiletsList!!.addAll<HomeData>(
                                it
                            )
                        }
                        mSavedToiletsAdapter.notifyDataSetChanged()
                        isLoading = !mGetFavModel.lastPage.equals(true)
                    } else if (mGetFavModel.status == 0) {
//                    showToast(mActivity, mGetFavModel.message)
                    }
                }
            })
    }

    private fun setAllToiletAdapter() {
        mSavedToiletsAdapter = SavedToiletAdapter(
            mActivity,
            mSavedToiletsList,
            onItemClickListener,
            mLoadMoreScrollListner
        )
        savedToiletsRV.layoutManager = LinearLayoutManager(mActivity)
        savedToiletsRV.adapter = mSavedToiletsAdapter
    }

    override fun onItemClick(positon: Int, toilet_id: String) {
        this.mPos = positon
        this.mToiletId = toilet_id
        performFavUnFavClick(mPos)
    }

    private fun performFavUnFavClick(mPos: Int) {
        if (isNetworkAvailable(mActivity))
            executeFavUnFavRequest(mPos)
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }

    /*
     * Execute api param
     * */
    private fun mFavUnfavParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["toiletID"] = mToiletId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeFavUnFavRequest(mPos: Int) {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.favUnfavRequest(mFavUnfavParam())
            ?.enqueue(object : Callback<FavUnfavModel> {
                override fun onFailure(call: Call<FavUnfavModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<FavUnfavModel>,
                    response: Response<FavUnfavModel>
                ) {
                    dismissProgressDialog()
                    val mModel: FavUnfavModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            showToast(mActivity, mModel.message)
                            mSavedToiletsList!!.removeAt(mPos)
                            mSavedToiletsAdapter.notifyDataSetChanged()
                            if (mSavedToiletsList!!.size > 0) {
                                savedToiletsRV.visibility = View.VISIBLE
                                txtNoDataFountTV.visibility = View.GONE
                            } else {
                                savedToiletsRV.visibility = View.GONE
                                txtNoDataFountTV.visibility = View.VISIBLE
                            }
                        } else if (mModel.status == 0) {
                            showToast(mActivity, mModel.message)
                        }
                        else{
                            showToast(mActivity, mModel.message)
                        }
                    }
                }
            })
    }
}