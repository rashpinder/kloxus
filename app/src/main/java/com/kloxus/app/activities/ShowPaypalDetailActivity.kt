package com.kloxus.app.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.kloxus.app.R
import com.kloxus.app.model.PaypalLoginModel
import com.kloxus.app.model.ShowBankDetailsModel
import com.kloxus.app.model.ShowPaypalDetailModel
import com.kloxus.app.model.StatusMsgModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.AUTH_TOKEN
import com.kloxus.app.utils.KloxusPrefrences
import com.kloxus.app.utils.USER_ID
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ShowPaypalDetailActivity : BaseActivity() {


    @BindView(R.id.nameTV)
    lateinit var nameTV: TextView

    @BindView(R.id.emailTV)
    lateinit var emailTV: TextView

    @BindView(R.id.stateTV)
    lateinit var stateTV: TextView

    @BindView(R.id.cityTV)
    lateinit var cityTV: TextView

    @BindView(R.id.countryTV)
    lateinit var countryTV: TextView

    @BindView(R.id.addTV)
    lateinit var addTV: TextView

    @BindView(R.id.postalCodeTV)
    lateinit var postalCodeTV: TextView

//    @BindView(R.id.stateTV)
//    lateinit var stateTV: TextView

    @BindView(R.id.txtUpdateTV)
    lateinit var txtUpdateTV: TextView

    @BindView(R.id.imgBackIV)
    lateinit var imgBackIV: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_paypal_detail)
        ButterKnife.bind(this)
        setStatusBar(mActivity)

    }


    override fun onResume() {
        super.onResume()
        getBankDetails()
    }


    @OnClick(
        R.id.imgBackIV,
        R.id.txtUpdateTV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackIV -> performBackClick()
            R.id.txtUpdateTV -> performUpdateClick()
        }
    }



    private fun performUpdateClick() {
        if (isNetworkAvailable(mActivity))
            executeConnectPaypalRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }

    private fun executeConnectPaypalRequest() {
        showProgressDialog(mActivity)
        val userID: RequestBody = (KloxusPrefrences().readString(
            mActivity,
            USER_ID,
            null
        ).toString())
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val authToken: RequestBody = (KloxusPrefrences().readString(
            mActivity,
            AUTH_TOKEN,
            null
        ).toString())
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())

        val mApiInterface: ApiInterface? =
            ApiClient().getApiClient()?.create(ApiInterface::class.java)
        val mCall: Call<PaypalLoginModel?>? = mApiInterface?.paypalLoginRequest(
            userID,
            authToken
        )

        mCall?.enqueue(object : Callback<PaypalLoginModel?> {
            override fun onFailure(call: Call<PaypalLoginModel?>, t: Throwable) {
                dismissProgressDialog()
                Log.e(TAG, "onFailure: " + t.localizedMessage)
            }

            override fun onResponse(
                call: Call<PaypalLoginModel?>,
                response: Response<PaypalLoginModel?>
            ) {
                dismissProgressDialog()
                val mModel: PaypalLoginModel? = response.body()
                if (mModel != null) {
                    if (mModel.status.equals(1)) {
                        val i = Intent(mActivity, PaypalWebviewActivity::class.java)
                        i.putExtra("url",mModel.login_url)
                        startActivity(i)
                    } else {
                        showAlertDialog(mActivity, "Error, Please try again later")
                    }
                }
            }
        })
    }



//    private fun performDeleteClick() {
//        if (isNetworkAvailable(mActivity))
//            executeDeleteBankDetailsRequest()
//        else
//            showToast(mActivity, getString(R.string.internet_connection_error))
//
//    }

//    private fun mDeleteParams(): MutableMap<String?, String?> {
//        val mMap: MutableMap<String?, String?> = HashMap()
//        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
//        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
//        mMap["accountID"] = acc_id
//        Log.e("params", "**PARAM**$mMap")
//        return mMap
//    }
//
//    private fun executeDeleteBankDetailsRequest() {
//        showProgressDialog(this)
//        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
//        mApiInterface?.deleteBankDetailsRequest(mDeleteParams())
//            ?.enqueue(object : Callback<StatusMsgModel> {
//                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
//                    dismissProgressDialog()
//                }
//
//                override fun onResponse(
//                    call: Call<StatusMsgModel>,
//                    response: Response<StatusMsgModel>
//                ) {
//                    dismissProgressDialog()
//                    val mModel: StatusMsgModel? = response.body()
//                    if (mModel != null) {
//                        if (mModel.status.equals(1)) {
//                            showToast(mActivity, mModel.message)
//                            finish()
//                        } else {
//                            showAlertDialog(mActivity, mModel.message)
//                        }
//                    }
//                }
//            })
//    }


    private fun getBankDetails() {
        if (isNetworkAvailable(mActivity))
            executeGetBankDetailsRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }

    private fun mPaypalParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetBankDetailsRequest() {
        showProgressDialog(this)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getPaypalDetailsRequest(mPaypalParams())
            ?.enqueue(object : Callback<ShowPaypalDetailModel> {
                override fun onFailure(call: Call<ShowPaypalDetailModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<ShowPaypalDetailModel>,
                    response: Response<ShowPaypalDetailModel>
                ) {
                    dismissProgressDialog()
                    val mModel: ShowPaypalDetailModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            nameTV.text = mModel.data.paypalName
                            emailTV.text = mModel.data.paypalEmail
                            cityTV.text = mModel.data.paypalCity
                            countryTV.text = mModel.data.paypalCountry
                            addTV.text = mModel.data.paypalStreet1
                            stateTV.text = mModel.data.paypalState
                            postalCodeTV.text = mModel.data.paypalPostalCode
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }

    private fun performBackClick() {
        onBackPressed()
    }

}