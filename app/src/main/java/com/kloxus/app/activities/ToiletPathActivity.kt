package com.kloxus.app.activities

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentActivity
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.firebase.FirebaseApp
import com.google.maps.DirectionsApi
import com.google.maps.GeoApiContext
import com.google.maps.android.PolyUtil
import com.google.maps.model.DirectionsResult
import com.google.maps.model.TravelMode
import com.kloxus.app.R
import com.kloxus.app.model.ToiletDetailModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.*
import de.hdodenhof.circleimageview.CircleImageView
import org.joda.time.DateTime
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import java.util.concurrent.TimeUnit


class ToiletPathActivity : FragmentActivity(), OnMapReadyCallback, LocationListener,
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
    ResultCallback<LocationSettingsResult>,GoogleMap.OnMarkerClickListener,GoogleMap.InfoWindowAdapter{
    var progressDialog: Dialog? = null

    lateinit var result: DirectionsResult
    @BindView(R.id.imgBackRL)
    lateinit var imgBackRL: RelativeLayout
    @BindView(R.id.imgMapRL)
    lateinit var imgMapRL: RelativeLayout

    @BindView(R.id.icBackIV)
    lateinit var icBackIV: ImageView

    @BindView(R.id.txtMakePaymentTV)
    lateinit var txtMakePaymentTV: TextView


    private var mMap: GoogleMap? = null
    var mLastLocation: Location? = null
    var mCurrMarker: Marker? = null
    var mdestMarker: Marker? = null
    var mGoogleApiClient: GoogleApiClient? = null
    var mLocationRequest: LocationRequest? = null
    var mapView: View? = null
    var mToiletDetailModel: ToiletDetailModel? = null
    var mLatitude: String = ""
    var mLongitude: String = ""
    var price: String = ""
    var rating: String = ""
    var city: String = ""
    var totalPrice: String = ""
    var serviceCharge: String = ""

    //    var user_city: String = ""
    var city_dest: String = ""
    var toilet_title: String = ""
    var mToiletID: String = ""
    var book_id: String = ""
    var height = 80
    var width = 80
    var mDestinationLat = 0.0
    var mDestinationLong = 0.0
    var mDoubleLatitude = 0.0
    var mDoubleLongitude = 0.0
//    var mMapLatitude = 0.0
//    var mMapLongitude = 0.0
    var mapFragment: SupportMapFragment? = null
    private val mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION
    private val mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION
    val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
    protected val REQUEST_CHECK_SETTINGS = 0x1
    protected val KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates"
    protected val KEY_LOCATION = "location"
    protected val KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string"
    protected var mLocationSettingsRequest: LocationSettingsRequest? = null
    protected var mCurrentLocation: Location? = null
    protected var mRequestingLocationUpdates: Boolean? = null
    protected var mLastUpdateTime: String? = null

    //     var gccd: Geocoder? = null
    var toilet_id: String = ""
    var mDefault: Int = 0
    var mPrice: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mRequestingLocationUpdates = false
        mLastUpdateTime = ""
        buildGoogleApiClient()
        FirebaseApp.initializeApp(this)
        createLocationRequest()
        buildLocationSettingsRequest()
        setContentView(R.layout.activity_toilet_path)
        ButterKnife.bind(this)
        setStatusBar(this)
        getIntentData()

        mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapView = mapFragment?.view
        mapFragment!!.getMapAsync(this)
        if (Build.VERSION.SDK_INT > 9) {
            val policy = ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
        }
//        mMap!!.setOnMarkerClickListener(this)

    }



    private fun getToiletDetail() {
        if (isNetworkAvailable(this))
            executeGetToiletDetailRequest()
        else
            showToast(this, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mToiletDetailParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["toiletID"] = mToiletID
        mMap["bookID"] =book_id
        mMap["timeZone"] = TimeZone.getDefault().id
        Log.e("msg", "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetToiletDetailRequest() {
        showProgressDialog(this)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getToiletDetailRequest(mToiletDetailParam())
            ?.enqueue(object : Callback<ToiletDetailModel> {
                override fun onFailure(call: Call<ToiletDetailModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<ToiletDetailModel>,
                    response: Response<ToiletDetailModel>
                ) {
                    dismissProgressDialog()
                    mToiletDetailModel = response.body()
//                    if (mToiletDetailModel != null) {
                    if (mToiletDetailModel!!.status.equals(1)) {
                        mDestinationLat = mToiletDetailModel!!.data.lat.toDouble()
                        mDestinationLong = mToiletDetailModel!!.data.log.toDouble()
                        toilet_id = mToiletDetailModel!!.data.toiletID
                        mPrice = mToiletDetailModel!!.data.price
                        totalPrice = mToiletDetailModel!!.data.totalPrice
                        serviceCharge = mToiletDetailModel!!.data.serviceCharge

                        if (mToiletDetailModel!!.data.paymentStatus == "1") {
                            txtMakePaymentTV.isVisible = false
                        } else {
                            txtMakePaymentTV.isVisible = true
                        }

                        mMap!!.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                LatLng(
                                    mDoubleLatitude,
                                    mDoubleLongitude
                                ), 9f
                            )
                        )
                        val gccd = Geocoder(applicationContext, Locale.getDefault())

                        val user_add: List<Address> =
                            gccd.getFromLocation(mDoubleLatitude, mDoubleLongitude, 1)
                        if (user_add.isNotEmpty()) {

                            city = user_add[0].getAddressLine(0)
                            if (city.contains("Unnamed road")) {
                                city = user_add[0].subAdminArea

                            }
                        } else {
                            // do your stuff
                        }

                        val now = DateTime()
//                        var now: DateTime = DateTime()
                        mCurrMarker = mMap?.let {
                            CreateCurrentLocMarker(
                                it
                            )
                        }

                        mCurrMarker!!.tag = "Current_loc"
                        mdestMarker = mMap?.let {
                            CreateToiletMarker(
                                it
                            )
                        }
                        mdestMarker!!.tag = "Dest_loc"

                        result=
                            DirectionsApi.newRequest(getGeoContext()).mode(TravelMode.DRIVING)
                                .origin(
                                    city
                                )
                                .destination(mToiletDetailModel!!.data.address).departureTime(now)
                                .await()


                        addPolyline(result, mMap!!)


//                            addMarkersToMap(result, mMap!!)

//                        mMap!!.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
//                            // Use default InfoWindow frame
//                            override fun getInfoWindow(arg0: Marker): View? {
//                                return null
//                            }
//
//                            // Defines the contents of the InfoWindow
//                            override fun getInfoContents(arg0: Marker): View {
//                                // Getting view from the layout file info_window_layout
//                                val v: View = layoutInflater.inflate(
//                                    R.layout.item_path_window,
//                                    null
//                                )
//
//                                // Getting the position from the marker
//                                val latLng = arg0.position
//
//                                // Getting reference to the TextView to set latitude
//                                val imgDotIV =
//                                    v.findViewById<View>(R.id.imgDotIV) as CircleImageView
//
//                                // Getting reference to the TextView to set longitude
//                                val txtTitleTV =
//                                    v.findViewById<View>(R.id.txtTitleTV) as TextView
//
//                                val txtLocTV =
//                                    v.findViewById<View>(R.id.txtLocTV) as TextView
//
//                                if (arg0.tag.equals("Current_loc")) {
//                                    txtLocTV.text = city
//                                    txtTitleTV.text = getString(R.string.user_address)
//                                    txtTitleTV.setTextColor(resources.getColor(R.color.colorGreen))
//                                    imgDotIV.setColorFilter(resources.getColor(R.color.colorGreen))
//                                } else {
////                                        txtLocTV.isVisible = false
//                                    txtTitleTV.text =
//                                        getString(R.string.toilet) + ":" + mToiletDetailModel!!.data.toiletLocation
//                                    txtLocTV.text = getEndLocationTitle(result)
//                                    txtTitleTV.setTextColor(resources.getColor(R.color.colorRed))
//                                    imgDotIV.setColorFilter(resources.getColor(R.color.colorRed))
//                                }
//
//                                // Returning the view containing InfoWindow contents
//                                return v
//                            }
//                        })

                        onMarkerClick(mCurrMarker!!)
                        mMap!!.setInfoWindowAdapter(this@ToiletPathActivity)

                    } else {
                        showAlertDialog(this@ToiletPathActivity, mToiletDetailModel?.message)
                    }
                }
            })
    }

//fun getAddress(
//    ctx: Context?,
//    lat: Double,
//    lng: Double
//): String? {
//    var fullAdd: String? = null
//    try {
//        val geocoder = Geocoder(ctx, Locale.getDefault())
//        val addresses = geocoder.getFromLocation(lat, lng, 1)
//        if (addresses.size > 0) {
//            val address = addresses[0]
//            fullAdd = address.getAddressLine(0)
//
//            // if you want only city or pin code use following code //
//            /* String Location = address.getLocality();
//        String zip = address.getPostalCode();
//        String Country = address.getCountryName(); */
//        }
//    } catch (ex: IOException) {
//        ex.printStackTrace()
//    }
//    return fullAdd
//}

    private fun addPolyline(results: DirectionsResult, mMap: GoogleMap) {
        val decodedPath: List<LatLng> =
            PolyUtil.decode(results.routes[0].overviewPolyline.encodedPath)
        mMap.addPolyline(PolylineOptions().addAll(decodedPath))
    }

    private fun getGeoContext(): GeoApiContext? {
        val geoApiContext = GeoApiContext()
        return geoApiContext.setQueryRateLimit(10).setApiKey(getString(R.string.google_map_api_key))
            .setConnectTimeout(5, TimeUnit.MINUTES).setReadTimeout(5, TimeUnit.MINUTES)
            .setWriteTimeout(5, TimeUnit.MINUTES)
    }

    private fun getEndLocationTitle(results: DirectionsResult): String? {
        return "Time Left: " + results.routes[0].legs[0].duration.humanReadable + "," + " Distance : " + results.routes[0].legs[0].distance.humanReadable
    }


    private fun getIntentData() {
        if (intent != null) {
            mToiletID = intent.getStringExtra("toilet_id").toString()
            book_id = intent.getStringExtra("book_id").toString()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap!!.uiSettings.isCompassEnabled = false
        mMap!!.uiSettings.isMapToolbarEnabled = false
        if (mapView != null &&
            mapView!!.findViewById<View?>("1".toInt()) != null
        ) {

            // Get the button view
            val locationButton =
                (mapView!!.findViewById<View>("1".toInt()).parent as View).findViewById<View>("2".toInt())
            // and next place it, on bottom right (as Google Maps app)
            val layoutParams = locationButton.layoutParams as RelativeLayout.LayoutParams
//            locationButton.setBackgroundResource(R.drawable.ic_lloc)login

            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
            layoutParams.setMargins(0, 0, 30, 30)
        }
        //Custom Theme
        val style = MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style_dark)
        mMap!!.setMapStyle(style)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
                == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
                == PackageManager.PERMISSION_GRANTED
            ) {
                checkLocationSettings()
//                buildGoogleApiClient()
                mMap!!.isMyLocationEnabled = true


            } else {
                requestPermission()
            }
        } else {
//            buildGoogleApiClient()
            mMap!!.isMyLocationEnabled = true
        }
//        getToiletDetail()
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(mAccessFineLocation, mAccessCourseLocation),
            REQUEST_PERMISSION_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_PERMISSION_CODE -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    checkLocationSettings()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    requestPermission()
                }
                return
            }
        }
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * [SettingsApi.checkLocationSettings] method, with the results provided through a `PendingResult`.
     */
    fun checkLocationSettings() {
        val result = LocationServices.SettingsApi.checkLocationSettings(
            mGoogleApiClient,
            mLocationSettingsRequest
        )
        result.setResultCallback(this)
    }

    /**
     * The callback invoked when
     * [SettingsApi.checkLocationSettings] is called. Examines the
     * [LocationSettingsResult] object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    override fun onResult(locationSettingsResult: LocationSettingsResult) {
        val status = locationSettingsResult.status
        when (status.statusCode) {
            LocationSettingsStatusCodes.SUCCESS -> {
                Log.i("", "All location settings are satisfied.")
                startLocationUpdates()
            }
            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                Log.i(
                    "", "Location settings are not satisfied. Show the user a dialog to" +
                            "upgrade location settings "
                )
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(this, REQUEST_CHECK_SETTINGS)
                } catch (e: IntentSender.SendIntentException) {
                    Log.i("", "PendingIntent unable to execute request.")
                }
            }
            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(
                "", "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created."
            )
            else -> {
            }
        }
    }


    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected fun startLocationUpdates() {
        try {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
            ).setResultCallback { mRequestingLocationUpdates = true }
        } catch (e: Exception) {
            e.printStackTrace()
//            Log.e(TAG, "msg" + e.printStackTrace())
        }
    }

    /******************
     * Fursed Google Location
     */
    private fun updateValuesFromBundle(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet()
                    .contains(KEY_REQUESTING_LOCATION_UPDATES)
            ) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                    KEY_REQUESTING_LOCATION_UPDATES
                )
            }
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION)
            }
            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime =
                    savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING)
            }
        }
    }

    protected fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    /**
     * Uses a [LocationSettingsRequest.Builder] to build
     * a [LocationSettingsRequest] that is used for checking
     * if a device has the needed location settings.
     */
    protected fun buildLocationSettingsRequest() {
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest).setAlwaysShow(true)
        mLocationSettingsRequest = builder.build()
    }

    /** */
    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) ACCESS_FINE_LOCATION
     * 2) ACCESS_COARSE_LOCATION
     */
    fun checkPermission(): Boolean {
        val mlocationFineP = ContextCompat.checkSelfPermission(this, mAccessFineLocation)
        val mlocationCourseP = ContextCompat.checkSelfPermission(this, mAccessCourseLocation)
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED
    }



    protected fun CreateCurrentLocMarker(
        googleMap: GoogleMap
    ): Marker? {
        return googleMap.addMarker(
            MarkerOptions()
                .position(LatLng(mDoubleLatitude, mDoubleLongitude))
                .anchor(0.5f, 0.5f)
//                .title("User Address\n"+toilet_title)
                .icon(getBitmapFromVector(this, R.drawable.ic_user_marker))
        )


    }

    protected fun CreateToiletMarker(
        googleMap: GoogleMap
    ): Marker? {
        return googleMap.addMarker(
            MarkerOptions()
                .position(LatLng(mDestinationLat, mDestinationLong))
                .anchor(0.5f, 0.5f)
//                .title("Toilet")
                .icon(getBitmapFromVector(this, R.drawable.ic_marker))

        )
    }


    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API).build()
        with(mGoogleApiClient) { this!!.connect() }
    }

    override fun onConnected(bundle: Bundle?) {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = 1000
        mLocationRequest!!.fastestInterval = 1000
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
            )
        }
    }

    override fun onConnectionSuspended(i: Int) {}

    override fun onLocationChanged(location: Location) {
        if (location != null) {
            mLastLocation = location

            //Place current location marker
            val latLng = LatLng(location.latitude, location.longitude)
            Log.e("latlng", "**latlng**$latLng")
            mLatitude = location.latitude.toString()
            mLongitude = location.longitude.toString()
            mDoubleLatitude = location.latitude
            mDoubleLongitude = location.longitude
            Log.e("latlng", "**latlng**$mLatitude")
            Log.e("latlng", "**latlng**$mLongitude")
            KloxusPrefrences().writeString(this, CURR_LAT, mLatitude)
            KloxusPrefrences().writeString(this, CURR_LNG, mLongitude)
            //move map camera
//        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
//        mMap!!.animateCamera(CameraUpdateFactory.zoomTo(11f))

            //stop location updates
            if (mGoogleApiClient != null) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
            }

            getToiletDetail()
//        if (mToiletID != "") {

//        } else {
//        }

        }
    }

    override fun onPause() {
        super.onPause()
        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
        }
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult?) {}

    fun getBitmapFromVector(
        context: Context,
        @DrawableRes vectorResourceId: Int
    ): BitmapDescriptor? {
        val vectorDrawable = ResourcesCompat.getDrawable(
            context.resources, vectorResourceId, null
        )
        if (vectorDrawable == null) {
//            Log.e(TAG, "Requested vector resource was not found")
            return BitmapDescriptorFactory.defaultMarker()
        }
        val bitmap = Bitmap.createBitmap(
            80,
            100, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
//        DrawableCompat.setTint(vectorDrawable)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    /*
     Transparent status bar
      */
    fun setStatusBar(mActivity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mActivity.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            // edited here
            mActivity.window.statusBarColor = resources.getColor(R.color.white)
        }
    }

/*
 * Show Progress Dialog
 * */

    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)
            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        progressDialog!!.show()
//        if (progressDialog != null) progressDialog!!.show()
    }


    /*
     * Hide Progress Dialog
     * */
    fun dismissProgressDialog() {
//        if (progressDialog != null && progressDialog!!.isShowing) {
//            progressDialog!!.dismiss()
//        }
        try {
            if (this.progressDialog != null && this.progressDialog!!.isShowing) {
                this.progressDialog!!.dismiss()
            }
        } catch (e: IllegalArgumentException) {
            // Handle or log or ignore
        } catch (e: Exception) {
            // Handle or log or ignore
        } finally {
            this.progressDialog = null
        }
    }


    /*
     * Check Internet Connections
     * */
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }


    /*
     * Toast Message
     * */
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }


    /*
     *
     * Error Alert Dialog
     * */
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

    /*
     *
     * Error Alert Dialog FinishActivity
     * */
    fun showAlertDialogFinish(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            mActivity.finish()
        }
        alertDialog.show()
    }


    @OnClick(
        R.id.imgBackRL,
        R.id.txtMakePaymentTV,
        R.id.imgMapRL

    )

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackRL -> performCancelClick()
            R.id.txtMakePaymentTV -> performMakePaymentClick()
            R.id.imgMapRL -> performMapClick()
        }
    }

    private fun performMapClick() {
        val uri =
            "http://maps.google.com/maps?saddr=" + mDoubleLatitude + "," + mDoubleLongitude + "&daddr=" + mDestinationLat + "," + mDestinationLong
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        startActivity(intent)
//        val uri = java.lang.String.format(Locale.ENGLISH, "geo:%f,%f", mMapLatitude, mMapLongitude)
//        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
//       startActivity(intent)
    }

    private fun performMakePaymentClick() {
        val i = Intent(this, SelectPaymentMethodActivity::class.java)
        i.putExtra("toilet_id", toilet_id)
        i.putExtra("mPrice", mPrice)
        i.putExtra("book_id", book_id)
        i.putExtra("totalPrice", book_id)
        i.putExtra("serviceCharge", serviceCharge)
        i.putExtra("totalPrice",totalPrice)
        startActivity(i)
        finish()

    }


    private fun performCancelClick() {
        onBackPressed()
    }

    override fun onMarkerClick(arg0: Marker): Boolean {
        getInfoContents(mCurrMarker!!)
//        Toast.makeText(this,"mhkgmhgnhjgnh",Toast.LENGTH_LONG).show();
        return true
    }

    override fun getInfoWindow(arg0: Marker): View? {
        return null
    }

    override fun getInfoContents(arg0: Marker): View? {
        // Getting view from the layout file info_window_layout
        val v: View = layoutInflater.inflate(
            R.layout.item_path_window,
            null
        )

        // Getting the position from the marker
        val latLng = arg0.position

        // Getting reference to the TextView to set latitude
        val imgDotIV =
            v.findViewById<View>(R.id.imgDotIV) as CircleImageView

        // Getting reference to the TextView to set longitude
        val txtTitleTV =
            v.findViewById<View>(R.id.txtTitleTV) as TextView

        val txtLocTV =
            v.findViewById<View>(R.id.txtLocTV) as TextView

        if (arg0.tag.equals("Current_loc")) {
            txtLocTV.text = city
            txtTitleTV.text = getString(R.string.user_address)
            txtTitleTV.setTextColor(resources.getColor(R.color.colorGreen))
            imgDotIV.setColorFilter(resources.getColor(R.color.colorGreen))
//            mMapLatitude=mDoubleLatitude
//            mMapLongitude=mDoubleLongitude
        } else {
//                                        txtLocTV.isVisible = false
            txtTitleTV.text =
                getString(R.string.toilet) + ":" + mToiletDetailModel!!.data.toiletLocation
            txtLocTV.text = getEndLocationTitle(result)
            txtTitleTV.setTextColor(resources.getColor(R.color.colorRed))
            imgDotIV.setColorFilter(resources.getColor(R.color.colorRed))
//            mMapLatitude=mDestinationLat
//            mMapLongitude=mDestinationLong
        }

        // Returning the view containing InfoWindow contents
        return v
    }

//    override fun onMarkerClick(p0: Marker): Boolean {
//return true
//    }

}