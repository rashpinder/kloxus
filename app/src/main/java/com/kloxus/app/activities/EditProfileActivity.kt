package com.kloxus.app.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.kloxus.app.R
import com.kloxus.app.model.EditProfileModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.*
import com.rilixtech.widget.countrycodepicker.Country
import com.rilixtech.widget.countrycodepicker.CountryCodePicker
import com.rilixtech.widget.countrycodepicker.CountryCodePicker.OnCountryChangeListener
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class EditProfileActivity : BaseActivity() {

    @BindView(R.id.passRL)
    lateinit var passRL: RelativeLayout

    @BindView(R.id.profileFL)
    lateinit var profileFL: FrameLayout

    @BindView(R.id.imgCrossIV)
    lateinit var imgCrossIV: ImageView

    @BindView(R.id.editEmailET)
    lateinit var editEmailET: TextView

    @BindView(R.id.editFirstNameET)
    lateinit var editFirstNameET: EditText

    @BindView(R.id.editLastNameET)
    lateinit var editLastNameET: EditText

    @BindView(R.id.editPhoneET)
    lateinit var editPhoneET: EditText

    @BindView(R.id.imgProfileIV)
    lateinit var imgProfileIV: CircleImageView

    @BindView(R.id.txtSaveTV)
    lateinit var txtSaveTV: TextView
    @BindView(R.id.editPasswordET)
    lateinit var editPasswordET: TextView

    @BindView(R.id.firstNameRL)
    lateinit var firstNameRL: RelativeLayout

    @BindView(R.id.lastNameRL)
    lateinit var lastNameRL: RelativeLayout
    @BindView(R.id.phoneRL)
    lateinit var phoneRL: RelativeLayout
    @BindView(R.id.ccp)
    lateinit var ccp: CountryCodePicker
    @BindView(R.id.txtVerifiedTV)
    lateinit var txtVerifiedTV: TextView

    var mBitmap: Bitmap? = null
    var value: String =""
    var countryCode = ""
    var phoneNumber = ""
    /*
        * Initialize Menifest Permissions:
        * & Camera Gallery Request @params
        * */
    var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    var writeCamera = Manifest.permission.CAMERA

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        getIntentData()
        getProfileDetail()
        editTextSignupSelector(editFirstNameET, firstNameRL, "")
        editTextSignupSelector(editLastNameET, lastNameRL, "")
        editTextSignupSelector(editPhoneET, phoneRL, "")

        /*Set Country Code Picker*/
        setCountryCodePicker()
    }


    fun setCountryCodePicker() {
        hideCloseKeyboard(mActivity)
        ccp.enableHint(false)
//        ccp.setTextColor(resources.getColor(R.color.white))
        ccp.registerPhoneNumberTextView(editPhoneET)
//        ccp.setDefaultCountryUsingNameCodeAndApply("US")
        Log.e(TAG, "Code== " + ccp.getSelectedCountryCodeWithPlus())

        if (KloxusPrefrences().readString(mActivity, COUNTRY_CODE,null)!=null&&KloxusPrefrences().readString(mActivity, COUNTRY_CODE,null)!=""){
        KloxusPrefrences().readString(mActivity, COUNTRY_CODE,null)?.let {
            ccp.setCountryForPhoneCode(
                it.toInt())
        }
            countryCode = KloxusPrefrences().readString(mActivity, COUNTRY_CODE,null).toString()
        }
        else{
            countryCode = ccp.getSelectedCountryCodeWithPlus()
        }

        ccp.setOnCountryChangeListener(object : OnCountryChangeListener {
            override fun onCountrySelected(selectedCountry: Country) {
                countryCode = ccp.getSelectedCountryCode()
            }
        })
    }

    override fun onResume() {
        super.onResume()

    }
    private fun getIntentData() {
        if (intent != null) {
            value = intent.getStringExtra("value").toString()

        }
    }
    fun showCursorAtEnd(mEditText: EditText){
        mEditText.setSelection(mEditText.text.toString().length)
    }

    private fun getProfileDetail() {
        Glide.with(mActivity).load(KloxusPrefrences().readString(mActivity, PROFILE_IMAGE, null))
            .placeholder(R.drawable.ic_ph)
            .error(R.drawable.ic_ph)
            .into(imgProfileIV)
        editEmailET.text = KloxusPrefrences().readString(mActivity, USER_EMAIL, null)
        editFirstNameET.setText(KloxusPrefrences().readString(mActivity, FIRST_NAME, null))
        editLastNameET.setText(KloxusPrefrences().readString(mActivity, LAST_NAME, null))
        editPhoneET.setText(KloxusPrefrences().readString(mActivity, PHONE, null))
        editPasswordET.setText(KloxusPrefrences().readString(mActivity, PASSWORD, null))
        showCursorAtEnd(editFirstNameET)
        showCursorAtEnd(editLastNameET)
        showCursorAtEnd(editPhoneET)
//        if (isNetworkAvailable(this))
//            executeGetProfileDetailRequest()
//        else
//            showToast(this, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mProfileParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

//    private fun executeGetProfileDetailRequest() {
//        showProgressDialog(this)
//        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
//        mApiInterface?.getProfileDetailRequest(mProfileParam())
//            ?.enqueue(object : Callback<EditProfileModel> {
//                override fun onFailure(call: Call<EditProfileModel>, t: Throwable) {
//                    dismissProgressDialog()
//                }
//
//                override fun onResponse(
//                    call: Call<EditProfileModel>,
//                    response: Response<EditProfileModel>
//                ) {
//                    dismissProgressDialog()
//                    val mModel: EditProfileModel? = response.body()
//                    if (mModel != null) {
//                        if (mModel.status.equals(1)) {
//                            Glide.with(mActivity).load(mModel.data.profileImage)
//                                .placeholder(R.drawable.ic_ph)
//                                .error(R.drawable.ic_ph)
//                                .into(imgProfileIV)
//                            editFirstNameET.setText(mModel.data.firstName)
//                            editLastNameET.setText(mModel.data.lastName)
//                            editPhoneET.setText(mModel.data.phoneNo)
//                            editPasswordET.setText(mModel.data.password)
//                            editEmailET.setText(mModel.data.email)
//                            showCursorAtEnd(editFirstNameET)
//                            showCursorAtEnd(editLastNameET)
//                            showCursorAtEnd(editPhoneET)
////                            Glide.with(mActivity).load(KloxusPrefrences().readString(mActivity, PROFILE_IMAGE,null))
////                                .placeholder(R.drawable.ic_ph)
////                                .error(R.drawable.ic_ph)
////                                .into(imgProfileIV)
//
//                        } else {
//                            showAlertDialog(mActivity, mModel?.message)
//                        }
//                    }
//                }
//            })
//    }


    @OnClick(
        R.id.imgCrossIV,
        R.id.editPasswordET,
        R.id.txtSaveTV,
        R.id.profileFL,
        R.id.txtVerifiedTV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgCrossIV -> performCancelClick()
            R.id.editPasswordET -> performPasswordClick()
            R.id.txtSaveTV -> performSaveClick()
            R.id.profileFL ->performAddImgClick()
            R.id.txtVerifiedTV ->performVerifyClick()
        }
    }

    private fun performVerifyClick() {
//        phoneNumber = countryCode + editPhoneET.getText().toString().trim { it <= ' ' }
//        if (phoneNumber == "") {
//            showAlertDialog(mActivity, getString(R.string.please_enter_phone_num))
//        } else if (editPhoneET.length() < 10) {
//            showAlertDialog(mActivity, getString(R.string.please_enter_phone_num_with_valid_length))
//        } else {
//            val intent = Intent(mActivity, VerifyOtpActivity::class.java)
//            intent.putExtra(Constants.PHONE_NUMBER, phoneNumber)
//            startActivity(intent)
//            finish()
//        }
    }

    private fun performContinueClick() {

    }

    private fun performPasswordClick() {
        val i = Intent(this, ChangePasswordActivity::class.java)
        startActivity(i)
    }

    private fun performCancelClick() {
        onBackPressed()
    }

    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity, arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 -> {
//                if (this[0] == PackageManager.PERMISSION_GRANTED) {
                if (checkPermission()==true){
                onSelectImageClick()}

            else {
                    showToast(mActivity,"Please allow permissions of camera & gallery from settings")
//                    requestPermission()
                    Log.e(TAG, "**Permission Denied**")
                }
        }}
    }

    fun IntArray.onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>?
    ) {
        when (requestCode) {
            369 -> {
                if (this[0] == PackageManager.PERMISSION_GRANTED) {
                    onSelectImageClick()
                } else {
                    Log.e(TAG, "**Permission Denied**")
                    showToast(mActivity,"Please allow permissions of camera & gallery from settings")
                }
            }
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            .crop()	    			//Crop image(Optional), Check Customization for more option
            .compress(720)			//Final image size will be less than 1 MB(Optional)
            .maxResultSize(720, 720)	//Final image resolution will be less than 1080 x 1080(Optional)
            .cropSquare()
            .start()
    }

    private fun performSaveClick() {
        if (isValidate())
            if (isNetworkAvailable(mActivity))
                executeAddToiletRequest()
            else
                showToast(mActivity, getString(R.string.internet_connection_error))
    }

    private fun executeAddToiletRequest() {
        showProgressDialog(mActivity)
        var mMultipartBody: MultipartBody.Part? = null
        if (mBitmap != null) {
            val requestFile1: RequestBody? = convertBitmapToByteArrayUncompressed(mBitmap!!)?.let {
                RequestBody.create(
                    "multipart/form-data".toMediaTypeOrNull(),
                    it
                )
            }
            mMultipartBody = requestFile1?.let {
                MultipartBody.Part.createFormData(
                    "profileImage", getAlphaNumericString()!!.toString() + ".jpeg",
                    it
                )
            }
        }

        val userID : RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(), (KloxusPrefrences().readString(
                mActivity,
                USER_ID,
                null
            ).toString())
        )
        val authToken : RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(), (KloxusPrefrences().readString(
                mActivity,
                AUTH_TOKEN,
                null
            ).toString())
        )
        val firstName: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            editFirstNameET.text.toString().trim()
        )
        val lastName: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            editLastNameET.text.toString().trim()
        )
        val phoneNo: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            editPhoneET.text.toString().trim()
        )
        val countryCode: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            countryCode
        )

        val mApiInterface: ApiInterface? = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        val mCall: Call<EditProfileModel?>? = mApiInterface?.editProfileRequest(
            userID, authToken, firstName, lastName, phoneNo, countryCode,mMultipartBody)

        mCall?.enqueue(object : Callback<EditProfileModel?> {
            override fun onFailure(call: Call<EditProfileModel?>, t: Throwable) {
                dismissProgressDialog()
                Log.e(TAG, "onFailure: " + t.localizedMessage)
            }

            override fun onResponse(
                call: Call<EditProfileModel?>,
                response: Response<EditProfileModel?>
            ) {
                dismissProgressDialog()
                val mModel: EditProfileModel? = response.body()
                if (mModel != null) {
                    if (mModel.status.equals(1)) {
                        KloxusPrefrences().writeString(mActivity, LAST_NAME, mModel.data?.lastName)
                        KloxusPrefrences().writeString(mActivity, FIRST_NAME, mModel.data?.firstName)
                        KloxusPrefrences().writeString(mActivity, PROFILE_IMAGE, mModel.data?.profileImage)
                        KloxusPrefrences().writeString(mActivity, USER_EMAIL, mModel.data?.email)
                        KloxusPrefrences().writeInteger(mActivity, IS_STRIPE, mModel.data?.isBank)
                        KloxusPrefrences().writeInteger(mActivity, IS_PAYPAL, mModel.data?.isPaypal)
                        KloxusPrefrences().writeString(mActivity, PASSWORD, mModel.data?.password)
                        KloxusPrefrences().writeString(mActivity, PHONE, mModel.data?.phoneNo)
                        KloxusPrefrences().writeString(mActivity, COUNTRY_CODE, mModel.data?.countryCode)
                        editFirstNameET.setText(mModel.data.firstName)
                        editLastNameET.setText(mModel.data.lastName)
                        editPhoneET.setText(mModel.data.phoneNo)
                        ccp.setCountryForPhoneCode(mModel.data.countryCode.toInt())
                        editPasswordET.setText(mModel.data.password)
                        editEmailET.setText(mModel.data.email)
                        showCursorAtEnd(editFirstNameET)
                        showCursorAtEnd(editLastNameET)
                        showCursorAtEnd(editPhoneET)
                        Glide.with(mActivity).load(KloxusPrefrences().readString(mActivity, PROFILE_IMAGE,null))
                            .placeholder(R.drawable.ic_ph)
                            .error(R.drawable.ic_ph)
                            .into(imgProfileIV)
                        showToast(mActivity, mModel.message)
                        finish()
                    } else {
                        showToast(mActivity, mModel.message)
                    }
                }
            }
        })

    }

    private fun performAddImgClick() {
        if (checkPermission()) {
            onSelectImageClick()
        } else {
            requestPermission()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val uri: Uri = data?.data!!

            // Use Uri object instead of File to avoid storage permissions
            imgProfileIV.setImageURI(uri)
            val imageStream = contentResolver.openInputStream(uri)
                    val selectedImage
                            = BitmapFactory.decodeStream(imageStream)
                    mBitmap = selectedImage
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
//            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
//            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
    /*
     * Set up validations for Sign In fields
     * */
    private fun isValidate(): Boolean {
        var flag = true
        if (editFirstNameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_first_name))
            flag = false
        } else if (editLastNameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_last_name))
            flag = false
        } else if (editPhoneET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_phone_num))
            flag = false
        }else if (editPhoneET.text.toString().trim().length<10 ||editPhoneET.text.toString().trim().length>15) {
            showAlertDialog(mActivity, getString(R.string.please_enter_phone_num_with_valid_length))
            flag = false
        }
        return flag
    }

}