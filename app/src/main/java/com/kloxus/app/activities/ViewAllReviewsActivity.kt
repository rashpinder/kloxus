package com.kloxus.app.activities
import LoadMoreReviews
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.kloxus.app.R
import com.kloxus.app.adapters.SeeAllReviewAdapter
import com.kloxus.app.model.*
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.AUTH_TOKEN
import com.kloxus.app.utils.KloxusPrefrences
import com.kloxus.app.utils.USER_ID
import kotlinx.android.synthetic.main.activity_view_all_reviews.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class ViewAllReviewsActivity : BaseActivity() {

    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var isLoading: Boolean = true
    var mToiletId: String = ""
    var mReviewsArrayList: ArrayList<RatingDetailX> = ArrayList<RatingDetailX>()
    lateinit var mReviewAdapter: SeeAllReviewAdapter

    @BindView(R.id.mProgressRL)
    lateinit var mProgressRL: RelativeLayout

    @BindView(R.id.imgCrossIV)
    lateinit var imgCrossIV: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_all_reviews)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        getIntentData()
        getAllReviewsDetail()
    }


    @OnClick(
        R.id.imgCrossIV
    )

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgCrossIV -> performCrossPwdClick()
        }
    }

    private fun performCrossPwdClick() {
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


    private fun getIntentData() {
        if (intent != null) {
            mToiletId = intent.getStringExtra("toilet_id").toString()
        }
    }

    private fun getAllReviewsDetail() {
        if (isNetworkAvailable(mActivity))
            executeGetAllToiletReviewsRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["toiletID"] = mToiletId
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetAllToiletReviewsRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getReviewsRequest(mParam())
            ?.enqueue(object : Callback<AllReviewsModel> {
                override fun onFailure(call: Call<AllReviewsModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<AllReviewsModel>,
                    response: Response<AllReviewsModel>
                ) {
                    dismissProgressDialog()
                    val mModel: AllReviewsModel? = response.body()
                    if (mModel!!.status.equals(1)) {
                        isLoading = !mModel.lastPage.equals(true)
                        mReviewsArrayList = mModel.ratingDetails
                        setRatingAdapter()
                    } else {
                        showAlertDialog(mActivity, mModel.message)
                    }
                }
            })
    }
    
private fun setRatingAdapter() {
    mReviewAdapter = SeeAllReviewAdapter(mActivity, mReviewsArrayList, mLoadMoreScrollListner)
    reviewRV.layoutManager = LinearLayoutManager(mActivity)
    reviewRV.adapter = mReviewAdapter
}


    var mLoadMoreScrollListner: SeeAllReviewAdapter.LoadMoreReviews = object : SeeAllReviewAdapter.LoadMoreReviews {
        override fun onLoadMoreListner(mModel: RatingDetailX) {
            if (isLoading) {
                ++mPageNo
                executeMoreReviewsAccordingDataRequest()
            }
        }
    }

    /*
     * Execute api param
     * */
    private fun mLoadMoreParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["toiletID"] = mToiletId
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeMoreReviewsAccordingDataRequest() {
        mProgressRL.visibility = View.VISIBLE
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getReviewsRequest(mLoadMoreParam())
            ?.enqueue(object : Callback<AllReviewsModel> {
                override fun onFailure(call: Call<AllReviewsModel>, t: Throwable) {
                    dismissProgressDialog()
                    mProgressRL.visibility = View.GONE
                }

                override fun onResponse(
                    call: Call<AllReviewsModel>,
                    response: Response<AllReviewsModel>
                ) {
                    mProgressRL.visibility = View.GONE
                    var mModel = response.body()!!
                    if (mModel.status == 1) {
                        mModel.ratingDetails.let {
                            mReviewsArrayList!!.addAll<RatingDetailX>(
                                it
                            )
                        }
                        mReviewAdapter.notifyDataSetChanged()
                        isLoading = !mModel.lastPage.equals(true)

                    } else if (mModel.status == 0) {
//                    showToast(mActivity, mGetFavModel.message)
                    }
                }
            })
    }
}