package com.kloxus.app.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.kloxus.app.R
import com.kloxus.app.adapters.BookingDetailAdapter
import com.kloxus.app.interfaces.LoadMoreDataByCityListener
import com.kloxus.app.model.GetToiletbyCityData
import com.kloxus.app.model.GetToiletbyCityNameModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.AUTH_TOKEN
import com.kloxus.app.utils.KloxusPrefrences
import com.kloxus.app.utils.USER_ID
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BookingDetailActivity : BaseActivity() {

    lateinit var mBookingDetailAdapter: BookingDetailAdapter

    @BindView(R.id.bookingDetailRV)
    lateinit var bookingDetailRV: RecyclerView

    @BindView(R.id.imgBackIV)
    lateinit var imgBackIV: ImageView

    @BindView(R.id.txtNoDataFountTV)
    lateinit var txtNoDataFountTV: TextView

    @BindView(R.id.txtTitleTV)
    lateinit var txtTitleTV: TextView

    var mAllToiletsByCitiesList: ArrayList<GetToiletbyCityData>? = ArrayList()
    var mAddress: String = ""
    var mtype: String = ""

    @BindView(R.id.mProgressRL)
    lateinit var mProgressRL: RelativeLayout

    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var isLoading: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_detail)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        getIntentData()
        setIntentData()
        getAllToiletsByCity()
    }

    private fun setIntentData() {
        txtTitleTV.text = mAddress
    }

    private fun getIntentData() {
        if (intent != null) {
            mAddress = intent.getStringExtra("location").toString()
            mtype = intent.getStringExtra("type").toString()
        }
    }

    @OnClick(R.id.imgBackIV)

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackIV -> performCrossClick()
        }
    }

    private fun performCrossClick() {
        onBackPressed()
    }

    private fun getAllToiletsByCity() {
        if (isNetworkAvailable(mActivity))
            executeGetAllToiletByCityRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["location"] = mAddress
        mMap["type"] = mtype
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetAllToiletByCityRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getAllToiletByCitiesRequest(mParam())
            ?.enqueue(object : Callback<GetToiletbyCityNameModel> {
                override fun onFailure(call: Call<GetToiletbyCityNameModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<GetToiletbyCityNameModel>,
                    response: Response<GetToiletbyCityNameModel>
                ) {
                    dismissProgressDialog()
                    val mModel: GetToiletbyCityNameModel? = response.body()
                    if (mModel!!.status.equals(1)) {
                        isLoading = !mModel.lastPage.equals(true)
                        mAllToiletsByCitiesList = mModel.data
                        setAllToiletsByCityAdapter()
                        txtNoDataFountTV.visibility = View.GONE
                    } else {
//                        showAlertDialog(mActivity, mModel.message)
                        txtNoDataFountTV.visibility = View.VISIBLE
                        txtNoDataFountTV.text = mModel.message
                    }
                }
            })
    }

    var mLoadMoreScrollListner: LoadMoreDataByCityListener = object : LoadMoreDataByCityListener {
        override fun onLoadMoreListner(mModel: GetToiletbyCityData) {
            if (isLoading) {
                ++mPageNo
                executeMoreAccordingDataRequest()
            }
        }
    }

    /*
     * Execute api param
     * */
    private fun mLoadMoreParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["location"] = mAddress
        mMap["type"] = mtype
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeMoreAccordingDataRequest() {
        mProgressRL.visibility = View.VISIBLE
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getAllToiletByCitiesRequest(mLoadMoreParam())
            ?.enqueue(object : Callback<GetToiletbyCityNameModel> {
                override fun onFailure(call: Call<GetToiletbyCityNameModel>, t: Throwable) {
                    dismissProgressDialog()
                    mProgressRL.visibility = View.GONE
                }

                override fun onResponse(
                    call: Call<GetToiletbyCityNameModel>,
                    response: Response<GetToiletbyCityNameModel>
                ) {
                    mProgressRL.visibility = View.GONE
                    var mModel = response.body()!!
                    if (mModel.status == 1) {
                        mModel.data.let {
                            mAllToiletsByCitiesList!!.addAll<GetToiletbyCityData>(
                                it
                            )
                        }
                        mBookingDetailAdapter.notifyDataSetChanged()
                        isLoading = !mModel.lastPage.equals(true)
                    } else if (mModel.status == 0) {
//                    showToast(mActivity, mGetFavModel.message)
                    }
                }
            })
    }

    private fun setAllToiletsByCityAdapter() {
        mBookingDetailAdapter =
            BookingDetailAdapter(mActivity, mAllToiletsByCitiesList, mLoadMoreScrollListner, mtype,mAddress)
        bookingDetailRV.layoutManager = LinearLayoutManager(mActivity)
        bookingDetailRV.adapter = mBookingDetailAdapter
    }
}