package com.kloxus.app.activities

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.kloxus.app.R
import com.kloxus.app.utils.ROOM_ID
import com.kloxus.app.utils.USER_NAME
import org.json.JSONObject
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class SplashActivity : BaseActivity() {

    val SPLASH_TIME_OUT = 2000L
    var mTitle = ""
    var mBody = ""
    var mName = ""
    var mRoomID = ""
    var userID = ""
    var otherUserID = ""
    var detailsID = ""
    var bookID = ""
    var notification_read_status = ""
    var created = ""
    var mType = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setSplashStatusBar(mActivity)
        printKeyHash(mActivity)

        var mIntent: Intent? = null
        if (intent != null && intent.extras != null) {
            for (key in intent.extras!!.keySet()) {
                val value = intent.extras!!.getString(key)
                Log.e(TAG, "Key:::: $key Value:::: $value")
                var mJsonObject: JSONObject
                if (key == "data") {
                    mJsonObject = JSONObject(value)
                    if (!mJsonObject.isNull("notification_type")) {
                        mType = mJsonObject.getString("notification_type")
                    }
                    if (!mJsonObject.isNull("title")) {
                        mTitle = mJsonObject.getString("title")
                    }
                    if (!mJsonObject.isNull("description")) {
                        mBody = mJsonObject.getString("description")
                    }
                    if (!mJsonObject.isNull("userID")) {
                        userID = mJsonObject.getString("userID")
                    }
                    if (!mJsonObject.isNull("otherUserID")) {
                        otherUserID = mJsonObject.getString("otherUserID")
                    }
                    if (!mJsonObject.isNull("roomID")) {
                        mRoomID = mJsonObject.getString("roomID")
                    }
                    if (!mJsonObject.isNull("userName")) {
                        mName = mJsonObject.getString("userName")
                    }
                    if (!mJsonObject.isNull("detailsID")) {
                        detailsID = mJsonObject.getString("detailsID")
                    }
                    if (!mJsonObject.isNull("notification_read_status")) {
                        notification_read_status = mJsonObject.getString("notification_read_status")
                    }
                    if (!mJsonObject.isNull("created")) {
                        created = mJsonObject.getString("created")
                    }
                    if (!mJsonObject.isNull("bookID")) {
                        bookID = mJsonObject.getString("bookID")
                    }
                }
            }
            if (mType.equals("1")) {
                mIntent = Intent(mActivity, HomeActivity::class.java)
                mActivity.startActivity(mIntent)
                finish()
            } else if (mType.equals("3")) {
                mIntent = Intent(mActivity, ChatActivity::class.java)
                mIntent!!.putExtra(USER_NAME, mTitle)
                mIntent!!.putExtra(ROOM_ID, mRoomID)
                mActivity.startActivity(mIntent)
                finish()
            }
            else if (mType.equals("2")) {
                mIntent = Intent(mActivity, NotificationsActivity::class.java)
                mActivity.startActivity(mIntent)
                finish()
            }
            else if (mType.equals("5")) {
                mIntent = Intent(mActivity, ToiletPathActivity::class.java)
                mIntent.putExtra("toilet_id", detailsID)
                mIntent.putExtra("book_id", bookID)
                 mActivity.startActivity(mIntent)
                finish()
            } else if (mType.equals("6")) {
                mIntent = Intent(mActivity, NotificationsActivity::class.java)
                mActivity.startActivity(mIntent)
                finish()
            } else if (mType.equals("4")) {
                mIntent = Intent(mActivity, AddBankDetailsTypeActivity::class.java)
                mActivity.startActivity(mIntent)
                finish()
            }else if (mType.equals("7")) {
                mIntent = Intent(mActivity, AddBankDetailsTypeActivity::class.java)
                mActivity.startActivity(mIntent)
                finish()
            }
            else if (mType.equals("8")) {
                mIntent = Intent(mActivity, ToiletProfileActivity::class.java)
                mIntent.putExtra("toilet_id", detailsID)
                mActivity.startActivity(mIntent)
                finish()
            }
            else {
                setUpSplash()
            }
        } else {
            setUpSplash()
        }
    }

    private fun setUpSplash() {
        Handler().postDelayed({
//            if (isUserLogin()) {
                val mIntent = Intent(mActivity, HomeActivity::class.java)
                startActivity(mIntent)
                finish()
//            } else if (!isUserLogin()) {
//                val mIntent = Intent(mActivity, LoginActivity::class.java)
//                startActivity(mIntent)
//                finish()
//            } else {
//                val mIntent = Intent(mActivity, LoginActivity::class.java)
//                startActivity(mIntent)
//                finish()
//            }
        }, SPLASH_TIME_OUT)
    }

    fun printKeyHash(context: Activity): String? {
        val packageInfo: PackageInfo
        var key: String? = null
        try {
            //getting application package name, as defined in manifest
            val packageName = context.applicationContext.packageName

            //Retriving package info
            packageInfo = context.packageManager.getPackageInfo(
                packageName,
                PackageManager.GET_SIGNATURES
            )
            Log.e("Package Name=", context.applicationContext.packageName)
            for (signature in packageInfo.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                key = String(Base64.encode(md.digest(), 0))
                Log.e(TAG, "Key Hash=++++++=$key")
            }
        } catch (e1: PackageManager.NameNotFoundException) {
            Log.e("Name not found", e1.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("No such an algorithm", e.toString())
        } catch (e: java.lang.Exception) {
            Log.e("Exception", e.toString())
        }
        return key
    }

    override fun onResume() {
        super.onResume()

    }
}


