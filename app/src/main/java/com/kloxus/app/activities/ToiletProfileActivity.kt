package com.kloxus.app.activities

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.viewpager.widget.ViewPager
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.chahinem.pageindicator.PageIndicator
import com.kloxus.app.R
import com.kloxus.app.adapters.SlidingImageAdapter
import com.kloxus.app.model.StatusMsgModel
import com.kloxus.app.model.ToiletDetailModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.AUTH_TOKEN
import com.kloxus.app.utils.KloxusPrefrences
import com.kloxus.app.utils.USER_ID
import com.suke.widget.SwitchButton
import kotlinx.android.synthetic.main.activity_add_toilet.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class ToiletProfileActivity : BaseActivity() {

    @BindView(R.id.imgBackIV)
    lateinit var imgBackIV: ImageView

    @BindView(R.id.imgEditIV)
    lateinit var imgEditIV: ImageView

    @BindView(R.id.imgDeleteIV)
    lateinit var imgDeleteIV: ImageView

    @BindView(R.id.PriceRL)
    lateinit var PriceRL: RelativeLayout

    @BindView(R.id.imgCheckPaidIV)
    lateinit var imgCheckPaidIV: ImageView

    @BindView(R.id.imgCheckFreeIV)
    lateinit var imgCheckFreeIV: ImageView

//    @BindView(R.id.imgProfileIV)
//    lateinit var imgProfileIV: ImageView

    @BindView(R.id.toiletNameTV)
    lateinit var toiletNameTV: TextView

    @BindView(R.id.addressTV)
    lateinit var addressTV: TextView

    @BindView(R.id.txtDescriptionTV)
    lateinit var txtDescriptionTV: TextView

    @BindView(R.id.txtPriceTV)
    lateinit var txtPriceTV: TextView

    @BindView(R.id.priceTV)
    lateinit var priceTV: TextView

    @BindView(R.id.GenderRL)
    lateinit var GenderRL: RelativeLayout

    @BindView(R.id.availableOpenTimeET)
    lateinit var availableOpenTimeET: TextView


    @BindView(R.id.genderTV)
    lateinit var genderTV: TextView


    var mToiletId: String = ""
    var noti: String = ""

    @BindView(R.id.imageRL)
    lateinit var imageRL: RelativeLayout

    @BindView(R.id.mViewPagerVP)
    lateinit var mViewPagerVP: ViewPager

    @BindView(R.id.switchActivateSB)
    lateinit var switchActivateSB: SwitchButton

    @BindView(R.id.indicator)
    lateinit var indicator: PageIndicator
    var mImagesArrayList1: ArrayList<String> = ArrayList<String>()
    var mGenderArrayList: ArrayList<String> = ArrayList<String>()
    var mItemsList: ArrayList<String> = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_toilet_profile)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        txtDescriptionTV.movementMethod = ScrollingMovementMethod()
//        txtDescriptionTV.setOnTouchListener(View.OnTouchListener setOnTouchListener@{ v: View, event: MotionEvent ->
//            if (txtDescriptionTV.hasFocus()) {
//                v.parent.requestDisallowInterceptTouchEvent(true)
//                when (event.action and MotionEvent.ACTION_MASK) {
//                    MotionEvent.ACTION_SCROLL -> {
//                        v.parent.requestDisallowInterceptTouchEvent(false)
//                        return@setOnTouchListener true
//                    }
//                }
//            }
//            false
//        })

    }


    //Set ViewPager Adapter
    private fun setViewPagerAdapter() {
        val mSlidingImageAdapter =
            SlidingImageAdapter(
                mActivity,
                mImagesArrayList1,
                object : SlidingImageAdapter.AddClick {
                    override fun Click(position: Int) {

                    }
                })

        mViewPagerVP.adapter = mSlidingImageAdapter
        indicator.attachTo(mViewPagerVP)
//        (mViewPagerVP.adapter as SlidingImageAdapter).notifyDataSetChanged();
    }


    private fun getIntentData() {
        if (intent != null) {
            mToiletId = intent.getStringExtra("toilet_id").toString()
            noti = intent.getStringExtra("case").toString()
if (noti.equals("noti")){
    imgEditIV.visibility= View.GONE
    imgDeleteIV.visibility= View.GONE
}
            else{
    imgEditIV.visibility=View.VISIBLE
    imgDeleteIV.visibility= View.VISIBLE
            }
        }
    }


    override fun onResume() {
        super.onResume()
        getIntentData()
        mGenderArrayList.clear()
        mImagesArrayList1.clear()
        switchActivateSB.isEnabled = true
        getToiletProfileDetail()
    }

    private fun getToiletProfileDetail() {
        if (isNetworkAvailable(mActivity)){
            mItemsList.clear()
            executeGetToiletDetailRequest()}
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["toiletID"] = mToiletId
        mMap["timeZone"] = TimeZone.getDefault().id
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetToiletDetailRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getToiletDetailRequest(mParam())
            ?.enqueue(object : Callback<ToiletDetailModel> {
                override fun onFailure(call: Call<ToiletDetailModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<ToiletDetailModel>,
                    response: Response<ToiletDetailModel>
                ) {
                    dismissProgressDialog()
                    val mModel: ToiletDetailModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
//                            Glide.with(mActivity).load(mModel.data.toiletImage)
//                                .placeholder(R.drawable.ic_booking_ph)
//                                .error(R.drawable.ic_booking_ph)
//                                .into(imgProfileIV)
                            toiletNameTV.text = mModel.data.title
                            addressTV.text = mModel.data.address
//                            txtDescriptionTV.text = mModel.data.description
                            if (mModel.data.isPaid.equals("1")){
                                imgCheckPaidIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgCheckFreeIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
                                priceTV.isVisible=true
                                PriceRL.isVisible=true
                            txtPriceTV.text = mModel.data.currencySymbol + mModel.data.price}
                            else{
                                imgCheckPaidIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
                                imgCheckFreeIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                priceTV.isVisible=false
                                PriceRL.isVisible=false

                            }

                            for (i in mModel.data.newGender.indices) {
                                mGenderArrayList.add(mModel.data.newGender.get(i))
                            }
                            if (mGenderArrayList.size == 1) {
                                if (mGenderArrayList.contains("1")) {
                                    genderTV.setText(R.string.male)
                                } else if (mGenderArrayList.contains("2")) {
                                    genderTV.setText(R.string.female)
                                } else if (mGenderArrayList.contains("3")) {
                                    genderTV.setText(R.string.other)
                                }
                            } else if (mGenderArrayList.size == 2) {
                                if (mGenderArrayList.contains("3") && mGenderArrayList.contains("1")) {
                                    genderTV.setText(R.string.male_other)
                                } else if (mGenderArrayList.contains("3") && mGenderArrayList.contains(
                                        "2"
                                    )
                                ) {
                                    genderTV.setText(R.string.female_other)
                                } else if (mGenderArrayList.contains("1") && mGenderArrayList.contains(
                                        "2"
                                    )
                                ) {
                                    genderTV.setText(R.string.male_female)
                                }
                            } else {
                                if (mGenderArrayList.contains("1") && mGenderArrayList.contains("2") && mGenderArrayList.contains(
                                        "3"
                                    )
                                ) {
                                    genderTV.setText(R.string.male_female_other)
                                }
                            }

//        options = mModel.data.toiletOptions
                            for (i in mModel.data.toiletOptions.indices) {

                                mItemsList.add(mModel.data.toiletOptions.get(i))
                            }
                            if (mItemsList.contains("3")&&mItemsList.contains("2") && mItemsList.contains("4") && mItemsList.contains("1")&& mItemsList.contains("5")) {
                                txtDescriptionTV.text=getString(R.string.all)
                            }
                            else if (mItemsList.contains("1") && mItemsList.contains("2") && mItemsList.contains("3")&& mItemsList.contains("4")) {
                                txtDescriptionTV.text= getString(R.string.a)
                            }
                            else if (mItemsList.contains("1") && mItemsList.contains("2") && mItemsList.contains("3")&& mItemsList.contains("5")) {
                                txtDescriptionTV.text= getString(R.string.b)
                            }else if (mItemsList.contains("1") && mItemsList.contains("4") && mItemsList.contains("3")&& mItemsList.contains("5")) {
                                txtDescriptionTV.text= getString(R.string.c)
                            }
                            else if (mItemsList.contains("2") && mItemsList.contains("4") && mItemsList.contains("3")&& mItemsList.contains("5")) {
                                txtDescriptionTV.text= getString(R.string.d)
                            }
                            else if (mItemsList.contains("2") && mItemsList.contains("4") && mItemsList.contains("1")&& mItemsList.contains("5")) {
                                txtDescriptionTV.text=getString(R.string.e)
                            }

                            else if (mItemsList.contains("1") && mItemsList.contains("2") && mItemsList.contains("3")) {
                                txtDescriptionTV.text= getString(R.string.f)
                            } else if (mItemsList.contains("1") && mItemsList.contains("2") && mItemsList.contains("4")) {
                                txtDescriptionTV.text= getString(R.string.ff)
                            } else if (mItemsList.contains("1") && mItemsList.contains("3") && mItemsList.contains("4")) {
                                txtDescriptionTV.text= getString(R.string.g)
                            } else if (mItemsList.contains("1") && mItemsList.contains("5") && mItemsList.contains("4")) {
                                txtDescriptionTV.text= getString(R.string.h)
                            }else if (mItemsList.contains("2") && mItemsList.contains("3") && mItemsList.contains("4")) {
                                txtDescriptionTV.text= getString(R.string.i)
                            }else if (mItemsList.contains("2") && mItemsList.contains("3") && mItemsList.contains("5")) {
                                txtDescriptionTV.text= getString(R.string.j)
                            }else if (mItemsList.contains("2") && mItemsList.contains("4") && mItemsList.contains("5")) {
                                txtDescriptionTV.text= getString(R.string.k)
                            }else if (mItemsList.contains("1") && mItemsList.contains("3") && mItemsList.contains("5")) {
                                txtDescriptionTV.text= getString(R.string.fff)
                            }
                            else if (mItemsList.contains("3") && mItemsList.contains("4") && mItemsList.contains("5")) {
                                txtDescriptionTV.text= getString(R.string.ffff)
                            }
                            else if (mItemsList.contains("1") && mItemsList.contains("2")) {
                                txtDescriptionTV.text=getString(R.string.l)
                            } else if (mItemsList.contains("1") && mItemsList.contains("3")) {
                                txtDescriptionTV.text= getString(R.string.m)
                            } else if (mItemsList.contains("1") && mItemsList.contains("4")) {
                                txtDescriptionTV.text= getString(R.string.n)
                            } else if (mItemsList.contains("1") && mItemsList.contains("5")) {
                                txtDescriptionTV.text= getString(R.string.o)
                            } else if (mItemsList.contains("2") && mItemsList.contains("3")) {
                                txtDescriptionTV.text= getString(R.string.p)
                            } else if (mItemsList.contains("2") && mItemsList.contains("4")) {
                                txtDescriptionTV.text= getString(R.string.q)
                            } else if (mItemsList.contains("2") && mItemsList.contains("5")) {
                                txtDescriptionTV.text= getString(R.string.r)
                            } else if (mItemsList.contains("3") && mItemsList.contains("4")) {
                                txtDescriptionTV.text= getString(R.string.s)
                            } else if (mItemsList.contains("3") && mItemsList.contains("5")) {
                                txtDescriptionTV.text= getString(R.string.t)
                            } else if (mItemsList.contains("4") && mItemsList.contains("5")) {
                                txtDescriptionTV.text= getString(R.string.u)
                            }
                            else if (mItemsList.contains("1")) {
                                txtDescriptionTV.text= resources.getString(R.string.toilet_paper)
                            } else if (mItemsList.contains("2")) {
                                txtDescriptionTV.text= resources.getString(R.string.soap)
                            } else if (mItemsList.contains("3")) {
                                txtDescriptionTV.text= resources.getString(R.string.hand_dryer)
                            } else if (mItemsList.contains("4")) {
                                txtDescriptionTV.text=resources.getString(R.string.ser)
                            } else if (mItemsList.contains("5")) {
                                txtDescriptionTV.text= resources.getString(R.string.dis)
                            }

                            availableOpenTimeET.text = mModel.data.available

                            if (mModel.data.toiletImage != "") {
                                mImagesArrayList1.add(mModel.data.toiletImage)
                            }
                            if (mModel.data.toiletImage1 != "") {
                                mImagesArrayList1.add(mModel.data.toiletImage1)
                            }
                            if (mModel.data.toiletImage2 != "") {
                                mImagesArrayList1.add(mModel.data.toiletImage2)
                            }
                            if (mModel.data.toiletImage3 != "") {
                                mImagesArrayList1.add(mModel.data.toiletImage3)
                            }
                            if (mModel.data.isRequestSend.equals("1")){
                                switchActivateSB.isChecked = true
                            }
                            else{
                                switchActivateSB.isChecked = false
                            }
                            switchActivateSB.isEnabled = false
                            setViewPagerAdapter()
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }


    @OnClick(
        R.id.imgBackIV,
        R.id.imgEditIV,
        R.id.imgDeleteIV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackIV -> performCancelClick()
            R.id.imgEditIV -> performEditClick()
            R.id.imgDeleteIV -> performDeleteClick()
        }
    }

    private fun performDeleteClick() {
        showDeleteAlert(mActivity, getString(R.string.delete_sure))
    }


    fun showDeleteAlert(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.signout_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnNo = alertDialog.findViewById<TextView>(R.id.btnNo)
        val btnYes = alertDialog.findViewById<TextView>(R.id.btnYes)
        txtMessageTV.text = strMessage
        btnNo.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
        btnYes.setOnClickListener {
            deleteToilet()
            alertDialog.dismiss()
        }
    }

    private fun deleteToilet() {
        if (isNetworkAvailable(this))
            executeDeleteToiletRequest()
        else
            showToast(this, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mDeleteParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["toiletID"] = mToiletId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeDeleteToiletRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.deleteToiletRequest(mDeleteParam())
            ?.enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {
                    dismissProgressDialog()
                    val mModel: StatusMsgModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                                showAlertDialogFinish(mActivity, mModel.message)
                            }
                            else{
                            showAlertDialog(mActivity, mModel.message)
                            }
                        } else {
                            showAlertDialog(mActivity, mModel?.message)
                        }
                    }
            })
    }

    private fun performEditClick() {
        val i = Intent(this, EditToiletProfileActivity::class.java)
        i.putExtra("toilet_id", mToiletId)
        startActivity(i)

    }

    private fun performCancelClick() {
        onBackPressed()
    }


}