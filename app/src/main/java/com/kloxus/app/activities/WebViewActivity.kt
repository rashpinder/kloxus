package com.kloxus.app.activities

import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.kloxus.app.R
import com.kloxus.app.utils.*

class WebViewActivity : BaseActivity() {

    @BindView(R.id.imgCrossIV) lateinit var imgCrossIV: ImageView
    @BindView(R.id.txtHeadingTV) lateinit var txtHeadingTV: TextView
    @BindView(R.id.mWebViewWV) lateinit var mWebViewWV: WebView

    var linkType : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        getIntentData()
    }


    @OnClick(R.id.imgCrossIV)

    fun onClick(view : View) {
        when (view.id) {
            R.id.imgCrossIV -> performCrossClick()
        }
    }


    private fun getIntentData() {
        if (intent != null){
            linkType = intent.getStringExtra(LINK_TYPE).toString()
            if (linkType.equals(LINK_ABOUT)){
                txtHeadingTV.setText(getString(R.string.about))
                setUpWebView(ABOUT_WEB_LINK)
            }else if (linkType.equals(LINK_TERMS)){
                txtHeadingTV.setText(getString(R.string.terms))
                setUpWebView(TERMS_WEB_LINK)
            }
        }
    }

    private fun setUpWebView(mUrl : String){
        mWebViewWV.webViewClient = WebViewClient()
        // this will load the url of the website
        mWebViewWV.loadUrl(mUrl)
        // this will enable the javascript settings
        mWebViewWV.settings.javaScriptEnabled = true
        // if you want to enable zoom feature
        mWebViewWV.settings.setSupportZoom(true)
    }
    private fun performCrossClick() {
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}