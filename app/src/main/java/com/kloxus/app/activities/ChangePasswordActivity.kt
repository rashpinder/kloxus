package com.kloxus.app.activities

import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.kloxus.app.R
import com.kloxus.app.model.StatusMsgModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.AUTH_TOKEN
import com.kloxus.app.utils.KloxusPrefrences
import com.kloxus.app.utils.PASSWORD
import com.kloxus.app.utils.USER_ID
import kotlinx.android.synthetic.main.activity_change_password.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ChangePasswordActivity : BaseActivity() {

    @BindView(R.id.imgCrossIV)
    lateinit var imgCrossIV: ImageView
    @BindView(R.id.editCurrentPasswordET)
    lateinit var editCurrentPasswordET: EditText
    @BindView(R.id.editNewPasswordET)
    lateinit var editNewPasswordET: EditText
    @BindView(R.id.editConfirmPasswordET)
    lateinit var editConfirmPasswordET: EditText
    @BindView(R.id.txtSaveTV)
    lateinit var txtSaveTV: TextView
    @BindView(R.id.oldPassRL)
    lateinit var oldPassRL: RelativeLayout
    @BindView(R.id.newPassRL)
    lateinit var newPassRL: RelativeLayout
    @BindView(R.id.confirmPassRL)
    lateinit var confirmPassRL: RelativeLayout
    @BindView(R.id.imgEyeCurrentPassIV)
    lateinit var imgEyeCurrentPassIV: ImageView

    @BindView(R.id.imgEyeNewPassIV)
    lateinit var imgEyeNewPassIV: ImageView

    @BindView(R.id.imgEyeConfirmPassIV)
    lateinit var imgEyeConfirmPassIV: ImageView

    var current_value: Boolean = true
    var new_value: Boolean = true
    var confirm_value: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        editTextSignupSelector(editCurrentPasswordET, oldPassRL, "")
        editTextSignupSelector(editNewPasswordET, newPassRL, "")
        editTextSignupSelector(editConfirmPasswordET, confirmPassRL, "")

    }

    @OnClick(R.id.imgCrossIV,
        R.id.txtSaveTV,
        R.id.imgEyeCurrentPassIV,
        R.id.imgEyeNewPassIV,
        R.id.imgEyeConfirmPassIV,
    )

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgCrossIV -> performCrossClick()
            R.id.txtSaveTV -> performSubmitClick()
            R.id.imgEyeCurrentPassIV -> performCurrentPasswordClick()
            R.id.imgEyeNewPassIV -> performNewPasswordClick()
            R.id.imgEyeConfirmPassIV -> performConfirmPasswordClick()
        }
    }

    private fun performConfirmPasswordClick() {
            if (current_value == true) {
                imgEyeConfirmPassIV.setImageDrawable(getDrawable(R.drawable.ic_eye_hide))
                editConfirmPasswordET.inputType = InputType.TYPE_CLASS_TEXT or
                        InputType.TYPE_TEXT_VARIATION_PASSWORD
                editConfirmPasswordET.setSelection(editConfirmPasswordET.text.length)
                current_value=false
            } else if (current_value==false) {
                imgEyeConfirmPassIV.setImageDrawable(getDrawable(R.drawable.ic_eye))
                editConfirmPasswordET.inputType = InputType.TYPE_CLASS_TEXT or
                        InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                editConfirmPasswordET.setSelection(editConfirmPasswordET.text.length)

                current_value=true
            }

    }

    private fun performNewPasswordClick() {
        if (new_value == true) {
            imgEyeNewPassIV.setImageDrawable(getDrawable(R.drawable.ic_eye_hide))
            editNewPasswordET.inputType = InputType.TYPE_CLASS_TEXT or
                    InputType.TYPE_TEXT_VARIATION_PASSWORD
            editNewPasswordET.setSelection(editNewPasswordET.text.length)
            new_value=false
        } else if (new_value==false) {
            imgEyeNewPassIV.setImageDrawable(getDrawable(R.drawable.ic_eye))
            editNewPasswordET.inputType = InputType.TYPE_CLASS_TEXT or
                    InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
            editNewPasswordET.setSelection(editNewPasswordET.text.length)
            new_value=true
        }
    }

    private fun performCurrentPasswordClick() {
        if (confirm_value == true) {
            imgEyeCurrentPassIV.setImageDrawable(getDrawable(R.drawable.ic_eye_hide))
            editCurrentPasswordET.inputType = InputType.TYPE_CLASS_TEXT or
                    InputType.TYPE_TEXT_VARIATION_PASSWORD
            editCurrentPasswordET.setSelection(editCurrentPasswordET.text.length)
            confirm_value=false
        } else if (confirm_value==false) {
            imgEyeCurrentPassIV.setImageDrawable(getDrawable(R.drawable.ic_eye))
            editCurrentPasswordET.inputType = InputType.TYPE_CLASS_TEXT or
                    InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
            editCurrentPasswordET.setSelection(editCurrentPasswordET.text.length)

            confirm_value=true
        }
    }

    private fun performCrossClick() {
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


    private fun performSubmitClick() {
        if (isValidate())
            if (isNetworkAvailable(mActivity))
                executeChangePwdRequest()
            else
                showToast(mActivity, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["newPassword"] = editNewPasswordET.text.toString().trim { it <= ' ' }
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["oldPassword"] = editCurrentPasswordET.text.toString().trim { it <= ' ' }
        mMap["confirmPassword"] = editConfirmPasswordET.text.toString().trim { it <= ' ' }
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeChangePwdRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.changePasswordRequest(mParam())?.enqueue(object : Callback<StatusMsgModel> {

            override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<StatusMsgModel>,
                response: Response<StatusMsgModel>
            ) {
                dismissProgressDialog()
                val mModel: StatusMsgModel? = response.body()
                if (mModel != null) {
                    if (mModel.status.equals(1)) {
                        showToast(mActivity, mModel.message)
                        KloxusPrefrences().writeString(mActivity, PASSWORD,
                            editConfirmPasswordET.text.toString())
                        editCurrentPasswordET.setText("")
                        editNewPasswordET.setText("")
                        editConfirmPasswordET.setText("")
                        finish()
                    } else {
                        showAlertDialog(mActivity, mModel.message)
                    }
                }
            }
        })
    }

    /*
        * Set up validations for Sign In fields
        * */
    private fun isValidate(): Boolean {
        var flag = true
        if (editCurrentPasswordET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.manage_enter_old_pass))
            flag = false
        } else if (editNewPasswordET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_new_password))
            flag = false
        } else if (editConfirmPasswordET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_confirm_password))
            flag = false
        } else if (editConfirmPasswordET.text.toString()
                .trim { it <= ' ' } != editNewPasswordET.text.toString().trim { it <= ' ' }
        ) {
            showAlertDialog(mActivity, getString(R.string.password_not))
            flag = false
        }
        return flag
    }

}