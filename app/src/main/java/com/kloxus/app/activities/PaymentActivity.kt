package com.kloxus.app.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.kloxus.app.R
import com.kloxus.app.adapters.AddPaymentMethodAdapter
import com.kloxus.app.interfaces.OnCardClickInterface
import com.kloxus.app.model.CardsData
import com.kloxus.app.model.GetCardsModel
import com.kloxus.app.model.StatusMsgModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.AUTH_TOKEN
import com.kloxus.app.utils.KloxusPrefrences
import com.kloxus.app.utils.USER_ID
import kotlinx.android.synthetic.main.activity_edit_profile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PaymentActivity : BaseActivity() {

    lateinit var mAddPaymentMethodAdapter: AddPaymentMethodAdapter
    @BindView(R.id.paymentMethodRV)
    lateinit var paymentMethodRV: RecyclerView
    @BindView(R.id.addPaymentLL)
    lateinit var addPaymentLL: LinearLayout
    @BindView(R.id.imgCrossIV)
    lateinit var imgCrossIV: ImageView
//    @BindView(R.id.txtNoDataFountTV)
//    lateinit var txtNoDataFountTV: TextView
    var mAllCardsList: ArrayList<CardsData>? = ArrayList()
    var mPosition: Int = 0
    var mCard_id: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        ButterKnife.bind(this)
        setStatusBar(mActivity)

    }

    override fun onResume() {
        super.onResume()
//        getAllCards()
    }

    @OnClick(
        R.id.imgCrossIV,
        R.id.addPaymentLL
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgCrossIV -> performCancelClick()
            R.id.addPaymentLL -> performAddPaymentClick()
        }
    }

    private fun performCancelClick() {
       onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    private fun performAddPaymentClick() {
//        val i = Intent(this, AddPaymentMethodActivity::class.java)
////        i.putExtra("value","payment")
//        startActivity(i)
    }



    private fun getAllCards() {
        if (isNetworkAvailable(mActivity))
            executeGetAllCardsRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetAllCardsRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getCardDetailsRequest(mParam())
            ?.enqueue(object : Callback<GetCardsModel> {
                override fun onFailure(call: Call<GetCardsModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<GetCardsModel>,
                    response: Response<GetCardsModel>
                ) {

                    dismissProgressDialog()
                    val mModel: GetCardsModel? = response.body()
                        if (mModel!!.status.equals(1)) {
                            mAllCardsList = mModel.data
                            getCardsAdapter()
//                            paymentMethodRV.setVisibility(View.GONE)
//                            txtNoDataFountTV.setVisibility(View.GONE)
                        }
                        else {
//                            txtNoDataFountTV.setVisibility(View.GONE)
//                            paymentMethodRV.setVisibility(View.GONE)
//                            txtNoDataFountTV.setText(mModel.message)
                        }
                }
            })
    }

    private fun getCardsAdapter() {
        mAddPaymentMethodAdapter = AddPaymentMethodAdapter(mActivity, mAllCardsList,mItemCardClickListner)
        paymentMethodRV.layoutManager = LinearLayoutManager(mActivity)
        paymentMethodRV.adapter = mAddPaymentMethodAdapter
        }


    var mItemCardClickListner: OnCardClickInterface = object : OnCardClickInterface {
        override fun onItemClickListner(mCard_id: String, mPosition: Int) {
            this@PaymentActivity.mPosition = mPosition
            this@PaymentActivity.mCard_id = mCard_id
            deleteCardApi()
        }
    }

    private fun deleteCardApi() {
        if (isNetworkAvailable(mActivity))
            executeDeleteCardRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))

    }


    /*
     * Execute api param
     * */
    private fun mDeleteCardParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["cardID"] = mCard_id
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeDeleteCardRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.deleteCardRequest(mDeleteCardParam())
            ?.enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {

                    dismissProgressDialog()
                    val mModel: StatusMsgModel? = response.body()
                        if (mModel!!.status.equals(1))  {
                            showToast(mActivity, mModel.message)
                        mAllCardsList!!.removeAt(mPosition)
                        mAddPaymentMethodAdapter.notifyDataSetChanged()
                        if (mAllCardsList!!.size > 0) {
                            paymentMethodRV.visibility = View.VISIBLE
//                            txtNoDataFountTV.visibility = View.GONE
                        } else {
//                            paymentMethodRV.visibility = View.GONE
//                            txtNoDataFountTV.visibility = View.VISIBLE
                        }
                    } else if (mModel!!.status == 0) {
                        showToast(mActivity, mModel.message)
                    }
                    else{
                        showToast(mActivity, mModel.message)
                }
                    }
            })
    }
}