package com.kloxus.app.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.kloxus.app.R


class AddPaymentMethodActivity : BaseActivity() {

    @BindView(R.id.paypalLL)
    lateinit var paypalLL: LinearLayout

    @BindView(R.id.creditLL)
    lateinit var creditLL: LinearLayout

    @BindView(R.id.stripeLL)
    lateinit var stripeLL: LinearLayout

    @BindView(R.id.imgBackIV)
    lateinit var imgBackIV: ImageView
    var value: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_payment_method)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        getIntentData()
    }


    private fun getIntentData() {
        if (intent != null) {
//            value = intent.getStringExtra("value").toString()
        }
    }

    @OnClick(
        R.id.imgBackIV,
        R.id.stripeLL,
        R.id.paypalLL,
        R.id.creditLL
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackIV -> performCancelClick()
            R.id.creditLL -> performCreditClick()
            R.id.paypalLL -> performPaypalClick()
            R.id.stripeLL -> performStripeClick()
        }
    }

    private fun performStripeClick() {
        val i = Intent(this, AddCardActivity::class.java)
        startActivity(i)
    }

    private fun performPaypalClick() {
        val i = Intent(this, AddCardActivity::class.java)
        startActivity(i)
    }

    private fun performCreditClick() {
        val i = Intent(this, AddCardActivity::class.java)
        startActivity(i)

    }

    private fun performCancelClick() {
        onBackPressed()
    }

}