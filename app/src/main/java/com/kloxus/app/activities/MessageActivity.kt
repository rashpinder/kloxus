package com.kloxus.app.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.kloxus.app.R
import com.kloxus.app.adapters.ChatUserDataAdapter
import com.kloxus.app.interfaces.GetChatUserInterface
import com.kloxus.app.interfaces.ItemChatClickListner
import com.kloxus.app.interfaces.LoadMoreScrollListner
import com.kloxus.app.model.*
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class MessageActivity : BaseActivity() {

    lateinit var mChatUserDataAdapter: ChatUserDataAdapter

    @BindView(R.id.messagesRV)
    lateinit var messagesRV: RecyclerView

    @BindView(R.id.txtNoDataFountTV)
    lateinit var txtNoDataFountTV: TextView

    @BindView(R.id.imgCrossIV)
    lateinit var imgCrossIV: ImageView

    val mUserArrayList: ArrayList<GetAllChatUsersData?>? = ArrayList()
//    var mPageNo: Int = 1
//    var mPerPage: Int = 70
    var mPosition: Int = -1


    @BindView(R.id.mProgressRL)
    lateinit var mProgressRL: RelativeLayout

    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var isLoading: Boolean = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_messages)
        ButterKnife.bind(this)
        setStatusBar(mActivity)

    }

    override fun onResume() {
        super.onResume()
        if (mUserArrayList!=null){
            mUserArrayList.clear()
        }
        mPageNo=1
        getAllChatData()
    }

    fun getAllChatData() {
        if (isNetworkAvailable(mActivity)) {
            executeAllChatRequest()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }


    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(mActivity, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(mActivity, AUTH_TOKEN, null)
        mMap["perPage"] = "" + mPerPage
        mMap["pageNo"] = "" + mPageNo

        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeAllChatRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getAllChatUsersRequest(mParam())
            ?.enqueue(object : Callback<GetAllChatUsersModel> {
                override fun onFailure(call: Call<GetAllChatUsersModel>, t: Throwable) {
                    dismissProgressDialog()
                    Log.e(TAG, t.message.toString())
                }

                override fun onResponse(
                    call: Call<GetAllChatUsersModel>,
                    response: Response<GetAllChatUsersModel>
                ) {
                    dismissProgressDialog()
                    Log.e(TAG, response.body().toString())
                    val mModel = response.body()
                    if (mModel?.status == 1) {
                        isLoading = !mModel.lastPage.equals(true)
                        txtNoDataFountTV.visibility = View.GONE
                        mModel.data?.let { mUserArrayList?.addAll(it) }
                        setDataOnWidgets()
                    } else if (mModel?.status == 0) {
                        messagesRV.visibility = View.GONE
                        txtNoDataFountTV.visibility = View.VISIBLE
                        txtNoDataFountTV.text=mModel.message

                    }
                }
            })
    }


    var mLoadMoreScrollListner: GetChatUserInterface = object : GetChatUserInterface {
        override fun onLoadMoreListner(mModel: GetAllChatUsersData) {
            if (isLoading) {
                ++mPageNo
                executeMoreDataRequest()
            }
        }
    }

    private fun mLoadMoreParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(mActivity, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(mActivity, AUTH_TOKEN, null)
        mMap["perPage"] = "" + mPerPage
        mMap["pageNo"] = "" + mPageNo

        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }


    private fun executeMoreDataRequest() {
        mProgressRL.visibility = View.VISIBLE
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getAllChatUsersRequest(mLoadMoreParam())
            ?.enqueue(object : Callback<GetAllChatUsersModel> {
                override fun onFailure(call: Call<GetAllChatUsersModel>, t: Throwable) {
                    dismissProgressDialog()
                    mProgressRL.visibility = View.GONE
                }

                override fun onResponse(
                    call: Call<GetAllChatUsersModel>,
                    response: Response<GetAllChatUsersModel>
                ) {
                    mProgressRL.visibility = View.GONE
                    var mModel = response.body()!!
                    if (mModel.status == 1) {
                        mModel.data.let {
                            mUserArrayList!!.addAll<GetAllChatUsersData>(
                                it
                            )
                        }
                        mChatUserDataAdapter.notifyDataSetChanged()
                        isLoading = !mModel.lastPage.equals(true)
                    } else if (mModel.status == 0) {
//                    showToast(mActivity, mGetFavModel.message)
                    }
                }
            })
    }


    private fun setDataOnWidgets() {
        if (mUserArrayList!!.size > 0) {
            messagesRV.visibility = View.VISIBLE
            txtNoDataFountTV.visibility = View.GONE
            mChatUserDataAdapter =
                ChatUserDataAdapter(mActivity, mUserArrayList, mItemChatClickListner,mLoadMoreScrollListner)
            messagesRV.layoutManager = LinearLayoutManager(mActivity)
            messagesRV.adapter = mChatUserDataAdapter
        } else {
            messagesRV.visibility = View.GONE
            txtNoDataFountTV.visibility = View.VISIBLE
        }
    }

    var mItemChatClickListner: ItemChatClickListner = object : ItemChatClickListner {
        override fun onItemClickListner(mModel: GetAllChatUsersData, position: Int) {
            mPosition = position
            var mIntent = Intent(mActivity, ChatActivity::class.java)
            mIntent.putExtra(USER_NAME, mModel.name)
            mIntent.putExtra(ROOM_ID, mModel.roomID)
            mIntent.putExtra("other_user_id", mModel.otherUserID)
            startActivityForResult(mIntent, 808)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null && requestCode == 808) {
            if (data!!.getStringExtra(LAST_MSG) != null) {
                mUserArrayList!!.get(mPosition)!!.lastMessage = data!!.getStringExtra(LAST_MSG)!!
                mUserArrayList!!.add(0, mUserArrayList!!.get(mPosition))
                mUserArrayList!!.removeAt(mPosition + 1)
                mChatUserDataAdapter.notifyDataSetChanged()
            }
        }
    }



    @OnClick(R.id.imgCrossIV)
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgCrossIV -> performCrossClick()
        }
    }

    private fun performCrossClick() {
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

}