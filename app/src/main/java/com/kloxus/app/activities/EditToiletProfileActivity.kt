package com.kloxus.app.activities

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.*
import android.graphics.drawable.Drawable
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.View.VISIBLE
import android.view.WindowManager
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.exifinterface.media.ExifInterface
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.kloxus.app.R
import com.kloxus.app.model.AddEditToiletModel
import com.kloxus.app.model.ToiletDetailModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.*
import com.suke.widget.SwitchButton
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_add_toilet.*
import kotlinx.android.synthetic.main.activity_add_toilet.imgCheckDisinfectantIV
import kotlinx.android.synthetic.main.activity_add_toilet.imgCheckServietteIV
import kotlinx.android.synthetic.main.activity_add_toilet.imgCheckSoapIV
import kotlinx.android.synthetic.main.activity_add_toilet.imgCheckToiletPaperIV
import kotlinx.android.synthetic.main.activity_add_toilet.imgCheckhandDryerIV
import kotlinx.android.synthetic.main.activity_edit_toilet_profile.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*


class EditToiletProfileActivity : BaseActivity(), SwitchButton.OnCheckedChangeListener {

    @BindView(R.id.imgImageIV1)
    lateinit var imgImageIV1: ImageView

    @BindView(R.id.imgImageIV2)
    lateinit var imgImageIV2: ImageView

    @BindView(R.id.imgImageIV3)
    lateinit var imgImageIV3: ImageView

    @BindView(R.id.imgImageIV4)
    lateinit var imgImageIV4: ImageView

    @BindView(R.id.imgCancelIV1)
    lateinit var imgCancelIV1: ImageView

    @BindView(R.id.imgCancelIV2)
    lateinit var imgCancelIV2: ImageView

    @BindView(R.id.imgCancelIV3)
    lateinit var imgCancelIV3: ImageView

    @BindView(R.id.imgCancelIV4)
    lateinit var imgCancelIV4: ImageView

    @BindView(R.id.availableOpenTimeRL)
    lateinit var availableOpenTimeRL: RelativeLayout

    @BindView(R.id.GenderRL)
    lateinit var GenderRL: RelativeLayout

    @BindView(R.id.GenderFemaleRL)
    lateinit var GenderFemaleRL: RelativeLayout

    @BindView(R.id.GenderOtherRL)
    lateinit var GenderOtherRL: RelativeLayout

    @BindView(R.id.imgCheckMaleIV)
    lateinit var imgCheckMaleIV: ImageView

    @BindView(R.id.imgCheckFemaleIV)
    lateinit var imgCheckFemaleIV: ImageView

    @BindView(R.id.imgCheckOtherIV)
    lateinit var imgCheckOtherIV: ImageView

    @BindView(R.id.editToiletNameET)
    lateinit var editToiletNameET: EditText

    @BindView(R.id.editDescriptionET)
    lateinit var editDescriptionET: EditText

    @BindView(R.id.ToiletNameRL)
    lateinit var ToiletNameRL: RelativeLayout

    @BindView(R.id.PriceRL)
    lateinit var PriceRL: RelativeLayout

    @BindView(R.id.addressRL)
    lateinit var addressRL: RelativeLayout

    @BindView(R.id.descRL)
    lateinit var descRL: RelativeLayout

    @BindView(R.id.imgBackIV)
    lateinit var imgBackIV: ImageView

//    @BindView(R.id.imgProfileIV)
//    lateinit var imgProfileIV: ImageView

    @BindView(R.id.editAddressET)
    lateinit var editAddressET: TextView

    @BindView(R.id.txtSaveTV)
    lateinit var txtSaveTV: TextView

    @BindView(R.id.txtPriceTV)
    lateinit var txtPriceTV: TextView

//    @BindView(R.id.GenderRL)
//    lateinit var GenderRL: RelativeLayout

    @BindView(R.id.txtStartTimeTV)
    lateinit var txtStartTimeTV: TextView

    @BindView(R.id.txtEndTimeTV)
    lateinit var txtEndTimeTV: TextView

    @BindView(R.id.startTimeLL)
    lateinit var startTimeLL: LinearLayout

    @BindView(R.id.endTimeLL)
    lateinit var endTimeLL: LinearLayout

    @BindView(R.id.switchActivateSB)
    lateinit var switchActivateSB: SwitchButton
//    @BindView(R.id.genderSpinner)
//    lateinit var genderSpinner: Spinner

    @BindView(R.id.editPriceET)
    lateinit var editPriceET: EditText

    @BindView(R.id.currRL)
    lateinit var currRL: RelativeLayout

    @BindView(R.id.currSpinner)
    lateinit var currSpinner: Spinner

    var gender = ""
    var options = ""
    var array_gender = ""
    var address: String? = null
    var city: String? = null
    var mBitmap: Bitmap? = null
    var value: String = ""
    var new_selectedHour = 0
    var start_time: String? = null
    var end_time: String? = null
    var AUTOCOMPLETE_REQUEST_CODE = 5
    var latLng: LatLng? = null
    var mLatitude = 0.0
    var mLongitude = 0.0
    var currency = ""
    var currency_symbol = ""
    var isRequestSend = "0"
    var arr_curr = ""
    var mImagesArrayList1: ArrayList<String> = ArrayList<String>()
    var sel_male_gender = "m_click"
    var sel_female_gender = "f_click"
    var sel_other_gender = "o_click"
    var mBitmapImage1: Bitmap? = null
    var mBitmapImage2: Bitmap? = null
    var mBitmapImage3: Bitmap? = null
    var mBitmapImage4: Bitmap? = null
    var mGenderArrayList: ArrayList<String> = ArrayList<String>()
    var mItemsList: ArrayList<String> = ArrayList<String>()
    var strImageType = ""

    var mImage1Url = ""
    var mImage2Url = ""
    var mImage3Url = ""
    var mImage4Url = ""
    var mImg1 = ""
    var mImg2 = ""
    var mImg3 = ""
    var mImg4 = ""
    var sel_soap = "soap_click"
    var sel_dryer = "dryer_click"
    var sel_disinfectant = "disinfectant_click"
    var sel_serviette = "serviette_click"
    var sel_paper = "paper_click"
    var paid_free = "paid"
    var isPaid = "1"


    @BindView(R.id.imgCheckPaidIV)
    lateinit var imgCheckPaidIV: ImageView

    @BindView(R.id.imgCheckFreeIV)
    lateinit var imgCheckFreeIV: ImageView
    @BindView(R.id.paidRL)
    lateinit var paidRL: RelativeLayout

    @BindView(R.id.freeRL)
    lateinit var freeRL: RelativeLayout

    @BindView(R.id.ToiletPaperRL)
    lateinit var ToiletPaperRL: RelativeLayout

    @BindView(R.id.soapRL)
    lateinit var soapRL: RelativeLayout

    @BindView(R.id.handDryerRL)
    lateinit var handDryerRL: RelativeLayout

    @BindView(R.id.disinfectantRL)
    lateinit var disinfectantRL: RelativeLayout

    @BindView(R.id.servietteRL)
    lateinit var servietteRL: RelativeLayout

    /*
        * Initialize Menifest Permissions:
        * & Camera Gallery Request @params
        * */
    var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    var writeCamera = Manifest.permission.CAMERA
    var mToiletId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_toilet_profile)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        getIntentData()
        switchActivateSB.setOnCheckedChangeListener(this)
//        setGenderAdapter()
        setCurrencyAdapter()
        getToiletProfileDetail()
        /*
               * For places Location google api
               * */initializePlaceSdKLocation()
        editDescriptionET.setOnTouchListener(OnTouchListener setOnTouchListener@{ v: View, event: MotionEvent ->
            if (editDescriptionET.hasFocus()) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (event.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_SCROLL -> {
                        v.parent.requestDisallowInterceptTouchEvent(false)
                        return@setOnTouchListener true
                    }
                }
            }
            false
        })

        editTextSignupSelector(editToiletNameET, ToiletNameRL, "")
        editTextSignupSelector(editPriceET, PriceRL, "")
        editTextSignupSelector(editDescriptionET, descRL, "")

    }

    private fun performPaidClick() {
        imgCheckPaidIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
        imgCheckFreeIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        txtPriceTV.isVisible=true
        editPriceET.setText("")
        PriceRL.isVisible=true
        paid_free = "paid"
        isPaid = "1"
    }

    private fun performFreeClick() {
        imgCheckPaidIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        imgCheckFreeIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
        txtPriceTV.isVisible=false
        PriceRL.isVisible=false
        paid_free = "free"
        isPaid = "0"
    }

    private fun setCurrencyAdapter() {
        val mCurrArry = resources.getStringArray(R.array.currency)
        // access the spinner
        if (currSpinner != null) {
            val adapter = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item, mCurrArry
            )
            currSpinner.adapter = adapter
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            currSpinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View, position: Int, id: Long
                ) {
                    arr_curr = mCurrArry[position]
                    if (arr_curr.equals("$")) {
                        currency = "usd"
                        currency_symbol = "$"
                    } else {
                        currency = "eur"
                        currency_symbol = "€"
                    }
                    Log.e(TAG, "currency" + currency)
                    PriceRL.setBackgroundResource(R.drawable.bg_et_selected)
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }
    }

    /*
     * Initailze places location
     *
     * */
    fun initializePlaceSdKLocation() {
        // Initialize the SDK
        Places.initialize(mActivity, getString(R.string.places_api_key))

        // Create a new Places client instance
        val placesClient: PlacesClient = Places.createClient(mActivity)
    }


    private fun setSearchIntent() {
        // Set the fields to specify which types of place data to
        // return after the user has made a selection.
        val fields: List<Place.Field> = Arrays.asList<Place.Field>(
            Place.Field.ID,
            Place.Field.NAME,
            Place.Field.LAT_LNG,
            Place.Field.ADDRESS
        )
        // Start the autocomplete intent.
        val intent: Intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.FULLSCREEN, fields
        )
            .build(mActivity)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }

    fun showCursorAtEnd(mEditText: EditText) {
        mEditText.setSelection(mEditText.text.toString().length)
    }

    fun showTimePicker(mTextview: TextView) {
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime[Calendar.HOUR_OF_DAY]
        val minute = mcurrentTime[Calendar.MINUTE]
        val am = mcurrentTime[Calendar.AM_PM]
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(
            mActivity, AlertDialog.THEME_HOLO_LIGHT,
            { timePicker, selectedHour, selectedMinute ->
                val am_pm: String
                if (selectedHour > 12) {
                    new_selectedHour = selectedHour - 12
                    am_pm = "pm"
                } else if (selectedHour == 12) {
                    new_selectedHour = selectedHour
                    am_pm = "pm"
                }
                else if (selectedHour == 0) {
                    new_selectedHour = 12
                    am_pm = "am"
                }
                else {
                    new_selectedHour = selectedHour
                    am_pm = "am"
                }
                // mTextview.setText(selectedHour + ":" + selectedMinute);
                mTextview.text =
                    new_selectedHour.toString() + ":" + convertDate(
                        selectedMinute
                    ) + " " + am_pm
                mTextview.setTextColor(resources.getColor(R.color.black))
                start_time = mTextview.text.toString().trim { it <= ' ' }
            }, hour, minute, false
        ) //Yes 24 hour time
        mTimePicker.setTitle("Select Time")
        mTimePicker.window!!.setLayout(
            800,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        mTimePicker.window!!.setGravity(Gravity.CENTER)
        mTimePicker.show()

    }

    fun showEndTimePicker(mTextview: TextView) {
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime[Calendar.HOUR_OF_DAY]
        val minute = mcurrentTime[Calendar.MINUTE]
        val am = mcurrentTime[Calendar.AM_PM]
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(
            mActivity, AlertDialog.THEME_HOLO_LIGHT,
            { timePicker, selectedHour, selectedMinute ->
                val am_pm: String
                if (selectedHour > 12) {
                    new_selectedHour = selectedHour - 12
                    am_pm = "pm"
                } else if (selectedHour == 12) {
                    new_selectedHour = selectedHour
                    am_pm = "pm"
                }
                else if (selectedHour == 0) {
                    new_selectedHour = 12
                    am_pm = "am"
                }
                else {
                    new_selectedHour = selectedHour
                    am_pm = "am"
                }
                // mTextview.setText(selectedHour + ":" + selectedMinute);
                mTextview.text =
                    new_selectedHour.toString() + ":" + convertEndDate(selectedMinute) + " " + am_pm
                mTextview.setTextColor(resources.getColor(R.color.black))
                end_time = mTextview.text.toString().trim { it <= ' ' }

            }, hour, minute, false
        ) //Yes 24 hour time
        mTimePicker.setTitle("Select Time")
        mTimePicker.window!!.setLayout(
            800,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        mTimePicker.window!!.setGravity(Gravity.CENTER)
        mTimePicker.show()
    }


    fun convertDate(input: Int): String? {
        return if (input >= 10) {
            input.toString()
        } else {
            "0$input"
        }
    }

    fun convertEndDate(input: Int): String {
        return if (input >= 10) {
            input.toString()
        } else {
            "0$input"
        }
    }


//    /*
//     * Gender adapter
//     * */
//    private fun setGenderAdapter() {
//        val mStatusArry = resources.getStringArray(R.array.gender)
//        // access the spinner
//        if (genderSpinner != null) {
//            val adapter = ArrayAdapter(
//                this,
//                android.R.layout.simple_spinner_item, mStatusArry
//            )
//            genderSpinner.adapter = adapter
//            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//
//            genderSpinner.onItemSelectedListener = object :
//                AdapterView.OnItemSelectedListener {
//                override fun onItemSelected(
//                    parent: AdapterView<*>,
//                    view: View, position: Int, id: Long
//                ) {
//                    array_gender = mStatusArry[position]
//                    if (array_gender=="Male"){
//                        gender="1"
//                    }  else if (array_gender=="Female"){
//                        gender="2"
//                    }else{
//                        gender="3"
//                    }
//                    GenderRL.setBackgroundResource(R.drawable.bg_et_selected)
//                }
//
//                override fun onNothingSelected(parent: AdapterView<*>) {
//                    // write code to perform some action
//                }
//            }
//        }
//    }

    private fun getIntentData() {
        if (intent != null) {
            mToiletId = intent.getStringExtra("toilet_id").toString()

        }
    }

    private fun getToiletProfileDetail() {
        if (isNetworkAvailable(mActivity))
            executeGetToiletDetailRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["toiletID"] = mToiletId
        mMap["timeZone"] = TimeZone.getDefault().id
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetToiletDetailRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getToiletDetailRequest(mParam())
            ?.enqueue(object : Callback<ToiletDetailModel> {
                override fun onFailure(call: Call<ToiletDetailModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<ToiletDetailModel>,
                    response: Response<ToiletDetailModel>
                ) {
                    dismissProgressDialog()
                    val mModel: ToiletDetailModel? = response.body()

                    if (mGenderArrayList != null) {
                        mGenderArrayList.clear()
                    }
                    if (mItemsList != null) {
                        mItemsList.clear()
                    }
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            //Set data on widgets
                            setDataOnWidgets(mModel)
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }

    private fun setDataOnWidgets(mModel: ToiletDetailModel) {
        if (mModel.data.toiletImage != null && mModel.data.toiletImage.contains("http")) {
//            Glide.with(mActivity).load(mModel.data.toiletImage)
//                .placeholder(R.drawable.ic_upp)
//                .error(R.drawable.ic_upp)
//                .into(imgImageIV1)


            Glide.with(this).asBitmap()
                .load(mModel.data.toiletImage)
                .into(object : CustomTarget<Bitmap?>() {
                    override fun onLoadCleared(placeholder: Drawable?) {
                    }

                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: com.bumptech.glide.request.transition.Transition<in Bitmap?>?
                    ) {
                        imgImageIV1.setImageBitmap(resource)
                        //mBitmap1 = resource
                        mBitmapImage1 = resource
                    }
                })


            mImage1Url = mModel.data.toiletImage
            imgCancelIV1.visibility = VISIBLE
        }
        if (mModel.data.toiletImage1 != null && mModel.data.toiletImage1.contains("http")) {

//            Glide.with(mActivity).load(mModel.data.toiletImage1)
//                .placeholder(R.drawable.ic_upp)
//                .error(R.drawable.ic_upp).into(imgImageIV2)

            Glide.with(this).asBitmap()
                .load(mModel.data.toiletImage1)
                .into(object : CustomTarget<Bitmap?>() {
                    override fun onLoadCleared(placeholder: Drawable?) {
                    }

                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: com.bumptech.glide.request.transition.Transition<in Bitmap?>?
                    ) {
                        imgImageIV2.setImageBitmap(resource)
                        //mBitmap1 = resource
                        mBitmapImage2 = resource
                    }
                })

            imgCancelIV2.visibility = VISIBLE
            mImage2Url = mModel.data.toiletImage1
        }

        if (mModel.data.toiletImage2 != null && mModel.data.toiletImage2.contains("http")) {
//            Glide.with(mActivity).load(mModel.data.toiletImage2)
//                .placeholder(R.drawable.ic_upp)
//                .error(R.drawable.ic_upp)
//                .into(imgImageIV3)


            Glide.with(this).asBitmap()
                .load(mModel.data.toiletImage2)
                .into(object : CustomTarget<Bitmap?>() {
                    override fun onLoadCleared(placeholder: Drawable?) {
                    }

                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: com.bumptech.glide.request.transition.Transition<in Bitmap?>?
                    ) {
                        imgImageIV3.setImageBitmap(resource)
                        //mBitmap1 = resource
                        mBitmapImage3 = resource
                    }
                })

            imgCancelIV3.visibility = VISIBLE
            mImage3Url = mModel.data.toiletImage2
        }
        if (mModel.data.toiletImage3 != null && mModel.data.toiletImage3.contains("http")) {
//            Glide.with(mActivity).load(mModel.data.toiletImage3)
//                .placeholder(R.drawable.ic_upp)
//                .error(R.drawable.ic_upp)
//                .into(imgImageIV4)


            Glide.with(this).asBitmap()
                .load(mModel.data.toiletImage3)
                .into(object : CustomTarget<Bitmap?>() {
                    override fun onLoadCleared(placeholder: Drawable?) {
                    }

                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: com.bumptech.glide.request.transition.Transition<in Bitmap?>?
                    ) {
                        imgImageIV4.setImageBitmap(resource)
                        //mBitmap1 = resource
                        mBitmapImage4 = resource
                    }
                })

            imgCancelIV4.visibility = VISIBLE
            mImage4Url = mModel.data.toiletImage3
        }


        editToiletNameET.setText(mModel.data.title)
        editAddressET.text = mModel.data.address
        editDescriptionET.setText(mModel.data.description)


       if (mModel.data.isPaid.equals("1")){
           txtPriceTV.isVisible=true
           PriceRL.isVisible=true
           imgCheckPaidIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
           imgCheckFreeIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
           paid_free = "paid"
           isPaid = "1"
           editPriceET.setText(mModel.data.price)
       }
        else{
           txtPriceTV.isVisible=false
           PriceRL.isVisible=false
           imgCheckPaidIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
           imgCheckFreeIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
           paid_free = "free"
           isPaid = "0"
           editPriceET.setText("")
       }

        if (mModel.data.isRequestSend.equals("1")) {
            switchActivateSB.setChecked(true)
            isRequestSend = "1"
        } else {
            switchActivateSB.setChecked(false)
            isRequestSend = "0"
        }

        editPriceET.setText(mModel.data.price)
        KloxusPrefrences().writeString(
            mActivity,
            LOC_ADD_TOILET,
            mModel.data.toiletLocation
        )

        KloxusPrefrences().writeString(
            mActivity,
            LAT_ADD_TOILET,
            mModel.data.lat
        )
        KloxusPrefrences().writeString(
            mActivity,
            LNG_ADD_TOILET,
            mModel.data.log
        )
        editPriceET.setText(mModel.data.price)
        gender = mModel.data.gender
        for (i in mModel.data.newGender.indices) {

            mGenderArrayList.add(mModel.data.newGender.get(i))
        }
        if (mGenderArrayList.size == 1) {
            if (mGenderArrayList.contains("1")) {
                imgCheckMaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                sel_male_gender = "male_click"
            } else if (mGenderArrayList.contains("2")) {
                imgCheckFemaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                sel_female_gender = "female_click"
            } else if (mGenderArrayList.contains("3")) {
                imgCheckOtherIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                sel_other_gender = "other_click"
            }
        } else if (mGenderArrayList.size == 2) {
            if (mGenderArrayList.contains("3") && mGenderArrayList.contains("1")) {
                imgCheckOtherIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                imgCheckMaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                sel_male_gender = "male_click"
                sel_other_gender = "other_click"
            } else if (mGenderArrayList.contains("3") && mGenderArrayList.contains(
                    "2"
                )
            ) {
                imgCheckOtherIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                imgCheckFemaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                sel_female_gender = "female_click"
                sel_other_gender = "other_click"
            } else if (mGenderArrayList.contains("1") && mGenderArrayList.contains(
                    "2"
                )
            ) {
                imgCheckMaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                imgCheckFemaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                sel_female_gender = "female_click"
                sel_male_gender = "male_click"
            }
        } else {
            if (mGenderArrayList.contains("1") && mGenderArrayList.contains("2") && mGenderArrayList.contains(
                    "3"
                )
            ) {
                imgCheckMaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                imgCheckFemaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                imgCheckOtherIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                sel_female_gender = "female_click"
                sel_other_gender = "other_click"
                sel_male_gender = "male_click"
            }
        }

//        options = mModel.data.toiletOptions
        for (i in mModel.data.toiletOptions.indices) {

            mItemsList.add(mModel.data.toiletOptions.get(i))
        }
        if (mItemsList.contains("3")&&mItemsList.contains("2") && mItemsList.contains("4") && mItemsList.contains("1")&& mItemsList.contains("5")) {
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("4")
//            mItemsList.add("1")
//            mItemsList.add("5")
//            mItemsList.add("2")
//            mItemsList.add("3")
        }
        else if (mItemsList.contains("1") && mItemsList.contains("2") && mItemsList.contains("3")&& mItemsList.contains("4")) {
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("2")
//            mItemsList.add("3")
//            mItemsList.add("4")
//            mItemsList.add("1")
        }
        else if (mItemsList.contains("1") && mItemsList.contains("2") && mItemsList.contains("3")&& mItemsList.contains("5")) {
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("2")
//            mItemsList.add("3")
//            mItemsList.add("5")
//            mItemsList.add("1")
        }else if (mItemsList.contains("1") && mItemsList.contains("4") && mItemsList.contains("3")&& mItemsList.contains("5")) {
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("4")
//            mItemsList.add("3")
//            mItemsList.add("5")
//            mItemsList.add("1")
        }
        else if (mItemsList.contains("2") && mItemsList.contains("4") && mItemsList.contains("3")&& mItemsList.contains("5")) {
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("4")
//            mItemsList.add("3")
//            mItemsList.add("5")
//            mItemsList.add("2")
        }
        else if (mItemsList.contains("2") && mItemsList.contains("4") && mItemsList.contains("1")&& mItemsList.contains("5")) {
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("4")
//            mItemsList.add("1")
//            mItemsList.add("5")
//            mItemsList.add("2")
        }

        else if (mItemsList.contains("1") && mItemsList.contains("2") && mItemsList.contains("3")) {
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("1")
//            mItemsList.add("2")
//            mItemsList.add("3")
        }   else if (mItemsList.contains("3") && mItemsList.contains("4") && mItemsList.contains("5")) {
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("1")
//            mItemsList.add("2")
//            mItemsList.add("3")
        } else if (mItemsList.contains("1") && mItemsList.contains("3") && mItemsList.contains("4")) {
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("1")
//            mItemsList.add("4")
//            mItemsList.add("3")
        }
        else if (mItemsList.contains("1") && mItemsList.contains("2") && mItemsList.contains("4")) {
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//
        } else if (mItemsList.contains("1") && mItemsList.contains("5") && mItemsList.contains("4")) {
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("1")
//            mItemsList.add("4")
//            mItemsList.add("5")
        }else if (mItemsList.contains("2") && mItemsList.contains("3") && mItemsList.contains("4")) {
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("2")
//            mItemsList.add("4")
//            mItemsList.add("3")
        }else if (mItemsList.contains("2") && mItemsList.contains("3") && mItemsList.contains("5")) {
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("2")
//            mItemsList.add("5")
//            mItemsList.add("3")
        }else if (mItemsList.contains("2") && mItemsList.contains("4") && mItemsList.contains("5")) {
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("2")
//            mItemsList.add("5")
//            mItemsList.add("4")
        }
        else if (mItemsList.contains("1") && mItemsList.contains("3") && mItemsList.contains("5")) {
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
        }
        else if (mItemsList.contains("1") && mItemsList.contains("2")) {
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("1")
//            mItemsList.add("2")
        } else if (mItemsList.contains("1") && mItemsList.contains("3")) {
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("1")
//            mItemsList.add("3")
        } else if (mItemsList.contains("1") && mItemsList.contains("4")) {
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("1")
//            mItemsList.add("4")
        } else if (mItemsList.contains("1") && mItemsList.contains("5")) {
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("1")
//            mItemsList.add("5")
        } else if (mItemsList.contains("2") && mItemsList.contains("3")) {
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("1")
//            mItemsList.add("3")
        } else if (mItemsList.contains("2") && mItemsList.contains("4")) {
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("2")
//            mItemsList.add("4")
        } else if (mItemsList.contains("2") && mItemsList.contains("5")) {
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("2")
//            mItemsList.add("5")
        } else if (mItemsList.contains("3") && mItemsList.contains("4")) {
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("3")
//            mItemsList.add("4")
        } else if (mItemsList.contains("3") && mItemsList.contains("5")) {
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("3")
//            mItemsList.add("5")
        } else if (mItemsList.contains("4") && mItemsList.contains("5")) {
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("4")
//            mItemsList.add("5")
        }
        else if (mItemsList.contains("1")) {
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("1")
        } else if (mItemsList.contains("2")) {
//            mItemsList.add("2")
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
        } else if (mItemsList.contains("3")) {
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("3")
        } else if (mItemsList.contains("4")) {
//            mItemsList.add("4")
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
        } else if (mItemsList.contains("5")) {
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
//            mItemsList.add("5")
        }

        if (mModel.data.currencySymbol.equals("$")) {
            currSpinner.setSelection(0)
        } else {
            currSpinner.setSelection(1)
        }

        txtStartTimeTV.text = mModel.data.openTime + " "
        txtEndTimeTV.text = " " + mModel.data.closeTime
        showCursorAtEnd(editToiletNameET)
        showCursorAtEnd(editDescriptionET)
        showCursorAtEnd(editPriceET)
    }


    private fun getBodyForImage(imagePath: String, name: String): MultipartBody.Part {
        val selectedFile = File(imagePath)
        val filePathBody: RequestBody =
            selectedFile.asRequestBody("image/*".toMediaTypeOrNull())
        return MultipartBody.Part.createFormData(name, selectedFile.name, filePathBody)
    }


    @OnClick(
        R.id.imgBackIV,
        R.id.txtSaveTV,
        R.id.startTimeLL,
        R.id.endTimeLL,
        R.id.editAddressET,
        R.id.imgImageIV1,
        R.id.imgCancelIV1,
        R.id.imgImageIV2,
        R.id.imgCancelIV2,
        R.id.imgImageIV3,
        R.id.imgCancelIV3,
        R.id.imgImageIV4,
        R.id.imgCancelIV4,
        R.id.GenderRL,
        R.id.GenderFemaleRL,
        R.id.GenderOtherRL,
        R.id.ToiletPaperRL,
        R.id.handDryerRL,
        R.id.servietteRL,
        R.id.soapRL,
        R.id.disinfectantRL,
        R.id.freeRL,
        R.id.paidRL
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackIV -> performCancelClick()
            R.id.txtSaveTV -> performSaveClick()
            R.id.endTimeLL -> performEndTimeClick()
            R.id.startTimeLL -> performStartTimeClick()
            R.id.editAddressET -> setSearchIntent()
            R.id.GenderRL -> performMaleClick()
            R.id.GenderFemaleRL -> performFemaleClick()
            R.id.GenderOtherRL -> performOtherClick()
            R.id.imgImageIV1 -> performImageOneClick()
            R.id.imgImageIV2 -> performImageTwoClick()
            R.id.imgImageIV3 -> performImageThreeClick()
            R.id.imgImageIV4 -> performImagefourClick()
            R.id.imgCancelIV1 -> performCancelImageOneClick()
            R.id.imgCancelIV2 -> performCancelImageTwoClick()
            R.id.imgCancelIV3 -> performCancelImageThreeClick()
            R.id.imgCancelIV4 -> performCancelImageFourClick()
            R.id.ToiletPaperRL -> performToiletPaperClick()
            R.id.handDryerRL -> performHandDryerClick()
            R.id.servietteRL -> performServietteClick()
            R.id.soapRL -> performSoapClick()
            R.id.disinfectantRL -> performDisInfectantClick()
            R.id.freeRL -> performFreeClick()
            R.id.paidRL -> performPaidClick()

        }
    }


    private fun performDisInfectantClick() {
        if (mItemsList.contains("5")) {
            mItemsList.remove("5")
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        } else {
            mItemsList.add("5")
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
        }

    }

    private fun performSoapClick() {
        if (mItemsList.contains("2")) {
            mItemsList.remove("2")
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        } else {
            mItemsList.add("2")
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
        }

    }

    private fun performHandDryerClick() {
        if (mItemsList.contains("3")) {
            mItemsList.remove("3")
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        } else {
            mItemsList.add("3")
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
        }

    }

    private fun performServietteClick() {
        if (mItemsList.contains("4")) {
            mItemsList.remove("4")
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        } else {
            mItemsList.add("4")
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
        }

    }

    private fun performToiletPaperClick() {
        if (mItemsList.contains("1")) {
            mItemsList.remove("1")
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        } else {
            mItemsList.add("1")
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
        }
    }


    private fun performCancelImageThreeClick() {
        imgCancelIV3.visibility = View.GONE
        imgImageIV3.setImageResource(R.drawable.ic_upp)
        mBitmapImage3 = null
        mImage3Url = ""
        mImg3 = ""
    }

    private fun performCancelImageTwoClick() {
        imgCancelIV2.visibility = View.GONE
        imgImageIV2.setImageResource(R.drawable.ic_upp)
        mBitmapImage2 = null
        mImage2Url = ""
        mImg2 = ""
    }

    private fun performCancelImageOneClick() {
        imgCancelIV1.visibility = View.GONE
        imgImageIV1.setImageResource(R.drawable.ic_upp)
        mBitmapImage1 = null
        mImage1Url = ""
        mImg1 = ""
    }

    private fun performImagefourClick() {
        strImageType = "image4"
        performAddImgClick()
    }

    private fun performImageThreeClick() {
        strImageType = "image3"
        performAddImgClick()
    }

    private fun performImageTwoClick() {
        strImageType = "image2"
        performAddImgClick()
    }

    private fun performImageOneClick() {
        strImageType = "image1"
        performAddImgClick()
    }


    private fun performCancelImageFourClick() {
        imgCancelIV4.visibility = View.GONE
        imgImageIV4.setImageResource(R.drawable.ic_upp)
        mBitmapImage4 = null
        mImage4Url = ""
        mImg4 = ""
    }

    private fun performOtherClick() {
        if (sel_other_gender.equals("o_click")) {
            mGenderArrayList.add("3")
            imgCheckOtherIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            sel_other_gender = "other_click"
        } else {
            mGenderArrayList.remove("3")
            sel_other_gender = "o_click"
            imgCheckOtherIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        }
    }

    private fun performFemaleClick() {
        if (sel_female_gender.equals("f_click")) {
            mGenderArrayList.add("2")
            imgCheckFemaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            sel_female_gender = "female_click"
        } else {
            mGenderArrayList.remove("2")
            sel_female_gender = "f_click"
            imgCheckFemaleIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        }
    }

    private fun performMaleClick() {
        if (sel_male_gender.equals("m_click")) {
            mGenderArrayList.add("1")
            sel_male_gender = "male_click"
            imgCheckMaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
        } else {
            mGenderArrayList.remove("1")
            sel_male_gender = "m_click"
            imgCheckMaleIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        }

    }

    private fun performStartTimeClick() {
        availableOpenTimeRL.setBackgroundResource(R.drawable.bg_et_selected)
        showTimePicker(txtStartTimeTV)

    }

    private fun performEndTimeClick() {
        showEndTimePicker(txtEndTimeTV)

    }


    private fun performCancelClick() {
        onBackPressed()
    }

    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity, arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 -> {
//                if (this[0] == PackageManager.PERMISSION_GRANTED) {
                if (checkPermission() == true) {
                    onSelectImageClick()
                } else {
//                    requestPermission()
                    showToast(mActivity,"Please allow permissions of camera & gallery from settings")
                    Log.e(TAG, "**Permission Denied**")
                }
            }
        }
    }

    fun IntArray.onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>?
    ) {
        when (requestCode) {
            369 -> {
                if (this[0] == PackageManager.PERMISSION_GRANTED) {
                    onSelectImageClick()
                } else {
                    Log.e(TAG, "**Permission Denied**")
                }
            }
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            .crop()                    //Crop image(Optional), Check Customization for more option
            .compress(720)            //Final image size will be less than 1 MB(Optional)
            .maxResultSize(
                720,
                720
            )    //Final image resolution will be less than 1080 x 1080(Optional)
            .cropSquare()
            .start()
    }

    /**
     * Start crop image activity for the given image.
     */
    private fun startCropImageActivity(imageUri: Uri) {
        CropImage.activity(imageUri)
            .setGuidelines(CropImageView.Guidelines.OFF)
            .setAspectRatio(70, 70)
            .setMultiTouchEnabled(false)
            .start(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val place: Place = Autocomplete.getPlaceFromIntent(data!!)
                latLng = place.latLng
                mLatitude = latLng!!.latitude
                mLongitude = latLng!!.longitude
                Log.e(TAG, "*********SLATITUDE********$mLatitude")
                Log.e(TAG, "*********SLONGITUDE********$mLongitude")
                val gcd = Geocoder(this, Locale.getDefault())
                val addresses: List<Address> = gcd.getFromLocation(mLatitude, mLongitude, 1)
                if (addresses.size > 0) {
                    city = addresses[0].locality
                    KloxusPrefrences().writeString(
                        mActivity,
                        LOC_ADD_TOILET,
                        city
                    )
                } else {
                    // do your stuff
                }
                KloxusPrefrences().writeString(
                    mActivity,
                    LAT_ADD_TOILET,
                    mLatitude.toString()
                )
                KloxusPrefrences().writeString(
                    mActivity,
                    LNG_ADD_TOILET,
                    mLongitude.toString()
                )
//                Log.e(TAG, "*********SLATITUDE********$mLatitude")
//                Log.e(TAG, "*********SLONGITUDE********$mLongitude")
                address = place.address
//                city = place.name

                KloxusPrefrences().writeString(
                    mActivity,
                    SELECTED_TOILET_LOCATION,
                    address
                )
                editAddressET.text = place.address
                Log.e(
                    TAG,
                    "Place: " + place.name.toString() + ", " + place.id
                        .toString() + ", " + place.latLng
                        .toString() + ", " + place.address
                )
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                val status: Status =
                    Autocomplete.getStatusFromIntent(data!!)
                Log.e(TAG, "msg" + status.statusMessage)
            } else if (resultCode == RESULT_CANCELED) {
                showToast(mActivity, "Error")
//                Log.e(TAG, resultCode.toString())
                // The user canceled the operation.
            }
        } else {
            if (resultCode == Activity.RESULT_OK) {
                //Image Uri will not be null for RESULT_OK
                val uri: Uri = data?.data!!
                imgImageIV1.scaleType = ImageView.ScaleType.FIT_XY
                imgImageIV2.scaleType = ImageView.ScaleType.FIT_XY
                imgImageIV3.scaleType = ImageView.ScaleType.FIT_XY
                imgImageIV4.scaleType = ImageView.ScaleType.FIT_XY
                try {
                    val imageStream =
                        contentResolver.openInputStream(uri)

                    val selectedImage = BitmapFactory.decodeStream(imageStream)

                    if (strImageType == "image1") {
                        imgImageIV1.setImageBitmap(selectedImage)
                        imgCancelIV1.visibility = View.VISIBLE
                        mBitmapImage1 = selectedImage
                        //mImage1Url = compressImage(getRealPathFromURI(uri.toString()))

                        Log.e("img1", "img$mBitmapImage1")
                    } else if (strImageType == "image2") {
                        imgImageIV2.setImageBitmap(selectedImage)
                        imgCancelIV2.visibility = View.VISIBLE
                        mBitmapImage2 = selectedImage
                        //mImage2Url = compressImage(getRealPathFromURI(uri.toString()))
                    } else if (strImageType == "image3") {
                        imgImageIV3.setImageBitmap(selectedImage)
                        imgCancelIV3.visibility = View.VISIBLE
                        mBitmapImage3 = selectedImage
                        //mImage3Url = compressImage(getRealPathFromURI(uri.toString()))
                    } else if (strImageType == "image4") {
                        imgImageIV4.setImageBitmap(selectedImage)
                        imgCancelIV4.visibility = View.VISIBLE
                        mBitmapImage4 = selectedImage
                        //mImage4Url = compressImage(getRealPathFromURI(uri.toString()))
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                    Log.e(TAG, "msg" + e.message)
                }
            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(mActivity, "Image Not Valid", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(mActivity, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }

        }
    }

    private fun performSaveClick() {
        if (isValidate())
            if (isNetworkAvailable(mActivity))
                executeAddToiletRequest()
            else
                showToast(mActivity, getString(R.string.internet_connection_error))
    }

    private fun executeAddToiletRequest() {
        showProgressDialog(mActivity)
//        var mMultipartBody1: MultipartBody.Part? = null
//        var mMultipartBody2: MultipartBody.Part? = null
//        var mMultipartBody3: MultipartBody.Part? = null
//        var mMultipartBody4: MultipartBody.Part? = null
//        mMultipartBody1=getBodyForImage(mImage1Url,"toiletImage")
//        mMultipartBody2=getBodyForImage(mImage2Url,"toiletImage1")
//        mMultipartBody3=getBodyForImage(mImage3Url,"toiletImage2")
//        mMultipartBody4=getBodyForImage(mImage4Url,"toiletImage3")

        var mMultipartBody1: MultipartBody.Part? = null
        if (mBitmapImage1 != null) {
            val requestFile1: RequestBody? =
                convertBitmapToByteArrayUncompressed(mBitmapImage1!!)?.let {
                    RequestBody.create(
                        "multipart/form-data".toMediaTypeOrNull(),
                        it
                    )
                }
            mMultipartBody1 = requestFile1?.let {
                MultipartBody.Part.createFormData(
                    "toiletImage", getAlphaNumericString()!!.toString() + ".jpeg",
                    it
                )
            }
        }

        var mMultipartBody2: MultipartBody.Part? = null
        if (mBitmapImage2 != null) {
            val requestFile2: RequestBody? =
                convertBitmapToByteArrayUncompressed(mBitmapImage2!!)?.let {
                    RequestBody.create(
                        "multipart/form-data".toMediaTypeOrNull(),
                        it
                    )
                }
            mMultipartBody2 = requestFile2?.let {
                MultipartBody.Part.createFormData(
                    "toiletImage1", getAlphaNumericString()!!.toString() + ".jpeg",
                    it
                )
            }
        }
        var mMultipartBody3: MultipartBody.Part? = null
        if (mBitmapImage3 != null) {
            val requestFile3: RequestBody? =
                convertBitmapToByteArrayUncompressed(mBitmapImage3!!)?.let {
                    RequestBody.create(
                        "multipart/form-data".toMediaTypeOrNull(),
                        it
                    )
                }
            mMultipartBody3 = requestFile3?.let {
                MultipartBody.Part.createFormData(
                    "toiletImage2", getAlphaNumericString()!!.toString() + ".jpeg",
                    it
                )
            }
        }
        var mMultipartBody4: MultipartBody.Part? = null
        if (mBitmapImage4 != null) {
            val requestFile4: RequestBody? =
                convertBitmapToByteArrayUncompressed(mBitmapImage4!!)?.let {
                    RequestBody.create(
                        "multipart/form-data".toMediaTypeOrNull(),
                        it
                    )
                }
            mMultipartBody4 = requestFile4?.let {
                MultipartBody.Part.createFormData(
                    "toiletImage3", getAlphaNumericString()!!.toString() + ".jpeg",
                    it
                )
            }
        }

        val userID: RequestBody = (KloxusPrefrences().readString(
            mActivity,
            USER_ID,
            null
        ).toString())
            .toRequestBody("text/plain".toMediaTypeOrNull())
        val authToken: RequestBody = (KloxusPrefrences().readString(
            mActivity,
            AUTH_TOKEN,
            null
        ).toString())
            .toRequestBody("text/plain".toMediaTypeOrNull())
        val description: RequestBody = editDescriptionET.text.toString().trim()
            .toRequestBody("text/plain".toMediaTypeOrNull())
        val price: RequestBody = editPriceET.text.toString().trim()
            .toRequestBody("text/plain".toMediaTypeOrNull())
        val address: RequestBody = editAddressET.text.toString().trim()
            .toRequestBody("text/plain".toMediaTypeOrNull())
        val latitude: RequestBody = (KloxusPrefrences().readString(
            mActivity,
            LAT_ADD_TOILET,
            null
        ).toString())
            .toRequestBody("text/plain".toMediaTypeOrNull())

        val longitude: RequestBody = (KloxusPrefrences().readString(
            mActivity,
            LNG_ADD_TOILET,
            null
        ).toString())
            .toRequestBody("text/plain".toMediaTypeOrNull())



        Log.e(TAG, "*********SLATITUDE********$mLatitude")
        Log.e(TAG, "*********SLONGITUDE********$mLongitude")


        val location: RequestBody = KloxusPrefrences().readString(
            mActivity,
            LOC_ADD_TOILET,
            null
        ).toString()
            .toRequestBody("text/plain".toMediaTypeOrNull())

        val title: RequestBody = editToiletNameET.text.toString().trim()
            .toRequestBody("text/plain".toMediaTypeOrNull())
        val available: RequestBody =
            (txtStartTimeTV.text.toString().trim() + "-" + txtEndTimeTV.text.toString().trim()
                    ).toRequestBody("text/plain".toMediaTypeOrNull())
        val gender: RequestBody = mGenderArrayList.joinToString(separator = ",")
            .toRequestBody("text/plain".toMediaTypeOrNull())
        val toiletOptions: RequestBody = mItemsList.joinToString(separator = ",")
            .toRequestBody("text/plain".toMediaTypeOrNull())
        val toiletID: RequestBody = mToiletId
            .toRequestBody("text/plain".toMediaTypeOrNull())
        val currencyy: RequestBody = currency
            .toRequestBody("text/plain".toMediaTypeOrNull())
        val currency_symbol: RequestBody = currency_symbol
            .toRequestBody("text/plain".toMediaTypeOrNull())
        val isRequestSend: RequestBody = isRequestSend
            .toRequestBody("text/plain".toMediaTypeOrNull())
        val isPaid: RequestBody = isPaid
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        Log.e(TAG, "available " + start_time + "-" + end_time)

//        var fileToUpload1: MultipartBody.Part? = null
//        var fileToUpload2: MultipartBody.Part? = null
//        var fileToUpload3: MultipartBody.Part? = null
//        var fileToUpload4: MultipartBody.Part? = null

//        if (!mImage1Url.equals("")) {
//            val file = File(mImage1Url)
//            val mFile: RequestBody = file.asRequestBody("image/*".toMediaTypeOrNull())
//            fileToUpload1 = MultipartBody.Part.createFormData("toiletImage", file.name, mFile)
//        }
//
//        if (!mImage2Url.equals("")) {
//            val file = File(mImage2Url)
//            val mFile: RequestBody = file.asRequestBody("image/*".toMediaTypeOrNull())
//            fileToUpload2 = MultipartBody.Part.createFormData("toiletImage1", file.name, mFile)
//        }
//
//        if (!mImage3Url.equals("")) {
//            val file = File(mImage3Url)
//            val mFile: RequestBody = file.asRequestBody("image/*".toMediaTypeOrNull())
//            fileToUpload3 = MultipartBody.Part.createFormData("toiletImage2", file.name, mFile)
//        }
//
//        if (!mImage4Url.equals("")) {
//            val file = File(mImage4Url)
//            val mFile: RequestBody = file.asRequestBody("image/*".toMediaTypeOrNull())
//            fileToUpload4 = MultipartBody.Part.createFormData("toiletImage3", file.name, mFile)
//        }

        val mApiInterface: ApiInterface? =
            ApiClient().getApiClient()?.create(ApiInterface::class.java)
        val mCall: Call<AddEditToiletModel?>? = mApiInterface?.editToiletRequest(
            userID,
            toiletID,
            authToken,
            description,
            title,
            price,
            address,
            latitude,
            longitude,
            location,
            available,
            gender,
            toiletOptions,
            currencyy,
            currency_symbol,
            isRequestSend,
            isPaid,
            mMultipartBody1,
            mMultipartBody2,
            mMultipartBody3,
            mMultipartBody4
        )

        mCall?.enqueue(object : Callback<AddEditToiletModel?> {
            override fun onFailure(call: Call<AddEditToiletModel?>, t: Throwable) {
                dismissProgressDialog()
                Log.e(TAG, "onFailure: " + t.localizedMessage)
            }

            override fun onResponse(
                call: Call<AddEditToiletModel?>,
                response: Response<AddEditToiletModel?>
            ) {
                dismissProgressDialog()
                val mModel: AddEditToiletModel? = response.body()
                if (mModel != null) {
                    if (mModel.status.equals(1)) {
                        showToast(mActivity, mModel.message)
                        finish()
                    } else {
                        showToast(mActivity, mModel.message)
                    }
                }
            }
        })


    }

    private fun performAddImgClick() {
        if (checkPermission()) {
            onSelectImageClick()
        } else {
            requestPermission()
        }
    }

    /*
     * Set up validations for Sign In fields
     * */
    private fun isValidate(): Boolean {
        var flag = true
        if (editToiletNameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_toilet_name))
            flag = false
        } else if (editAddressET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_address))
            flag = false
        }
        else if(paid_free.equals("paid")){
            if (editPriceET.text.toString().trim { it <= ' ' } == "") {
                showAlertDialog(mActivity, getString(R.string.please_enter_price))
                flag = false
            }}
//        else if (editPriceET.text.toString().trim { it <= ' ' } == "") {
//            showAlertDialog(mActivity, getString(R.string.please_enter_price))
//            flag = false
//        }
        else if (mGenderArrayList.size == 0) {
            showAlertDialog(mActivity, getString(R.string.please_enter_gender))
            flag = false

        } else if (mItemsList.size == 0) {
            showAlertDialog(mActivity, getString(R.string.please_enter_items))
            flag = false

        }
//        else if (editDescriptionET.text.toString().trim { it <= ' ' } == "") {
//            showAlertDialog(mActivity, getString(R.string.please_enter_toilet_desc))
//            flag = false
//        }
        return flag
    }

    /*
   * Get Bitmap From Path
   * */
    open fun getBitmap(path: String, img: ImageView): Bitmap? {
        Glide.with(this).asBitmap()
            .load(path)
            .into(object : CustomTarget<Bitmap?>() {
                override fun onLoadCleared(placeholder: Drawable?) {
                }

                override fun onResourceReady(
                    resource: Bitmap,
                    transition: com.bumptech.glide.request.transition.Transition<in Bitmap?>?
                ) {
                    img.setImageBitmap(resource)
                    mBitmap = resource
                }
            })
        return mBitmap
    }

    fun getRealPathFromURI(contentURI: String): String {
        val contentUri = Uri.parse(contentURI)
        val cursor: Cursor? = this.contentResolver.query(contentUri, null, null, null, null)
        return if (cursor == null) {
            contentUri.path!!
        } else {
            cursor.moveToFirst()
            val index: Int = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            cursor.getString(index)
        }
    }

    fun getFilename(): String? {
        val file =
            File(Environment.getExternalStorageDirectory().path, "MyFolder/Images")
        if (!file.exists()) {
            file.mkdirs()
        }
        return file.absolutePath + "/" + System.currentTimeMillis() + ".jpg"
    }

    private fun compressImage(absolutePath: String): String {
        val filePath = getRealPathFromURI(absolutePath)
        var scaledBitmap: Bitmap? = null
        val options = BitmapFactory.Options()

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true
        var bitmap = BitmapFactory.decodeFile(filePath, options)
        var actualHeight = options.outHeight
        var actualWidth = options.outWidth

//      max Height and width values of the compressed image is taken as 816x612
        val maxHeight = 816.0f
        val maxWidth = 612.0f
        var imgRatio = actualWidth / actualHeight.toFloat()
        val maxRatio = maxWidth / maxHeight

//      width and height values are set maintaining the aspect ratio of the image
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight
                actualWidth = (imgRatio * actualWidth).toInt()
                actualHeight = maxHeight.toInt()
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth
                actualHeight = (imgRatio * actualHeight).toInt()
                actualWidth = maxWidth.toInt()
            } else {
                actualHeight = maxHeight.toInt()
                actualWidth = maxWidth.toInt()
            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight)

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true
        options.inInputShareable = true
        options.inTempStorage = ByteArray(16 * 1024)
        try {
//          load the bitmap from its path
            bitmap = BitmapFactory.decodeFile(filePath, options)
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()
        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888)
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()
        }
        val ratioX = actualWidth / options.outWidth.toFloat()
        val ratioY = actualHeight / options.outHeight.toFloat()
        val middleX = actualWidth / 2.0f
        val middleY = actualHeight / 2.0f
        val scaleMatrix = Matrix()
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY)
        val canvas = Canvas(scaledBitmap!!)
        canvas.setMatrix(scaleMatrix)
        canvas.drawBitmap(
            bitmap,
            middleX - bitmap.width / 2,
            middleY - bitmap.height / 2,
            Paint(Paint.FILTER_BITMAP_FLAG)
        )

//      check the rotation of the image and display it properly
        val exif: ExifInterface
        try {
            exif = ExifInterface(filePath)
            val orientation: Int = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION, 0
            )
            Log.d("EXIF", "Exif: $orientation")
            val matrix = Matrix()
            if (orientation == 6) {
                matrix.postRotate(90F)
                Log.d("EXIF", "Exif: $orientation")
            } else if (orientation == 3) {
                matrix.postRotate(180F)
                Log.d("EXIF", "Exif: $orientation")
            } else if (orientation == 8) {
                matrix.postRotate(270F)
                Log.d("EXIF", "Exif: $orientation")
            }
            scaledBitmap = Bitmap.createBitmap(
                scaledBitmap, 0, 0,
                scaledBitmap.width, scaledBitmap.height, matrix,
                true
            )
        } catch (e: IOException) {
            e.printStackTrace()
        }
        var out: FileOutputStream? = null
        val filename = getFilename()
        try {
            out = FileOutputStream(filename)

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap!!.compress(Bitmap.CompressFormat.JPEG, 80, out)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        return filename!!
    }

    private fun calculateInSampleSize(
        options: BitmapFactory.Options,
        actualWidth: Int,
        actualHeight: Int
    ): Int {

        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1

        if (height > actualHeight || width > actualWidth) {
            val heightRatio =
                Math.round(height.toFloat() / actualHeight.toFloat())
            val widthRatio =
                Math.round(width.toFloat() / actualWidth.toFloat())

            inSampleSize = if (heightRatio < widthRatio) heightRatio.toInt() else widthRatio.toInt()
        }

        val totalPixels = width * height
        val totalReqPixelsCap = actualWidth * actualHeight * 2

        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++
        }

        return inSampleSize
    }


    override fun onCheckedChanged(view: SwitchButton?, isChecked: Boolean) {
        if (isChecked.equals(true)) {
            switchActivateSB.isChecked = true
            isRequestSend = "1"
        } else {
            switchActivateSB.isChecked = false
            isRequestSend = "0"
        }
    }
}