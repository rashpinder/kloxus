package com.kloxus.app.activities

//import com.paypal.android.sdk.payments.*
//import com.paypal.android.sdk.payments.PaymentActivity
import android.content.Intent
import android.icu.util.CurrencyAmount
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.kloxus.app.R
import com.kloxus.app.adapters.SelectPaymentAdapter
import com.kloxus.app.interfaces.OnCardClickInterface
import com.kloxus.app.model.*
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.*
import com.paypal.checkout.PayPalCheckout
import com.paypal.checkout.approve.OnApprove
import com.paypal.checkout.config.CheckoutConfig
import com.paypal.checkout.config.Environment
import com.paypal.checkout.config.SettingsConfig
import com.paypal.checkout.createorder.CreateOrder
import com.paypal.checkout.createorder.CurrencyCode
import com.paypal.checkout.createorder.OrderIntent
import com.paypal.checkout.createorder.UserAction
import com.paypal.checkout.order.Amount
import com.paypal.checkout.order.AppContext
import com.paypal.checkout.order.Order
import com.paypal.checkout.order.PurchaseUnit
import com.paypal.checkout.paymentbutton.PayPalButton
import kotlinx.android.synthetic.main.activity_select_payment_method.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.set


class SelectPaymentMethodActivity : BaseActivity() {
    val clientKey =
        "ASmFHItJHAg93uqsvTKmIzwoqnaA_ScFsgCtCKAP3l07YaM8F63BRgwXvyQPqwDlbKYNqMxU155csalp"
//    val clientSecret =
//        "EDLR7iGCaC74deKIa1GjzHyHMgZsRVuobqjRfV1GDjRv_ZT-BfrUEAzj_jjux9bQueur5Wu2GI1UbUw4"
    val PAYPAL_REQUEST_CODE = 123

    lateinit var mSelectPaymentAdapter: SelectPaymentAdapter

    @BindView(R.id.paymentMethodRV)
    lateinit var paymentMethodRV: RecyclerView

    @BindView(R.id.imgCrossIV)
    lateinit var imgCrossIV: ImageView

    @BindView(R.id.payPalButton)
    lateinit var payPalButton: PayPalButton

    @BindView(R.id.mainLL)
    lateinit var mainLL: LinearLayout

    @BindView(R.id.paypalLL)
    lateinit var paypalLL: LinearLayout

    @BindView(R.id.imgRBPaypalIV)
    lateinit var imgRBPaypalIV: ImageView

    @BindView(R.id.imgRadioButtonIV)
    lateinit var imgRadioButtonIV: ImageView

    @BindView(R.id.txtNoDataFountTV)
    lateinit var txtNoDataFountTV: TextView

    @BindView(R.id.txtContinueTV)
    lateinit var txtContinueTV: TextView

    @BindView(R.id.addPaymentLL)
    lateinit var addPaymentLL: LinearLayout
    var mAllCardsList: ArrayList<CardsData>? = ArrayList()
    var mPosition: Int = 0
    var mCard_id: String = ""
    var toilet_id: String = ""
    var bookID: String = ""
    var totalPrice: String = ""
    var serviceCharge: String = ""
    var click_value: String = ""
    var mDefault: Int = 0
    var mPrice: String = ""
    var mCurr: String = ""
    var mAccesstoken: String = ""
    var payID: String? = null
    var value: String = ""

    //    lateinit var payment: PayPalPayment
    lateinit var payment: CurrencyAmount
    lateinit var currencyCode: CurrencyCode
    var orderID: String = ""
    var payerId: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_payment_method)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        getIntentData()
        KloxusPrefrences().writeString(mActivity, "card_id", "")
        val config = CheckoutConfig(
            application = application,
            clientId = clientKey,
            environment = Environment.LIVE,
            returnUrl = "com.kloxus.app://paypalpay",
            currencyCode = CurrencyCode.USD,
            userAction = UserAction.PAY_NOW,
            settingsConfig = SettingsConfig(
                loggingEnabled = true
            )
        )
        PayPalCheckout.setConfig(config)
        getPayment()
        payPalButton.isActivated = false
        payPalButton.isEnabled = false
        payPalButton.isClickable = false
    }

    private fun getIntentData() {
        if (intent != null) {
            toilet_id = intent.getStringExtra("toilet_id").toString()
            mPrice = intent.getStringExtra("mPrice").toString()
            mCurr = intent.getStringExtra("mCurr").toString()
            bookID = intent.getStringExtra("book_id").toString()
            totalPrice = intent.getStringExtra("totalPrice").toString()
            serviceCharge = intent.getStringExtra("serviceCharge").toString()

        }
    }


    override fun onResume() {
        super.onResume()
        getProfileDetail()
//        if (mAllCardsList != null) {
//            mAllCardsList!!.clear()
//        }
//        getAllCards()
    }

    private fun getProfileDetail() {
        if (isNetworkAvailable(this))
            executeGetProfileDetailRequest()
        else
            showToast(this, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mProfileParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetProfileDetailRequest() {
        if (click_value == "other") {

        } else {
            showProgressDialog(this)
        }
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getProfileDetailRequest(mProfileParam())
            ?.enqueue(object : Callback<EditProfileModel> {
                override fun onFailure(call: Call<EditProfileModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<EditProfileModel>,
                    response: Response<EditProfileModel>
                ) {
                    dismissProgressDialog()
                    val mModel: EditProfileModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            mDefault = mModel.data.defaultPaymentMethod
                            KloxusPrefrences().writeString(
                                mActivity, "paymentMethod",
                                mDefault.toString()
                            )
                            if (mDefault == 0) {
                                imgRadioButtonIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                imgRBPaypalIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
                                KloxusPrefrences().writeString(mActivity, "paymentMethod", "0")
                            } else if (mDefault == 2) {
                                imgRadioButtonIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
                                imgRBPaypalIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
                                KloxusPrefrences().writeString(mActivity, "paymentMethod", "2")
                            } else {
                                imgRadioButtonIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
                                imgRBPaypalIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
                                KloxusPrefrences().writeString(mActivity, "paymentMethod", "1")
                            }
//                            if (mAllCardsList != null) {
//                                mAllCardsList!!.clear()
//                            }
//                            getAllCards()

//                            if (mModel.status.equals(1)) {
//                                if (KloxusPrefrences().readString(mActivity, "paymentMethod", null)
//                                        .equals(
//                                            "1"
//                                        )
//                                ) {
//                                    executeCardTokenApi()
//                                }
//                        else if (KloxusPrefrences().readString(
//                                        mActivity,
//                                        "paymentMethod",
//                                        null
//                                    ).equals("2")
//                                ) {
//                                    executeBookNowPaypalApi()
//                                } else {
//                                    executeBookNowApi()
//                                }


                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }

    @OnClick(
        R.id.imgCrossIV,
        R.id.addPaymentLL,
        R.id.paypalLL,
        R.id.imgRBPaypalIV,
        R.id.mainLL,
        R.id.txtContinueTV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgCrossIV -> performCancelClick()
            R.id.addPaymentLL -> performAddPaymentClick()
            R.id.mainLL -> performCashPaymentClick()
            R.id.paypalLL -> performPaypalPaymentClick()
            R.id.txtContinueTV -> performContinueClick()
            R.id.imgRBPaypalIV -> performPaypalPaymentClick()
        }
    }

    private fun performContinueClick() {
        if (mDefault == 0) {
            KloxusPrefrences().writeString(mActivity, "paymentMethod", "0")
            getPaymentStatusDetail()
        } else if (mDefault == 2) {
            KloxusPrefrences().writeString(mActivity, "paymentMethod", "2")
            payPalButton.isActivated = true
            payPalButton.isEnabled = true
            payPalButton.performClick()
        } else {
//            imgRadioButtonIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
//            imgRBPaypalIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
            KloxusPrefrences().writeString(mActivity, "paymentMethod", "1")
            executeCardTokenApi()
        }
    }

    private fun performPaypalPaymentClick() {
        KloxusPrefrences().writeString(mActivity, "paymentMethod", "2")
        imgRBPaypalIV.setImageResource(R.drawable.ic_active_rb)
        imgRadioButtonIV.setImageResource(R.drawable.ic_uncheck_radio)
        click_value = "other"
        KloxusPrefrences().writeString(mActivity, "card_id", "")
        // calling a method to get payment.
        executePaymentMethodClick()
//        finish()
    }

    private fun executePaymentMethodClick() {
        if (isNetworkAvailable(mActivity))
            executeSelectPaymentMethodRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }

    /*
     * Execute api param
     * */
    private fun mPaymentMethodParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["cardID"] = KloxusPrefrences().readString(mActivity, "card_id", null)
        mMap["paymentMethod"] = KloxusPrefrences().readString(mActivity, "paymentMethod", null)
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeSelectPaymentMethodRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.updatePaymentMethodRequest(mPaymentMethodParam())
            ?.enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {

//                    dismissProgressDialog()
                    val mModel: StatusMsgModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
//                            showToast(mActivity, mModel.message)
                            getProfileDetail()
//                            finish()
//                            getPayment()
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }

    private fun executeSelectPaymentMethodCashRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.updatePaymentMethodRequest(mPaymentMethodParam())
            ?.enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {

//                    dismissProgressDialog()
                    val mModel: StatusMsgModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
//                            showToast(mActivity, mModel.message)
                            getProfileDetail()
//                            finish()
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }

    private fun executeSelectPaymentMethodStripeRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.updatePaymentMethodRequest(mPaymentMethodParam())
            ?.enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {

//                    dismissProgressDialog()
                    val mModel: StatusMsgModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
//                            executeCardTokenApi()
//                            showToast(mActivity, mModel.message)
                            getProfileDetail()
//                            onBackPressed()
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }


    /*
    * Execute api param
    * */
    private fun mCardParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["toiletID"] = toilet_id
        mMap["cardID"] = KloxusPrefrences().readString(mActivity, "card_id", null)
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeCardTokenApi() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.sendPaymentRequest(mCardParam())
            ?.enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {

                    dismissProgressDialog()
                    val mModel: StatusMsgModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            getPaymentStatusDetail()
//                            showAlertDialogFinish(mActivity,mModel.message)
//                            executeBookNowApi()
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }


    private fun getPayment() {
        KloxusPrefrences().writeString(mActivity, "paymentMethod", "2")
        imgRBPaypalIV.setImageResource(R.drawable.ic_active_rb)
        imgRadioButtonIV.setImageResource(R.drawable.ic_uncheck_radio)
        click_value = "other"
        KloxusPrefrences().writeString(mActivity, "card_id", "")
//         Getting the amount from editText
        value = totalPrice
        if (mCurr == "$") {
            currencyCode = CurrencyCode.USD
        } else {
            currencyCode = CurrencyCode.EUR
        }
        payPalButton.setup(
            createOrder = CreateOrder { createOrderActions ->
                val order = Order(
                    intent = OrderIntent.CAPTURE,
                    appContext = AppContext(
                        userAction = UserAction.PAY_NOW
                    ),
                    purchaseUnitList = listOf(
                        PurchaseUnit(
                            amount = Amount(
                                currencyCode = currencyCode,
                                value = value

                            )
                        )
                    )

                )

                createOrderActions.create(order)
            },
            onApprove = OnApprove { approval ->
                approval.orderActions.capture { captureOrderResult ->
                    Log.i("CaptureOrder", "CaptureOrderResult: $captureOrderResult")
                    payID = approval.data.orderId

//                    payID= approval.data.paymentId
//                    payerId=approval.data.payerId
                    Log.e(
                        "IDDDD",
                        "iddd" + "orderID:  " + orderID + "payId:  " + payID + "payerid: " + payerId
                    )
                    getAccessToken()

                }

            }
        )
    }

//    private fun buildRequestBody(): OrderRequest? {
//        val orderRequest = OrderRequest()
//        orderRequest.intent("CAPTURE")
//        val applicationContext = ApplicationContext()
//        val purchaseUnitRequests: MutableList<PurchaseUnitRequest> =
//            ArrayList<PurchaseUnitRequest>()
//        val purchaseUnitRequest: PurchaseUnitRequest = PurchaseUnitRequest()
//            .amount(AmountWithBreakdown().currencyCode("USD").value("220.00"))
//        val payee = Payee()
//        payee.emailAddress("payee@email.com")
//        purchaseUnitRequest.payee(payee)
//        purchaseUnitRequests.add(purchaseUnitRequest)
//        orderRequest.purchaseUnits(purchaseUnitRequests)
//        return orderRequest
//    }


    private fun getAccessToken() {
        if (isNetworkAvailable(this))
            executeGetAccessTokenRequest()
        else
            showToast(this, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mGetAccessTokenParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetAccessTokenRequest() {
        showProgressDialog(this)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getAccessTokenRequest(mGetAccessTokenParam())
            ?.enqueue(object : Callback<GetAccessTokenModel> {
                override fun onFailure(call: Call<GetAccessTokenModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<GetAccessTokenModel>,
                    response: Response<GetAccessTokenModel>
                ) {
//                    dismissProgressDialog()
                    val mModel: GetAccessTokenModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            mAccesstoken = mModel.accessToken
                            getTransDetails()
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }

    private fun getTransDetails() {
        if (isNetworkAvailable(this))
            executeGetTransactionDetailsRequest()
        else
            showToast(this, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mGetTransParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["payID"] = payID
        mMap["bookID"] = bookID
        mMap["toiletID"] = toilet_id
        mMap["accessToken"] = mAccesstoken
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetTransactionDetailsRequest() {
//        showProgressDialog(this)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getTransactionDetailsRequest(mGetTransParam())
            ?.enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {
                    dismissProgressDialog()
                    val mModel: StatusMsgModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
//                            showAlertDialog(mActivity, mModel.message)
//                            showToast(mActivity, mModel.message)
//                            finish()
                            getPaymentStatusDetail()
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }


    private fun performCashPaymentClick() {
        KloxusPrefrences().writeString(mActivity, "paymentMethod", "0")
        imgRBPaypalIV.setImageResource(R.drawable.ic_uncheck_radio)
        imgRadioButtonIV.setImageResource(R.drawable.ic_active_rb)
        click_value = "other"
        KloxusPrefrences().writeString(mActivity, "card_id", "")
        executeSelectPaymentMethodCashRequest()

    }

    private fun performAddPaymentClick() {
        val i = Intent(this, AddPaymentMethodActivity::class.java)
        i.putExtra("value", "select_card")
        startActivity(i)
    }

    private fun performCancelClick() {
        onBackPressed()
    }


    private fun getAllCards() {
        if (isNetworkAvailable(mActivity))
            executeGetAllCardsRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetAllCardsRequest() {
//        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getCardDetailsRequest(mParam())
            ?.enqueue(object : Callback<GetCardsModel> {
                override fun onFailure(call: Call<GetCardsModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<GetCardsModel>,
                    response: Response<GetCardsModel>
                ) {

                    dismissProgressDialog()
                    val mModel: GetCardsModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            mAllCardsList = mModel.data
                            for (i in mModel.data.indices) {
                                if (mModel.data.get(i).isDefault == 1) {
                                    KloxusPrefrences().writeString(
                                        mActivity,
                                        "card_id",
                                        mModel.data.get(i).card_id
                                    )
                                }
                            }
                            getCardsAdapter()
                            txtNoDataFountTV.visibility = View.GONE
                        } else {
                            txtNoDataFountTV.visibility = View.GONE
                            paymentMethodRV.visibility = View.GONE
                            txtNoDataFountTV.text = mModel.message
                        }
                    }
                }
            })
    }

    private fun getCardsAdapter() {
        mSelectPaymentAdapter =
            SelectPaymentAdapter(mActivity, mAllCardsList, mItemCardClickListner)
        paymentMethodRV.layoutManager = LinearLayoutManager(mActivity)
        paymentMethodRV.adapter = mSelectPaymentAdapter
    }

    var mItemCardClickListner: OnCardClickInterface = object : OnCardClickInterface {
        override fun onItemClickListner(mCard_id: String, mPosition: Int) {
            this@SelectPaymentMethodActivity.mPosition = mPosition
            this@SelectPaymentMethodActivity.mCard_id = mCard_id
            KloxusPrefrences().writeString(mActivity, "card_id", mCard_id)
            KloxusPrefrences().writeString(mActivity, "paymentMethod", "1")
            imgRadioButtonIV.setImageResource(R.drawable.ic_uncheck_radio)
            imgRBPaypalIV.setImageResource(R.drawable.ic_uncheck_radio)
//            KloxusPrefrences().writeBoolean(mActivity,"select_payment",true)
//           onBackPressed()
            click_value = "other"
            executeSelectPaymentMethodStripeRequest()


        }


    }


    private fun getPaymentStatusDetail() {
        if (isNetworkAvailable(this))
            executeGetPaymentStatusRequest()
        else
            showToast(this, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mPaymentParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["bookID"] = bookID
        mMap["toiletID"] = toilet_id
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetPaymentStatusRequest() {
//        if (!progressDialog!!.isShowing){
            showProgressDialog(mActivity)
//        }
//        else{
//
//        }
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getpaymentStatusRequest(mPaymentParam())
            ?.enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {
                    dismissProgressDialog()
                    val mModel: StatusMsgModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            if(mModel.isRequestSend.equals("0")){
                                showAlertDialogFinish(mActivity, mModel.message)
                            }
                            else{
                                val intent = Intent(mActivity, ToiletPathActivity::class.java)
                                intent.putExtra("toilet_id",toilet_id)
                                intent.putExtra("book_id", mModel.bookID)
                                startActivity(intent)
                                finish()
                            }

//                            finish()
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }
}
