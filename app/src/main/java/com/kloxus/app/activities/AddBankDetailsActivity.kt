package com.kloxus.app.activities

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.gson.JsonObject
import com.kloxus.app.R
import com.kloxus.app.model.UploadDocModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.AUTH_TOKEN
import com.kloxus.app.utils.KloxusPrefrences
import com.kloxus.app.utils.USER_ID
import com.rilixtech.widget.countrycodepicker.CountryCodePicker
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class AddBankDetailsActivity : BaseActivity() {


    @BindView(R.id.editFirstNameET)
    lateinit var editFirstNameET: EditText

    @BindView(R.id.editLastNameET)
    lateinit var editLastNameET: EditText

    @BindView(R.id.editCityNameET)
    lateinit var editCityNameET: EditText

    @BindView(R.id.ccp)
    lateinit var ccp: CountryCodePicker

    @BindView(R.id.editAddressLineOneET)
    lateinit var editAddressLineOneET: EditText

    @BindView(R.id.editAddressLineTwoET)
    lateinit var editAddressLineTwoET: EditText

    @BindView(R.id.editPostalCodeET)
    lateinit var editPostalCodeET: EditText

    @BindView(R.id.editStateNameET)
    lateinit var editStateNameET: EditText

    @BindView(R.id.txtDobET)
    lateinit var txtDobET: TextView

    @BindView(R.id.txtUploadImg)
    lateinit var txtUploadImg: TextView

    @BindView(R.id.txtUploadImgtwo)
    lateinit var txtUploadImgtwo: TextView

    @BindView(R.id.txtSaveTV)
    lateinit var txtSaveTV: TextView

    @BindView(R.id.editIdNumET)
    lateinit var editIdNumET: EditText

    @BindView(R.id.editPhoneET)
    lateinit var editPhoneET: EditText

    @BindView(R.id.editssnET)
    lateinit var editssnET: EditText

    @BindView(R.id.editAHNameET)
    lateinit var editAHNameET: EditText

    @BindView(R.id.editRoutingNoET)
    lateinit var editRoutingNoET: EditText

    @BindView(R.id.editAccNoET)
    lateinit var editAccNoET: EditText

    @BindView(R.id.imgBackIV)
    lateinit var imgBackIV: ImageView

    @BindView(R.id.imgAddImgOneIV)
    lateinit var imgAddImgOneIV: ImageView

    @BindView(R.id.imgAddImgTwoIV)
    lateinit var imgAddImgTwoIV: ImageView

    @BindView(R.id.FirstNameRL)
    lateinit var FirstNameRL: RelativeLayout

    @BindView(R.id.LastNameRL)
    lateinit var LastNameRL: RelativeLayout

    @BindView(R.id.CityNameRL)
    lateinit var CityNameRL: RelativeLayout

    @BindView(R.id.AddressLineOneRL)
    lateinit var AddressLineOneRL: RelativeLayout

    @BindView(R.id.AddressLineTwoRL)
    lateinit var AddressLineTwoRL: RelativeLayout

    @BindView(R.id.postalCodeRL)
    lateinit var postalCodeRL: RelativeLayout

    @BindView(R.id.stateNameRL)
    lateinit var stateNameRL: RelativeLayout

    @BindView(R.id.idNumRL)
    lateinit var idNumRL: RelativeLayout

    @BindView(R.id.phoneRL)
    lateinit var phoneRL: RelativeLayout

    @BindView(R.id.ssnRL)
    lateinit var ssnRL: RelativeLayout

    @BindView(R.id.AHNameRL)
    lateinit var AHNameRL: RelativeLayout

    @BindView(R.id.routingNoRL)
    lateinit var routingNoRL: RelativeLayout

    @BindView(R.id.AccNoRL)
    lateinit var AccNoRL: RelativeLayout

    @BindView(R.id.CountryNameRL)
    lateinit var CountryNameRL: RelativeLayout

    @BindView(R.id.dobRL)
    lateinit var dobRL: RelativeLayout

    var mBitmap_front: Bitmap? = null
    var mBitmap_back: Bitmap? = null
    var value: String = ""
    var mFileImgOne: String = ""
    var mFileImgTwo: String = ""

    /*
        * Initialize Menifest Permissions:
        * & Camera Gallery Request @params
        * */
    var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    var writeCamera = Manifest.permission.CAMERA

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_bank_details)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        editTextSignupSelector(editFirstNameET, FirstNameRL, "")
        editTextSignupSelector(editLastNameET, LastNameRL, "")
        editTextSignupSelector(editCityNameET, CityNameRL, "")
        editTextSignupSelector(editAddressLineOneET, AddressLineOneRL, "")
        editTextSignupSelector(editAddressLineTwoET, AddressLineTwoRL, "")
        editTextSignupSelector(editPostalCodeET, postalCodeRL, "")
        editTextSignupSelector(editStateNameET, stateNameRL, "")
        editTextSignupSelector(editIdNumET, idNumRL, "")
        editTextSignupSelector(editPhoneET, phoneRL, "")
        editTextSignupSelector(editssnET, ssnRL, "")
        editTextSignupSelector(editAHNameET, AHNameRL, "")
        editTextSignupSelector(editRoutingNoET, routingNoRL, "")
        editTextSignupSelector(editAccNoET, AccNoRL, "")

    }

    @OnClick(
        R.id.imgBackIV,
        R.id.txtSaveTV,
        R.id.txtDobET,
        R.id.imgAddImgOneIV,
        R.id.CountryNameRL,
        R.id.imgAddImgTwoIV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.txtSaveTV -> performSaveClick()
            R.id.imgBackIV -> performBackClick()
//            R.id.txtUploadImg -> performAddImgClick()
//            R.id.txtUploadImgtwo -> performAddImgTwoClick()
            R.id.imgAddImgOneIV -> performAddImgClick()
            R.id.imgAddImgTwoIV -> performAddImgTwoClick()
            R.id.txtDobET -> performDOBClick()
            R.id.CountryNameRL -> performCountryClick()
        }
    }

    private fun performCountryClick() {
        ccp.showCountryCodePickerDialog()
    }

    private fun performDOBClick() {
        showDatePickerDialog(mActivity, txtDobET)
        dobRL.setBackgroundResource(R.drawable.bg_et_selected)
    }

    /*
     * Date Picker Dialog
     * */
    fun showDatePickerDialog(mActivity: Activity?, mTextView: TextView) {
        val mYear: Int
        val mMonth: Int
        val mDay: Int
        // Get Current Date
        val c = Calendar.getInstance()
        mYear = c[Calendar.YEAR]
        mMonth = c[Calendar.MONTH]
        mDay = c[Calendar.DAY_OF_MONTH]
        val datePickerDialog = DatePickerDialog(
            mActivity!!, R.style.MyDatePickerDialogStyle,
            { view, year, monthOfYear, dayOfMonth ->
                val intMonth = monthOfYear + 1
                mTextView.text = "" + year + "-" + getFormatedString(
                    "" + intMonth
                ) + "-" + getFormatedString(
                    "" + dayOfMonth
                )
            }, mYear, mMonth, mDay


        )
        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
        datePickerDialog.show()
    }

    fun getFormatedString(strTemp: String): String? {
        var strActual = ""
        if (strTemp.length == 1) {
            strActual = "0$strTemp"
        } else if (strTemp.length == 2) {
            strActual = strTemp
        }
        return strActual
    }

    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity, arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 -> {
//                if (this[0] == PackageManager.PERMISSION_GRANTED) {
                if (checkPermission() == true) {
                    onSelectImageClick()
                } else {
                    requestPermission()
                    Log.e(TAG, "**Permission Denied**")
                }
            }
        }
    }

    fun IntArray.onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>?
    ) {
        when (requestCode) {
            369 -> {
                if (this[0] == PackageManager.PERMISSION_GRANTED) {
                    onSelectImageClick()
                } else {
                    Log.e(TAG, "**Permission Denied**")
                }
            }
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            .crop()                    //Crop image(Optional), Check Customization for more option
            .compress(1024)            //Final image size will be less than 1 MB(Optional)
            .maxResultSize(
                1080,
                1080
            )    //Final image resolution will be less than 1080 x 1080(Optional)
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val uri: Uri = data?.data!!
            val imageStream = contentResolver.openInputStream(uri)
            val selectedImage = BitmapFactory.decodeStream(imageStream)

            if (value.equals("front_img")) {
                // Use Uri object instead of File to avoid storage permissions
                imgAddImgOneIV.setImageURI(uri)
                mBitmap_front = selectedImage
                performUploadImgOneClick()
            } else {
                imgAddImgTwoIV.setImageURI(uri)
                mBitmap_back = selectedImage
                performUploadImgTwoClick()
            }
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
//            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
//            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    private fun performSaveClick() {
        if (isValidate())
            if (isNetworkAvailable(mActivity))
                executeAddBankDetailsRequest()
            else
                showToast(mActivity, getString(R.string.internet_connection_error))
    }

    private fun mBankParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["firstName"] = editFirstNameET.text.toString().trim()
        mMap["lastName"] = editLastNameET.text.toString().trim()
        mMap["city"] = editCityNameET.text.toString().trim()
        mMap["country"] = ccp.selectedCountryNameCode
        mMap["line1"] = editAddressLineOneET.text.toString().trim()
        mMap["line2"] = editAddressLineTwoET.text.toString().trim()
        mMap["postalCode"] = editPostalCodeET.text.toString().trim()
        mMap["state"] = editStateNameET.text.toString().trim()
        mMap["dob"] = txtDobET.text.toString().trim()
        mMap["idNumber"] = editIdNumET.text.toString().trim()
        mMap["phone"] = editPhoneET.text.toString().trim()
        mMap["ssnLast4"] = editssnET.text.toString().trim()
        mMap["backDocument"] = mFileImgTwo
        mMap["frontDocument"] = mFileImgOne
        mMap["accountHolderName"] = editAHNameET.text.toString().trim()
        mMap["routingNumber"] = editRoutingNoET.text.toString().trim()
        mMap["accountNumber"] = editAccNoET.text.toString().trim()
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    private fun executeAddBankDetailsRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.addBankDetailsRequest(mBankParams())?.enqueue(object : Callback<JsonObject> {

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                dismissProgressDialog()
                if (response.body() != null) {
                    try {
                        val jsonObjectMM = JSONObject(response.body().toString())
                        if (jsonObjectMM.getString("status").equals("1")) {
                            showAlertDialogFinish(mActivity, jsonObjectMM.getString("message"))
                        } else {
                            mBitmap_front = null
                            mBitmap_back = null
                            mFileImgOne = ""
                            mFileImgTwo = ""
                            imgAddImgTwoIV.setImageDrawable(resources.getDrawable(R.drawable.ic_up_doc))
                            imgAddImgOneIV.setImageDrawable(resources.getDrawable(R.drawable.ic_up_doc))

                            showAlertDialog(mActivity, jsonObjectMM.getString("message"))
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        mBitmap_front = null
                        mBitmap_back = null
                        mFileImgOne = ""
                        mFileImgTwo = ""
                        imgAddImgTwoIV.setImageDrawable(resources.getDrawable(R.drawable.ic_up_doc))
                        imgAddImgOneIV.setImageDrawable(resources.getDrawable(R.drawable.ic_up_doc))

                    }

                }
            }
        })
    }

    private fun performUploadImgOneClick() {
        if (isNetworkAvailable(mActivity))
            executeUploadImgRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }

    private fun executeUploadImgRequest() {
        showProgressDialog(mActivity)

        var mMultipartBody: MultipartBody.Part? = null
        if (mBitmap_front != null) {
            val requestFile1: RequestBody? =
                convertBitmapToByteArrayUncompressed(mBitmap_front!!)?.let {
                    it
                        .toRequestBody(
                            "multipart/form-data".toMediaTypeOrNull(),
                            0
                        )
                }
            mMultipartBody = requestFile1?.let {
                MultipartBody.Part.createFormData(
                    "documentImage", getAlphaNumericString()!!.toString() + ".jpeg",
                    it
                )
            }
        }

        val userID: RequestBody = (KloxusPrefrences().readString(
            mActivity,
            USER_ID,
            null
        ).toString())
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val authToken: RequestBody = (KloxusPrefrences().readString(
            mActivity,
            AUTH_TOKEN,
            null
        ).toString())
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val imageType: RequestBody = "1"
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())

        val mApiInterface: ApiInterface? =
            ApiClient().getApiClient()?.create(ApiInterface::class.java)
        val mCall: Call<UploadDocModel?>? = mApiInterface?.uploadDocRequest(
            userID,
            authToken,
            imageType,
            mMultipartBody
        )

        mCall?.enqueue(object : Callback<UploadDocModel?> {
            override fun onFailure(call: Call<UploadDocModel?>, t: Throwable) {
                dismissProgressDialog()
                Log.e(TAG, "onFailure: " + t.localizedMessage)
            }

            override fun onResponse(
                call: Call<UploadDocModel?>,
                response: Response<UploadDocModel?>
            ) {
                dismissProgressDialog()
                val mModel: UploadDocModel? = response.body()
                if (mModel != null) {
                    if (mModel.status.equals(1)) {
                        showToast(mActivity, mModel.message)
                        mFileImgOne = mModel.fileID

                    } else {
                        showAlertDialog(mActivity, mModel.message)
                    }
                }
            }
        })
    }

    private fun performUploadImgTwoClick() {
        if (isNetworkAvailable(mActivity))
            executeUploadImgTwoRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }

    private fun executeUploadImgTwoRequest() {
        showProgressDialog(mActivity)

        var mMultipartBody: MultipartBody.Part? = null
        if (mBitmap_back != null) {
            val requestFile1: RequestBody? =
                convertBitmapToByteArrayUncompressed(mBitmap_back!!)?.let {
                    it
                        .toRequestBody(
                            "multipart/form-data".toMediaTypeOrNull(),
                            0
                        )
                }
            mMultipartBody = requestFile1?.let {
                MultipartBody.Part.createFormData(
                    "documentImage", getAlphaNumericString()!!.toString() + ".jpeg",
                    it
                )
            }
        }

        val userID: RequestBody = (KloxusPrefrences().readString(
            mActivity,
            USER_ID,
            null
        ).toString())
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val authToken: RequestBody = (KloxusPrefrences().readString(
            mActivity,
            AUTH_TOKEN,
            null
        ).toString())
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val imageType: RequestBody = "2"
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())

        val mApiInterface: ApiInterface? =
            ApiClient().getApiClient()?.create(ApiInterface::class.java)
        val mCall: Call<UploadDocModel?>? = mApiInterface?.uploadDocRequest(
            userID,
            authToken,
            imageType,
            mMultipartBody
        )

        mCall?.enqueue(object : Callback<UploadDocModel?> {
            override fun onFailure(call: Call<UploadDocModel?>, t: Throwable) {
                dismissProgressDialog()
                Log.e(TAG, "onFailure: " + t.localizedMessage)
            }

            override fun onResponse(
                call: Call<UploadDocModel?>,
                response: Response<UploadDocModel?>
            ) {
                dismissProgressDialog()
                val mModel: UploadDocModel? = response.body()
                if (mModel != null) {
                    if (mModel.status.equals(1)) {
                        showToast(mActivity, mModel.message)
                        mFileImgTwo = mModel.fileID

                    } else {
                        showAlertDialog(mActivity, mModel.message)
                    }
                }
            }
        })
    }

    private fun performAddImgClick() {
        value = "front_img"
        if (checkPermission()) {
            onSelectImageClick()
        } else {
            requestPermission()
        }
    }

    private fun performAddImgTwoClick() {
        value = "back_img"
        if (checkPermission()) {
            onSelectImageClick()
        } else {
            requestPermission()
        }
    }

    private fun performBackClick() {
        onBackPressed()
    }


    /*
     * Set up validations for Sign In fields
     * */
    private fun isValidate(): Boolean {
        var flag = true
        if (editFirstNameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_first_name))
            flag = false
        } else if (editLastNameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_last_name))
            flag = false
        } else if (editCityNameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_city))
            flag = false
        } else if (editAddressLineOneET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_address_line_one))
            flag = false
        } else if (editAddressLineTwoET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_address_line_two))
            flag = false
        } else if (editPostalCodeET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_postal_code))
            flag = false
        } else if (editPostalCodeET.text.toString().trim().length > 8) {
            showAlertDialog(mActivity, getString(R.string.please_enter_postal_code_length_eight))
            flag = false
        } else if (editStateNameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_state))
            flag = false
        } else if (txtDobET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_dob))
            flag = false
        } else if (editIdNumET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_id_num))
            flag = false
        } else if (editIdNumET.text.toString().trim().length > 9) {
            showAlertDialog(mActivity, getString(R.string.please_enter_id_num_length))
            flag = false
        } else if (editPhoneET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_phone_num))
            flag = false
        } else if (editssnET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_ssn))
            flag = false
        } else if (editssnET.text.toString().trim().length > 4) {
            showAlertDialog(mActivity, getString(R.string.please_enter_ssn_four_digits))
            flag = false
        } else if (mBitmap_front == null) {
            showAlertDialog(mActivity, getString(R.string.please_add_doc_front_img))
            flag = false
        } else if (mBitmap_back == null) {
            showAlertDialog(mActivity, getString(R.string.please_add_doc_back_img))
            flag = false
        } else if (editAHNameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_acc_holder_name))
            flag = false
        } else if (editRoutingNoET.text.toString().trim().length > 9) {
            showAlertDialog(mActivity, getString(R.string.please_enter_routing_length_nine))
            flag = false
        } else if (editRoutingNoET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_routing))
            flag = false
        } else if (editAccNoET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_account_num))
            flag = false
        } else if (editAccNoET.text.toString().trim().length > 30) {
            showAlertDialog(mActivity, getString(R.string.please_enter_account_num_length))
            flag = false
        }
        return flag
    }

}