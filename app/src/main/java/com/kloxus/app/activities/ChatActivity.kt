package com.kloxus.app.activities

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.gson.Gson
import com.kloxus.app.R
import com.kloxus.app.adapters.ChatMessagesAdapter
import com.kloxus.app.model.ChatMessageData
import com.kloxus.app.model.GetMessagesModel
import com.kloxus.app.model.SendMessageModel
import com.kloxus.app.model.StatusMsgModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.*
import io.socket.client.Socket
import io.socket.emitter.Emitter
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class ChatActivity : BaseActivity() {

    @BindView(R.id.chatDataRV)
    lateinit var chatDataRV: RecyclerView

    @BindView(R.id.imgCrossIV)
    lateinit var imgCrossIV: ImageView

    @BindView(R.id.imgLinkIV)
    lateinit var imgLinkIV: ImageView

    @BindView(R.id.sendRL)
    lateinit var sendRL: RelativeLayout

    @BindView(R.id.mSwipeRefreshSR)
    lateinit var mSwipeRefreshSR: SwipeRefreshLayout

    @BindView(R.id.imgSendIV)
    lateinit var imgSendIV: ImageView

    @BindView(R.id.mProgressPB)
    lateinit var mProgressPB: com.wang.avi.AVLoadingIndicatorView

    @BindView(R.id.editMsgET)
    lateinit var editMsgET: EditText
    @BindView(R.id.txtOtherUsernameTV)
    lateinit var txtOtherUsernameTV: TextView


    lateinit var mChatMessagesAdapter: ChatMessagesAdapter
    var mMsgArrayList: ArrayList<ChatMessageData?>? = ArrayList()
    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var mIsPagination = true
    var mRoomId = ""
    var otherUserId = ""
    private var mSocket: Socket? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        getIntentData()
        setTopSwipeRefresh()
        setEditFocus()
        setDataOnWidgets()
    }

    private fun seenMsgApi() {
        if (isNetworkAvailable(mActivity))
            executeSeenMsgesRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))

    }
    /*
       * Execute api param
       * */
    private fun mSeenMsgParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = otherUserId
        mMap["roomID"] = mRoomId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeSeenMsgesRequest() {
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.seenMsgRequest(mSeenMsgParam())
            ?.enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    Log.e(TAG, t.message.toString())
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {
                    Log.e(TAG, response.body().toString())
                    val mModel = response.body()
                    if (mModel?.status == 1) {
                        Log.e(TAG, "*****Msg****" + "Msg Seen")
                    } else if (mModel?.status == 0) {
                        showToast(mActivity, mModel.message)
                    }
                }
            })
    }


    /*
    * Set Edit Message Focus
    * */
    private fun setEditFocus() {
        editMsgET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {
            }

            override fun onTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {
                chatDataRV.scrollToPosition(mMsgArrayList!!.size - 1)
            }

            override fun afterTextChanged(editable: Editable) {}
        })
    }


    private fun setTopSwipeRefresh() {
        mSwipeRefreshSR.setColorSchemeColors(resources.getColor(R.color.black));
        mSwipeRefreshSR.setOnRefreshListener {
            if (mIsPagination == true) {
                ++mPageNo
                getAllMsgesData()
            } else {
                mSwipeRefreshSR.isRefreshing = false
            }

        }
    }

    private fun getIntentData() {
        if (intent != null) {
            mRoomId = intent.getStringExtra(ROOM_ID)!!
            txtOtherUsernameTV.setText(intent.getStringExtra(USER_NAME))
            Log.e("TAG","un"+txtOtherUsernameTV)
            setUpSocketData()
        }
    }


    private fun setUpSocketData() {
        val app: KloxusApp = application as KloxusApp
        mSocket = app.getSocket()
        mSocket!!.emit("ConncetedChat", mRoomId)
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
        mSocket!!.on("newMessage", onNewMessage)
        mSocket!!.on("leaveChat", onUserLeft)
        mSocket!!.connect()

        getAllMsgesData()

    }

    private fun getAllMsgesData() {
        if (isNetworkAvailable(mActivity))
            executeGetAllMegesRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }


    @OnClick(
        R.id.imgCrossIV,
        R.id.imgLinkIV,
        R.id.sendRL
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.sendRL -> performSendClick()
            R.id.imgLinkIV -> performCameraClick()
            R.id.imgCrossIV -> onBackPressed()
        }
    }

    private fun performCameraClick() {

    }

    override fun onBackPressed() {
        if (mMsgArrayList != null && mMsgArrayList!!.size > 0) {
            var mIntent: Intent = Intent()
            mIntent.putExtra(LAST_MSG, mMsgArrayList!!.get(mMsgArrayList!!.size - 1)!!.message)
            setResult(808, mIntent)
            super.onBackPressed()
        } else {
            super.onBackPressed()
        }

    }

    private fun performSendClick() {
//        preventMultipleClick()
        if (isNetworkAvailable(mActivity)) {
            if (isValidate()) {
                executeSendMsgRequest();
            }
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }

    }


    /*
     * Set up validations for Sign In fields
     * */
    private fun isValidate(): Boolean {
        var flag = true
        if (editMsgET.text.toString().trim { it <= ' ' } == "") {
            flag = false
        }
        return flag
    }

    /*
    * Execute api param
    * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(mActivity, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(mActivity, AUTH_TOKEN, null)
        mMap["message"] = editMsgET.text.toString().trim()
        mMap["roomID"] = mRoomId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeSendMsgRequest() {
        mProgressPB.visibility = View.VISIBLE
        imgSendIV.visibility = View.GONE
        editMsgET.isEnabled = false
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.sendMessageRequest(mParam())
            ?.enqueue(object : Callback<SendMessageModel> {
                override fun onFailure(call: Call<SendMessageModel>, t: Throwable) {
                    Log.e(TAG, t.message.toString())
                    mProgressPB.visibility = View.GONE
                    imgSendIV.visibility = View.VISIBLE
                    editMsgET.isEnabled = true
                }

                override fun onResponse(
                    call: Call<SendMessageModel>,
                    response: Response<SendMessageModel>
                ) {
                    Log.e(TAG, response.body().toString())
                    mProgressPB.visibility = View.GONE
                    imgSendIV.visibility = View.VISIBLE
                    editMsgET.isEnabled = true
                    val mModel = response.body()
                    if (mModel?.status == 1) {
                        mModel.data.viewType = RIGHT_VIEW_HOLDER
                        mMsgArrayList!!.add(mModel.data)
                        mChatMessagesAdapter.notifyDataSetChanged()
                        scrollToBottom()
                        // perform the sending message attempt.
                        val gson = Gson()
                        val mOjectString = gson.toJson(mModel.data)
                        Log.e(TAG, "*****Msg****" + mOjectString)
                        mSocket!!.emit("newMessage", mRoomId, mOjectString)
                        editMsgET.setText("")
                    } else if (mModel?.status == 0) {
                        showToast(mActivity, mModel.message)
                    }
                }
            })
    }


    private fun mGetMsgesParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(mActivity, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(mActivity, AUTH_TOKEN, null)
        mMap["roomID"] = mRoomId
        mMap["perPage"] = "" + mPerPage
        mMap["pageNo"] = "" + mPageNo
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetAllMegesRequest() {
        mSwipeRefreshSR.isRefreshing = true
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getAllChatMessageRequest(mGetMsgesParam())
            ?.enqueue(object : Callback<GetMessagesModel> {
                override fun onFailure(call: Call<GetMessagesModel>, t: Throwable) {
                    mSwipeRefreshSR.isRefreshing = false
                    Log.e(TAG, t.message.toString())
                }

                override fun onResponse(
                    call: Call<GetMessagesModel>,
                    response: Response<GetMessagesModel>
                ) {
                    mSwipeRefreshSR.isRefreshing = false
                    Log.e(TAG, response.body().toString())
                    val mModel = response.body()
                    if (mModel?.status == 1) {
                        if (mModel.data.size < 10) {
                            mIsPagination = false;
                        }
                        for (i in 0..mModel.data.size - 1) {
                            if (mModel.data.get(i).userID.equals(
                                    KloxusPrefrences().readString(
                                        mActivity,
                                        USER_ID,
                                        null
                                    )
                                )
                            ) {
                                mModel.data.get(i).viewType = RIGHT_VIEW_HOLDER
                            } else {
                                mModel.data.get(i).viewType = LEFT_VIEW_HOLDER
                            }
                        }
                        mMsgArrayList!!.addAll(0, mModel.data)
                        mMsgArrayList!!.sortedWith(compareBy({ it!!.created }))
                        mChatMessagesAdapter.notifyDataSetChanged()
                        if (mPageNo == 1) {
                            chatDataRV.scrollToPosition(mMsgArrayList!!.size - 1)
                        }
                        otherUserId=mModel.userDetails.userID
                        seenMsgApi()
                    } else if (mModel?.status == 0) {
                        mChatMessagesAdapter.notifyDataSetChanged()
                    }
                }
            })
    }

    private fun setDataOnWidgets() {
        mChatMessagesAdapter = ChatMessagesAdapter(mActivity, mMsgArrayList)
        chatDataRV.setNestedScrollingEnabled(false)
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(mActivity)
        chatDataRV.setLayoutManager(mLayoutManager)
        chatDataRV.setItemAnimator(DefaultItemAnimator())
        chatDataRV.setAdapter(mChatMessagesAdapter)

        if (editMsgET.hasFocus()) {
            chatDataRV.scrollToPosition(mMsgArrayList!!.size - 1)
        }

        chatDataRV.scrollToPosition(mMsgArrayList!!.size - 1)
    }


    private fun scrollToBottom() {
        chatDataRV.scrollToPosition(mChatMessagesAdapter.getItemCount() - 1)
    }


    /*
    * Socket Chat Implementations:
    * */

    private val onConnect = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onDisconnect = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onConnectError = Emitter.Listener {
        runOnUiThread(Runnable { Log.e(TAG, "Error connecting") })
    }

    private val onNewMessage = Emitter.Listener { args ->
        runOnUiThread(Runnable {
            try {
                Log.e(TAG, "****MessageObject****" + args.get(1).toString())
                val mGson = Gson()
                val mChatMessageData: ChatMessageData =
                    mGson.fromJson(args.get(1).toString(), ChatMessageData::class.java)
                if (mChatMessageData.userID.equals(
                        KloxusPrefrences().readString(
                            mActivity,
                            USER_ID,
                            null
                        )
                    )
                ) {
                    mChatMessageData.viewType = RIGHT_VIEW_HOLDER
                } else {
                    mChatMessageData.viewType = LEFT_VIEW_HOLDER
                }

                mMsgArrayList!!.add(mChatMessageData)
                mChatMessagesAdapter.notifyDataSetChanged()
                editMsgET.setText("")
                scrollToBottom()
                seenMsgApi()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }
    private val onUserJoined = Emitter.Listener { args ->
        runOnUiThread(Runnable {
            var mJsonData: JSONObject? = null
            try {
                mJsonData = JSONObject(args[1].toString())
                scrollToBottom()
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        })
    }
    private val onUserLeft = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onTyping =
        Emitter.Listener { args -> runOnUiThread(Runnable { val isTyping = args[1] as Boolean }) }
    private val onStopTyping = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onTypingTimeout = Runnable { }


}