package com.kloxus.app.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.core.view.isVisible
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.kloxus.app.R
import com.kloxus.app.model.EditProfileModel
import com.kloxus.app.model.PaypalLoginModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class AddBankDetailsTypeActivity : BaseActivity() {

    @BindView(R.id.txtStripeBankDetailTV)
    lateinit var txtStripeBankDetailTV: TextView

    @BindView(R.id.txtPaypalTV)
    lateinit var txtPaypalTV: TextView

    @BindView(R.id.stripeLL)
    lateinit var stripeLL: LinearLayout

    @BindView(R.id.addStripeBankDetailsLL)
    lateinit var addStripeBankDetailsLL: LinearLayout

    @BindView(R.id.paypalDetailsLL)
    lateinit var paypalDetailsLL: LinearLayout

    @BindView(R.id.addpaypalLL)
    lateinit var addpaypalLL: LinearLayout

    @BindView(R.id.imgCrossIV)
    lateinit var imgCrossIV: ImageView

    var is_bank: Int = 0
    var is_paypal: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
//        getIntentData()
    }

//    private fun getIntentData() {
//        if (intent != null) {
//            is_bank = intent.getIntExtra("is_bank",0)
//            is_paypal = intent.getIntExtra("is_paypal",0)
//        }
//        if (is_bank==1){
//            addStripeBankDetailsLL.isVisible=false
//            stripeLL.isVisible=true
//        }
//        else{
//            addStripeBankDetailsLL.isVisible=true
//            stripeLL.isVisible=false
//        }
//        if (is_paypal==1){
//            addpaypalLL.isVisible=false
//            paypalDetailsLL.isVisible=true
//        }
//        else{
//            addpaypalLL.isVisible=true
//            paypalDetailsLL.isVisible=false
//        }
//    }



    override fun onResume() {
        super.onResume()
        getProfileDetail()
    }

    private fun getProfileDetail() {
        if (isNetworkAvailable(this))
            executeGetProfileDetailRequest()
        else
            showToast(this, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mProfileParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetProfileDetailRequest() {
        showProgressDialog(this)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getProfileDetailRequest(mProfileParam())
            ?.enqueue(object : Callback<EditProfileModel> {
                override fun onFailure(call: Call<EditProfileModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<EditProfileModel>,
                    response: Response<EditProfileModel>
                ) {
                    dismissProgressDialog()
                    val mModel: EditProfileModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            is_bank=mModel.data.isBank
                            is_paypal=mModel.data.isPaypal
                            if (is_bank==1){
                                addStripeBankDetailsLL.isVisible=false
                                stripeLL.isVisible=false
                            }
                            else{
                                addStripeBankDetailsLL.isVisible=false
                                stripeLL.isVisible=false
                            }
                            if (is_paypal==1){
                                addpaypalLL.isVisible=false
                                paypalDetailsLL.isVisible=true
                            }
                            else{
                                addpaypalLL.isVisible=true
                                paypalDetailsLL.isVisible=false
                            }

                        } else {
                            showAlertDialog(mActivity, mModel?.message)
                        }
//
//                        if (mModel.status.equals(1)) {
//                            is_bank=mModel.data.isBank
//                            is_paypal=mModel.data.isPaypal
//                            if (is_bank==1){
//                                addStripeBankDetailsLL.isVisible=false
//                                stripeLL.isVisible=true
//                            }
//                            else{
//                                addStripeBankDetailsLL.isVisible=true
//                                stripeLL.isVisible=false
//                            }
//                            if (is_paypal==1){
//                                addpaypalLL.isVisible=false
//                                paypalDetailsLL.isVisible=true
//                            }
//                            else{
//                                addpaypalLL.isVisible=true
//                                paypalDetailsLL.isVisible=false
//                            }

//                        }
//                    else {
//                            showAlertDialog(mActivity, mModel?.message)
//                        }
                    }
                }
            })
    }

    @OnClick(
        R.id.paypalDetailsLL,
        R.id.stripeLL,
        R.id.addStripeBankDetailsLL,
        R.id.imgCrossIV,
        R.id.addpaypalLL,
    )

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgCrossIV -> performCrossPwdClick()
            R.id.paypalDetailsLL -> performPaypalClick()
            R.id.addStripeBankDetailsLL -> performAddStripeClick()
            R.id.stripeLL -> performStripeClick()
            R.id.addpaypalLL -> performAddPaypalDetailsClick()
        }
    }

    private fun performAddPaypalDetailsClick() {
        if (isNetworkAvailable(mActivity))
            executeConnectPaypalRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }

    private fun executeConnectPaypalRequest() {
        showProgressDialog(mActivity)
        val userID: RequestBody = (KloxusPrefrences().readString(
            mActivity,
            USER_ID,
            null
        ).toString())
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val authToken: RequestBody = (KloxusPrefrences().readString(
            mActivity,
            AUTH_TOKEN,
            null
        ).toString())
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())

        val mApiInterface: ApiInterface? =
            ApiClient().getApiClient()?.create(ApiInterface::class.java)
        val mCall: Call<PaypalLoginModel?>? = mApiInterface?.paypalLoginRequest(
            userID,
            authToken
        )

        mCall?.enqueue(object : Callback<PaypalLoginModel?> {
            override fun onFailure(call: Call<PaypalLoginModel?>, t: Throwable) {
                dismissProgressDialog()
                Log.e(TAG, "onFailure: " + t.localizedMessage)
            }

            override fun onResponse(
                call: Call<PaypalLoginModel?>,
                response: Response<PaypalLoginModel?>
            ) {
                dismissProgressDialog()
                val mModel: PaypalLoginModel? = response.body()
                if (mModel != null) {
                    if (mModel.status.equals(1)) {
                        val i = Intent(mActivity, PaypalWebviewActivity::class.java)
                        i.putExtra("url",mModel.login_url)
                        startActivity(i)
                    } else {
                        showAlertDialog(mActivity, "Error, Please try again later")
                    }
                }
            }
        })
    }

    private fun performAddStripeClick() {
        val i = Intent(mActivity, AddBankDetailsActivity::class.java)
        startActivity(i)
    }

    private fun performPaypalClick() {
        val i = Intent(mActivity, ShowPaypalDetailActivity::class.java)
        startActivity(i)
    }

    private fun performStripeClick() {
        val i = Intent(mActivity, ShowBankDetailsActivity::class.java)
        startActivity(i)
    }

    private fun performCrossPwdClick() {
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

}