package com.kloxus.app.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.kloxus.app.R
import com.kloxus.app.adapters.ManageToiletAdapter
import com.kloxus.app.interfaces.LoadMoreListner
import com.kloxus.app.model.Data
import com.kloxus.app.model.GetAllToiletModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.AUTH_TOKEN
import com.kloxus.app.utils.KloxusPrefrences
import com.kloxus.app.utils.USER_ID
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ManageToiletProfileActivity : BaseActivity() {

    lateinit var mAllToiletsAdapter: ManageToiletAdapter

    @BindView(R.id.manageToiletRV)

    lateinit var manageToiletRV: RecyclerView

    @BindView(R.id.txtNoDataFountTV)
    lateinit var txtNoDataFountTV: TextView


    var mAllToiletsList: ArrayList<Data>? = ArrayList()

    @BindView(R.id.imgBackIV)
    lateinit var imgBackIV: ImageView

    @BindView(R.id.mProgressRL)
    lateinit var mProgressRL: RelativeLayout

    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var isLoading: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_toilet_profile)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
    }

    override fun onResume() {
        super.onResume()
        mPageNo=1
        getAllToilets()
    }

    @OnClick(
        R.id.imgBackIV
    )

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackIV -> performCrossPwdClick()
        }
    }

    private fun performCrossPwdClick() {
        onBackPressed()
    }


    private fun getAllToilets() {
        if (isNetworkAvailable(mActivity))
            executeGetAllToiletRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetAllToiletRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getAllToiletRequest(mParam())
            ?.enqueue(object : Callback<GetAllToiletModel> {
                override fun onFailure(call: Call<GetAllToiletModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<GetAllToiletModel>,
                    response: Response<GetAllToiletModel>
                ) {
                    dismissProgressDialog()
                    val mModel: GetAllToiletModel? = response.body()
                    if (mModel!!.status.equals(1)) {
                        isLoading = !mModel.lastPage.equals(true)
                        mAllToiletsList = mModel.data
                        setAllToiletAdapter()
                        txtNoDataFountTV.visibility = View.GONE
                    } else {
//                        showAlertDialog(mActivity, mModel.message)
                        txtNoDataFountTV.visibility = View.VISIBLE
                        txtNoDataFountTV.text = mModel.message
                    }
                }
            })
    }


    var mLoadMoreScrollListner: LoadMoreListner = object : LoadMoreListner {
        override fun onLoadMoreListner(mModel: Data) {
            if (isLoading) {
                ++mPageNo
                executeMoreDataRequest()
            }

        }
    }

    /*
     * Execute api param
     * */
    private fun mLoadMoreParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeMoreDataRequest() {
        mProgressRL.visibility = View.VISIBLE
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getAllToiletRequest(mLoadMoreParam())
            ?.enqueue(object : Callback<GetAllToiletModel> {
                override fun onFailure(call: Call<GetAllToiletModel>, t: Throwable) {
                    dismissProgressDialog()
                    mProgressRL.visibility = View.GONE
                }

                override fun onResponse(
                    call: Call<GetAllToiletModel>,
                    response: Response<GetAllToiletModel>
                ) {
                    mProgressRL.visibility = View.GONE
                    var mModel = response.body()!!
                    if (mModel.status == 1) {
                        mModel.data.let {
                            mAllToiletsList!!.addAll<Data>(
                                it
                            )
                        }
                        mAllToiletsAdapter.notifyDataSetChanged()
                        isLoading = !mModel.lastPage.equals(true)
                    } else if (mModel.status == 0) {
//                    showToast(mActivity, mGetFavModel.message)
                    }
                }
            })
    }

    private fun setAllToiletAdapter() {
        mAllToiletsAdapter = ManageToiletAdapter(mActivity, mAllToiletsList, mLoadMoreScrollListner)
        manageToiletRV.layoutManager = LinearLayoutManager(mActivity)
        manageToiletRV.adapter = mAllToiletsAdapter
    }
}