package com.kloxus.app.activities

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.kloxus.app.R
import com.kloxus.app.utils.*
import java.util.*

class PaypalWebviewActivity : BaseActivity() {

    @BindView(R.id.imgCrossIV) lateinit var imgCrossIV: ImageView
    @BindView(R.id.thankyouBt) lateinit var thankyouBt: TextView
    @BindView(R.id.mWebViewWV) lateinit var mWebViewWV: WebView

    var url : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paypal_webview)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        getIntentData()
    }


    @OnClick(R.id.imgCrossIV)

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgCrossIV -> performCrossClick()
        }
    }



    private fun getIntentData() {
        if (intent != null){
            if (intent != null){
                url = intent.getStringExtra("url").toString()
                setUpWebView()
            }
        }
    }

    private fun setUpWebView(){
        if (intent != null) {
            val settings: WebSettings = mWebViewWV.getSettings()
            settings.setUserAgentString("Kloxus")
            settings.javaScriptEnabled = true
            mWebViewWV.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY)
            val alertDialog = AlertDialog.Builder(this).create()
            mWebViewWV.setWebViewClient(object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    Log.i(TAG, "Processing webview url click...")
                    view.loadUrl(url)
                    return true
                }

                override fun onPageFinished(view: WebView, url: String) {
                    Log.i(TAG, "Finished loading URL: $url")
                    if (url.contains("https://www.dharmani.com/kloxus/webservices/paypalTokenGenerate.php")) {
                        thankyouBt.visibility = View.VISIBLE
                        thankyouBt.setOnClickListener { finish() }
                    } else {
                        thankyouBt.visibility = View.GONE
                    }
                }

                override fun onReceivedError(
                    view: WebView,
                    errorCode: Int,
                    description: String,
                    failingUrl: String
                ) {
                    Log.e(TAG, "Error: $description")
                    Toast.makeText(mActivity, "Oh no! $description", Toast.LENGTH_SHORT).show()
                    alertDialog.setTitle("Error")
                    alertDialog.setMessage(description)
                    alertDialog.setButton("OK",
                        DialogInterface.OnClickListener { dialog, which -> return@OnClickListener })
                    alertDialog.show()
                }
            })
            mWebViewWV.loadUrl(url)
        }
    }
//        mWebViewWV.webViewClient = WebViewClient()
//        // this will load the url of the website
//        mWebViewWV.loadUrl(mUrl)
//        // this will enable the javascript settings
//        mWebViewWV.settings.javaScriptEnabled = true
//        // if you want to enable zoom feature
//        mWebViewWV.settings.setSupportZoom(true)

    private fun performCrossClick() {
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}