package com.kloxus.app.activities

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.core.view.isVisible
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.kloxus.app.R
import com.kloxus.app.model.EditProfileModel
import com.kloxus.app.model.StatusMsgModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class SettingActivity : BaseActivity() {

    @BindView(R.id.imgCrossRL)
    lateinit var imgCrossRL: RelativeLayout

    @BindView(R.id.showBankDetailsRL)
    lateinit var showBankDetailsRL: RelativeLayout

    @BindView(R.id.aboutUsRL)
    lateinit var aboutUsRL: RelativeLayout

    @BindView(R.id.termsOfUseRL)
    lateinit var termsOfUseRL: RelativeLayout

    @BindView(R.id.addBankDetailsRL)
    lateinit var addBankDetailsRL: RelativeLayout

    @BindView(R.id.manageToiletProfileRL)
    lateinit var manageToiletProfileRL: RelativeLayout

    @BindView(R.id.profileLL)
    lateinit var profileLL: LinearLayout

    @BindView(R.id.logoutRL)
    lateinit var logoutRL: RelativeLayout

    @BindView(R.id.imgProfileIV)
    lateinit var imgProfileIV: ImageView

    @BindView(R.id.txtUserNameTV)
    lateinit var txtUserNameTV: TextView

    @BindView(R.id.txtMailTV)
    lateinit var txtMailTV: TextView
    var is_bank: Int = 0
    var is_paypal: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
    }


    override fun onResume() {
        super.onResume()
        Glide.with(mActivity).load(KloxusPrefrences().readString(mActivity, PROFILE_IMAGE,null))
            .placeholder(R.drawable.ic_ph)
            .error(R.drawable.ic_ph)
            .into(imgProfileIV)
        txtMailTV.setText(KloxusPrefrences().readString(mActivity, USER_EMAIL,null))
        txtUserNameTV.setText(KloxusPrefrences().readString(mActivity, FIRST_NAME,null)+ " " + KloxusPrefrences().readString(mActivity, LAST_NAME,null))
        is_bank=KloxusPrefrences().readInteger(mActivity, IS_STRIPE,0)
        is_paypal=KloxusPrefrences().readInteger(mActivity, IS_PAYPAL,0)
//        getProfileDetail()
    }

    private fun getProfileDetail() {
        if (isNetworkAvailable(this))
            executeGetProfileDetailRequest()
        else
            showToast(this, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mProfileParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetProfileDetailRequest() {
        showProgressDialog(this)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getProfileDetailRequest(mProfileParam())
            ?.enqueue(object : Callback<EditProfileModel> {
                override fun onFailure(call: Call<EditProfileModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<EditProfileModel>,
                    response: Response<EditProfileModel>
                ) {
                    dismissProgressDialog()
                    val mModel: EditProfileModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            Glide.with(mActivity).load(mModel.data.profileImage)
                                .placeholder(R.drawable.ic_ph)
                                .error(R.drawable.ic_ph)
                                .into(imgProfileIV)
                            txtMailTV.setText(mModel.data.email)
                            txtUserNameTV.setText(mModel.data.firstName + " " + mModel.data.lastName)
                            is_bank=mModel.data.isBank
                            is_paypal=mModel.data.isPaypal
//                            if (is_bank==1){
//                                addBankDetailsRL.isVisible=false
//                                showBankDetailsRL.isVisible=true
//                            }
//                            else{
//                                addBankDetailsRL.isVisible=true
//                                showBankDetailsRL.isVisible=false
//                            }
                        } else {
                            showAlertDialog(mActivity, mModel?.message)
                        }
                    }
                }
            })
    }


    @OnClick(
        R.id.imgCrossRL,
        R.id.aboutUsRL,
        R.id.termsOfUseRL,
        R.id.profileLL,
        R.id.logoutRL,
        R.id.manageToiletProfileRL,
//        R.id.addBankDetailsRL,
        R.id.showBankDetailsRL
    )

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgCrossRL -> performCrossPwdClick()
            R.id.aboutUsRL -> performAboutClick()
            R.id.termsOfUseRL -> performTermsClick()
            R.id.profileLL -> performEditProfileClick()
            R.id.manageToiletProfileRL -> performManageToiletProfileClick()
            R.id.logoutRL -> performLogoutProfileClick()
//            R.id.addBankDetailsRL -> performBankDetailsClick()
            R.id.showBankDetailsRL -> performShowBankDetailsClick()
        }
    }

    private fun performShowBankDetailsClick() {
        val i = Intent(mActivity, AddBankDetailsTypeActivity::class.java)
        i.putExtra("is_bank",is_bank)
        i.putExtra("is_paypal",is_paypal)
        startActivity(i)
//        val i = Intent(mActivity, ShowBankDetailsActivity::class.java)
//        startActivity(i)
    }

    private fun performBankDetailsClick() {
        val i = Intent(mActivity, AddBankDetailsActivity::class.java)
        startActivity(i)
    }

    private fun performLogoutProfileClick() {
        showSignoutAlert(mActivity, getString(R.string.logout_sure))
    }

    fun showSignoutAlert(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.signout_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnNo = alertDialog.findViewById<TextView>(R.id.btnNo)
        val btnYes = alertDialog.findViewById<TextView>(R.id.btnYes)
        txtMessageTV.text = strMessage
        btnNo.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
        btnYes.setOnClickListener {
            logout()
            alertDialog.dismiss()
        }
    }


    private fun logout() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeLogoutApi()
        }
    }

    private fun performManageToiletProfileClick() {
        val i = Intent(mActivity, ManageToiletProfileActivity::class.java)
        startActivity(i)
    }

    private fun performEditProfileClick() {
        val i = Intent(mActivity, EditProfileActivity::class.java)
        startActivity(i)
    }

    private fun performTermsClick() {
        preventMultipleViewClick()
        val i = Intent(mActivity, WebViewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_TERMS)
        startActivity(i)
    }

    private fun performAboutClick() {
        preventMultipleViewClick()
        val i = Intent(mActivity, WebViewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_ABOUT)
        startActivity(i)
    }

    private fun performCrossPwdClick() {
        onBackPressed()
    }

    private fun executeLogoutApi() {
        if (isNetworkAvailable(mActivity))
            executeLogoutRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeLogoutRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.logoutRequest(mParam())?.enqueue(object : Callback<StatusMsgModel> {
            override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<StatusMsgModel>,
                response: Response<StatusMsgModel>
            ) {
                dismissProgressDialog()
                val mModel: StatusMsgModel? = response.body()
                if (mModel != null) {
                    if (mModel.status.equals(1)) {
                        Toast.makeText(mActivity, mModel.message, Toast.LENGTH_SHORT).show()
                        val preferences: SharedPreferences = KloxusPrefrences().getPreferences(
                            Objects.requireNonNull(mActivity)
                        )
                        val editor = preferences.edit()
                        editor.clear()
                        editor.apply()
                        mActivity.onBackPressed()
//                        val mIntent = Intent(mActivity, LoginActivity::class.java)
                        val mIntent = Intent(mActivity, HomeActivity::class.java)
                        mIntent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP // To clean up all activities
                        startActivity(mIntent)
                        finish()
                        finishAffinity()
                    } else {
                        showAlertDialog(mActivity, mModel.message)
                    }
                }
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

}