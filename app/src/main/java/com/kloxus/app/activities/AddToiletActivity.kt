package com.kloxus.app.activities

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.viewpager.widget.ViewPager
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.chahinem.pageindicator.PageIndicator
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.kloxus.app.R
import com.kloxus.app.adapters.SlidingImageAdapter
import com.kloxus.app.model.AddEditToiletModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.*
import com.suke.widget.SwitchButton
import kotlinx.android.synthetic.main.activity_add_toilet.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*


class AddToiletActivity : BaseActivity(), SwitchButton.OnCheckedChangeListener {

    @BindView(R.id.imageRL)
    lateinit var imageRL: RelativeLayout

    @BindView(R.id.paidRL)
    lateinit var paidRL: RelativeLayout
    @BindView(R.id.txtPriceTV)
    lateinit var txtPriceTV: TextView

    @BindView(R.id.freeRL)
    lateinit var freeRL: RelativeLayout

    @BindView(R.id.mViewPagerVP)
    lateinit var mViewPagerVP: ViewPager

    @BindView(R.id.indicator)
    lateinit var indicator: PageIndicator

    @BindView(R.id.editToiletNameET)
    lateinit var editToiletNameET: EditText

    //    var mImagesArrayList: ArrayList<ImagesModel> = ArrayList<ImagesModel>()
    var mImagesArrayList1: ArrayList<String> = ArrayList<String>()
    var mImagesArrayListt1: ArrayList<Bitmap> = ArrayList<Bitmap>()

    var mGenderArrayList: ArrayList<String> = ArrayList<String>()
    var mItemsList: ArrayList<String> = ArrayList<String>()

//    @BindView(R.id.txtDollarTV)
//    lateinit var txtDollarTV: TextView

    @BindView(R.id.currRL)
    lateinit var currRL: RelativeLayout

    @BindView(R.id.availableOpenTimeRL)
    lateinit var availableOpenTimeRL: RelativeLayout

    @BindView(R.id.editAddressET)
    lateinit var editAddressET: TextView

    @BindView(R.id.editPriceET)
    lateinit var editPriceET: EditText

//    @BindView(R.id.GenderRL)
//    lateinit var GenderRL: RelativeLayout

    @BindView(R.id.editDescriptionET)
    lateinit var editDescriptionET: EditText

    @BindView(R.id.imgBackIV)
    lateinit var imgBackIV: ImageView

    @BindView(R.id.imgAddImgIV)
    lateinit var imgAddImgIV: ImageView

    @BindView(R.id.imgCheckPaidIV)
    lateinit var imgCheckPaidIV: ImageView

    @BindView(R.id.imgCheckFreeIV)
    lateinit var imgCheckFreeIV: ImageView

    @BindView(R.id.txtSaveTV)
    lateinit var txtSaveTV: TextView

    @BindView(R.id.txtStartTimeTV)
    lateinit var txtStartTimeTV: TextView

    @BindView(R.id.txtEndTimeTV)
    lateinit var txtEndTimeTV: TextView

    @BindView(R.id.startTimeLL)
    lateinit var startTimeLL: LinearLayout

    @BindView(R.id.endTimeLL)
    lateinit var endTimeLL: LinearLayout

    @BindView(R.id.toiletNameRL)
    lateinit var toiletNameRL: RelativeLayout

    @BindView(R.id.AddressRL)
    lateinit var AddressRL: RelativeLayout

    @BindView(R.id.PriceRL)
    lateinit var PriceRL: RelativeLayout

    @BindView(R.id.descRL)
    lateinit var descRL: RelativeLayout

    @BindView(R.id.ToiletPaperRL)
    lateinit var ToiletPaperRL: RelativeLayout

    @BindView(R.id.soapRL)
    lateinit var soapRL: RelativeLayout

    @BindView(R.id.handDryerRL)
    lateinit var handDryerRL: RelativeLayout

    @BindView(R.id.disinfectantRL)
    lateinit var disinfectantRL: RelativeLayout

    @BindView(R.id.servietteRL)
    lateinit var servietteRL: RelativeLayout

//    @BindView(R.id.genderSpinner)
//    lateinit var genderSpinner: Spinner

    @BindView(R.id.currSpinner)
    lateinit var currSpinner: Spinner


    @BindView(R.id.GenderRL)
    lateinit var GenderRL: RelativeLayout

    @BindView(R.id.GenderFemaleRL)
    lateinit var GenderFemaleRL: RelativeLayout

    @BindView(R.id.GenderOtherRL)
    lateinit var GenderOtherRL: RelativeLayout

    @BindView(R.id.imgCheckMaleIV)
    lateinit var imgCheckMaleIV: ImageView

    @BindView(R.id.imgCheckFemaleIV)
    lateinit var imgCheckFemaleIV: ImageView

    @BindView(R.id.imgCheckOtherIV)
    lateinit var imgCheckOtherIV: ImageView

    @BindView(R.id.imgAddImageIV)
    lateinit var imgAddImageIV: ImageView

    @BindView(R.id.switchActivateSB)
    lateinit var switchActivateSB: SwitchButton

    var gender = ""
    var currency = ""
    var currency_symbol = ""
    var isRequestSend = "0"
    var sel_male_gender = "m_click"
    var sel_female_gender = "f_click"
    var sel_other_gender = "o_click"
    var sel_soap = "soap_click"
    var sel_dryer = "dryer_click"
    var sel_disinfectant = "disinfectant_click"
    var sel_serviette = "serviette_click"
    var sel_paper = "paper_click"
    var paid_free = "paid"
    var isPaid = "1"
    var arr_gender = ""
    var arr_curr = ""
    var mBitmap1: Bitmap? = null
    var mBitmap2: Bitmap? = null
    var mBitmap3: Bitmap? = null
    var mBitmap4: Bitmap? = null
    var new_selectedHour = 0
    var start_time: String? = null
    var end_time: String? = null
    var latLng: LatLng? = null
    var address: String? = null
    var city: String? = null

    private val mArrayList = ArrayList<String>()

    /*
        * Initialize Menifest Permissions:
        * & Camera Gallery Request @params
        * */
    var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    var writeCamera = Manifest.permission.CAMERA
    var AUTOCOMPLETE_REQUEST_CODE = 5
    var mLatitude = 0.0
    var mLongitude = 0.0

    var strImage1 = ""
    var strImage2 = ""
    var strImage3 = ""
    var strImage4 = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_toilet)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        switchActivateSB.setOnCheckedChangeListener(this)
        /*
       * For places Location google api
       * */initializePlaceSdKLocation()

//        setGenderAdapter()
        setCurrencyAdapter()
        editTextSignupSelector(editToiletNameET, toiletNameRL, "")
        editTextSignupSelector(editPriceET, PriceRL, "")
        editTextSignupSelector(editDescriptionET, descRL, "")

    }

    //Set ViewPager Adapter
    private fun setViewPagerAdapter() {
        val mSlidingImageAdapter =
            SlidingImageAdapter(
                mActivity,
                mImagesArrayList1,
                object : SlidingImageAdapter.AddClick {
                    override fun Click(position: Int) {
                        if (mImagesArrayList1.size < 4) {
                            performAddImgClick()
                        } else {
                            showAlertDialog(mActivity, "You can only add maximum four Images")
                        }
                    }
                })

        mViewPagerVP.adapter = mSlidingImageAdapter
        indicator.attachTo(mViewPagerVP)
    }

    /*
     * Initailze places location
     *
     * */
    fun initializePlaceSdKLocation() {
        // Initialize the SDK
        Places.initialize(mActivity, getString(R.string.places_api_key))

        // Create a new Places client instance
        val placesClient: PlacesClient = Places.createClient(mActivity)
    }


    private fun setSearchIntent() {
        // Set the fields to specify which types of place data to
        // return after the user has made a selection.
        val fields: List<Place.Field> = Arrays.asList<Place.Field>(
            Place.Field.ID,
            Place.Field.NAME,
            Place.Field.LAT_LNG,
            Place.Field.ADDRESS
        )
        // Start the autocomplete intent.
        val intent: Intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.FULLSCREEN, fields
        )
            .build(mActivity)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }

//    /*
//     * Gender adapter
//     * */
//    private fun setGenderAdapter() {
//        val mStatusArry = resources.getStringArray(R.array.gender)
//        // access the spinner
//        if (genderSpinner != null) {
//            val adapter = ArrayAdapter(
//                this,
//                android.R.layout.simple_spinner_item, mStatusArry
//            )
//            genderSpinner.adapter = adapter
//            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//
//            genderSpinner.onItemSelectedListener = object :
//                AdapterView.OnItemSelectedListener {
//                override fun onItemSelected(
//                    parent: AdapterView<*>,
//                    view: View, position: Int, id: Long
//                ) {
//                    arr_gender = mStatusArry[position]
//                    if (arr_gender=="Female"){
//                        gender="1"
//                    }  else if (arr_gender=="Female"){
//                        gender="2"
//                    }else{
//                        gender="3"
//                    }
//                    Log.e(TAG, "gender" + gender)
//                    GenderRL.setBackgroundResource(R.drawable.bg_et_selected)
//                }
//
//                override fun onNothingSelected(parent: AdapterView<*>) {
//                    // write code to perform some action
//                }
//            }
//        }
//    }


    private fun setCurrencyAdapter() {
        val mCurrArry = resources.getStringArray(R.array.currency)
        // access the spinner
        if (currSpinner != null) {
            val adapter = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item, mCurrArry
            )
            currSpinner.adapter = adapter
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            currSpinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View, position: Int, id: Long
                ) {
                    arr_curr = mCurrArry[position]
                    if (arr_curr.equals("$")) {
                        currency = "usd"
                        currency_symbol = "$"
                    } else {
                        currency = "eur"
                        currency_symbol = "€"
                    }
                    Log.e(TAG, "currency" + currency)
                    Log.e(TAG, "currency_SYM" + currency_symbol)
                    PriceRL.setBackgroundResource(R.drawable.bg_et_selected)
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }
    }

    @OnClick(
        R.id.txtSaveTV,
        R.id.imgBackIV,
        R.id.imgAddImgIV,
        R.id.endTimeLL,
        R.id.startTimeLL,
        R.id.editAddressET,
        R.id.GenderRL,
        R.id.GenderFemaleRL,
        R.id.GenderOtherRL,
        R.id.imgAddImageIV,
        R.id.ToiletPaperRL,
        R.id.handDryerRL,
        R.id.servietteRL,
        R.id.soapRL,
        R.id.disinfectantRL,
        R.id.freeRL,
        R.id.paidRL
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.txtSaveTV -> performSaveClick()
            R.id.imgBackIV -> performBackClick()
            R.id.imgAddImgIV -> performAddImgClick()
            R.id.endTimeLL -> performEndTimeClick()
            R.id.startTimeLL -> performStartTimeClick()
            R.id.editAddressET -> setSearchIntent()
            R.id.GenderRL -> performMaleClick()
            R.id.GenderFemaleRL -> performFemaleClick()
            R.id.GenderOtherRL -> performOtherClick()
            R.id.imgAddImageIV -> performAddImgClick()
            R.id.ToiletPaperRL -> performToiletPaperClick()
            R.id.handDryerRL -> performHandDryerClick()
            R.id.servietteRL -> performServietteClick()
            R.id.soapRL -> performSoapClick()
            R.id.disinfectantRL -> performDisInfectantClick()
            R.id.freeRL -> performFreeClick()
            R.id.paidRL -> performPaidClick()
        }
    }

    private fun performPaidClick() {
        imgCheckPaidIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
        imgCheckFreeIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        txtPriceTV.isVisible=true
        PriceRL.isVisible=true
        paid_free = "paid"
        isPaid = "1"
    }

    private fun performFreeClick() {
        imgCheckPaidIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        imgCheckFreeIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
        txtPriceTV.isVisible=false
        PriceRL.isVisible=false
        paid_free = "free"
        isPaid = "0"
    }

    private fun performDisInfectantClick() {
        if (sel_disinfectant.equals("disinfectant_click")) {
            mItemsList.add("5")
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            sel_disinfectant = "dis_click"
        } else {
            mItemsList.remove("5")
            sel_disinfectant = "disinfectant_click"
            imgCheckDisinfectantIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        }

    }

    private fun performSoapClick() {
        if (sel_soap.equals("soap_click")) {
            mItemsList.add("2")
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            sel_soap = "so_click"
        } else {
            mItemsList.remove("2")
            sel_soap = "soap_click"
            imgCheckSoapIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        }

    }

    private fun performHandDryerClick() {
        if (sel_dryer.equals("dryer_click")) {
            mItemsList.add("3")
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            sel_dryer = "dr_click"
        } else {
            mItemsList.remove("3")
            sel_dryer = "dryer_click"
            imgCheckhandDryerIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        }

    }

    private fun performServietteClick() {
        if (sel_serviette.equals("serviette_click")) {
            mItemsList.add("4")
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            sel_serviette = "ser_click"
        } else {
            mItemsList.remove("4")
            sel_serviette = "serviette_click"
            imgCheckServietteIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        }

    }

    private fun performToiletPaperClick() {
        if (sel_paper.equals("paper_click")) {
            mItemsList.add("1")
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            sel_paper = "pa_click"
        } else {
            mItemsList.remove("1")
            sel_paper = "paper_click"
            imgCheckToiletPaperIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        }


    }

    private fun performOtherClick() {
        if (sel_other_gender.equals("o_click")) {
            mGenderArrayList.add("3")
            imgCheckOtherIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            sel_other_gender = "other_click"
        } else {
            mGenderArrayList.remove("3")
            sel_other_gender = "o_click"
            imgCheckOtherIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        }
    }

    private fun performFemaleClick() {
        if (sel_female_gender.equals("f_click")) {
            mGenderArrayList.add("2")
            imgCheckFemaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
            sel_female_gender = "female_click"
        } else {
            mGenderArrayList.remove("2")
            sel_female_gender = "f_click"
            imgCheckFemaleIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        }
    }

    private fun performMaleClick() {
        if (sel_male_gender.equals("m_click")) {
            mGenderArrayList.add("1")
            sel_male_gender = "male_click"
            imgCheckMaleIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
        } else {
            mGenderArrayList.remove("1")
            sel_male_gender = "m_click"
            imgCheckMaleIV.setImageDrawable(getDrawable(R.drawable.ic_uncheck_radio))
        }

    }

    private fun performCurrClick() {

    }

    private fun performStartTimeClick() {
        showTimePicker(txtStartTimeTV)
        availableOpenTimeRL.setBackgroundResource(R.drawable.bg_et_selected)

    }

    private fun performEndTimeClick() {
        showEndTimePicker(txtEndTimeTV)

    }

    fun showTimePicker(mTextview: TextView) {
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime[Calendar.HOUR_OF_DAY]
        val minute = mcurrentTime[Calendar.MINUTE]
        val am = mcurrentTime[Calendar.AM_PM]
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(
            mActivity, AlertDialog.THEME_HOLO_LIGHT,
            { timePicker, selectedHour, selectedMinute ->
                val am_pm: String
                if (selectedHour > 12) {
                    new_selectedHour = selectedHour - 12
                    am_pm = "pm"
                } else if (selectedHour == 12) {
                    new_selectedHour = selectedHour
                    am_pm = "pm"
                } else if (selectedHour == 0) {
                    new_selectedHour = 12
                    am_pm = "am"
                } else {
                    new_selectedHour = selectedHour
                    am_pm = "am"
                }

                mTextview.text =
                    new_selectedHour.toString() + ":" + convertDate(
                        selectedMinute
                    ) + " " + am_pm
                mTextview.setTextColor(resources.getColor(R.color.black))
                start_time = mTextview.text.toString().trim { it <= ' ' }
            }, hour, minute, false
        ) //Yes 24 hour time
        Log.e(TAG, "available " + start_time)
        mTimePicker.setTitle("Select Time")
        mTimePicker.getWindow()!!.setLayout(
            800,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        mTimePicker.getWindow()!!.setGravity(Gravity.CENTER)
        mTimePicker.show()

    }


    fun showEndTimePicker(mTextview: TextView) {
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime[Calendar.HOUR_OF_DAY]
        val minute = mcurrentTime[Calendar.MINUTE]
        val am = mcurrentTime[Calendar.AM_PM]
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(
            mActivity, AlertDialog.THEME_HOLO_LIGHT,
            { timePicker, selectedHour, selectedMinute ->
                val am_pm: String
                if (selectedHour > 12) {
                    new_selectedHour = selectedHour - 12
                    am_pm = "pm"
                } else if (selectedHour == 12) {
                    new_selectedHour = selectedHour
                    am_pm = "pm"
                }
                else if (selectedHour == 0) {
                    new_selectedHour = 12
                    am_pm = "am"
                }
                else {
                    new_selectedHour = selectedHour
                    am_pm = "am"
                }
                // mTextview.setText(selectedHour + ":" + selectedMinute);
                mTextview.setText(new_selectedHour.toString() + ":" + convertEndDate(selectedMinute) + " " + am_pm)
                mTextview.setTextColor(resources.getColor(R.color.black))
                end_time = mTextview.text.toString().trim { it <= ' ' }

            }, hour, minute, false
        ) //Yes 24 hour time

        Log.e(TAG, "available " + end_time)
        mTimePicker.setTitle("Select Time")
        mTimePicker.getWindow()!!.setLayout(
            800,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        mTimePicker.getWindow()!!.setGravity(Gravity.CENTER)
        mTimePicker.show()
    }


    fun convertDate(input: Int): String? {
        return if (input >= 10) {
            input.toString()
        } else {
            "0$input"
        }
    }

    fun convertEndDate(input: Int): String {
        return if (input >= 10) {
            input.toString()
        } else {
            "0$input"
        }
    }


    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity, arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 -> {
//                if (this[0] == PackageManager.PERMISSION_GRANTED) {
                if (checkPermission() == true) {
                    onSelectImageClick()
                } else {
//                    requestPermission()
//    checkPermission()
                    showToast(mActivity,"Please allow permissions of camera & gallery from settings")
                    Log.e(TAG, "**Permission Denied**")
                }
            }
        }
    }

    fun IntArray.onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>?
    ) {
        when (requestCode) {
            369 -> {
                if (this[0] == PackageManager.PERMISSION_GRANTED) {
                    onSelectImageClick()
                } else {
                    Log.e(TAG, "**Permission Denied**")
                }
            }
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            .crop()                    //Crop image(Optional), Check Customization for more option
            .compress(720)           //Final image size will be less than 1 MB(Optional)
            .maxResultSize(
                720,
                720
            )    //Final image resolution will be less than 1080 x 1080(Optional)
            .cropSquare()
            .start()
//        ImagePicker.with(this)
//            .crop()	    			//Crop image(Optional), Check Customization for more option //Final image size will be less than 1 MB(Optional)
//            .maxResultSize(720, 720)	//Final image resolution will be less than 1080 x 1080(Optional)
//            .start()
//        CropImage.startPickImageActivity(mActivity)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
//            if (resultCode == RESULT_OK && data!!.getData()!=null) {
                val place: Place = Autocomplete.getPlaceFromIntent(data!!)
                latLng = place.latLng
                mLatitude = latLng!!.latitude
                mLongitude = latLng!!.longitude
                Log.e(TAG, "*********SLATITUDE********$mLatitude")
                Log.e(TAG, "*********SLONGITUDE********$mLongitude")
                val gcd = Geocoder(this, Locale.getDefault())
                try{
                    val addresses: List<Address> = gcd.getFromLocation(mLatitude, mLongitude, 1)
                    if (addresses.size > 0) {
                        city = addresses[0].getLocality()

                    } else {
                        // do your stuff
                    }}

                catch (e: IOException){
                    val addresses: List<Address> = gcd.getFromLocation(mLatitude, mLongitude, 1)
                    if (addresses.size > 0) {
                        city = addresses[0].getLocality()

                    } else {
                        // do your stuff
                    }
                }
                KloxusPrefrences().writeString(
                    mActivity,
                    LAT_ADD_TOILET,
                    mLatitude.toString()
                )
                KloxusPrefrences().writeString(
                    mActivity,
                    LNG_ADD_TOILET,
                    mLongitude.toString()
                )
                Log.e(TAG, "*********SLATITUDE********$mLatitude")
                Log.e(TAG, "*********SLONGITUDE********$mLongitude")
                address = place.address
//                city = place.name

                KloxusPrefrences().writeString(
                    mActivity,
                    SELECTED_TOILET_LOCATION,
                    address
                )
                editAddressET.setText(place.address)
                Log.e(
                    TAG,
                    "Place: " + place.name.toString() + ", " + place.id
                        .toString() + ", " + place.latLng
                        .toString() + ", " + place.address
                )
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                val status: Status =
                    Autocomplete.getStatusFromIntent(data!!)
                Log.e(TAG, "msg" + status.statusMessage)
            } else if (resultCode == RESULT_CANCELED) {
//                showToast(mActivity,"Error")
//                Log.e(TAG, resultCode.toString())
                // The user canceled the operation.
            }
        } else {
            if (resultCode == Activity.RESULT_OK) {
                //Image Uri will not be null for RESULT_OK
                val uri: Uri = data?.data!!

                // Use Uri object instead of File to avoid storage permissions
                imgAddImgIV.scaleType = ImageView.ScaleType.FIT_XY
//            Glide.with(mActivity)
//                .load(uri)
//                .centerCrop()
//                .placeholder(R.drawable.ic_ph_add_toilet)
//                .into(imgAddImgIV)

                val imageStream = contentResolver.openInputStream(uri)
                val selectedImage = BitmapFactory.decodeStream(imageStream)
//                mBitmap = selectedImage

                mImagesArrayList1.add(uri.toString())
                mImagesArrayListt1.add(selectedImage)
//            for (i in mImagesArrayList1.indices) {
                if (mImagesArrayList1.size == 1) {
                    strImage1 = mImagesArrayList1.get(0)
                    mBitmap1 = mImagesArrayListt1.get(0)
                } else if (mImagesArrayList1.size == 2) {
                    strImage1 = mImagesArrayList1.get(0)
                    strImage2 = mImagesArrayList1.get(1)
                    mBitmap1 = mImagesArrayListt1.get(0)
                    mBitmap2 = mImagesArrayListt1.get(1)
                } else if (mImagesArrayList1.size == 3) {
                    strImage2 = mImagesArrayList1.get(1)
                    strImage3 = mImagesArrayList1.get(2)
                    strImage1 = mImagesArrayList1.get(0)
                    mBitmap1 = mImagesArrayListt1.get(0)
                    mBitmap2 = mImagesArrayListt1.get(1)
                    mBitmap3 = mImagesArrayListt1.get(2)
                } else {
                    strImage2 = mImagesArrayList1.get(1)
                    strImage3 = mImagesArrayList1.get(2)
                    strImage1 = mImagesArrayList1.get(0)
                    strImage4 = mImagesArrayList1.get(3)
                    mBitmap1 = mImagesArrayListt1.get(0)
                    mBitmap2 = mImagesArrayListt1.get(1)
                    mBitmap3 = mImagesArrayListt1.get(2)
                    mBitmap4 = mImagesArrayListt1.get(3)
                }

                setViewPagerAdapter()
            }
//        else {
//            if (data != null) {
//                val mFileUri = data.data
//                showImage(mFileUri)
//            }
            else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(mActivity, "Image Not Valid", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(mActivity, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }

//        } else if (resultCode == ImagePicker.RESULT_ERROR) {
////            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
//        } else {
////            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
//        }
    }

    private fun performSaveClick() {
        if (isValidate())
            if (isNetworkAvailable(mActivity))
                executeAddToiletRequest()
            else
                showToast(mActivity, getString(R.string.internet_connection_error))
    }

    private fun executeAddToiletRequest() {
        showProgressDialog(mActivity)

        var mMultipartBody1: MultipartBody.Part? = null
        var mMultipartBody3: MultipartBody.Part? = null
        var mMultipartBody4: MultipartBody.Part? = null
        var mMultipartBody2: MultipartBody.Part? = null

        if (mBitmap1 != null) {
            val requestFile1: RequestBody? = convertBitmapToByteArrayUncompressed(mBitmap1!!)?.let {
                RequestBody.create(
                    "multipart/form-data".toMediaTypeOrNull(),
                    it
                )
            }
            mMultipartBody1 = requestFile1?.let {
                MultipartBody.Part.createFormData(
                    "toiletImage", getAlphaNumericString()!!.toString() + ".jpeg",
                    it
                )
            }
        }

        if (mBitmap2 != null) {
            val requestFile2: RequestBody? = convertBitmapToByteArrayUncompressed(mBitmap2!!)?.let {
                RequestBody.create(
                    "multipart/form-data".toMediaTypeOrNull(),
                    it
                )
            }
            mMultipartBody2 = requestFile2?.let {
                MultipartBody.Part.createFormData(
                    "toiletImage1", getAlphaNumericString()!!.toString() + ".jpeg",
                    it
                )
            }
        }

        if (mBitmap3 != null) {
            val requestFile3: RequestBody? = convertBitmapToByteArrayUncompressed(mBitmap3!!)?.let {
                RequestBody.create(
                    "multipart/form-data".toMediaTypeOrNull(),
                    it
                )
            }
            mMultipartBody3 = requestFile3?.let {
                MultipartBody.Part.createFormData(
                    "toiletImage2", getAlphaNumericString()!!.toString() + ".jpeg",
                    it
                )
            }
        }

        if (mBitmap4 != null) {
            val requestFile4: RequestBody? = convertBitmapToByteArrayUncompressed(mBitmap4!!)?.let {
                RequestBody.create(
                    "multipart/form-data".toMediaTypeOrNull(),
                    it
                )
            }
            mMultipartBody4 = requestFile4?.let {
                MultipartBody.Part.createFormData(
                    "toiletImage3", getAlphaNumericString()!!.toString() + ".jpeg",
                    it
                )
            }
        }

        val userID: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(), (KloxusPrefrences().readString(
                mActivity,
                USER_ID,
                null
            ).toString())
        )
        val authToken: RequestBody = (KloxusPrefrences().readString(
            mActivity,
            AUTH_TOKEN,
            null
        ).toString())
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val description: RequestBody = editDescriptionET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val price: RequestBody = editPriceET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val address: RequestBody = editAddressET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val latitude: RequestBody = mLatitude.toString()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val longitude: RequestBody = mLongitude.toString()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val location: RequestBody = city.toString()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())

        val title: RequestBody = editToiletNameET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val available: RequestBody =
            (txtStartTimeTV.text.toString().trim() + "-" + txtEndTimeTV.text.toString().trim()
                    ).toRequestBody("multipart/form-data".toMediaTypeOrNull())
//        val available: RequestBody =
//            (txtStartTimeTV.text.toString().trim() + "-" + txtEndTimeTV.text.toString().trim()
//                    ).toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val gender: RequestBody = mGenderArrayList.joinToString(separator = ",")
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val toiletOptions: RequestBody = mItemsList.joinToString(separator = ",")
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val currencyy: RequestBody = currency
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val currency_symbol: RequestBody = currency_symbol
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val isRequestSend: RequestBody = isRequestSend
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val isPaid: RequestBody = isPaid
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        Log.e(TAG, "available " + start_time + "-" + end_time)

        val mApiInterface: ApiInterface? =
            ApiClient().getApiClient()?.create(ApiInterface::class.java)
        val mCall: Call<AddEditToiletModel?>? = mApiInterface?.addToiletRequest(
            userID,
            authToken,
            description,
            title,
            price,
            address,
            latitude,
            longitude,
            location,
            available,
            gender,
            toiletOptions,
            currencyy,
            currency_symbol,
            isRequestSend,
            isPaid,
            mMultipartBody1,
            mMultipartBody2,
            mMultipartBody3,
            mMultipartBody4
        )

        mCall?.enqueue(object : Callback<AddEditToiletModel?> {
            override fun onFailure(call: Call<AddEditToiletModel?>, t: Throwable) {
                dismissProgressDialog()
                Log.e(TAG, "onFailure: " + t.localizedMessage)
            }

            override fun onResponse(
                call: Call<AddEditToiletModel?>,
                response: Response<AddEditToiletModel?>
            ) {
                dismissProgressDialog()
                val mModel: AddEditToiletModel? = response.body()
                if (mModel != null) {
                    if (mModel.status.equals(1)) {
                        showToast(mActivity, mModel.message)
                        finish()
                    } else {
                        showToast(mActivity, mModel.message)
                    }
                }
            }
        })
    }

    private fun performAddImgClick() {
        if (mImagesArrayList1.size < 4) {
            if (checkPermission()) {
                onSelectImageClick()
            } else {
                requestPermission()
            }
        } else {
            showAlertDialog(mActivity, "You can only add maximum four Images")
        }
    }

    private fun performBackClick() {
        onBackPressed()
    }


    /*
     * Set up validations for Sign In fields
     * */
    private fun isValidate(): Boolean {
        var flag = true
//        if (mBitmap == null) {
//            showAlertDialog(mActivity, getString(R.string.please_add_toilet_img))
//            flag = false
//        }
        if (editToiletNameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_toilet_name))
            flag = false
        } else if (editAddressET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_address))
            flag = false
        }
        else if(paid_free.equals("paid") && editPriceET.text.toString().trim { it <= ' ' } == "")
        {
            showAlertDialog(mActivity, getString(R.string.please_enter_price))
            flag = false
        }
        else if (txtStartTimeTV.text == null||txtStartTimeTV.text.toString()=="") {
            showAlertDialog(mActivity, getString(R.string.please_enter_open_time))
            flag = false
        } else if (txtEndTimeTV.text == null ||txtEndTimeTV.text.toString()==""||txtEndTimeTV.text.toString().equals("null")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_close_time))
            flag = false
        } else if (mGenderArrayList.size == 0) {
            showAlertDialog(mActivity, getString(R.string.please_enter_gender))
            flag = false

        } else if (mItemsList.size == 0) {
            showAlertDialog(mActivity, getString(R.string.please_enter_items))
            flag = false

        }
//        else if (editDescriptionET.text.toString().trim { it <= ' ' } == "") {
//            showAlertDialog(mActivity, getString(R.string.please_enter_owner_desc))
//            flag = false
//        }
        return flag
    }

    override fun onCheckedChanged(view: SwitchButton?, isChecked: Boolean) {
        if (isChecked.equals(true)) {
            switchActivateSB.isChecked = true
            isRequestSend = "1"
        } else {
            switchActivateSB.isChecked = false
            isRequestSend = "0"
        }
    }
}
