package com.kloxus.app.activities

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialogFragment
import com.kloxus.app.R
import com.kloxus.app.model.StatusMsgModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.*
//import com.stripe.android.Stripe
//import com.stripe.android.TokenCallback
//import com.stripe.android.model.Card
//import com.stripe.android.model.Token
//import com.stripe.exception.AuthenticationException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class AddCardActivity : BaseActivity() {

    @BindView(R.id.editCardHolderNameET)
    lateinit var editCardHolderNameET: EditText

    @BindView(R.id.editCardNumberET)
    lateinit var editCardNumberET: EditText

    @BindView(R.id.txtExpiryDateTV)
    lateinit var txtExpiryDateTV: TextView

    @BindView(R.id.editCVVNumberET)
    lateinit var editCVVNumberET: EditText

    @BindView(R.id.txtSaveTV)
    lateinit var txtSaveTV: TextView

    @BindView(R.id.imgBackIV)
    lateinit var imgBackIV: ImageView
    var yearSelected = 0
    var monthSelected = 0
    var cardNumber: String = ""
    var strToken: String = ""

    /*
    * Initialize Objects
    * */
    var mValidator = CardValidator()

    /*
    * Payment
    * */
//    var mCard: Card? = null
//    var mStripe: Stripe? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_card)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        //Set CardNumber Formatting
//        setCardNumberFormatting()
//        editTextCardSelector(editCardHolderNameET, "")
//        editTextCardSelector(editCardNumberET, "")
//        editTextCardSelector(editCVVNumberET, "")
    }

//    fun editTextCardSelector(
//        mEditText: EditText,
//        tag: String?
//    ) {
//        mEditText.addTextChangedListener(object : TextWatcher {
//            override fun beforeTextChanged(
//                s: CharSequence,
//                start: Int,
//                count: Int,
//                after: Int
//            ) {
//                mEditText.setBackgroundResource(R.drawable.bg_et)
//            }
//
//            override fun onTextChanged(
//                s: CharSequence,
//                start: Int,
//                before: Int,
//                count: Int
//            ) {
//            }
//
//            override fun afterTextChanged(s: Editable) {
//                if (s.length > 0) {
//                    mEditText.setBackgroundResource(R.drawable.bg_et_selected)
//                } else {
//                    mEditText.setBackgroundResource(R.drawable.bg_et)
//                }
//            }
//        })
//    }


//    /*
//    * Card Validations
//    *
//    * */
//    private fun setCardNumberFormatting() {
//        editCardNumberET.addTextChangedListener(object : TextWatcher {
//            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
//            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//            }
//
//            override fun afterTextChanged(s: Editable) {
//            }
//        })
//    }

    @OnClick(
        R.id.imgBackIV,
        R.id.txtSaveTV,
        R.id.txtExpiryDateTV
    )

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackIV -> performCancelClick()
            R.id.txtSaveTV -> performSaveClick()
            R.id.txtExpiryDateTV -> performExpiryDateClick()
        }
    }

    private fun performSaveClick() {
        preventMultipleClick()
        if (isValidate()) {
            if (isNetworkAvailable(mActivity))
//                executeCardTokenApi()
            else showToast(
                mActivity,
                getString(R.string.internet_connection_error)
            )
        }
    }


//    private fun executeCardTokenApi() {
//        showProgressDialog(this)
//        try {
//            mStripe = Stripe(STRIPE_API_KEY)
//        } catch (e: AuthenticationException) {
//            e.printStackTrace()
//        }
//
//        val mMonthYearArray: List<String> = txtExpiryDateTV.text.toString().trim().split("/")
//        val exMonth = mMonthYearArray[0]
//        val exyear = mMonthYearArray[1]
//
//        val card = Card(
//            editCardNumberET.text.toString().trim(), exMonth.toInt(), exyear.toInt(),
//            editCVVNumberET.text.toString().trim()
//        )
//        card.currency = "USD"
//
//        mStripe?.createToken(card, STRIPE_API_KEY, object : TokenCallback() {
//            override fun onSuccess(token: Token) {
//                Log.e(TAG, "**Stripe Token**" + token.id)
//                strToken = token.id
//                executeSubmitPaymentAPI()
//            }
//
//            override fun onError(error: Exception) {
//                dismissProgressDialog()
//                showToast(mActivity, error.localizedMessage)
//            }
//        })
//    }

    /*
     * Execute api param
     * */
    private fun mCardParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["stripeToken"] = strToken
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeSubmitPaymentAPI() {
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.saveCardDetailsRequest(mCardParam())
            ?.enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {
                    dismissProgressDialog()
                    val mModel: StatusMsgModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            showSuccessAlertDialog(mActivity, mModel.message)
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }

    /*
     *
     * Success Alert Dialog Finish Activity
     * */
    fun showSuccessAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            finish()
        }
        alertDialog.show()
    }

    private fun performExpiryDateClick() {
        //Set default values
//        val calendar = Calendar.getInstance()
//        calendar.clear()
//        calendar.set(2021, 0, 1)
//        yearSelected = calendar[Calendar.YEAR]
//        monthSelected = calendar[Calendar.MONTH]
//        val dialogFragment = MonthYearPickerDialogFragment
//            .getInstance(monthSelected, yearSelected)
//        dialogFragment.show(supportFragmentManager, null)
//        dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.AlertDialogCustom)
//        dialogFragment.setOnDateSetListener { year, monthOfYear ->
//            if (year < 2021) {
//                showAlertDialog(mActivity, "Please select future year")
//            } else {
//                txtExpiryDateTV.text = getMonthNumber(monthOfYear.toString()) + "/" + year.toString()
//            }
//        }
    }

//    /*
//     *
//     * Convert Month Name to Month Number
//     * */
//    fun getMonthNumber(strMonth: String): String? {
//        if (strMonth == Constant().MONTHS_OF_YEAR.get(0)) {
//            return "01"
//        }
//        if (strMonth == Constant().MONTHS_OF_YEAR.get(1)) {
//            return "02"
//        }
//        if (strMonth == Constant().MONTHS_OF_YEAR.get(2)) {
//            return "03"
//        }
//        if (strMonth == Constant().MONTHS_OF_YEAR.get(3)) {
//            return "04"
//        }
//        if (strMonth == Constant().MONTHS_OF_YEAR.get(4)) {
//            return "05"
//        }
//        if (strMonth == Constant().MONTHS_OF_YEAR.get(5)) {
//            return "06"
//        }
//        if (strMonth == Constant().MONTHS_OF_YEAR.get(6)) {
//            return "07"
//        }
//        if (strMonth == Constant().MONTHS_OF_YEAR.get(7)) {
//            return "08"
//        }
//        if (strMonth == Constant().MONTHS_OF_YEAR.get(8)) {
//            return "09"
//        }
//        if (strMonth == Constant().MONTHS_OF_YEAR.get(9)) {
//            return "10"
//        }
//        if (strMonth == Constant().MONTHS_OF_YEAR.get(10)) {
//            return "11"
//        }
//        return if (strMonth == Constant().MONTHS_OF_YEAR.get(11)) {
//            "12"
//        } else {
//            null
//        }
//    }

    /*
     * Card Validations
     * */
    private fun isValidate(): Boolean {
        var flag = true
        if (editCardHolderNameET.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_card_holder))
            flag = false
        } else if (editCardNumberET.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_card_number))
            flag = false
        } else if (!CardValidator().validateCardNumber(editCardNumberET.text.toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_carnumber))
            flag = false
        } else if (txtExpiryDateTV.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_expiry_date))
            flag = false
        } else if (editCVVNumberET.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_cvv))
            flag = false
        }
        return flag
    }

    private fun performCancelClick() {
        onBackPressed()

    }

}