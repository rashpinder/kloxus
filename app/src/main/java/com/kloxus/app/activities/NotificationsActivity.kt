package com.kloxus.app.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.kloxus.app.R
import com.kloxus.app.adapters.NotificationAdapter
import com.kloxus.app.interfaces.AcceptRejectbookingInterface
import com.kloxus.app.interfaces.LoadMoreNotificationListener
import com.kloxus.app.interfaces.LoadMoreScrollListner
import com.kloxus.app.model.*
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.AUTH_TOKEN
import com.kloxus.app.utils.KloxusPrefrences
import com.kloxus.app.utils.USER_ID
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class NotificationsActivity : BaseActivity() {

    lateinit var mNotificationAdapter: NotificationAdapter

    @BindView(R.id.notificationsRV)
    lateinit var notificationsRV: RecyclerView

    @BindView(R.id.imgCrossIV)
    lateinit var imgCrossIV: ImageView

    @BindView(R.id.txtNoDataFountTV)
    lateinit var txtNoDataFountTV: TextView
    var mNotificationsList: ArrayList<DataX>? = ArrayList()
    var mPosition: Int = 0
    var mNotiType: Int = 0
    var mId: String = ""
    var detailsId: String = ""

    @BindView(R.id.mProgressRL)
    lateinit var mProgressRL: RelativeLayout

    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var isLoading: Boolean = true
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifications)
        ButterKnife.bind(this)
        setStatusBar(mActivity)

        if(mNotificationsList!=null){
            mNotificationsList!!.clear()
        }

        getNotifications()

    }

    @OnClick(R.id.imgCrossIV)

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgCrossIV -> performCrossClick()
        }
    }

    private fun performCrossClick() {
     onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    private fun getNotifications() {
        if (isNetworkAvailable(mActivity))
            executeGetNotificationsRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> =HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetNotificationsRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getNotificationsRequest(mParam())
            ?.enqueue(object : Callback<NotificationsModel> {
                override fun onFailure(call: Call<NotificationsModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<NotificationsModel>,
                    response: Response<NotificationsModel>
                ) {

                    dismissProgressDialog()
                    val mModel: NotificationsModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            isLoading = !mModel.lastPage.equals(true)
                            mNotificationsList = mModel.data
                            getNotificationsAdapterAdapter()
                            txtNoDataFountTV.setVisibility(View.GONE)
                        } else {
                            txtNoDataFountTV.setVisibility(View.VISIBLE)
                            notificationsRV.setVisibility(View.GONE)
                            txtNoDataFountTV.setText(mModel.message)
                        }
                    }
                }
            })
    }


    var mLoadMoreScrollListner: LoadMoreNotificationListener = object : LoadMoreNotificationListener {
        override fun onLoadMoreListner(mModel: DataX) {
            if (isLoading) {
                ++mPageNo
                executeMoreAccordingDataRequest()
            }

        }
    }

    /*
     * Execute api param
     * */
    private fun mLoadMoreParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> =HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }


    private fun executeMoreAccordingDataRequest() {
        mProgressRL.visibility = View.VISIBLE
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getNotificationsRequest(mLoadMoreParam())
            ?.enqueue(object : Callback<NotificationsModel> {
                override fun onFailure(call: Call<NotificationsModel>, t: Throwable) {
                    dismissProgressDialog()
                    mProgressRL.visibility = View.GONE
                }

                override fun onResponse(
                    call: Call<NotificationsModel>,
                    response: Response<NotificationsModel>
                ) {
                    mProgressRL.visibility = View.GONE
                    var mModel = response.body()!!
                    if (mModel.status == 1) {
                        mModel.data.let {
                            mNotificationsList!!.addAll<DataX>(
                                it
                            )
                        }
                        mNotificationAdapter.notifyDataSetChanged()
                        isLoading = !mModel.lastPage.equals(true)
                    } else if (mModel.status == 0) {
//                    showToast(mActivity, mGetFavModel.message)
                    }
                }
            })
    }



    private fun getNotificationsAdapterAdapter() {
        mNotificationAdapter = NotificationAdapter(mActivity, mNotificationsList,mItemClickListner,mLoadMoreScrollListner)
        notificationsRV.setLayoutManager(LinearLayoutManager(mActivity))
        notificationsRV.setAdapter(mNotificationAdapter)
    }

    var mItemClickListner: AcceptRejectbookingInterface = object : AcceptRejectbookingInterface {
        override fun onItemClickListner(mNotiId: String, position: Int,mType : Int,mDetailsId: String) {
            mPosition = position
            mId = mNotiId
            mNotiType = mType
            detailsId = mDetailsId
            performAcceptRejectClick()
        }
    }

    private fun performAcceptRejectClick() {
            if (isNetworkAvailable(mActivity))
                executeAcceptRejectRequest()
            else
                showToast(mActivity, getString(R.string.internet_connection_error))

    }

    /*
     * Execute api param
     * */
    private fun mARRequestParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["notificationID"] =mId
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["type"] =mNotiType.toString()
        mMap["bookingID"] =detailsId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeAcceptRejectRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.approveRejectRequest(mARRequestParam())?.enqueue(object : Callback<StatusMsgModel> {

            override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<StatusMsgModel>,
                response: Response<StatusMsgModel>
            ) {
                dismissProgressDialog()
                val mModel: StatusMsgModel? = response.body()
                if (mModel != null) {
                    if (mModel.status.equals(1)) {
                        showToast(mActivity, mModel.message)
                        mNotificationsList!!.removeAt(mPosition)
                        mNotificationAdapter.notifyDataSetChanged()
                    } else {
                        showAlertDialog(mActivity, mModel.message)
                    }
                }
            }
        })
    }
}