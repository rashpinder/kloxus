package com.kloxus.app.activities

import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.kloxus.app.R
import com.kloxus.app.adapters.SavedToiletAdapter
import com.kloxus.app.adapters.SeeAllAdapter
import com.kloxus.app.interfaces.LoadMoreScrollListner
import com.kloxus.app.model.HomeData
import com.kloxus.app.model.HomeModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.*
import kotlinx.android.synthetic.main.activity_see_all.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class SeeAllActivity : BaseActivity() {

    lateinit var mSeeAllAdapter: SeeAllAdapter

    @BindView(R.id.bottomsheetRV)
    lateinit var bottomsheetRV: RecyclerView

    @BindView(R.id.imgCrossIV)
    lateinit var imgCrossIV: ImageView

    @BindView(R.id.imgCancelIV)
    lateinit var imgCancelIV: ImageView

    @BindView(R.id.editSearchET)
    lateinit var editSearchET: EditText

    @BindView(R.id.txtNoDataFountTV)
    lateinit var txtNoDataFountTV: TextView


    @BindView(R.id.mProgressRL)
    lateinit var mProgressRL: RelativeLayout

    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var isLoading: Boolean = true

    var mLatitude: String = ""
    var mLongitude: String = ""
    var mRecentItemsList: List<*>? = null
    var mHomeArrayList: ArrayList<HomeData>? = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_see_all)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        getIntentData()
        getHomeData()

//        editSearchET.setOnTouchListener(
//            View.OnTouchListener { v, event ->
//                Log.i("click", "onMtouch")
//                editSearchET.setFocusable(true)
//                editSearchET.requestFocus()
//                SearchItem()
//                false
//            },
//        )
    }


    private fun getIntentData() {
        if (intent != null) {
            mLatitude = intent.getStringExtra(LATITUDE).toString()
            mLongitude = intent.getStringExtra(LONGITUTE).toString()

        }
    }

    private fun getHomeData() {
        if (isNetworkAvailable(this))
            executeHomeDataRequest()
        else
            showToast(this, getString(R.string.internet_connection_error))
    }

    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["latitude"] = mLatitude
        mMap["longitude"] = mLongitude
        mMap["search"] = ""
        mMap["perPage"] = mPerPage.toString()
        mMap["pageNo"] = mPageNo.toString()
        mMap["timeZone"] = TimeZone.getDefault().id
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }


    private fun executeHomeDataRequest() {
        if (mHomeArrayList != null) {
            mHomeArrayList!!.clear()
        }
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.homeRequest(mParams())?.enqueue(object :
            Callback<HomeModel> {
            override fun onFailure(call: Call<HomeModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<HomeModel>,
                response: Response<HomeModel>
            ) {
                dismissProgressDialog()
                var mModel = response.body()
                if (mModel?.status == 1) {
                    if (mModel.lastPage.equals(true)) {
                        isLoading = false
                    } else {
                        isLoading = true
                    }
                    mHomeArrayList = mModel.data
                    setAllToiletAdapter()
                } else if (mModel?.status == 0) {
                    showAlertDialog(mActivity, mModel.message)
                }
            }
        })
    }


    private fun setAllToiletAdapter() {
        mSeeAllAdapter = SeeAllAdapter(
            mActivity,
            mHomeArrayList!!,
            mLoadMoreScrollListner
        )
        bottomsheetRV.layoutManager = LinearLayoutManager(mActivity)
        bottomsheetRV.adapter = mSeeAllAdapter
    }

    var mLoadMoreScrollListner: LoadMoreScrollListner = object : LoadMoreScrollListner {
        override fun onLoadMoreListner(mModel: HomeData) {
            if (isLoading) {
                ++mPageNo
                executeMoreAccordingCatIdRequest()
            }
        }
    }

    private fun mLoadMoreParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["latitude"] = mLatitude
        mMap["longitude"] = mLongitude
        mMap["search"] = ""
        mMap["perPage"] = mPerPage.toString()
        mMap["pageNo"] = mPageNo.toString()
        mMap["timeZone"] = TimeZone.getDefault().id
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }


    private fun executeMoreAccordingCatIdRequest() {
        mProgressRL.visibility = View.VISIBLE
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.homeRequest(mLoadMoreParam())
            ?.enqueue(object : Callback<HomeModel> {
                override fun onFailure(call: Call<HomeModel>, t: Throwable) {
                    dismissProgressDialog()
                    mProgressRL.visibility = View.GONE
                }

                override fun onResponse(
                    call: Call<HomeModel>,
                    response: Response<HomeModel>
                ) {
                    mProgressRL.visibility = View.GONE
                    val mGetFavModel = response.body()
                    if (mGetFavModel!!.status == 1) {
                        mGetFavModel.data.let {
                            mHomeArrayList!!.addAll<HomeData>(
                                it
                            )
                        }
                        mSeeAllAdapter.notifyDataSetChanged()
                        isLoading = !mGetFavModel.lastPage.equals(true)
                    } else if (mGetFavModel.status == 0) {
//                    showToast(mActivity, mGetFavModel.message)
                    }
                }
            })
    }

    @OnClick(
        R.id.imgCrossIV,
        R.id.imgCancelIV
    )

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgCrossIV -> performCrossPwdClick()
            R.id.imgCancelIV -> performCancelClick()
        }
    }

    private fun performCancelClick() {
        hideCloseKeyboard(this)
        editSearchET.setText("")
    }

    private fun performCrossPwdClick() {
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }}

//    private fun SearchItem() {
//        editSearchET.addTextChangedListener(object : TextWatcher {
//            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//
//                // TODO Auto-generated method stub
//            }
//
//            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
//
//                // TODO Auto-generated method stub
//            }
//
//            override fun afterTextChanged(s: Editable) {
//
//                // filter your list from your input
//                filter(s.toString())
//                //you can use runnable postDelayed like 500 ms to delay search text
//            }
//        })
//        editSearchET.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
//            if (actionId == EditorInfo.IME_ACTION_DONE) {
//                //do here your stuff f
//                if (mRecentItemsList!!.size == 0) {
//                    showToast(this, "No Result found")
//                }
//                hideCloseKeyboard(this)
//                return@OnEditorActionListener true
//            }
//            false
//        })
//    }


//    fun filter(text: String) {
//        mRecentItemsList = ArrayList<HomeData>()
//        for (d in mHomeArrayList!!) {
//            //or use .equal(text) with you want equal match
//            //use .toLowerCase() for better matches
//            if (d.title.toLowerCase().contains(text.toLowerCase()) || d.toiletLocation.toLowerCase()
//                    .contains(text.toLowerCase())
//            ) {
//                (mRecentItemsList as ArrayList<HomeData>).add(d)
//            }
//        }
//
//        //update recyclerview
//        if (mHomeArrayList!!.size > 0) {
//            mSeeAllAdapter!!.updateWithSearchData(mRecentItemsList as ArrayList<HomeData>?)
//        }
//    }
//}
