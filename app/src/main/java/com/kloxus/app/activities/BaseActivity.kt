package com.kloxus.app.activities

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.kloxus.app.R
import com.kloxus.app.utils.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*
import java.util.regex.Pattern

open class BaseActivity : AppCompatActivity() {

    private var mLastClickTab1: Long = 0
    /*
    * Get Class Name
    * */
    var TAG = this@BaseActivity.javaClass.simpleName

    /*
    * Initialize Activity
    * */
    var mActivity: Activity = this@BaseActivity

    /*
    * Initialize Other Classes Objects...
    * */
    var progressDialog: Dialog? = null



     /*
     * Getting User ID
     * */
     fun getUserId()  : String? {
        return KloxusPrefrences().readString(mActivity, USER_ID, "")
     }

     /*
     * Getting auth token
     * */
     fun getAuthToken()  : String? {
        return KloxusPrefrences().readString(mActivity, AUTH_TOKEN, "")
     }

     /*
     * Getting User Email
     * */
     fun getUserEmail()  : String? {
         return KloxusPrefrences().readString(mActivity, USER_EMAIL, "")
     }

     /*
     * Getting User Name
     * */
     fun getFirstName() : String? {
         return KloxusPrefrences().readString(mActivity, FIRST_NAME, "")
     }
     /*
     * Getting User Bio
     * */
     fun getLastName(): String? {
         return KloxusPrefrences().readString(mActivity, LAST_NAME, "")
     }


     /*
     * Getting Profile Pic
     * */
     fun getProfilePic(): String? {
         return KloxusPrefrences().readString(mActivity, PROFILE_IMAGE, "")
     }

     /*
     * Is User Login
     * */
     fun isUserLogin() : Boolean{
         return KloxusPrefrences().readBoolean(mActivity, IS_LOGIN, false)
     }

    /*
     *Hide/Close Keyboard forcefully
     *
     * */
    open fun hideCloseKeyboard(activity: Activity) {
        val imm =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    open fun preventMultipleClick() {
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 2000) {
            return
        }
        mLastClickTab1 = SystemClock.elapsedRealtime()
    }
   open fun preventMultipleViewClick() {
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 4000) {
            return
        }
        mLastClickTab1 = SystemClock.elapsedRealtime()
    }


    fun editTextSignupSelector(
        mEditText: EditText,
        rl: RelativeLayout,
        tag: String?
    ) {
        mEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
                rl.setBackgroundResource(R.drawable.bg_et)
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {
                if (s.length > 0) {
                    rl.setBackgroundResource(R.drawable.bg_et_selected)
                } else {
                    rl.setBackgroundResource(R.drawable.bg_et)
                }
            }
        })
    }


    /*
    Transparent status bar
     */
    fun setStatusBar(mActivity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mActivity.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            // edited here
            mActivity.window.statusBarColor = resources.getColor(R.color.white)
        }
    }
/*
    Transparent status bar
     */
    fun setSplashStatusBar(mActivity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mActivity.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            // edited here
            mActivity.window.statusBarColor = resources.getColor(R.color.black)
        }
    }

    /*
     * Finish the activity
     * */
    override fun finish() {
        super.finish()
        overridePendingTransitionSlideDownExit()
    }

    /*
     * To Start the New Activity
     * */
    override fun startActivity(intent: Intent?) {
        super.startActivity(intent)
        overridePendingTransitionSlideUPEnter()
    }

//    open fun getBitmap(filePath: String?): Bitmap? {
//        val options = BitmapFactory.Options()
//        options.inJustDecodeBounds = true
//        BitmapFactory.decodeFile(filePath, options)
//        val scaleByHeight: Boolean =
//            Math.abs(options.outHeight - 100) >= Math
//                .abs(options.outWidth - 100)
//        if (options.outHeight * options.outWidth * 2 >= 16384) {
//            val sampleSize: Double =
//                if (scaleByHeight) options.outHeight / 100 else options.outWidth / 100
//            options.inSampleSize = Math.pow(
//                2.0, Math.floor(
//                    Math.log(sampleSize) / Math.log(2.0)
//                )
//            ).toInt()
//        }
//        options.inJustDecodeBounds = false
//        options.inTempStorage = ByteArray(512)
//        return BitmapFactory.decodeFile(filePath, options)
//    }
    /*
     * Get Bitmap From Path
     * */
    open fun getBitmapFromFilePath(path: String): Bitmap? {
        val bitmap: Bitmap
        try {
            val f = File(path)
            val options = BitmapFactory.Options()
            options.inPreferredConfig = Bitmap.Config.ARGB_8888
            bitmap = BitmapFactory.decodeFile(f.absolutePath)

//            bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
            val out = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 20, out)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            return null
        }
        return bitmap
    }
    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    fun overridePendingTransitionSlideUPEnter() {
        overridePendingTransition(R.anim.bottom_up, 0)
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    fun overridePendingTransitionSlideDownExit() {
        overridePendingTransition(0, R.anim.bottom_down)
    }


    /*to switch between fragments*/
    fun switchFragment(
        fragment: Fragment?,
        Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager = supportFragmentManager
        if (fragment != null) {
            val fragmentTransaction =
                fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.containerBookingsFL, fragment, Tag)
            if (addToStack) fragmentTransaction.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }

    /*
     * Show Progress Dialog
     * */

    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }


    /*
     * Hide Progress Dialog
     * */
    fun dismissProgressDialog() {
//        if (progressDialog != null && progressDialog!!.isShowing) {
//            progressDialog!!.dismiss()
//        }
        try {
            if (this.progressDialog != null && this.progressDialog!!.isShowing()) {
                this.progressDialog!!.dismiss()
            }
        } catch (e: IllegalArgumentException) {
            // Handle or log or ignore
        } catch (e: Exception) {
            // Handle or log or ignore
        } finally {
            this.progressDialog = null
        }
    }


    /*
     * Validate Email Address
     * */
    fun isValidEmaillId(email: String?): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }


    /*
     * Check Internet Connections
     * */
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }


    /*
     * Toast Message
     * */
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }


    /*
     *
     * Error Alert Dialog
     * */
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }


    /*
     *
     * Error Alert Dialog
     * */
    fun showLoginAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(this)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(true)
        alertDialog.setCancelable(true)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            val i = Intent(this, LoginActivity::class.java)
            startActivity(i)
            finish()
        }
        alertDialog.show()
    }
    /*
     *
     * Error Alert Dialog
     * */
    fun showBankAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(true)
        alertDialog.setCancelable(true)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            val i = Intent(this, AddBankDetailsActivity::class.java)
            startActivity(i)
        }
        alertDialog.show()
    }

    /*
     *
     * Error Alert Dialog FinishActivity
     * */
    fun showAlertDialogFinish(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss()
        mActivity.finish()

        }
        alertDialog.show()
    }

    /*
     *
     * Error Alert Dialog FinishActivity
     * */
    fun showAlertDialogDetailFinish(mActivity: Activity?, strMessage: String?,toilet_id:String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss()
//        mActivity.finish()
            val i = Intent(this, ToiletDetailActivity::class.java)
            i.putExtra("toilet_id",toilet_id)
            startActivity(i)
            finish()
        }
        alertDialog.show()
    }

     open fun convertBitmapToByteArrayUncompressed(bitmap: Bitmap): ByteArray? {
         val stream = ByteArrayOutputStream()
         bitmap.compress(Bitmap.CompressFormat.JPEG, 40, stream)
         return stream.toByteArray()
     }

    open fun getAlphaNumericString(): String? {
        val n = 20

        // chose a Character random from this String
        val AlphaNumericString = ("ABCDEFGHIJKLMNOPQRSTUVWXYZ" /*+ "0123456789"*/
                + "abcdefghijklmnopqrstuvxyz")

        // create StringBuffer size of AlphaNumericString
        val sb = StringBuilder(n)
        for (i in 0 until n) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            val index = (AlphaNumericString.length
                    * Math.random()).toInt()

            // add Character one by one in end of sb
            sb.append(
                AlphaNumericString[index]
            )
        }
        return sb.toString()
    }

}