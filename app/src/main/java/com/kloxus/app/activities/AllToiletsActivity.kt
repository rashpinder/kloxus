package com.kloxus.app.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.kloxus.app.R
import com.kloxus.app.adapters.AllToiletsAdapter
import com.kloxus.app.interfaces.LoadMoreListner
import com.kloxus.app.model.Data
import com.kloxus.app.model.GetAllToiletModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.AUTH_TOKEN
import com.kloxus.app.utils.IS_BANK
import com.kloxus.app.utils.KloxusPrefrences
import com.kloxus.app.utils.USER_ID
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AllToiletsActivity : BaseActivity() {

    lateinit var mAllToiletsAdapter: AllToiletsAdapter

    @BindView(R.id.allToiletsRV)

    lateinit var allToiletsRV: RecyclerView

    @BindView(R.id.txtNoDataFountTV)
    lateinit var txtNoDataFountTV: TextView

    var mAllToiletsList: ArrayList<Data>? = ArrayList()

    @BindView(R.id.imgCrossIV)
    lateinit var imgCrossIV: ImageView

    @BindView(R.id.imgAddIV)
    lateinit var imgAddIV: ImageView

    @BindView(R.id.mProgressRL)
    lateinit var mProgressRL: RelativeLayout

    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var isLoading: Boolean = true
//    var mOptionsList: ArrayList<String>? = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_toilets)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
    }

    override fun onResume() {
        super.onResume()
        mPageNo=1
        if (mAllToiletsList!=null){
            mAllToiletsList!!.clear()
        }
        getAllToilets()
    }

    @OnClick(
        R.id.imgCrossIV,
        R.id.imgAddIV
    )

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgCrossIV -> performCrossPwdClick()
            R.id.imgAddIV -> performAddClick()
        }
    }

    private fun performAddClick() {
//        if (KloxusPrefrences().readInteger(this, IS_BANK,0)==1){
        val i = Intent(this, AddToiletActivity::class.java)
        startActivity(i)
//        }
//        else{
//            showBankAlertDialog(this,"Please add your Bank details first")
//        }

    }

    private fun performCrossPwdClick() {
        onBackPressed()
    }

    private fun getAllToilets() {
        if (isNetworkAvailable(mActivity))
            executeGetAllToiletRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetAllToiletRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getAllToiletRequest(mParam())
            ?.enqueue(object : Callback<GetAllToiletModel> {
                override fun onFailure(call: Call<GetAllToiletModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<GetAllToiletModel>,
                    response: Response<GetAllToiletModel>
                ) {
                    dismissProgressDialog()
                    val mModel: GetAllToiletModel? = response.body()
                    if (mModel!!.status.equals(1)) {
                        isLoading = !mModel.lastPage.equals(true)
                        mAllToiletsList = mModel.data
                        setAllToiletAdapter()
                        txtNoDataFountTV.visibility = View.GONE
                    } else {
//                        showAlertDialog(mActivity, mModel.message)
                        txtNoDataFountTV.visibility = View.VISIBLE
                        txtNoDataFountTV.text = mModel.message
                    }
                }
            })
    }

    var mLoadMoreScrollListner: LoadMoreListner = object : LoadMoreListner {
        override fun onLoadMoreListner(mModel: Data) {
            if (isLoading) {
                ++mPageNo
                executeMoreAccordingDataRequest()
            }
        }
    }

    /*
     * Execute api param
     * */
    private fun mLoadMoreParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeMoreAccordingDataRequest() {
        mProgressRL.visibility = View.VISIBLE
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getAllToiletRequest(mLoadMoreParam())
            ?.enqueue(object : Callback<GetAllToiletModel> {
                override fun onFailure(call: Call<GetAllToiletModel>, t: Throwable) {
                    dismissProgressDialog()
                    mProgressRL.visibility = View.GONE
                }

                override fun onResponse(
                    call: Call<GetAllToiletModel>,
                    response: Response<GetAllToiletModel>
                ) {
                    mProgressRL.visibility = View.GONE
                    var mModel = response.body()!!
                    if (mModel.status == 1) {
                        mModel.data.let {
                            mAllToiletsList!!.addAll<Data>(
                                it
                            )
                        }
                        mAllToiletsAdapter.notifyDataSetChanged()
                        isLoading = !mModel.lastPage.equals(true)

                    } else if (mModel.status == 0) {
//                    showToast(mActivity, mGetFavModel.message)
                    }
                }
            })
    }

    private fun setAllToiletAdapter() {
        mAllToiletsAdapter = AllToiletsAdapter(mActivity, mAllToiletsList, mLoadMoreScrollListner)
        allToiletsRV.layoutManager = LinearLayoutManager(mActivity)
        allToiletsRV.adapter = mAllToiletsAdapter
        mAllToiletsAdapter.notifyDataSetChanged()
    }
}