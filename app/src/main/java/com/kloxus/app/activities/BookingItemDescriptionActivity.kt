package com.kloxus.app.activities

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.isVisible
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.iarcuschin.simpleratingbar.SimpleRatingBar
import com.joooonho.SelectableRoundedImageView
import com.kloxus.app.R
import com.kloxus.app.model.GetAllDetailsAfterBookingModel
import com.kloxus.app.model.StatusMsgModel
import com.kloxus.app.retrofit.ApiClient
import com.kloxus.app.retrofit.ApiInterface
import com.kloxus.app.utils.AUTH_TOKEN
import com.kloxus.app.utils.KloxusPrefrences
import com.kloxus.app.utils.USER_ID
import kotlinx.android.synthetic.main.activity_booking_item_description.*
import kotlinx.android.synthetic.main.rating_dialog.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class BookingItemDescriptionActivity : BaseActivity() {

    @BindView(R.id.txtAddressTV)
    lateinit var txtAddressTV: TextView

    lateinit var editReviewET: EditText

    @BindView(R.id.txtStatusTV)
    lateinit var txtStatusTV: TextView

    @BindView(R.id.txtRefundTV)
    lateinit var txtRefundTV: TextView

    @BindView(R.id.imgBackIV)
    lateinit var imgBackIV: ImageView

    @BindView(R.id.imgToiletIV)
    lateinit var imgToiletIV: SelectableRoundedImageView

    @BindView(R.id.txtNameTV)
    lateinit var txtNameTV: TextView

    @BindView(R.id.txtDescTV)
    lateinit var txtDescTV: TextView

    @BindView(R.id.txtTransactionIDTV)
    lateinit var txtTransactionIDTV: TextView

    @BindView(R.id.tranLL)
    lateinit var tranLL: LinearLayout

    @BindView(R.id.bookingLL)
    lateinit var bookingLL: LinearLayout

    @BindView(R.id.transLL)
    lateinit var transLL: LinearLayout

    @BindView(R.id.txtBookingIDTV)
    lateinit var txtBookingIDTV: TextView

    @BindView(R.id.ratingLL)
    lateinit var ratingLL: LinearLayout

    @BindView(R.id.ratingBar)
    lateinit var ratingBar: SimpleRatingBar

    var mItemsList: ArrayList<String> = ArrayList<String>()
    var mToiletId: String = ""
    var mtype: String = ""
    var review: String = ""
    var mAddress: String = ""
    var book_id: String = ""
//    var mModel: ToiletDetailModel? = null
    var mModel: GetAllDetailsAfterBookingModel? = null
    lateinit var alertDialog: Dialog
    var rating: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_item_description)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        getIntentData()
        getToiletDetail()
    }

    private fun getIntentData() {
        if (intent != null) {
            mToiletId = intent.getStringExtra("toilet_id").toString()
            mtype = intent.getStringExtra("mtype").toString()
            book_id = intent.getStringExtra("book_id").toString()
            mAddress = intent.getStringExtra("mAddress").toString()
        }
    }

    @OnClick(
        R.id.imgBackIV,
        R.id.txtRefundTV,
        R.id.ratingBar,
        R.id.ratingLL
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackIV -> performCancelClick()
            R.id.txtRefundTV -> performRefundClick()
            R.id.ratingLL -> performRatingClick()
            R.id.ratingBar -> performRatingClick()
        }
    }

    private fun performRatingClick() {
        ratingBar.rating = mModel!!.data.rating.toFloat()
        showRatingDialog(mActivity)
    }

    /*
     *
     * Error Alert Dialog
     * */
    fun showRatingDialog(mActivity: Activity?) {
        alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.rating_dialog)
        alertDialog.setCanceledOnTouchOutside(true)
        alertDialog.setCancelable(true)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val window = alertDialog.window
        val wlp = window!!.attributes
        wlp.gravity = Gravity.BOTTOM
        alertDialog.window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        // set the custom dialog components - text, image and button
        val txtSubmitTV = alertDialog.findViewById<TextView>(R.id.txtSubmitTV)
        editReviewET = alertDialog.findViewById<EditText>(R.id.editReviewET)
        val ratingBar = alertDialog.findViewById<SimpleRatingBar>(R.id.ratingBar)
        val btnDismiss = alertDialog.findViewById<ImageView>(R.id.btnDismiss)

//        review=editReviewET.text.toString().trim()

        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()

        ratingBar.setOnClickListener {
            rating = ratingBar.rating.toString()
        }

        txtSubmitTV.setOnClickListener {
            if (isNetworkAvailable(mActivity))
                executeRatingRequest(editReviewET.text.toString().trim())
            else
                showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    /*
     * Execute api param
     * */

    private fun executeRatingRequest(mString : String) {
        Log.e(TAG,"***EditText Value****"+mString)
        val mRatingParam: MutableMap<String?, String?> = HashMap()
        mRatingParam["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mRatingParam["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mRatingParam["toiletID"] = mToiletId
        mRatingParam["rating"] = rating
        mRatingParam["review"] = mString
        Log.e(TAG, "**PARAM**$mRatingParam")


        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.submitRatingRequest(mRatingParam)
            ?.enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {
                    dismissProgressDialog()
                    val mModel: StatusMsgModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            showToast(mActivity, mModel.message)
                            alertDialog.dismiss()
                            getToiletDetail()
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                            alertDialog.dismiss()
                        }
                    }
                }
            })
    }


    private fun performRefundClick() {
        if (isNetworkAvailable(mActivity))
            executeRefundRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))

    }

    /*
     * Execute api param
     * */
    private fun mRefundRequestParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["toiletID"] = mToiletId
        mMap["bookID"] = book_id
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeRefundRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.refundRequest(mRefundRequestParam())
            ?.enqueue(object : Callback<StatusMsgModel> {

                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {
                    dismissProgressDialog()
                    val mModel: StatusMsgModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status.equals(1)) {
                            showToast(mActivity, mModel.message)
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }

    private fun getToiletDetail() {
        if (isNetworkAvailable(mActivity))
            executeGetToiletDetailRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["userID"] = KloxusPrefrences().readString(this, USER_ID, null)
        mMap["authToken"] = KloxusPrefrences().readString(this, AUTH_TOKEN, null)
        mMap["toiletID"] = mToiletId
        mMap["bookID"] = book_id
        mMap["type"] = mtype
        mMap["location"] = mAddress
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetToiletDetailRequest() {
        showProgressDialog(mActivity)
        val mApiInterface = ApiClient().getApiClient()?.create(ApiInterface::class.java)
        mApiInterface?.getToiletDetailsFterBookingRequest(mParam())
            ?.enqueue(object : Callback<GetAllDetailsAfterBookingModel> {
                override fun onFailure(call: Call<GetAllDetailsAfterBookingModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<GetAllDetailsAfterBookingModel>,
                    response: Response<GetAllDetailsAfterBookingModel>
                ) {
                    dismissProgressDialog()
                    mModel = response.body()
                    if (mModel != null) {
                        if (mModel!!.status.equals(1)) {
                            Glide.with(mActivity).load(mModel!!.data.toiletImage)
                                .placeholder(R.drawable.ic_booking_ph)
                                .error(R.drawable.ic_booking_ph)
                                .into(imgToiletIV)
                            txtBookingIDTV.text = mModel!!.data.orderID
                            txtAddressTV.text = mModel!!.data.address
                            txtTransactionIDTV.text = mModel!!.data.transactionID
//                            txtDescTV.text = mModel!!.data.description
                            for (i in mModel!!.data.toiletOptions.indices) {

                                mItemsList.add(mModel!!.data.toiletOptions.get(i))
                            }
                            if (mItemsList.contains("3")&&mItemsList.contains("2") && mItemsList.contains("4") && mItemsList.contains("1")&& mItemsList.contains("5")) {
                                txtDescTV.text=getString(R.string.all)
                            }
                            else if (mItemsList.contains("1") && mItemsList.contains("2") && mItemsList.contains("3")&& mItemsList.contains("4")) {
                                txtDescTV.text= getString(R.string.a)
                            }
                            else if (mItemsList.contains("1") && mItemsList.contains("2") && mItemsList.contains("3")&& mItemsList.contains("5")) {
                                txtDescTV.text= getString(R.string.b)
                            }else if (mItemsList.contains("1") && mItemsList.contains("4") && mItemsList.contains("3")&& mItemsList.contains("5")) {
                                txtDescTV.text= getString(R.string.c)
                            }
                            else if (mItemsList.contains("2") && mItemsList.contains("4") && mItemsList.contains("3")&& mItemsList.contains("5")) {
                                txtDescTV.text= getString(R.string.d)
                            }
                            else if (mItemsList.contains("2") && mItemsList.contains("4") && mItemsList.contains("1")&& mItemsList.contains("5")) {
                                txtDescTV.text=getString(R.string.e)
                            }

                            else if (mItemsList.contains("1") && mItemsList.contains("2") && mItemsList.contains("3")) {
                                txtDescTV.text= getString(R.string.f)
                            } else if (mItemsList.contains("1") && mItemsList.contains("2") && mItemsList.contains("4")) {
                                txtDescTV.text= getString(R.string.ff)
                            } else if (mItemsList.contains("1") && mItemsList.contains("3") && mItemsList.contains("4")) {
                                txtDescTV.text= getString(R.string.g)
                            } else if (mItemsList.contains("1") && mItemsList.contains("5") && mItemsList.contains("4")) {
                                txtDescTV.text= getString(R.string.h)
                            }else if (mItemsList.contains("2") && mItemsList.contains("3") && mItemsList.contains("4")) {
                                txtDescTV.text= getString(R.string.i)
                            }else if (mItemsList.contains("2") && mItemsList.contains("3") && mItemsList.contains("5")) {
                                txtDescTV.text= getString(R.string.j)
                            }else if (mItemsList.contains("2") && mItemsList.contains("4") && mItemsList.contains("5")) {
                                txtDescTV.text= getString(R.string.k)
                            }else if (mItemsList.contains("1") && mItemsList.contains("3") && mItemsList.contains("5")) {
                                txtDescTV.text= getString(R.string.fff)
                            }
                            else if (mItemsList.contains("3") && mItemsList.contains("4") && mItemsList.contains("5")) {
                                txtDescTV.text= getString(R.string.ffff)
                            }
                            else if (mItemsList.contains("1") && mItemsList.contains("2")) {
                                txtDescTV.text=getString(R.string.l)
                            } else if (mItemsList.contains("1") && mItemsList.contains("3")) {
                                txtDescTV.text= getString(R.string.m)
                            } else if (mItemsList.contains("1") && mItemsList.contains("4")) {
                                txtDescTV.text= getString(R.string.n)
                            } else if (mItemsList.contains("1") && mItemsList.contains("5")) {
                                txtDescTV.text= getString(R.string.o)
                            } else if (mItemsList.contains("2") && mItemsList.contains("3")) {
                                txtDescTV.text= getString(R.string.p)
                            } else if (mItemsList.contains("2") && mItemsList.contains("4")) {
                                txtDescTV.text= getString(R.string.q)
                            } else if (mItemsList.contains("2") && mItemsList.contains("5")) {
                                txtDescTV.text= getString(R.string.r)
                            } else if (mItemsList.contains("3") && mItemsList.contains("4")) {
                                txtDescTV.text= getString(R.string.s)
                            } else if (mItemsList.contains("3") && mItemsList.contains("5")) {
                                txtDescTV.text= getString(R.string.t)
                            } else if (mItemsList.contains("4") && mItemsList.contains("5")) {
                                txtDescTV.text= getString(R.string.u)
                            }
                            else if (mItemsList.contains("1")) {
                                txtDescTV.text= resources.getString(R.string.toilet_paper)
                            } else if (mItemsList.contains("2")) {
                                txtDescTV.text= resources.getString(R.string.soap)
                            } else if (mItemsList.contains("3")) {
                                txtDescTV.text= resources.getString(R.string.hand_dryer)
                            } else if (mItemsList.contains("4")) {
                                txtDescTV.text=resources.getString(R.string.ser)
                            } else if (mItemsList.contains("5")) {
                                txtDescTV.text= resources.getString(R.string.dis)
                            }

                            txtNameTV.text = mModel!!.data.title
                            if (mModel!!.data.paymentMethod.equals("0")) {
                                txtRefundTV.isVisible=false
                            }
                            else if(mModel!!.data.paymentMethod.equals("2")){
                                txtRefundTV.isVisible=true
                            }
                            else{
                                txtRefundTV.isVisible=false
                            }

//                            if (mModel!!.data.transactionID.equals("")) {
//                                txtTransactionIDTV.text = mModel!!.data.paymentMethod
//                                transLL.isVisible = true
//                                txtRefundTV.isVisible = false
//                            } else {
//                                txtRefundTV.isVisible = true
//                            }
//                            if (mModel!!.data.orderID.equals("")) {
//                                txtBookingIDTV.text = mModel!!.data.paymentMethod
//                                bookingLL.isVisible = true
//
//                            }
                            if (mtype == "1") {
                                ratingLL.isVisible = true
                                txtReviewTV.isVisible = true
                                ratingBar.starBorderWidth = 1.8F
                                ratingBar.rating = mModel!!.data.rating.toFloat()
                                if (mModel!!.data.ratingDetails.size > 0) {
                                txtReviewTV.text = mModel!!.data.ratingDetails.get(0).review
                            } } else {
                                txtRefundTV.isVisible = false
                                txtReviewTV.isVisible = false
                                ratingLL.isVisible = false
//                                transLL.isVisible = !mModel!!.data.transactionID.equals("")
//                                bookingLL.isVisible = !mModel!!.data.bookingID.equals("")
                            }
                            txtStatusTV.isVisible=true
                            if (mModel!!.data.bookingStatus.equals("1")){
                                txtStatusTV.text= getString(R.string.request_approved)
                            }
                            else{
                                txtStatusTV.text= getString(R.string.request_rejected)
                            }

                        } else {
                            showAlertDialog(mActivity, mModel?.message)
                        }
                    }
                }
            })
    }

    private fun performCancelClick() {
        onBackPressed()
    }

}